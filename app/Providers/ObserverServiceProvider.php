<?php

namespace App\Providers;

use App\Models\Branch;
use App\Models\Driver;
use App\Models\DriverDocument;
use App\Models\Employee;
use App\Models\FuelRate;
use App\Models\LogbookEntry;
use App\Models\User;
use App\Models\Company;
use App\Models\Vehicle;
use App\Models\VehicleDocument;
use App\Models\VehicleDriver;
use App\Models\VehicleHistory;
use App\Observers\BranchObserver;
use App\Observers\CompanyObserver;
use App\Observers\DriverDocumentObserver;
use App\Observers\DriverObserver;
use App\Observers\EmployeeObserver;
use App\Observers\FuelRateObserver;
use App\Observers\LogbookEntryObserver;
use App\Observers\UserObserver;
use App\Observers\VehicleDocumentObserver;
use App\Observers\VehicleDriverObserver;
use App\Observers\VehicleHistoryObserver;
use App\Observers\VehicleObserver;
use Illuminate\Support\ServiceProvider;

/**
 * Class ObserverServiceProvider.
 */
class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        //Master Observers
        Company::observe(CompanyObserver::class);
        Branch::observe(BranchObserver::class);
        Employee::observe(EmployeeObserver::class);
        Driver::observe(DriverObserver::class);
        DriverDocument::observe(DriverDocumentObserver::class);
        Vehicle::observe(VehicleObserver::class);
        VehicleDocument::observe(VehicleDocumentObserver::class);
        VehicleHistory::observe(VehicleHistoryObserver::class);
        VehicleDriver::observe(VehicleDriverObserver::class);
        FuelRate::observe(FuelRateObserver::class);
        LogbookEntry::observe(LogbookEntryObserver::class);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //
    }
}
