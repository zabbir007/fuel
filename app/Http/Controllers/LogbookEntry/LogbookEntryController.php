<?php

namespace App\Http\Controllers\LogbookEntry;

use App\Exceptions\RecordNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\LogbookEntry\CreateLogbookEntryRequest;
use App\Http\Requests\LogbookEntry\ManageLogbookEntryRequest;
use App\Http\Requests\LogbookEntry\StoreLogbookEntryRequest;
use App\Http\Requests\LogbookEntry\UpdateLogbookEntryRequest;
use App\Repositories\BranchRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\FuelRepository;
use App\Repositories\LogbookEntryRepository;
use App\Repositories\VehicleRepository;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class LogbookEntryController.
 */
class LogbookEntryController extends BaseController
{
    /**
     * @var LogbookEntryRepository
     */
    private $repository;

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var mixed
     */
    private $branchRepository;

    /**
     * @var mixed
     */
    private $fuelRepository;

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var VehicleRepository
     */
    private $vehicleRepository;

    /**
     * UserLogbookEntryController constructor.
     *
     * @param  LogbookEntryRepository  $repository
     * @param  CompanyRepository  $companyRepository
     * @param  BranchRepository  $branchRepository
     * @param  FuelRepository  $fuelRepository
     * @param  EmployeeRepository  $employeeRepository
     * @param  VehicleRepository  $vehicleRepository
     */
    public function __construct(
        LogbookEntryRepository $repository,
        CompanyRepository $companyRepository,
        BranchRepository $branchRepository,
        FuelRepository $fuelRepository,
        EmployeeRepository $employeeRepository,
        VehicleRepository $vehicleRepository
    ) {
        $this->repository = $repository;
        $this->companyRepository = $companyRepository;
        $this->branchRepository = $branchRepository;
        $this->fuelRepository = $fuelRepository;
        $this->employeeRepository = $employeeRepository;
        $this->vehicleRepository = $vehicleRepository;

        $this->module_parent = 'Logbook Entry Manager';
        $this->module_sub = 'Logbook Entry';
        $this->module_name = 'logbook_entry';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'transactions-logbook-entry';
        $this->main_heading = 'Logbook Entry Manager';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'transactions.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ManageLogbookEntryRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageLogbookEntryRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  CreateLogbookEntryRequest  $request
     * @return mixed
     */
    public function generate(CreateLogbookEntryRequest $request)
    {
        Session::put('sess_id', Carbon::now()->timestamp);
        $sessionID = Session::get('sess_id');

        return view($this->module_view.'.generate')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub))
            ->with('companies', $this->companyRepository->toSelect())
            ->with('fuels', $this->fuelRepository->toSelect())
            ->with('branches', $this->branchRepository->toSelect())
            ->with('sess_id', $sessionID);
    }

    /**
     * @param  CreateLogbookEntryRequest  $request
     * @return mixed
     */
    public function create(CreateLogbookEntryRequest $request)
    {
        //get requests
        $transDate = $request->get('trans_date');
        $branchId = $request->get('branch_id');
        $sessionId = $request->get('sess_id');

        if (!Session::has('sess_id')) {
            return redirect()->route($this->module_route.'.generate')->with('flash_danger',
                'Please click process first!');
        } else {
            if (Session::get('sess_id') != $sessionId) {
                return redirect()->route($this->module_route.'.generate')->with('flash_danger',
                    'Please click process first!');
            }
        }


        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub))
            ->with('companies', $this->companyRepository->toSelect())
//            ->with('vehicles', $this->vehicleRepository->toSelectByBranch($request->get('branch_id')))
//            ->with('fuels', $this->fuelRepository->toSelect())
            ->with('trans_date', $transDate)
            ->with('branch_id', $branchId)
            ->with('branches', $this->branchRepository->toSelect())
            ->with('employee', $this->employeeRepository->getByBranch($branchId));
    }

    /**
     * @param  StoreLogbookEntryRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreLogbookEntryRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success', 'Record Successfully Saved');
    }


    /**
     * @param  ManageLogbookEntryRequest  $request
     * @param $id
     * @return mixed
     */
    public function edit(ManageLogbookEntryRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', $this->companyRepository->toSelect())
//            ->with('vehicles', $this->vehicleRepository->toSelectByBranch($request->get('branch_id')))
//            ->with('fuels', $this->fuelRepository->toSelect())
            ->with('branches', $this->branchRepository->toSelect())
            ->with('employee', $this->employeeRepository->getByBranch($module->branch_id));
    }

    /**
     * @param  UpdateLogbookEntryRequest  $request
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateLogbookEntryRequest $request)
    {
        $this->repository->update($request->except('_method', '_token'));

        return redirect()
            ->route($this->module_route.'.index')
            ->with('flash_success', 'Record Successfully Updated');
    }
}
