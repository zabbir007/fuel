<?php

namespace App\Http\Controllers\LogbookEntry;

use App\Http\Controllers\BaseController;
use App\Http\Requests\LogbookEntry\CreateLogbookEntryRequest;
use App\Repositories\BranchRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DriverRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\FuelRepository;
use App\Repositories\LogbookEntryRepository;
use App\Repositories\VehicleRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;

/**
 * Class LogbookEntryController.
 */
class LogbookEntryAjaxController extends BaseController
{
    /**
     * @var LogbookEntryRepository
     */
    private $repository;

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var mixed
     */
    private $branchRepository;

    /**
     * @var mixed
     */
    private $fuelRepository;

    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * @var VehicleRepository
     */
    private $vehicleRepository;

//    /**
//     * UserLogbookEntryController constructor.
//     *
//     * @param  LogbookEntryRepository  $repository
//     * @param  CompanyRepository  $companyRepository
//     * @param  BranchRepository  $branchRepository
//     * @param  FuelRepository  $fuelRepository
//     * @param  EmployeeRepository  $employeeRepository
//     * @param  VehicleRepository  $vehicleRepository
//     */
//    public function __construct(
//        LogbookEntryRepository $repository,
//        CompanyRepository $companyRepository,
//        BranchRepository $branchRepository,
//        FuelRepository $fuelRepository,
//        EmployeeRepository $employeeRepository,
//         $vehicleRepository
//    ) {
//        $this->repository = $repository;
//        $this->companyRepository = $companyRepository;
//        $this->branchRepository = $branchRepository;
//        $this->fuelRepository = $fuelRepository;
//        $this->employeeRepository = $employeeRepository;
//        $this->vehicleRepository = $vehicleRepository;
//    }

    /**
     * @param  CreateLogbookEntryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVehicles(CreateLogbookEntryRequest $request)
    {
        return \response()->json(app(VehicleRepository::class)->getByBranch($request->get('branch_id')));
    }

    /**
     * @param  CreateLogbookEntryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDrivers(CreateLogbookEntryRequest $request)
    {
        return \response()->json(app(DriverRepository::class)->getByBranchVehicle($request->get('branch_id'), $request->get('vehicle_id')));
    }

    /**
     * @param  CreateLogbookEntryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFuels(CreateLogbookEntryRequest $request)
    {
        return \response()->json(app(FuelRepository::class)->getByBranch($request->get('branch_id')));
    }

    public function postEntries(CreateLogbookEntryRequest $request){
        return \response()->json(app(LogbookEntryRepository::class)->postTransactions($request->all()));
    }

    public function approveEntries(CreateLogbookEntryRequest $request){
        return \response()->json(app(LogbookEntryRepository::class)->approveTransactions($request->all()));
    }

}
