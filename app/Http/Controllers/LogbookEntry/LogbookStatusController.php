<?php

namespace App\Http\Controllers\LogbookEntry;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Employee\DeleteEmployeeRequest;
use App\Http\Requests\Employee\ManageEmployeeRequest;
use App\Http\Requests\Employee\MarkEmployeeRequest;
use App\Http\Requests\Employee\RestoreEmployeeRequest;
use App\Http\Requests\Employee\ViewDeactivedEmployeeRequest;
use App\Http\Requests\Employee\ViewDeletedEmployeeRequest;
use App\Repositories\EmployeeRepository;
use Illuminate\Support\Str;
use Throwable;


/**
 * Class LogbookStatusController
 *
 * @package App\Http\Controllers\LogbookEntry
 */
class LogbookStatusController extends BaseController
{

    /**
     * @var EmployeeRepository
     */
    private $repository;

    /**
     * UserEmployeeController constructor.
     *
     * @param  EmployeeRepository  $repository
     */
    public function __construct(EmployeeRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Logbook Entry Manager';
        $this->module_sub = 'Logbook Entry';
        $this->module_name = 'logbook_entry';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'transactions-logbook-entry';
        $this->main_heading = 'Logbook Entry Manager';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'transactions.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedEmployeeRequest  $request
     *
     * @return mixed
     */
    public function getPending(ViewDeactivedEmployeeRequest $request)
    {
        return view($this->module_view.'.pending')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Pending '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function approve(MarkEmployeeRequest $request, $id)
    {
        $this->repository->mark($id, 1);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Approved');
    }
}
