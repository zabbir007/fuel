<?php

namespace App\Http\Controllers\LogbookEntry;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Employee\MarkEmployeeRequest;
use App\Http\Requests\LogbookEntry\ManageLogbookEntryRequest;
use App\Repositories\EmployeeRepository;
use App\Repositories\LogbookEntryRepository;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;


/**
 * Class LogbookEntryTableController
 *
 * @package App\Http\Controllers\LogbookEntry
 */
final class LogbookEntryTableController extends BaseController
{
    /**
     * @param  ManageLogbookEntryRequest  $request
     *
     * @param  LogbookEntryRepository  $repository
     * @return mixed
     */
    public function datatable(ManageLogbookEntryRequest $request, LogbookEntryRepository $repository)
    {
        return $repository->getForDataTable($request->get('status'),
            $request->get('trashed'));
    }
}
