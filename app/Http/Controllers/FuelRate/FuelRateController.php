<?php

namespace App\Http\Controllers\FuelRate;

use App\Exceptions\RecordNotFoundException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\FuelRate\CreateFuelRateRequest;
use App\Http\Requests\FuelRate\ManageFuelRateRequest;
use App\Http\Requests\FuelRate\StoreFuelRateRequest;
use App\Http\Requests\FuelRate\UpdateFuelRateRequest;
use App\Repositories\BranchRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\FuelRateRepository;
use App\Repositories\FuelRateTypeRepository;
use App\Repositories\FuelRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class FuelRateController.
 */
class FuelRateController extends BaseController
{
    /**
     * @var FuelRateRepository
     */
    private $repository;


    /**
     * @var mixed
     */
    private $branchRepository;

    /**
     * @var mixed
     */
    private $fuleRepository;

    /**
     * UserFuelRateController constructor.
     *
     * @param FuelRateRepository $repository
     * @param BranchRepository $branchRepository
     * @param FuelRepository $fuelRepository
     */
    public function __construct(FuelRateRepository $repository, BranchRepository $branchRepository, FuelRepository $fuelRepository)
    {
        $this->repository = $repository;
        $this->branchRepository = $branchRepository;
        $this->fuleRepository = $fuelRepository;

        $this->module_parent = 'Fuel Rate Manager';
        $this->module_sub = 'Fuel Rate';
        $this->module_name = 'fuel_rate';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'transactions-fuel-rate';
        $this->main_heading = 'Fuel Rate Manager';
        $this->module_title = $this->module_parent . ' - ' . app_name();
        $this->page_heading = $this->main_heading . ' - ' . app_name();
        $this->sub_heading = '';
        $this->module_route = 'transactions.' . $this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param ManageFuelRateRequest $request
     *
     * @return Factory|View
     */
    public function index(ManageFuelRateRequest $request)
    {
        return view($this->module_view . '.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active ' . Str::plural($this->module_sub))
            ->with('fuels', $this->fuleRepository->toSelect())
            ->with('all_fuel_rates', $this->repository->getActive());
    }

    /**
     * @param CreateFuelRateRequest $request
     * @return mixed
     */
    public function create(CreateFuelRateRequest $request)
    {
        return view($this->module_view . '.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New ' . Str::Singular($this->module_sub))
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('fuels', $this->fuleRepository->toSelect())
            ->with('branches', $this->branchRepository->toSelect());
    }

    /**
     * @param StoreFuelRateRequest $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreFuelRateRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Saved');
    }


    /**
     * @param ManageFuelRateRequest $request
     * @return mixed
     * @throws RecordNotFoundException
     */
    public function edit(ManageFuelRateRequest $request)
    {
        $modules = $this->repository
            ->where('branch_id', $request->get('branch_id'))
            ->where('entry_date', $request->get('date'))
            ->orderBy('created_at', 'desc')
            ->with(['branch', 'fuel'])
            ->get();

        if ($modules->count() == 0){
            throw new RecordNotFoundException('No Records found!', 404);
        }

        return view($this->module_view . '.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update ' . Str::Singular($this->module_sub))
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('fuels', $this->fuleRepository->toSelect())
            ->with('branches', $this->branchRepository->toSelect())
            ->with('modules', $modules);
    }

    /**
     * @param UpdateFuelRateRequest $request
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateFuelRateRequest $request)
    {
        $this->repository->update($request->except('_method', '_token'));

        return redirect()
            ->route($this->module_route . '.index')
            ->with('flash_success', 'Record Successfully Updated');
    }
}
