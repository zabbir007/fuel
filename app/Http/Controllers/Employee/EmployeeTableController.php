<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Employee\MarkEmployeeRequest;
use App\Repositories\EmployeeRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class UserEmployeeTableController.
 */
final class EmployeeTableController extends BaseController
{
    /**
     * @var EmployeeRepository
     */
    private $repository;


    /**
     * UserEmployeeTableController constructor.
     *
     * @param  EmployeeRepository  $repository
     */
    public function __construct(EmployeeRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Employee Management';
        $this->module_sub = 'Employee';
        $this->module_name = 'employee';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-employee';
        $this->main_heading = 'Employee Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'master.'.$this->module_name;
    }

    /**
     * @param  MarkEmployeeRequest  $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(MarkEmployeeRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id', 'name', 'short_name'])
            ->editColumn('employee_id', function ($q){
                return $q->employee_id == null ? 'N/A' : $q->employee_id;
            })
            ->addColumn('designation', function ($q){
                return $q->designation->name;
            })
            ->addColumn('branches', function ($q) {
                return $q->user->branches->pluck('name')->implode(', ');
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->rawColumns(['branch_logo'])
            ->make(true);
    }
}
