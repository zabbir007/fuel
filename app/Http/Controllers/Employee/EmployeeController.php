<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Employee\CreateEmployeeRequest;
use App\Http\Requests\Employee\DestroyEmployeeRequest;
use App\Http\Requests\Employee\ManageEmployeeRequest;
use App\Http\Requests\Employee\ShowEmployeeRequest;
use App\Http\Requests\Employee\StoreEmployeeRequest;
use App\Http\Requests\Employee\UpdateEmployeeRequest;
use App\Repositories\CompanyRepository;
use App\Repositories\DesignationRepository;
use App\Repositories\EmployeeRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class EmployeeController.
 */
class EmployeeController extends BaseController
{
    private $repository;

    /**
     * UserEmployeeController constructor.
     *
     * @param  EmployeeRepository  $repository
     */
    public function __construct(EmployeeRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Employee Management';
        $this->module_sub = 'Employee';
        $this->module_name = 'employee';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-employee';
        $this->main_heading = 'Employee Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ManageEmployeeRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageEmployeeRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  CreateEmployeeRequest  $request
     * @return mixed
     */
    public function create(CreateEmployeeRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub))
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('designations', app(DesignationRepository::class)->toSelect());
    }

    /**
     * @param  StoreEmployeeRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreEmployeeRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Saved');
    }

    /**
     * @param  ShowEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function show(ShowEmployeeRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', app(CompanyRepository::class)->toSelect());
    }

    /**
     * @param  ManageEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function edit(ManageEmployeeRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('designations', app(DesignationRepository::class)->toSelect());
    }

    /**
     * @param  UpdateEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DestroyEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyEmployeeRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted');
    }
}
