<?php

namespace App\Http\Controllers\Employee;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Employee\DeleteEmployeeRequest;
use App\Http\Requests\Employee\ManageEmployeeRequest;
use App\Http\Requests\Employee\MarkEmployeeRequest;
use App\Http\Requests\Employee\RestoreEmployeeRequest;
use App\Http\Requests\Employee\ViewDeactivedEmployeeRequest;
use App\Http\Requests\Employee\ViewDeletedEmployeeRequest;
use App\Repositories\EmployeeRepository;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class UserEmployeeStatusController.
 */
class EmployeeStatusController extends BaseController
{

    /**
     * @var EmployeeRepository
     */
    private $repository;

    /**
     * UserEmployeeController constructor.
     *
     * @param  EmployeeRepository  $repository
     */
    public function __construct(EmployeeRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Employee Management';
        $this->module_sub = 'Employee';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Employee Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedEmployeeRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedEmployeeRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedEmployeeRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedEmployeeRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkEmployeeRequest  $request
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(MarkEmployeeRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DeleteEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(DeleteEmployeeRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  RestoreEmployeeRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(RestoreEmployeeRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
