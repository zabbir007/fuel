<?php

namespace App\Http\Controllers\VehicleDriver;

use App\Http\Controllers\BaseController;
use App\Http\Requests\VehicleDriver\CreateVehicleDriverRequest;
use App\Http\Requests\VehicleDriver\DestroyVehicleDriverRequest;
use App\Http\Requests\VehicleDriver\EditVehicleDriverRequest;
use App\Http\Requests\VehicleDriver\ManageVehicleDriverRequest;
use App\Http\Requests\VehicleDriver\ShowVehicleDriverRequest;
use App\Http\Requests\VehicleDriver\StoreVehicleDriverRequest;
use App\Http\Requests\VehicleDriver\UpdateVehicleDriverRequest;
use App\Repositories\BranchRepository;
use App\Repositories\DriverRepository;
use App\Repositories\VehicleDriverRepository;
use App\Repositories\UserRepository;
use App\Repositories\VehicleRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class VehicleDriverController.
 */
class VehicleDriverController extends BaseController
{
    private $repository;

    /**
     * VehicleDriverController constructor.
     *
     * @param  VehicleDriverRepository  $repository
     */
    public function __construct(VehicleDriverRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Vehicle Driver Manager';
        $this->module_sub = 'Vehicle Driver';
        $this->module_name = 'vehicle_driver';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-vehicle-driver';
        $this->main_heading = 'Vehicle Driver Manager';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ManageVehicleDriverRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageVehicleDriverRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Assign Branch '.Str::plural($this->module_sub))
            ->with('vehicles', $this->repository->getAll());
    }

    /**
     * @param  CreateVehicleDriverRequest  $request
     * @return mixed
     */
    public function create(CreateVehicleDriverRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Assign New '.Str::Singular($this->module_sub))
            ->with('vehicles', app(VehicleRepository::class)->toSelect())
            ->with('branches', app(BranchRepository::class)->toSelect())
            ->with('drivers', app(DriverRepository::class)->toSelect());
    }

    /**
     * @param  StoreVehicleDriverRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreVehicleDriverRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Saved');
    }


    /**
     * @param EditVehicleDriverRequest $request
     * @param                        $id
     * @return mixed
     */
    public function edit(EditVehicleDriverRequest $request, $id)
    {
        $module = $this->repository->getById($id)->load(['vehicle','driver','branch']);


        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('vehicles', app(VehicleRepository::class)->toSelect())
            ->with('branches', app(BranchRepository::class)->toSelect())
            ->with('drivers', app(DriverRepository::class)->toSelect());
    }

    /**
     * @param  UpdateVehicleDriverRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateVehicleDriverRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DestroyVehicleDriverRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyVehicleDriverRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted');
    }
}
