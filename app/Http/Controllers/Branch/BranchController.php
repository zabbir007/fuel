<?php

namespace App\Http\Controllers\Branch;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Branch\CreateBranchRequest;
use App\Http\Requests\Branch\DestroyBranchRequest;
use App\Http\Requests\Branch\ManageBranchRequest;
use App\Http\Requests\Branch\ShowBranchRequest;
use App\Http\Requests\Branch\StoreBranchRequest;
use App\Http\Requests\Branch\UpdateBranchRequest;
use App\Repositories\BranchRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class UserBranchController.
 */
class BranchController extends BaseController
{
    private $repository;

    /**
     * UserBranchController constructor.
     *
     * @param  BranchRepository  $repository
     */
    public function __construct(BranchRepository $repository)
    {
        $this->repository = $repository;
        
        $this->module_parent = 'Branch Management';
        $this->module_sub = 'Branch';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Branch Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ManageBranchRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageBranchRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  CreateBranchRequest  $request
     * @return mixed
     */
    public function create(CreateBranchRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub));
    }

    /**
     * @param  StoreBranchRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreBranchRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Saved');
    }

    /**
     * @param  ShowBranchRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function show(ShowBranchRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param  ManageBranchRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function edit(ManageBranchRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param  UpdateBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateBranchRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DestroyBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyBranchRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted');
    }
}
