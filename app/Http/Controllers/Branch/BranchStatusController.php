<?php

namespace App\Http\Controllers\Branch;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Branch\DeleteBranchRequest;
use App\Http\Requests\Branch\ManageBranchRequest;
use App\Http\Requests\Branch\MarkBranchRequest;
use App\Http\Requests\Branch\RestoreBranchRequest;
use App\Http\Requests\Branch\ViewDeactivedBranchRequest;
use App\Http\Requests\Branch\ViewDeletedBranchRequest;
use App\Repositories\BranchRepository;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class UserBranchStatusController.
 */
class BranchStatusController extends BaseController
{

    /**
     * @var BranchRepository
     */
    private $repository;

    /**
     * UserBranchController constructor.
     *
     * @param  BranchRepository  $repository
     */
    public function __construct(BranchRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Branch Management';
        $this->module_sub = 'Branch';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Branch Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedBranchRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedBranchRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedBranchRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedBranchRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkBranchRequest  $request
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(MarkBranchRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DeleteBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(DeleteBranchRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  RestoreBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(RestoreBranchRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
