<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Log;

/**
 * Class BaseController.
 */
class BaseController extends Controller
{
    /**
     * Module Parent Name.
     *
     * @var
     */
    protected $module_parent;

    /**
     * Module Sub Name.
     *
     * @var
     */
    protected $module_sub;

    /**
     * Module Name.
     *
     * @var
     */
    protected $module_name;

    /**
     * Module Icon.
     *
     * @var
     */
    protected $module_icon;

    /**
     * Module Action.
     *
     * @var
     */
    protected $module_action;

    /**
     * Module Route.
     *
     * @var
     */
    protected $module_route;

    /**
     * Module View.
     *
     * @var
     */
    protected $module_view;

    /**
     * Module Title
     *
     * @var
     */
    protected $module_title;

    /**
     * @var
     */
    protected $module_permission;

    /**
     * Module Main Heading.
     * @var
     */
    protected $main_heading;

    /**
     * Module Page Heading.
     *
     * @var
     */
    protected $page_heading;

    /**
     * Module Page Sub Heading
     * @var
     */
    protected $sub_heading;

    /**
     * Get Logged User.
     *
     * @return Authenticatable|null
     */
    public function getLoggedUser(): ?Authenticatable
    {
        return request()->user();
    }

    /**
     * Get Logged User.
     *
     * @return mixed
     */
    public function getLoggedUserCompany()
    {
        return Auth::user()->load([
            'company' => function ($q) {
                return $q->select(['id', 'name', 'short_name', 'email_suffix']);
            }
        ])->company;
    }

    /**
     *
     */
    public function enableQueryLog(): void
    {
        DB::enableQueryLog();
    }

    /**
     *
     */
    public function disableQueryLog(): void
    {
        DB::disableQueryLog();
    }

    /**
     * @return array
     */
    public function getQueryLog(): array
    {
        return DB::getQueryLog();
    }
}
