<?php

namespace App\Http\Controllers\UserBranch;

use App\Http\Controllers\BaseController;
use App\Http\Requests\UserBranch\CreateUserBranchRequest;
use App\Http\Requests\UserBranch\DestroyUserBranchRequest;
use App\Http\Requests\UserBranch\ManageUserBranchRequest;
use App\Http\Requests\UserBranch\ShowUserBranchRequest;
use App\Http\Requests\UserBranch\StoreUserBranchRequest;
use App\Http\Requests\UserBranch\UpdateUserBranchRequest;
use App\Repositories\BranchRepository;
use App\Repositories\UserBranchRepository;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class UserBranchController.
 */
class UserBranchController extends BaseController
{
    private $repository;

    /**
     * UserBranchController constructor.
     *
     * @param  UserBranchRepository  $repository
     */
    public function __construct(UserBranchRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Branch User Management';
        $this->module_sub = 'Branch User';
        $this->module_name = 'user_branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch-user';
        $this->main_heading = 'Branch User Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ManageUserBranchRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageUserBranchRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Assign Branch '.Str::plural($this->module_sub))
            ->with('users', $this->repository->getAll());
    }

    /**
     * @param  CreateUserBranchRequest  $request
     * @return mixed
     */
    public function create(CreateUserBranchRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Assign New '.Str::Singular($this->module_sub))
            ->with('users', app(UserRepository::class)->toSelect())
            ->with('branches', app(BranchRepository::class)->toSelect());
    }

    /**
     * @param  StoreUserBranchRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreUserBranchRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Saved');
    }


    /**
     * @param  ManageUserBranchRequest  $request
     * @param                        $id
     * @return mixed
     */
    public function edit(ManageUserBranchRequest $request, $id)
    {
        $module = app(UserRepository::class)->getById($id)->load('branches');


        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('user_id', $module->id)
            ->with('user_branches', $module->branches->pluck('id'))
            ->with('users', app(UserRepository::class)->toSelect())
            ->with('branches', app(BranchRepository::class)->toSelect());
    }

    /**
     * @param  UpdateUserBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateUserBranchRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DestroyUserBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyUserBranchRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted');
    }
}
