<?php

namespace App\Http\Controllers\UserBranch;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\UserBranch\DeleteUserBranchRequest;
use App\Http\Requests\UserBranch\MarkUserBranchRequest;
use App\Http\Requests\UserBranch\RestoreUserBranchRequest;
use App\Http\Requests\UserBranch\ViewDeactivedUserBranchRequest;
use App\Http\Requests\UserBranch\ViewDeletedUserBranchRequest;
use App\Repositories\BranchRepository;
use App\Repositories\UserBranchRepository;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class UserBranchStatusController.
 */
class UserBranchStatusController extends BaseController
{

    /**
     * @var BranchRepository
     */
    private $repository;

    /**
     * UserBranchController constructor.
     *
     * @param  UserBranchRepository  $repository
     */
    public function __construct(UserBranchRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Branch Management';
        $this->module_sub = 'Branch';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Branch Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedUserBranchRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedUserBranchRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedUserBranchRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedUserBranchRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkUserBranchRequest  $request
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(MarkUserBranchRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DeleteUserBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(DeleteUserBranchRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  RestoreUserBranchRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(RestoreUserBranchRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
