<?php

namespace App\Http\Controllers\UserBranch;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Branch\MarkBranchRequest;
use App\Repositories\BranchRepository;
use App\Repositories\UserBranchRepository;
use DataTables;

/**
 * Class UserBranchTableController.
 */
final class UserBranchTableController extends BaseController
{
    /**
     * @var UserBranchRepository
     */
    private $repository;


    /**
     * UserBranchTableController constructor.
     *
     * @param  UserBranchRepository  $repository
     */
    public function __construct(UserBranchRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Branch Management';
        $this->module_sub = 'Branch';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Branch Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  MarkBranchRequest  $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(MarkBranchRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id', 'name', 'short_name'])
            ->addColumn('user', function ($q) {
                return $q->user->pluck('id');
            })
            ->addColumn('branches', function ($q) {
                return $q->pluck('name')->implode(',');
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->rawColumns(['branch_logo'])
            ->make(true);
    }
}