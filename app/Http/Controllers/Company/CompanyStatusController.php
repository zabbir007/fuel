<?php

namespace App\Http\Controllers\Company;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Company\DeleteCompanyRequest;
use App\Http\Requests\Company\ManageCompanyRequest;
use App\Http\Requests\Company\MarkCompanyRequest;
use App\Http\Requests\Company\RestoreCompanyRequest;
use App\Http\Requests\Company\ViewDeactivedCompanyRequest;
use App\Http\Requests\Company\ViewDeletedCompanyRequest;
use App\Repositories\CompanyRepository;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class CompanyStatusController.
 */
class CompanyStatusController extends BaseController
{

    /**
     * @var CompanyRepository
     */
    private $repository;

    /**
     * CompanyStatusController constructor.
     *
     * @param  CompanyRepository  $repository
     */
    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
        
        $this->module_parent = 'Company Management';
        $this->module_sub = 'Company';
        $this->module_name = 'company';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'Company Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedCompanyRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedCompanyRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedCompanyRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedCompanyRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkCompanyRequest  $request
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(MarkCompanyRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DeleteCompanyRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(DeleteCompanyRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.deleted')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  RestoreCompanyRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(RestoreCompanyRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
