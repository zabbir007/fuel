<?php

namespace App\Http\Controllers\Auth\User;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\User\ManageUserRequest;
use App\Repositories\UserRepository;
use Str;
use Throwable;

/**
 * Class UserStatusController.
 */
class UserStatusController extends BaseController
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Access Management';
        $this->module_sub = 'User';
        $this->module_name = 'user';
        $this->module_icon = 'fas fa-users';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'User Management';
        $this->module_title = $this->module_parent . ' - ' . app_name();
        $this->page_heading = $this->main_heading.' - '. app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageUserRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ManageUserRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  ManageUserRequest  $request
     * @param                     $id
     * @param                     $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(ManageUserRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  ManageUserRequest  $request
     * @param                     $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(ManageUserRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.deleted')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  ManageUserRequest  $request
     * @param                     $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(ManageUserRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
