<?php

namespace App\Http\Controllers\Auth\User;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\User\ManageUserRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Repositories\UserRepository;

/**
 * Class UserPasswordController.
 */
class UserPasswordController extends BaseController
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Access Management';
        $this->module_sub = 'User';
        $this->module_name = 'user';
        $this->module_icon = 'fas fa-users';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'User Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'auth.'.$this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ManageUserRequest  $request
     * @param                     $id
     * @return mixed
     */
    public function edit(ManageUserRequest $request, $id)
    {
        $user = $this->repository->getById($id);

        return view($this->module_view.'.change-password')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Change User Password')
            ->with('user', $user);
    }

    /**
     * @param  UpdatePasswordRequest  $request
     * @param                             $id
     * @return mixed
     * @throws GeneralException
     */
    public function update(UpdatePasswordRequest $request, $id)
    {
        $this->repository->updatePassword($id, $request->only('password'));

        return redirect()->route($this->module_route.'.index')->with('flash_success',
            __('alerts.users.updated_password'));
    }
}
