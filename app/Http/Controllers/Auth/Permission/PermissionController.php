<?php

namespace App\Http\Controllers\Auth\Permission;

use App\Events\Permission\PermissionDeleted;
use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Permission\CreatePermissionRequest;
use App\Http\Requests\Permission\DestroyPermissionRequest;
use App\Http\Requests\Permission\EditPermissionRequest;
use App\Http\Requests\Permission\ManagePermissionRequest;
use App\Http\Requests\Permission\ShowPermissionRequest;
use App\Http\Requests\Permission\StorePermissionRequest;
use App\Http\Requests\Permission\UpdatePermissionRequest;
use App\Repositories\PermissionRepository;
use Exception;
use Str;
use Throwable;

/**
 * Class PermissionController.
 */
class PermissionController extends BaseController
{
    /**
     * @var PermissionRepository
     */
    protected $repository;

    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    /**
     * @param  PermissionRepository  $repository
     * @param  PermissionRepository  $permissionRepository
     */
    public function __construct(PermissionRepository $repository, PermissionRepository $permissionRepository)
    {
        $this->repository = $repository;
        $this->permissionRepository = $permissionRepository;

        $this->module_parent = 'Access Manager';
        $this->module_sub = 'Permission';
        $this->module_name = 'permission';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-user';
        $this->main_heading = 'Permission Manager';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'auth.'.$this->module_name;
    }

    /**
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     */
    public function index(ManagePermissionRequest $request)
    {
        return view($this->module_view.'.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active '.Str::plural($this->module_sub));
    }

    /**
     * @param  CreatePermissionRequest  $request
     *
     * @return mixed
     */
    public function create(CreatePermissionRequest $request)
    {
        return view($this->module_view.'.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New '.Str::Singular($this->module_sub));
    }

    /**
     * @param  StorePermissionRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(StorePermissionRequest $request)
    {
        $this->repository->create($request->only('name', 'sort'));

        return redirect()->route($this->module_route.'.index')
            ->with('flash_success', 'Record Successfully Saved');
    }

    /**
     * @param  ShowPermissionRequest  $request
     * @param                     $id
     * @return mixed
     */
    public function show(ShowPermissionRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view.'.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View '.Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('permissions', $this->permissionRepository->get())
            ->with('PermissionPermissions', $module->permissions->pluck('name')->all());
    }

    /**
     * @param  EditPermissionRequest  $request
     * @param                     $id
     * @return mixed
     */
    public function edit(EditPermissionRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        if ($module->isDefault()) {
            return redirect()->route($this->module_route.'.index')
                ->with('flash_danger', 'You can not edit the default permission.');
        }

        return view($this->module_view.'.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update '.Str::Singular($this->module_sub))
            ->with('module', $module);
    }

    /**
     * @param  UpdatePermissionRequest  $request
     * @param                     $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        $this->repository->update($id, $request->only('name', 'permissions'));

        return redirect()->route($this->module_route.'.index')->with('flash_success', 'Record Successfully Updated');
    }

    /**
     * @param  DestroyPermissionRequest  $request
     * @param                     $id
     * @return mixed
     * @throws Exception
     */
    public function destroy(DestroyPermissionRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        if ($module->isSuperAdmin()) {
            return redirect()->route($this->module_route.'.index')
                ->with('flash_danger', __('exceptions..access.Permissions.cant_delete_admin'));
        }

        $this->repository->deleteById($module->id);

        event(new PermissionDeleted($module));

        return redirect()->route($this->module_route.'.index')->with('flash_success', __('alerts..Permissions.deleted'));
    }
}
