<?php

namespace App\Http\Controllers\Auth\Role;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Role\ManageRoleRequest;
use App\Repositories\RoleRepository;
use DataTables;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

final class RoleTableController extends BaseController
{
    /**
     * @var $repository
     */
    protected $repository;

    /**
     * RoleTableController constructor.
     *
     * @param  RoleRepository  $repository
     */
    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function datatable(ManageRoleRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->escapeColumns(['name','guard_name'])
            ->editColumn('name', function ($q) {
                return ucfirst(Str::title($q->name));
            })
            ->addColumn('identifier', function ($q) {
                return $q->name;
            })
            ->addColumn('total_users', function ($q) {
                return $q->users->count();
            })
            ->addColumn('total_permissions', function ($q) {
                return $q->id == 1 ? 'All' : $q->permissions->count();
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->removeColumn(['users', 'permissions'])
            ->make(true);
    }
}
