<?php

namespace App\Http\Controllers\Driver;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Driver\CreateDriverRequest;
use App\Http\Requests\Driver\DestroyDriverRequest;
use App\Http\Requests\Driver\ManageDriverRequest;
use App\Http\Requests\Driver\ShowDriverRequest;
use App\Http\Requests\Driver\StoreDriverRequest;
use App\Http\Requests\Driver\UpdateDriverRequest;
use App\Repositories\CompanyRepository;
use App\Repositories\DocumentTypeRepository;
use App\Repositories\DriverRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class DriverController.
 */
class DriverController extends BaseController
{
    /**
     * @var DriverRepository
     */
    private $repository;

    private $documentTypes;

    /**
     * UserDriverController constructor.
     *
     * @param DriverRepository $repository
     */
    public function __construct(DriverRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Driver Management';
        $this->module_sub = 'Driver';
        $this->module_name = 'driver';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-driver';
        $this->main_heading = 'Driver Management';
        $this->module_title = $this->module_parent . ' - ' . app_name();
        $this->page_heading = $this->main_heading . ' - ' . app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.' . $this->module_name;
        $this->module_view = $this->module_name;

        $this->documentTypes = app(DocumentTypeRepository::class)->toSelectDriver();
    }

    /**
     * @param ManageDriverRequest $request
     *
     * @return Factory|View
     */
    public function index(ManageDriverRequest $request)
    {
        return view($this->module_view . '.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active ' . Str::plural($this->module_sub));
    }

    /**
     * @param CreateDriverRequest $request
     * @return mixed
     */
    public function create(CreateDriverRequest $request)
    {
        return view($this->module_view . '.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New ' . Str::Singular($this->module_sub))
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('document_types', $this->documentTypes);
    }

    /**
     * @param StoreDriverRequest $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreDriverRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Saved');
    }

    /**
     * @param ShowDriverRequest $request
     * @param                        $id
     * @return mixed
     */
    public function show(ShowDriverRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view . '.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View ' . Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('document_types', $this->documentTypes);
    }

    /**
     * @param ManageDriverRequest $request
     * @param                        $id
     * @return mixed
     */
    public function edit(ManageDriverRequest $request, $id)
    {
        $module = $this->repository->getById($id)->load('documents');

        return view($this->module_view . '.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update ' . Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('document_types', $this->documentTypes);
    }

    /**
     * @param UpdateDriverRequest $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateDriverRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Updated');
    }

    /**
     * @param DestroyDriverRequest $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyDriverRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Deleted');
    }
}
