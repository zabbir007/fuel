<?php

namespace App\Http\Controllers\Driver;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Driver\DeleteDriverRequest;
use App\Http\Requests\Driver\ManageDriverRequest;
use App\Http\Requests\Driver\MarkDriverRequest;
use App\Http\Requests\Driver\RestoreDriverRequest;
use App\Http\Requests\Driver\ViewDeactivedDriverRequest;
use App\Http\Requests\Driver\ViewDeletedDriverRequest;
use App\Repositories\DriverRepository;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class UserDriverStatusController.
 */
class DriverStatusController extends BaseController
{

    /**
     * @var DriverRepository
     */
    private $repository;

    /**
     * UserDriverController constructor.
     *
     * @param  DriverRepository  $repository
     */
    public function __construct(DriverRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Driver Management';
        $this->module_sub = 'Driver';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Driver Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedDriverRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedDriverRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedDriverRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedDriverRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkDriverRequest  $request
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(MarkDriverRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DeleteDriverRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(DeleteDriverRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  RestoreDriverRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(RestoreDriverRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
