<?php

namespace App\Http\Controllers\Driver;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Driver\MarkDriverRequest;
use App\Repositories\DriverRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class UserDriverTableController.
 */
final class DriverTableController extends BaseController
{
    /**
     * @var DriverRepository
     */
    private $repository;


    /**
     * UserDriverTableController constructor.
     *
     * @param  DriverRepository  $repository
     */
    public function __construct(DriverRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Driver Management';
        $this->module_sub = 'Driver';
        $this->module_name = 'driver';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-driver';
        $this->main_heading = 'Driver Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'master.'.$this->module_name;
    }

    /**
     * @param  MarkDriverRequest  $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(MarkDriverRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id', 'name', 'short_name'])
            ->editColumn('employee_id', function ($q){
                return $q->employee_id == null ? 'N/A' : $q->employee_id;
            })
            ->addColumn('branches', function ($q) {
                return $q->branch->unique()->pluck('name')->implode(', ');
//                return '';
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->rawColumns(['branch_logo'])
            ->make(true);
    }
}
