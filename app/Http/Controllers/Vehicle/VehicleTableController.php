<?php

namespace App\Http\Controllers\Vehicle;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Vehicle\MarkVehicleRequest;
use App\Repositories\VehicleRepository;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class UserVehicleTableController.
 */
final class VehicleTableController extends BaseController
{
    /**
     * @var VehicleRepository
     */
    private $repository;


    /**
     * UserVehicleTableController constructor.
     *
     * @param  VehicleRepository  $repository
     */
    public function __construct(VehicleRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Vehicle Management';
        $this->module_sub = 'Vehicle';
        $this->module_name = 'driver';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-driver';
        $this->main_heading = 'Vehicle Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = $this->module_name;
        $this->module_view = 'master.'.$this->module_name;
    }

    /**
     * @param  MarkVehicleRequest  $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function datatable(MarkVehicleRequest $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable($request->get('status'),
            $request->get('trashed')))
            ->addIndexColumn()
            ->escapeColumns(['id', 'name', 'short_name'])
            ->editColumn('type', function ($q){
                return $q->type->name;
            })
            ->addColumn('tank_type', function ($q){
                return $q->is_dual_tank ? 'Dual' : 'Single';
            })
            ->addColumn('branches', function ($q) {
               // return $q->user->branches->pluck('name')->implode(', ');
                return '';
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at->diffForHumans();
            })
            ->make(true);
    }
}
