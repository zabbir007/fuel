<?php

namespace App\Http\Controllers\Vehicle;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Vehicle\DeleteVehicleRequest;
use App\Http\Requests\Vehicle\ManageVehicleRequest;
use App\Http\Requests\Vehicle\MarkVehicleRequest;
use App\Http\Requests\Vehicle\RestoreVehicleRequest;
use App\Http\Requests\Vehicle\ViewDeactivedVehicleRequest;
use App\Http\Requests\Vehicle\ViewDeletedVehicleRequest;
use App\Repositories\VehicleRepository;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class UserVehicleStatusController.
 */
class VehicleStatusController extends BaseController
{

    /**
     * @var VehicleRepository
     */
    private $repository;

    /**
     * UserVehicleController constructor.
     *
     * @param  VehicleRepository  $repository
     */
    public function __construct(VehicleRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Vehicle Management';
        $this->module_sub = 'Vehicle';
        $this->module_name = 'branch';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-branch';
        $this->main_heading = 'Vehicle Management';
        $this->module_title = $this->module_parent.' - '.app_name();
        $this->page_heading = $this->main_heading.' - '.app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.'.$this->module_name;
        $this->module_view = $this->module_name;
    }

    /**
     * @param  ViewDeactivedVehicleRequest  $request
     *
     * @return mixed
     */
    public function getDeactivated(ViewDeactivedVehicleRequest $request)
    {
        return view($this->module_view.'.deactivated')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Deactivated '.Str::plural($this->module_sub));
    }

    /**
     * @param  ViewDeletedVehicleRequest  $request
     *
     * @return mixed
     */
    public function getDeleted(ViewDeletedVehicleRequest $request)
    {
        return view($this->module_view.'.trashed')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View Trashed '.Str::plural($this->module_sub));
    }

    /**
     * @param  MarkVehicleRequest  $request
     * @param                        $id
     * @param                        $status
     *
     * @return mixed
     * @throws GeneralException
     */
    public function mark(MarkVehicleRequest $request, $id, $status)
    {
        $this->repository->mark($id, (int) $status);

        return redirect()->route( $this->module_route.'.index')->with('flash_success','Record Successfully Updated');
    }

    /**
     * @param  DeleteVehicleRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete(DeleteVehicleRequest $request, $id)
    {
        $this->repository->forceDelete($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Deleted Permanently');
    }

    /**
     * @param  RestoreVehicleRequest  $request
     * @param                        $id
     * @return mixed
     * @throws GeneralException
     */
    public function restore(RestoreVehicleRequest $request, $id)
    {
        $this->repository->restore($id);

        return redirect()->route($this->module_route.'.index')->with('flash_success','Record Successfully Restored');
    }
}
