<?php

namespace App\Http\Controllers\Vehicle;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Vehicle\CreateVehicleRequest;
use App\Http\Requests\Vehicle\DestroyVehicleRequest;
use App\Http\Requests\Vehicle\ManageVehicleRequest;
use App\Http\Requests\Vehicle\ShowVehicleRequest;
use App\Http\Requests\Vehicle\StoreVehicleRequest;
use App\Http\Requests\Vehicle\UpdateVehicleRequest;
use App\Repositories\CompanyRepository;
use App\Repositories\DocumentTypeRepository;
use App\Repositories\FuelRepository;
use App\Repositories\VehicleRepository;
use App\Repositories\VehicleTypeRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

/**
 * Class VehicleController.
 */
class VehicleController extends BaseController
{
    /**
     * @var VehicleRepository
     */
    private $repository;

    /**
     * @var mixed
     */
    private $documentTypes;

    /**
     * @var mixed
     */
    private $vehicleTypes;

    /**
     * @var mixed
     */
    private $fuelTypes;

    /**
     * UserVehicleController constructor.
     *
     * @param VehicleRepository $repository
     */
    public function __construct(VehicleRepository $repository)
    {
        $this->repository = $repository;

        $this->module_parent = 'Vehicle Management';
        $this->module_sub = 'Vehicle';
        $this->module_name = 'vehicle';
        $this->module_icon = 'fas fa-user-lock';
        $this->module_permission = 'setup-vehicle';
        $this->main_heading = 'Vehicle Management';
        $this->module_title = $this->module_parent . ' - ' . app_name();
        $this->page_heading = $this->main_heading . ' - ' . app_name();
        $this->sub_heading = '';
        $this->module_route = 'master.' . $this->module_name;
        $this->module_view = $this->module_name;

        $this->documentTypes = app(DocumentTypeRepository::class)->toSelectVehicle();
        $this->vehicleTypes = app(VehicleTypeRepository::class)->toSelect();
        $this->fuelTypes = app(FuelRepository::class)->toSelect();
    }

    /**
     * @param ManageVehicleRequest $request
     *
     * @return Factory|View
     */
    public function index(ManageVehicleRequest $request)
    {
        return view($this->module_view . '.index')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'All Active ' . Str::plural($this->module_sub));
    }

    /**
     * @param CreateVehicleRequest $request
     * @return mixed
     */
    public function create(CreateVehicleRequest $request)
    {
        return view($this->module_view . '.create')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Add New ' . Str::Singular($this->module_sub))
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('document_types', $this->documentTypes)
            ->with('vehicle_types', $this->vehicleTypes)
            ->with('fuel_types', $this->fuelTypes);
    }

    /**
     * @param StoreVehicleRequest $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function store(StoreVehicleRequest $request)
    {
        $this->repository->create($request->except('_method', '_token'));

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Saved');
    }

    /**
     * @param ShowVehicleRequest $request
     * @param                        $id
     * @return mixed
     */
    public function show(ShowVehicleRequest $request, $id)
    {
        $module = $this->repository->getById($id);

        return view($this->module_view . '.show')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'View ' . Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('document_types', $this->documentTypes)
            ->with('vehicle_types', $this->vehicleTypes)
            ->with('fuel_types', $this->fuelTypes);
    }

    /**
     * @param ManageVehicleRequest $request
     * @param                        $id
     * @return mixed
     */
    public function edit(ManageVehicleRequest $request, $id)
    {
        $module = $this->repository->getById($id)->load('documents');

        return view($this->module_view . '.edit')
            ->with('module_name', $this->module_name)
            ->with('module_icon', $this->module_icon)
            ->with('module_view', $this->module_view)
            ->with('module_route', $this->module_route)
            ->with('module_permission', $this->module_permission)
            ->with('title', $this->module_title)
            ->with('main_heading', $this->main_heading)
            ->with('page_heading', $this->page_heading)
            ->with('sub_heading', 'Update ' . Str::Singular($this->module_sub))
            ->with('module', $module)
            ->with('companies', app(CompanyRepository::class)->toSelect())
            ->with('document_types', $this->documentTypes)
            ->with('vehicle_types', $this->vehicleTypes)
            ->with('fuel_types', $this->fuelTypes);
    }

    /**
     * @param UpdateVehicleRequest $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function update(UpdateVehicleRequest $request, $id)
    {
        $this->repository->update($id, $request->except('_method', 'token'));

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Updated');
    }

    /**
     * @param DestroyVehicleRequest $request
     * @param                        $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy(DestroyVehicleRequest $request, $id)
    {
        $this->repository->destroy($id);

        return redirect()->route($this->module_route . '.index')->with('flash_success', 'Record Successfully Deleted');
    }
}
