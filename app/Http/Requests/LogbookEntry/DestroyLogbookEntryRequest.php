<?php

namespace App\Http\Requests\LogbookEntry;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DestroyLogbookEntryRequest
 *
 * @package App\Http\Requests\LogbookEntry
 */
class DestroyLogbookEntryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('transactions-logbook-entry-destroy');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
