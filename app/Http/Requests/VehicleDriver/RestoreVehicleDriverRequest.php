<?php

namespace App\Http\Requests\VehicleDriver;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class RestoreVehicleDriverRequest
 *
 * @package App\Http\Requests\VehicleDriver
 */
class RestoreVehicleDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-vehicle-driver-restore');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
