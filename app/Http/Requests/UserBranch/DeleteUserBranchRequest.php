<?php

namespace App\Http\Requests\UserBranch;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DeleteUserBranchRequest
 *
 * @package App\Http\Requests\UserBranch
 */
class DeleteUserBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-branch-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
