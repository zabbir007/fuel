<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class DeleteEmployeeRequest
 *
 * @package App\Http\Requests\Employee
 */
class DeleteEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-employee-delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
