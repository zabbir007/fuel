<?php

namespace App\Http\Requests\Branch;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class ShowBranchRequest
 *
 * @package App\Http\Requests\Branch
 */
class ShowBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('setup-branch-show');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
