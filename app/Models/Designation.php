<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\DesignationAttribute;
use App\Models\Traits\Method\DesignationMethod;
use App\Models\Traits\Relationship\DesignationRelationship;
use App\Models\Traits\Scope\DesignationScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Designation
 *
 * @package App\Models\Master
 */
class Designation extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        DesignationAttribute,
        DesignationMethod,
        DesignationRelationship,
        DesignationScope;

    private $module_name = 'Designation';
    private $module_route = 'master.designation';
    private $module_permission = 'setup-designation';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
    
    protected $table = 'employee_designations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
