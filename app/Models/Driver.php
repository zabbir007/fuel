<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\DriverAttribute;
use App\Models\Traits\Method\DriverMethod;
use App\Models\Traits\Relationship\DriverRelationship;
use App\Models\Traits\Scope\DriverScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Driver
 *
 * @package App\Models\Master
 */
class Driver extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        DriverAttribute,
        DriverMethod,
        DriverRelationship,
        DriverScope;

    private $module_name = 'Driver';
    private $module_route = 'master.driver';
    private $module_permission = 'setup-driver';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
