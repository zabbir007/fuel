<?php

namespace App\Models\Traits\Scope;

/**
 * Trait DriverDocumentScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait DriverDocumentScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
