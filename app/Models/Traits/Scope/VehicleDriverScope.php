<?php

namespace App\Models\Traits\Scope;

/**
 * Trait VehicleDriverScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait VehicleDriverScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeArchived($query, $status = false)
    {
        return $query->where('archived', $status);
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
