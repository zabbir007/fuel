<?php

namespace App\Models\Traits\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait BranchGlobalScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait BranchScope
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('branch', function (Builder $builder) {
            $branches = \Session::get('user_branches');

            if (is_array($branches)){
                $branches = \Arr::pluck($branches, 'id');
                $builder->whereIn('id', $branches);
            }
        });
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }

}
