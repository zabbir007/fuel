<?php

namespace App\Models\Traits\Scope;

use App\Scopes\CompanyGlobalScope;

/**
 * Trait DesignationScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait DesignationScope
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyGlobalScope());
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
