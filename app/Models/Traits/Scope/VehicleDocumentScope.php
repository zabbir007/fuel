<?php

namespace App\Models\Traits\Scope;

/**
 * Trait VehicleDocumentScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait VehicleDocumentScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
