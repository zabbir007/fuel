<?php

namespace App\Models\Traits\Scope;


use App\Scopes\BranchGlobalScope;
use App\Scopes\CompanyGlobalScope;

/**
 * Trait FuelRateScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait FuelRateScope
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyGlobalScope());
        static::addGlobalScope(new BranchGlobalScope());
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeArchived($query, $status = false)
    {
        return $query->where('archived', $status);
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
