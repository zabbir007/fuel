<?php

namespace App\Models\Traits\Scope;

/**
 * Trait RoleScope
 *
 * @package App\Models\Auth\Traits\Scope
 */
trait RoleScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
