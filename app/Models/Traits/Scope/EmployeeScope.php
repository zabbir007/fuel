<?php

namespace App\Models\Traits\Scope;

use App\Scopes\CompanyGlobalScope;

/**
 * Trait EmployeeScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait EmployeeScope
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyGlobalScope());
    }

    /**
     * @param        $query
     * @param $designation
     * @return mixed
     */
    public function scopeDesignation($query, $designation)
    {
        return $query->where('designation_id', $designation);
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
