<?php

namespace App\Models\Traits\Scope;

use App\Scopes\CompanyGlobalScope;

/**
 * Trait DocumentTypeScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait DocumentTypeScope
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyGlobalScope());
    }

    /**
     * @param        $query
     * @return mixed
     */
    public function scopeDriver($query)
    {
        return $query->where('type', 1);
    }

    /**
     * @param        $query
     * @return mixed
     */
    public function scopeVehicle($query)
    {
        return $query->where('type', 2);
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
