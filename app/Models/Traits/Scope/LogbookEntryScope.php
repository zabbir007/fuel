<?php

namespace App\Models\Traits\Scope;


use App\Scopes\BranchGlobalScope;
use App\Scopes\CompanyGlobalScope;

/**
 * Trait LogbookEntryScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait LogbookEntryScope
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CompanyGlobalScope());
        static::addGlobalScope(new BranchGlobalScope());
    }

    /**
     * @param        $query
     * @param  int  $status
     *
     * @return mixed
     */
    public function scopePosted($query, $status = 1)
    {
        return $query->where('posted', $status);
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = 1)
    {
        return $query->where('status', $status);
    }
}
