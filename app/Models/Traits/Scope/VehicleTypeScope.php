<?php

namespace App\Models\Traits\Scope;

/**
 * Trait VehicleTypeScope
 *
 * @package App\Models\Master\Traits\Scope
 */
trait VehicleTypeScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
