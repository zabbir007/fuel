<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait VehicleTypeMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait VehicleTypeMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
