<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait DriverMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait DriverMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
