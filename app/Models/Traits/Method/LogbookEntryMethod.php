<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait LogbookEntryMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait LogbookEntryMethod
{
    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->status == 2 && $this->approved;
    }

    /**
     * @return bool
     */
    public function isPosted()
    {
        return $this->status == 1 && $this->posted == 1;
    }
}
