<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait VehicleHistoryMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait VehicleHistoryMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
