<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait EmployeeMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait EmployeeMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
