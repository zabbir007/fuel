<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait DocumentTypeMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait DocumentTypeMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
