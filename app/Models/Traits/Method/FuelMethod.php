<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait FuelMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait FuelMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
