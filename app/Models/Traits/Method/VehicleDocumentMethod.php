<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait VehicleDocumentMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait VehicleDocumentMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
