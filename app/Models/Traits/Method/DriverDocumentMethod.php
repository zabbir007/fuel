<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait DriverDocumentMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait DriverDocumentMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
