<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait FuelRateMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait FuelRateMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
