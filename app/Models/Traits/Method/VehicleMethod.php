<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait VehicleMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait VehicleMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
