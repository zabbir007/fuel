<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait BranchMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait BranchMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
