<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait UserMethod.
 */
trait UserMethod
{
    /**
     * @return mixed
     */
    public function canChangeEmail()
    {
        return config('access.users.change_email');
    }

    /**
     * @return bool
     */
    public function canChangePassword()
    {
        return false;
    }

    /**
     * @param  bool  $size
     *
     * @return bool|UrlGenerator|mixed|string
     */
    public function getPicture($size = false)
    {
        return asset('storage/img/'.$this->profile_photo);
    }

    /**
     * @param $provider
     *
     * @return bool
     */
    public function hasProvider($provider)
    {
        foreach ($this->providers as $p) {
            if ($p->provider == $provider) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(config('access.users.superadmin_role'));
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->hasRole(config('access.users.admin_role'));
    }

    /**
     * @return mixed
     */
    public function isUser()
    {
        return $this->hasRole(config('access.users.default_role'));
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @return bool
     */
    public function isPending()
    {
        return config('access.users.requires_approval') && ! $this->confirmed;
    }
}
