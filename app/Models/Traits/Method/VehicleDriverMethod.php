<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait VehicleDriverMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait VehicleDriverMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
