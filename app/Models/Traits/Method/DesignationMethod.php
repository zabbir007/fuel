<?php

namespace App\Models\Traits\Method;

use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Trait DesignationMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait DesignationMethod
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
}
