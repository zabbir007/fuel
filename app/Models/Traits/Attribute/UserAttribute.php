<?php

namespace App\Models\Traits\Attribute;

use Illuminate\Support\Facades\Hash;
use function count;
use function strlen;

/**
 * Trait UserAttribute.
 */
trait UserAttribute
{
    /**
     * @param $password
     */
    public function setPasswordAttribute($password): void
    {
        // If password was accidentally passed in already hashed, try not to double hash it
        if (
            (strlen($password) === 60 && preg_match('/^\$2y\$/', $password)) ||
            (strlen($password) === 95 && preg_match('/^\$argon2i\$/', $password))
        ) {
            $hash = $password;
        } else {
            $hash = Hash::make($password);
        }

        // Note: Password Histories are logged from the \App\Observer\User\UserObserver class
        $this->attributes['password'] = $hash;
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name
            ? $this->first_name.' '.$this->last_name
            : $this->first_name;
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->full_name;
    }

    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        return $this->getPicture();
    }

    /**
     * @return string
     */
    public function getRolesLabelAttribute()
    {
        $roles = $this->getRoleNames()->toArray();

        if (count($roles)) {
            return implode(', ', array_map(function ($item) {
                return ucwords($item);
            }, $roles));
        }

        return 'N/A';
    }

    /**
     * @return string
     */
    public function getPermissionsLabelAttribute()
    {
        $permissions = $this->getDirectPermissions()->toArray();

        if (count($permissions)) {
            return implode(', ', array_map(function ($item) {
                return ucwords($item['name']);
            }, $permissions));
        }

        return 'N/A';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-show')) {
            return '<a href="'.route($this->module_route.'.show',
                    $this).'" data-toggle="tooltip" data-placement="top" title="View '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"><i class="fas fa-eye"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-edit')) {
            return '<a href="'.route($this->module_route.'.edit',
                    $this).'" data-toggle="tooltip" data-placement="top" title="Update '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-edit"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getEditBranchButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-edit')) {
            return '<a href="'.route('master.user_branch.edit',
                    $this).'" data-toggle="tooltip" data-placement="top" title="Update Branch" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-edit"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getChangePasswordButtonAttribute(): string
    {
        return '<a href="'.route($this->module_route.'.change-password',
                $this).'" data-toggle="tooltip" data-placement="top" title="Change User Password" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-warning"><i class="fas fa-lock"></i></a>';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute(): string
    {
        if ($this->id != auth()->id()) {
            switch ($this->active) {
                case 0:
                    if (request()->user()->can($this->module_permission.'-mark')) {
                        return '<a href="'.route($this->module_route.'.mark', [
                                $this, 1
                            ]).'" data-toggle="tooltip" data-placement="top" title="Set to Active" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-check"></i></a>';
                    }

                    return '';
                    break;

                case 1:
                    if (request()->user()->can($this->module_permission.'-mark')) {
                        return '<a href="'.route($this->module_route.'.mark', [
                                $this, 0
                            ]).'" data-toggle="tooltip" data-placement="top" title="Set to Inactive" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-ban"></i></a>';
                    }
                    return '';
                    break;

                default:
                    return '';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-delete')) {
            if ($this->id != auth()->id() && $this->id != 1) {
                return '<a href="'.route($this->module_route.'.destroy', $this).'"
                 data-method="delete"
                 data-toggle="tooltip"
                 title="Trash '.$this->module_name.'"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.general.are_you_sure').'"
                 class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger"><i class="fas fa-trash"></i></a> ';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-restore')) {
            return '<a href="'.route($this->module_route.'.restore',
                    $this).'" name="confirm_item" data-toggle="tooltip" data-placement="top" title="Restore '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"><i class="fas fa-sync"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-delete-permanently')) {
            return '<a href="'.route($this->module_route.'.delete-permanently',
                    $this).'" name="confirm_item" data-toggle="tooltip" data-placement="top" title="Permanently Delete '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger"><i class="fas fa-trash"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getLoginAsButtonAttribute(): string
    {
        if (request()->user()->isSuperAdmin()){
            // If the admin is currently NOT spoofing a user
            if (!session()->has('admin_user_id') || !session()->has('temp_user_id')) {
                //Won't break, but don't let them "Login As" themselves
                if ($this->id != auth()->id()) {
                    return '<a href="'.route('impersonate',
                            $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.access.users.login_as',
                            ['user' => e($this->full_name)]).'" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"><i class="fas fa-user-secret"></i></a>';
                }
            }

        }
        return '';
    }

    /**
     * @return string
     */
    public function getClearSessionButtonAttribute(): string
    {
        if (request()->user()->isSuperAdmin()){
            if ($this->id != auth()->id() && config('session.driver') === 'database') {
                return '<a href="'.route($this->module_route.'.clear-session', $this).'"
			 	 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.continue').'"
                 data-trans-title="'.__('strings.general.are_you_sure').'"
                 class="dropdown-item" name="confirm_item">'.__('buttons.access.users.clear_session').'</a> ';
            }
        }

        return '';
    }


    /**
     * @return string
     */
    public function getActionsAttribute(): string
    {
        if ($this->trashed()) {
            return $this->restore_button.$this->delete_permanently_button;
        }

        $actions = $this->show_button.
            $this->edit_button.
            $this->status_button;

        if ($this->canBeImpersonated()) {
            $actions .= $this->login_as_button;
        }

        if ($this->id !== auth()->id()) {
            $actions .= $this->clear_session_button;
        }

        //  if($this->isSuperAdmin() || $this->id == \Auth::user()->id){
        $actions .= $this->change_password_button;
        //}

        $actions .= $this->delete_button;


        return $actions;
    }

    /**
     * @return string
     */
    public function getBranchActionsAttribute(): string
    {
        $actions = $this->edit_branch_button;


        return $actions;
    }
}
