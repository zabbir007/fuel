<?php

namespace App\Models\Traits\Attribute;

/**
 * Trait FuelRateAttribute
 *
 * @package App\Models\Master\Traits\Attribute
 */
trait FuelRateAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-edit')) {
            return '<a href="'.route($this->module_route.'.edit',
                    ['branch_id' => $this->branch_id, 'date'=> $this->entry_date->toDateString()]).'" data-toggle="tooltip" data-placement="top" title="Update '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-edit"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionsAttribute(): string
    {
//        if ($this->trashed()) {
//            return $this->restore_button . $this->delete_permanently_button;
//        }

        return $this->edit_button;
    }
}
