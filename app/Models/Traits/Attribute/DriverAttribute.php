<?php

namespace App\Models\Traits\Attribute;

/**
 * Trait DriverAttribute
 *
 * @package App\Models\Master\Traits\Attribute
 */
trait DriverAttribute
{

    /**
     * @return mixed|string|null
     */
    public function getFirstNameAttribute(){
        $splits = explode(' ',$this->name);
        return count($splits) > 0 ? $splits[0] : null;
    }

    /**
     * @return mixed|string|null
     */
    public function getLastNameAttribute(){
        $splits = explode(' ',$this->name);
        return count($splits) > 0 ? $splits[1] : null;
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-show')) {
            return '<a href="'.route($this->module_route.'.show',
                    $this).'" data-toggle="tooltip" data-placement="top" title="View '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"><i class="fas fa-eye"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-edit')) {
            return '<a href="'.route($this->module_route.'.edit',
                    $this).'" data-toggle="tooltip" data-placement="top" title="Update '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-edit"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute(): string
    {
        switch ($this->active) {
            case 0:
                if (request()->user()->can($this->module_permission.'-mark')) {
                    return '<a href="'.route($this->module_route.'.mark', [
                            $this, 1
                        ]).'" data-toggle="tooltip" data-placement="top" title="Set to Active" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-check"></i></a>';
                }

                return '';
                break;

            case 1:
                if (request()->user()->can($this->module_permission.'-mark')) {
                    return '<a href="'.route($this->module_route.'.mark', [
                            $this, 0
                        ]).'" data-toggle="tooltip" data-placement="top" title="Set to Inactive" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-ban"></i></a>';
                }
                return '';
                break;

            default:
                return '';
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-delete')) {
                return '<a href="'.route($this->module_route.'.destroy', $this).'"
                 data-method="delete"
                 data-toggle="tooltip"
                 title="Trash '.$this->module_name.'"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.general.are_you_sure').'"
                 class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-restore')) {
            return '<a href="'.route($this->module_route.'.restore',
                    $this).'" name="confirm_item" data-toggle="tooltip" data-placement="top" title="Restore '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"><i class="fas fa-sync"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-delete-permanently')) {
            return '<a href="'.route($this->module_route.'.delete-permanently',
                    $this).'" name="confirm_item" data-toggle="tooltip" data-placement="top" title="Permanently Delete '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger"><i class="fas fa-trash"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionsAttribute(): string
    {
//        if ($this->trashed()) {
//            return $this->restore_button . $this->delete_permanently_button;
//        }

        return $this->show_button.
            $this->edit_button.
            $this->status_button.
            $this->delete_button;
    }
}
