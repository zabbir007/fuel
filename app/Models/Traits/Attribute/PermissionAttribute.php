<?php

namespace App\Models\Traits\Attribute;

/**
 * Trait PermissionAttribute
 *
 * @package App\Models\Traits\Attribute
 */
trait PermissionAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-show')) {
            return '<a href="'.route($this->module_route.'.show',
                    $this).'" data-toggle="tooltip" data-placement="top" title="View '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"><i class="fas fa-eye"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute(): string
    {
        if ($this->id != 1){
            if (request()->user()->can($this->module_permission.'-edit')) {
                return '<a href="'.route($this->module_route.'.edit',
                        $this).'" data-toggle="tooltip" data-placement="top" title="Update '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-edit"></i></a>';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionsAttribute(): string
    {
        $actions = $this->edit_button;

        return $actions;
    }
}
