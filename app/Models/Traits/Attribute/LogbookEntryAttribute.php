<?php

namespace App\Models\Traits\Attribute;

/**
 * Trait LogbookEntryAttribute
 *
 * @package App\Models\Master\Traits\Attribute
 */
trait LogbookEntryAttribute
{
    /**
     * @return string
     */
    public function getApproveLabelAttribute()
    {
        if ($this->isApproved()) {
            return '<span class="badge badge-success">Yes</span>';
        }

        return '<span class="badge badge-danger">No</span>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isPosted()) {
            return "<span class='badge badge-success'>Posted</span>";
        }
        if ($this->isApproved()) {
            return "<span class='badge badge-success'>Approved</span>";
        }

        return "<span class='badge badge-danger'>Draft</span>";
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-edit')) {
            return '<a href="'.route($this->module_route.'.edit', $this).'" data-toggle="tooltip" data-placement="top" title="Update '.$this->module_name.'" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"><i class="fas fa-edit"></i></a>';
        }

        return '';
    }


    /**
     * @return string
     */
    public function getApproveButtonAttribute(): string
    {
        if (request()->user()->can($this->module_permission.'-approve')) {
            return '<a href="'.route($this->module_route.'.approve', $this).'" data-toggle="tooltip" data-placement="top" title="Approve" class="btn btn-xs btn-flat btn-square mr-1 btn-success bg-gradient-green"><i class="fas fa-check"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionsAttribute(): string
    {
//        if ($this->trashed()) {
//            return $this->restore_button . $this->delete_permanently_button;
//        }

        return $this->edit_button. $this->approve_button;
    }
}
