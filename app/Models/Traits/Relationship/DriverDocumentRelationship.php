<?php

namespace App\Models\Traits\Relationship;

use App\Models\Driver;
use App\Models\User;

/**
 * Trait DriverDocumentRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait DriverDocumentRelationship
{
    /**
     * @return mixed
     */
    public function driver(){
        return $this->belongsTo(Driver::class);
    }
}
