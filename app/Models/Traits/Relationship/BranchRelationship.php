<?php

namespace App\Models\Traits\Relationship;

use App\Models\Driver;
use App\Models\User;
use App\Models\Vehicle;

/**
 * Trait BranchRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait BranchRelationship
{
    /**
     * @return mixed
     */
    public function users(){
        return $this->belongsToMany(User::class, 'user_branches');
    }

    /**
     * @return mixed
     */
    public function drivers(){
        return $this->hasMany(Driver::class, 'driver_id');
    }

    /**
     * @return mixed
     */
    public function vehicles(){
        return $this->hasMany(Vehicle::class, 'vehicle_id');
    }
}
