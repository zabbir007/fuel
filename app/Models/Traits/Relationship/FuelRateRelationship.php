<?php

namespace App\Models\Traits\Relationship;



use App\Models\Branch;
use App\Models\Company;
use App\Models\Fuel;

/**
 * Trait FuelRateRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait FuelRateRelationship
{
    /**
     * @return mixed
     */
    public function company(){
        return $this->belongsTo(Company::class);
    }

    /**
     * @return mixed
     */
    public function Branch(){
        return $this->belongsTo(Branch::class);
    }

    /**
     * @return mixed
     */
    public function fuel(){
        return $this->belongsTo(Fuel::class);
    }
}
