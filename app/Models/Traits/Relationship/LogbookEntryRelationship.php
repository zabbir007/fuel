<?php

namespace App\Models\Traits\Relationship;



use App\Models\Branch;
use App\Models\Company;
use App\Models\Driver;
use App\Models\Employee;
use App\Models\Fuel;
use App\Models\Vehicle;

/**
 * Trait LogbookEntryRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait LogbookEntryRelationship
{
    /**
     * @return mixed
     */
    public function company(){
        return $this->belongsTo(Company::class);
    }

    /**
     * @return mixed
     */
    public function branch(){
        return $this->belongsTo(Branch::class);
    }

    /**
     * @return mixed
     */
    public function driver(){
        return $this->belongsTo(Driver::class);
    }

    /**
     * @return mixed
     */
    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    /**
     * @return mixed
     */
    public function fuel(){
        return $this->belongsTo(Fuel::class);
    }

    /**
     * @return mixed
     */
    public function vehicle(){
        return $this->belongsTo(Vehicle::class);
    }
}
