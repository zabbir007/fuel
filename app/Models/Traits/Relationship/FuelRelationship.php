<?php

namespace App\Models\Traits\Relationship;

use App\Models\Fuel;
use App\Models\FuelRate;
use App\Models\User;

/**
 * Trait FuelRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait FuelRelationship
{
    /**
     * @return mixed
     */
    public function rates(){
        return $this->hasMany(FuelRate::class)->where('archived', 0)->latest();
    }

    /**
     * @return mixed
     */
    public function branch(){
        return $this->hasMany(FuelRate::class)->where('archived', 0)->latest();
    }

    /**
     * @return mixed
     */
    public function fuel(){
        return $this->hasMany(Fuel::class)->where('archived', 0)->latest();
    }
}
