<?php

namespace App\Models\Traits\Relationship;

use App\Models\Designation;
use App\Models\User;

/**
 * Trait EmployeeRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait EmployeeRelationship
{
    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    /**
     * @return mixed
     */
    public function designation(){
        return $this->belongsTo(Designation::class);
    }
}
