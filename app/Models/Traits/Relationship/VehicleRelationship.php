<?php

namespace App\Models\Traits\Relationship;

use App\Models\Branch;
use App\Models\Driver;
use App\Models\Fuel;
use App\Models\VehicleDocument;
use App\Models\VehicleDriver;
use App\Models\VehicleType;

/**
 * Trait VehicleRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait VehicleRelationship
{
    /**
     * @return mixed
     */
    public function type()
    {
        return $this->belongsTo(VehicleType::class, 'vehicle_type');
    }

    /**
     * @return mixed
     */
    public function main_fuel()
    {
        return $this->belongsTo(Fuel::class, 'main_tank_fuel_id');
    }

    /**
     * @return mixed
     */
    public function secondary_fuel()
    {
        return $this->belongsTo(Fuel::class, 'second_tank_fuel_id');
    }

    /**
     * @return mixed
     */
    public function documents()
    {
        return $this->hasMany(VehicleDocument::class, 'vehicle_id');
    }

    /**
     * @return mixed
     */
    public function drivers()
    {
        return $this->belongsToMany(Driver::class, 'vehicle_drivers')->where('archived', 0)->latest();
    }

    /**
     * @return mixed
     */
    public function histories()
    {
        return $this->hasMany(VehicleDriver::class)->where('archived', 0)->latest();
    }

    /**
     * @return mixed
     */
    public function branches()
    {
//         return $this->hasOneThrough(Branch::class, VehicleDriver::class,'driver_id','id','id','branch_id')->where('archived', 0)->latest();
//        return $this->belongsToMany(Branch::class,'vehicle_drivers')->where('vehicle_drivers.archived', 0)->latest();
        return $this->belongsToMany(Branch::class, 'vehicle_drivers')->where('vehicle_drivers.archived', 0)->latest();
//        return $this->hasManyThrough(Branch::class, VehicleDriver::class,'vehicle_id','id','id','driver_id');
    }
}
