<?php

namespace App\Models\Traits\Relationship;

use App\Models\Branch;
use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\VehicleDriver;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait VehicleDriverRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait VehicleDriverRelationship
{

    /**
     * @return BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    /**
     * @return BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    /**
     * @return BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
