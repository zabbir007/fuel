<?php

namespace App\Models\Traits\Relationship;

use App\Models\Driver;
use App\Models\User;

/**
 * Trait DocumentTypeRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait DocumentTypeRelationship
{
    /**
     * @return mixed
     */
    public function driver(){
        return $this->belongsTo(Driver::class);
    }
}
