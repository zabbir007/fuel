<?php

namespace App\Models\Traits\Relationship;

use App\Models\Branch;
use App\Models\DriverDocument;
use App\Models\User;
use App\Models\VehicleDriver;

/**
 * Trait DriverRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait DriverRelationship
{
    /**
     * @return mixed
     */
    public function documents(){
        return $this->hasMany(DriverDocument::class, 'driver_id');
    }

    /**
     * @return mixed
     */
    public function branch(){
       // return $this->hasOneThrough(Branch::class, VehicleDriver::class,'driver_id','id','driver_id','branch_id')->where('archived', 0)->latest();
//        return $this->hasOne(VehicleDriver::class)->where('archived', 0)->latest();
        return $this->belongsToMany(Branch::class,'vehicle_drivers')->where('archived', 0)->latest();
    }

    /**
     * @return mixed
     */
    public function vehicle(){
        return $this->hasOne(VehicleDriver::class)->where('archived', 0)->latest();
    }
}
