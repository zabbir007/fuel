<?php

namespace App\Models\Traits\Relationship;

use App\Models\Fuel;
use App\Models\Vehicle;
/**
 * Trait VehicleHistoryRelationship
 *
 * @package App\Models\Master\Traits\Relationship
 */
trait VehicleHistoryRelationship
{
    /**
     * @return mixed
     */
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }
}
