<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\DriverDocumentAttribute;
use App\Models\Traits\Method\DriverDocumentMethod;
use App\Models\Traits\Relationship\DriverDocumentRelationship;
use App\Models\Traits\Scope\DriverDocumentScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class DriverDocument
 *
 * @package App\Models\Master
 */
class DriverDocument extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        DriverDocumentAttribute,
        DriverDocumentMethod,
        DriverDocumentRelationship,
        DriverDocumentScope;

    private $module_name = 'DriverDocument';
    private $module_route = 'master.driver_document';
    private $module_permission = 'setup-driver_document';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
