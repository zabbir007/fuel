<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use App\Models\Traits\Attribute\RoleAttribute;
use App\Models\Traits\Method\RoleMethod;
use App\Models\Traits\Scope\RoleScope;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Models\Role as SpatieRole;

/**
 * Class Role.
 */
class Role extends SpatieRole implements Recordable
{
    use RoleAttribute,
        RecordableTrait,
        LogsActivity,
        RoleMethod,
        RoleScope;

    private $module_name = 'Role';
    private $module_route = 'role';
    private $module_permission = 'setup-role';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
}
