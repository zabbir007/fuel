<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\VehicleAttribute;
use App\Models\Traits\Method\VehicleMethod;
use App\Models\Traits\Relationship\VehicleRelationship;
use App\Models\Traits\Scope\VehicleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Vehicle
 *
 * @package App\Models\Master
 */
class Vehicle extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        VehicleAttribute,
        VehicleMethod,
        VehicleRelationship,
        VehicleScope;

    private $module_name = 'Vehicle';
    private $module_route = 'master.vehicle';
    private $module_permission = 'setup-vehicle';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    protected $appends = ['format_name','purchase_year'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_dual_tank' => 'boolean',
        'active' => 'boolean',
    ];
}
