<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\VehicleTypeAttribute;
use App\Models\Traits\Method\VehicleTypeMethod;
use App\Models\Traits\Relationship\VehicleTypeRelationship;
use App\Models\Traits\Scope\VehicleTypeScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class VehicleType
 *
 * @package App\Models\Master
 */
class VehicleType extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        VehicleTypeAttribute,
        VehicleTypeMethod,
        VehicleTypeRelationship,
        VehicleTypeScope;

    private $module_name = 'VehicleType';
    private $module_route = 'master.vehicle_type';
    private $module_permission = 'setup-vehicle_type';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
