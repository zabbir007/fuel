<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\VehicleAttribute;
use App\Models\Traits\Attribute\VehicleDriverAttribute;
use App\Models\Traits\Method\VehicleDriverMethod;
use App\Models\Traits\Method\VehicleMethod;
use App\Models\Traits\Relationship\VehicleDriverRelationship;
use App\Models\Traits\Relationship\VehicleRelationship;
use App\Models\Traits\Scope\VehicleDriverScope;
use App\Models\Traits\Scope\VehicleScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;


/**
 * Class VehicleDriver
 *
 * @package App\Models
 */
class VehicleDriver extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        VehicleDriverAttribute,
        VehicleDriverMethod,
        VehicleDriverRelationship,
        VehicleDriverScope;

    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
    /**
     * @var string
     */
    protected $table = 'vehicle_drivers';

    //for ActivityLog
    protected $guarded = ['id'];
    private $module_name = 'VehicleDriver';
    private $module_route = 'master.vehicle_driver';
    private $module_permission = 'setup-vehicle-driver';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'archived' => 'boolean',
        'active' => 'boolean',
    ];
}
