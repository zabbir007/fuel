<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\CompanyAttribute;
use App\Models\Traits\Method\CompanyMethod;
use App\Models\Traits\Relationship\CompanyRelationship;
use App\Models\Traits\Scope\CompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Company
 *
 * @package App\Models\Master
 */
class Company extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        CompanyAttribute,
        CompanyMethod,
        CompanyRelationship,
        CompanyScope;

    private $module_name = 'Company';
    private $module_route = 'master.company';
    private $module_permission = 'setup-company';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'short_name',
        'address',
        'logo',
        'email_suffix',
        'time_zone',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
