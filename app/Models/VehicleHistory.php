<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\VehicleHistoryAttribute;
use App\Models\Traits\Method\VehicleHistoryMethod;
use App\Models\Traits\Relationship\VehicleHistoryRelationship;
use App\Models\Traits\Scope\VehicleHistoryScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class VehicleHistory
 *
 * @package App\Models\Master
 */
class VehicleHistory extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        VehicleHistoryAttribute,
        VehicleHistoryMethod,
        VehicleHistoryRelationship,
        VehicleHistoryScope;

    private $module_name = 'VehicleHistory';
    private $module_route = 'master.vehicle_history';
    private $module_permission = 'setup-vehicle-history';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    protected $table = 'vehicle_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
