<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\FuelRateAttribute;
use App\Models\Traits\Method\FuelRateMethod;
use App\Models\Traits\Relationship\FuelRateRelationship;
use App\Models\Traits\Scope\FuelRateScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class FuelRate
 *
 * @package App\Models\Master
 */
class FuelRate extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        FuelRateAttribute,
        FuelRateMethod,
        FuelRateRelationship,
        FuelRateScope;

    private $module_name = 'FuelRate';
    private $module_route = 'transactions.fuel_rate';
    private $module_permission = 'transactions-fuel-rate';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['entry_date','deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'created_at' => 'datetime:Y-m-d',
    ];
}
