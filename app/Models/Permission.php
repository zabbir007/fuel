<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use App\Models\Traits\Attribute\PermissionAttribute;
use App\Models\Traits\Method\PermissionMethod;
use App\Models\Traits\Scope\PermissionScope;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Models\Permission as SpatiePermission;

/**
 * Class Permission.
 */
class Permission extends SpatiePermission implements Recordable
{
    use PermissionAttribute,
        RecordableTrait,
        LogsActivity,
        PermissionMethod,
        PermissionScope;

    private $module_name = 'Permission';
    private $module_route = 'permission';
    private $module_permission = 'setup-permission';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;
}
