<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\LogbookEntryAttribute;
use App\Models\Traits\Method\LogbookEntryMethod;
use App\Models\Traits\Relationship\LogbookEntryRelationship;
use App\Models\Traits\Scope\LogbookEntryScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class LogbookEntry
 *
 * @package App\Models\Master
 */
class LogbookEntry extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        LogbookEntryAttribute,
        LogbookEntryMethod,
        LogbookEntryRelationship,
        LogbookEntryScope;

    private $module_name = 'LogbookEntry';
    private $module_route = 'transactions.logbook_entry';
    private $module_permission = 'transactions-logbook-entry';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['entry_date','deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'posted' => 'boolean',
        'created_at' => 'datetime:Y-m-d',
    ];
}
