<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\VehicleDocumentAttribute;
use App\Models\Traits\Method\VehicleDocumentMethod;
use App\Models\Traits\Relationship\VehicleDocumentRelationship;
use App\Models\Traits\Scope\VehicleDocumentScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class VehicleDocument
 *
 * @package App\Models\Master
 */
class VehicleDocument extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        VehicleDocumentAttribute,
        VehicleDocumentMethod,
        VehicleDocumentRelationship,
        VehicleDocumentScope;

    private $module_name = 'VehicleDocument';
    private $module_route = 'master.vehicle_document';
    private $module_permission = 'setup-vehicle_document';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
