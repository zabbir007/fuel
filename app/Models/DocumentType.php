<?php

namespace App\Models;

use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Recordable as RecordableTrait;
use Altek\Eventually\Eventually;
use App\Models\Traits\Attribute\DocumentTypeAttribute;
use App\Models\Traits\Method\DocumentTypeMethod;
use App\Models\Traits\Relationship\DocumentTypeRelationship;
use App\Models\Traits\Scope\DocumentTypeScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class DocumentType
 *
 * @package App\Models\Master
 */
class DocumentType extends Model implements Recordable
{
    use Notifiable,
        Eventually,
        RecordableTrait,
        LogsActivity,
        SoftDeletes,
        DocumentTypeAttribute,
        DocumentTypeMethod,
        DocumentTypeRelationship,
        DocumentTypeScope;

    private $module_name = 'DocumentType';
    private $module_route = 'master.document_type';
    private $module_permission = 'setup-document_type';

    //for ActivityLog
    protected static $logName = 'system';
    protected static $logOnlyDirty = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
}
