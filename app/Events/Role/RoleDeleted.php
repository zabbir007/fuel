<?php

namespace App\Events\Role;

use Illuminate\Queue\SerializesModels;

/**
 * Class PermissionDeleted.
 */
class RoleDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $role;

    /**
     * @param $role
     */
    public function __construct($role)
    {
        $this->role = $role;
    }
}
