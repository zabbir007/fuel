<?php

namespace App\Events\FuelRate;

use Illuminate\Queue\SerializesModels;

/**
 * Class FuelRateRestored.
 */
class FuelRateRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
