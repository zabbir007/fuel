<?php

namespace App\Events\FuelRate;

use Illuminate\Queue\SerializesModels;

/**
 * Class FuelRateDeleted.
 */
class FuelRateDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
