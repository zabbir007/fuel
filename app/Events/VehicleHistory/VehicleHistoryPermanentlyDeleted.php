<?php

namespace App\Events\VehicleHistory;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleHistoryPermanentlyDeleted.
 */
class VehicleHistoryPermanentlyDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
