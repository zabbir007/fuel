<?php

namespace App\Events\VehicleHistory;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleHistoryCreated.
 */
class VehicleHistoryCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
