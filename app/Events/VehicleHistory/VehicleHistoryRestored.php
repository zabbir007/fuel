<?php

namespace App\Events\VehicleHistory;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleHistoryRestored.
 */
class VehicleHistoryRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
