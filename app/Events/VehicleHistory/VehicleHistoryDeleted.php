<?php

namespace App\Events\VehicleHistory;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleHistoryDeleted.
 */
class VehicleHistoryDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
