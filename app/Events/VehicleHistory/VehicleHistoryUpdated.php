<?php

namespace App\Events\VehicleHistory;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleHistoryUpdated.
 */
class VehicleHistoryUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
