<?php

namespace App\Events\Vehicle;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleCreated.
 */
class VehicleCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
