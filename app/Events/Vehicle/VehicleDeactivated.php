<?php

namespace App\Events\Vehicle;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleDeactivated.
 */
class VehicleDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $vehicle;

    /**
     * @param $vehicle
     */
    public function __construct($vehicle)
    {
        $this->vehicle = $vehicle;
    }
}
