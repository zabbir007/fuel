<?php

namespace App\Events\Permission;

use Illuminate\Queue\SerializesModels;


/**
 * Class PermissionReactivated
 *
 * @package App\Events\Permission
 */
class PermissionReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $module;

    /**
     * @param $module
     */
    public function __construct($module)
    {
        $this->module = $module;
    }
}
