<?php

namespace App\Events\Branch;

use Illuminate\Queue\SerializesModels;

/**
 * Class BranchDeactivated.
 */
class BranchDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $branch;

    /**
     * @param $branch
     */
    public function __construct($branch)
    {
        $this->branch = $branch;
    }
}
