<?php

namespace App\Events\Branch;

use Illuminate\Queue\SerializesModels;

/**
 * Class BranchCreated.
 */
class BranchCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $branch;

    /**
     * @param $branch
     */
    public function __construct($branch)
    {
        $this->branch = $branch;
    }
}
