<?php

namespace App\Events\Driver;

use Illuminate\Queue\SerializesModels;

/**
 * Class DriverCreated.
 */
class DriverCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
