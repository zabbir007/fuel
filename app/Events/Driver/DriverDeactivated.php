<?php

namespace App\Events\Driver;

use Illuminate\Queue\SerializesModels;

/**
 * Class DriverDeactivated.
 */
class DriverDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
