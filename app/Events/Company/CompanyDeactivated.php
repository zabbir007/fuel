<?php

namespace App\Events\Company;

use Illuminate\Queue\SerializesModels;

/**
 * Class CompanyDeactivated.
 */
class CompanyDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $company;

    /**
     * @param $company
     */
    public function __construct($company)
    {
        $this->company = $company;
    }
}
