<?php

namespace App\Events\LogbookEntry;

use Illuminate\Queue\SerializesModels;

/**
 * Class LogbookEntryUpdated.
 */
class LogbookEntryUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
