<?php

namespace App\Events\DriverDocument;

use Illuminate\Queue\SerializesModels;

/**
 * Class DriverDocumentReactivated.
 */
class DriverDocumentReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
