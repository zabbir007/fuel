<?php

namespace App\Events\DriverDocument;

use Illuminate\Queue\SerializesModels;

/**
 * Class DriverDocumentDeactivated.
 */
class DriverDocumentDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
