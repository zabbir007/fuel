<?php

namespace App\Events\Employee;

use Illuminate\Queue\SerializesModels;

/**
 * Class EmployeeRestored.
 */
class EmployeeRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $employee;

    /**
     * @param $employee
     */
    public function __construct($employee)
    {
        $this->employee = $employee;
    }
}
