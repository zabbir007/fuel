<?php

namespace App\Events\Employee;

use Illuminate\Queue\SerializesModels;

/**
 * Class EmployeeUpdated.
 */
class EmployeeUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $employee;

    /**
     * @param $employee
     */
    public function __construct($employee)
    {
        $this->employee = $employee;
    }
}
