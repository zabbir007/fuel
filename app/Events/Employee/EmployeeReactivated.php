<?php

namespace App\Events\Employee;

use Illuminate\Queue\SerializesModels;

/**
 * Class EmployeeReactivated.
 */
class EmployeeReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $employee;

    /**
     * @param $employee
     */
    public function __construct($employee)
    {
        $this->employee = $employee;
    }
}
