<?php

namespace App\Events\Employee;

use Illuminate\Queue\SerializesModels;

/**
 * Class EmployeeDeactivated.
 */
class EmployeeDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $employee;

    /**
     * @param $employee
     */
    public function __construct($employee)
    {
        $this->employee = $employee;
    }
}
