<?php

namespace App\Events\VehicleDocument;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleDocumentRestored.
 */
class VehicleDocumentRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
