<?php

namespace App\Events\VehicleDriver;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleDriverRestored.
 */
class VehicleDriverRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
