<?php

namespace App\Events\VehicleDriver;

use Illuminate\Queue\SerializesModels;

/**
 * Class VehicleDriverPermanentlyDeleted.
 */
class VehicleDriverPermanentlyDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver;
    }
}
