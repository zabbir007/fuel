<?php

namespace App\Listeners;

use App\Events\Employee\EmployeeCreated;
use App\Events\Employee\EmployeeDeleted;
use App\Events\Employee\EmployeeUpdated;
use App\Events\Employee\EmployeeRestored;
use App\Events\Employee\EmployeeDeactivated;
use App\Events\Employee\EmployeeReactivated;
use App\Events\Employee\EmployeePermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class EmployeeEventListener
 *
 * @package App\Listeners
 */
class EmployeeEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Employee Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Employee Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Employee Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Employee Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Employee Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Employee Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Employee Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            EmployeeCreated::class,
            'App\Listeners\EmployeeEventListener@onCreated'
        );

        $events->listen(
            EmployeeUpdated::class,
            'App\Listeners\EmployeeEventListener@onUpdated'
        );

        $events->listen(
            EmployeeDeleted::class,
            'App\Listeners\EmployeeEventListener@onDeleted'
        );

        $events->listen(
            EmployeeDeactivated::class,
            'App\Listeners\EmployeeEventListener@onDeactivated'
        );

        $events->listen(
            EmployeeReactivated::class,
            'App\Listeners\EmployeeEventListener@onReactivated'
        );

        $events->listen(
            EmployeePermanentlyDeleted::class,
            'App\Listeners\EmployeeEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            EmployeeRestored::class,
            'App\Listeners\EmployeeEventListener@onRestored'
        );
    }
}
