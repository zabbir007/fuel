<?php

namespace App\Listeners;

use App\Events\FuelRate\FuelRateCreated;
use App\Events\FuelRate\FuelRateDeleted;
use App\Events\FuelRate\FuelRateUpdated;
use App\Events\FuelRate\FuelRateRestored;
use App\Events\FuelRate\FuelRateDeactivated;
use App\Events\FuelRate\FuelRateReactivated;
use App\Events\FuelRate\FuelRatePermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class FuelRateEventListener
 *
 * @package App\Listeners
 */
class FuelRateEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('FuelRate Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('FuelRate Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('FuelRate Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('FuelRate Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('FuelRate Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('FuelRate Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('FuelRate Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            FuelRateCreated::class,
            'App\Listeners\FuelRateEventListener@onCreated'
        );

        $events->listen(
            FuelRateUpdated::class,
            'App\Listeners\FuelRateEventListener@onUpdated'
        );

        $events->listen(
            FuelRateDeleted::class,
            'App\Listeners\FuelRateEventListener@onDeleted'
        );

        $events->listen(
            FuelRateDeactivated::class,
            'App\Listeners\FuelRateEventListener@onDeactivated'
        );

        $events->listen(
            FuelRateReactivated::class,
            'App\Listeners\FuelRateEventListener@onReactivated'
        );

        $events->listen(
            FuelRatePermanentlyDeleted::class,
            'App\Listeners\FuelRateEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            FuelRateRestored::class,
            'App\Listeners\FuelRateEventListener@onRestored'
        );
    }
}
