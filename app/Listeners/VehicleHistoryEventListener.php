<?php

namespace App\Listeners;

use App\Events\VehicleHistory\VehicleHistoryCreated;
use App\Events\VehicleHistory\VehicleHistoryDeleted;
use App\Events\VehicleHistory\VehicleHistoryUpdated;
use App\Events\VehicleHistory\VehicleHistoryRestored;
use App\Events\VehicleHistory\VehicleHistoryDeactivated;
use App\Events\VehicleHistory\VehicleHistoryReactivated;
use App\Events\VehicleHistory\VehicleHistoryPermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class VehicleHistoryEventListener
 *
 * @package App\Listeners
 */
class VehicleHistoryEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('VehicleHistory Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('VehicleHistory Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('VehicleHistory Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('VehicleHistory Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('VehicleHistory Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('VehicleHistory Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('VehicleHistory Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            VehicleHistoryCreated::class,
            'App\Listeners\VehicleHistoryEventListener@onCreated'
        );

        $events->listen(
            VehicleHistoryUpdated::class,
            'App\Listeners\VehicleHistoryEventListener@onUpdated'
        );

        $events->listen(
            VehicleHistoryDeleted::class,
            'App\Listeners\VehicleHistoryEventListener@onDeleted'
        );

        $events->listen(
            VehicleHistoryDeactivated::class,
            'App\Listeners\VehicleHistoryEventListener@onDeactivated'
        );

        $events->listen(
            VehicleHistoryReactivated::class,
            'App\Listeners\VehicleHistoryEventListener@onReactivated'
        );

        $events->listen(
            VehicleHistoryPermanentlyDeleted::class,
            'App\Listeners\VehicleHistoryEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            VehicleHistoryRestored::class,
            'App\Listeners\VehicleHistoryEventListener@onRestored'
        );
    }
}
