<?php

namespace App\Listeners;

use App\Events\Driver\DriverCreated;
use App\Events\Driver\DriverDeleted;
use App\Events\Driver\DriverUpdated;
use App\Events\Driver\DriverRestored;
use App\Events\Driver\DriverDeactivated;
use App\Events\Driver\DriverReactivated;
use App\Events\Driver\DriverPermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class DriverEventListener
 *
 * @package App\Listeners
 */
class DriverEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Driver Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Driver Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Driver Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Driver Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Driver Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Driver Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Driver Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            DriverCreated::class,
            'App\Listeners\DriverEventListener@onCreated'
        );

        $events->listen(
            DriverUpdated::class,
            'App\Listeners\DriverEventListener@onUpdated'
        );

        $events->listen(
            DriverDeleted::class,
            'App\Listeners\DriverEventListener@onDeleted'
        );

        $events->listen(
            DriverDeactivated::class,
            'App\Listeners\DriverEventListener@onDeactivated'
        );

        $events->listen(
            DriverReactivated::class,
            'App\Listeners\DriverEventListener@onReactivated'
        );

        $events->listen(
            DriverPermanentlyDeleted::class,
            'App\Listeners\DriverEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            DriverRestored::class,
            'App\Listeners\DriverEventListener@onRestored'
        );
    }
}
