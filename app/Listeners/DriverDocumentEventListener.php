<?php

namespace App\Listeners;

use App\Events\DriverDocument\DriverDocumentCreated;
use App\Events\DriverDocument\DriverDocumentDeleted;
use App\Events\DriverDocument\DriverDocumentUpdated;
use App\Events\DriverDocument\DriverDocumentRestored;
use App\Events\DriverDocument\DriverDocumentDeactivated;
use App\Events\DriverDocument\DriverDocumentReactivated;
use App\Events\DriverDocument\DriverDocumentPermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class DriverDocumentEventListener
 *
 * @package App\Listeners
 */
class DriverDocumentEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('DriverDocument Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('DriverDocument Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('DriverDocument Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('DriverDocument Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('DriverDocument Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('DriverDocument Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('DriverDocument Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            DriverDocumentCreated::class,
            'App\Listeners\DriverDocumentEventListener@onCreated'
        );

        $events->listen(
            DriverDocumentUpdated::class,
            'App\Listeners\DriverDocumentEventListener@onUpdated'
        );

        $events->listen(
            DriverDocumentDeleted::class,
            'App\Listeners\DriverDocumentEventListener@onDeleted'
        );

        $events->listen(
            DriverDocumentDeactivated::class,
            'App\Listeners\DriverDocumentEventListener@onDeactivated'
        );

        $events->listen(
            DriverDocumentReactivated::class,
            'App\Listeners\DriverDocumentEventListener@onReactivated'
        );

        $events->listen(
            DriverDocumentPermanentlyDeleted::class,
            'App\Listeners\DriverDocumentEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            DriverDocumentRestored::class,
            'App\Listeners\DriverDocumentEventListener@onRestored'
        );
    }
}
