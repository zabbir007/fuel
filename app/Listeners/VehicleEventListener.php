<?php

namespace App\Listeners;

use App\Events\Vehicle\VehicleCreated;
use App\Events\Vehicle\VehicleDeleted;
use App\Events\Vehicle\VehicleUpdated;
use App\Events\Vehicle\VehicleRestored;
use App\Events\Vehicle\VehicleDeactivated;
use App\Events\Vehicle\VehicleReactivated;
use App\Events\Vehicle\VehiclePermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class VehicleEventListener
 *
 * @package App\Listeners
 */
class VehicleEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Vehicle Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Vehicle Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Vehicle Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Vehicle Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Vehicle Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Vehicle Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Vehicle Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            VehicleCreated::class,
            'App\Listeners\VehicleEventListener@onCreated'
        );

        $events->listen(
            VehicleUpdated::class,
            'App\Listeners\VehicleEventListener@onUpdated'
        );

        $events->listen(
            VehicleDeleted::class,
            'App\Listeners\VehicleEventListener@onDeleted'
        );

        $events->listen(
            VehicleDeactivated::class,
            'App\Listeners\VehicleEventListener@onDeactivated'
        );

        $events->listen(
            VehicleReactivated::class,
            'App\Listeners\VehicleEventListener@onReactivated'
        );

        $events->listen(
            VehiclePermanentlyDeleted::class,
            'App\Listeners\VehicleEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            VehicleRestored::class,
            'App\Listeners\VehicleEventListener@onRestored'
        );
    }
}
