<?php

namespace App\Listeners;

use App\Events\Branch\BranchCreated;
use App\Events\Branch\BranchDeleted;
use App\Events\Branch\BranchUpdated;
use App\Events\Branch\BranchRestored;
use App\Events\Branch\BranchDeactivated;
use App\Events\Branch\BranchReactivated;
use App\Events\Branch\BranchPermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class BranchEventListener
 *
 * @package App\Listeners
 */
class BranchEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Branch Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Branch Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Branch Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('Branch Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('Branch Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('Branch Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('Branch Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            BranchCreated::class,
            'App\Listeners\BranchEventListener@onCreated'
        );

        $events->listen(
            BranchUpdated::class,
            'App\Listeners\BranchEventListener@onUpdated'
        );

        $events->listen(
            BranchDeleted::class,
            'App\Listeners\BranchEventListener@onDeleted'
        );

        $events->listen(
            BranchDeactivated::class,
            'App\Listeners\BranchEventListener@onDeactivated'
        );

        $events->listen(
            BranchReactivated::class,
            'App\Listeners\BranchEventListener@onReactivated'
        );

        $events->listen(
            BranchPermanentlyDeleted::class,
            'App\Listeners\BranchEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            BranchRestored::class,
            'App\Listeners\BranchEventListener@onRestored'
        );
    }
}
