<?php

namespace App\Listeners;

use App\Events\VehicleDriver\VehicleDriverCreated;
use App\Events\VehicleDriver\VehicleDriverDeleted;
use App\Events\VehicleDriver\VehicleDriverUpdated;
use App\Events\VehicleDriver\VehicleDriverRestored;
use App\Events\VehicleDriver\VehicleDriverDeactivated;
use App\Events\VehicleDriver\VehicleDriverReactivated;
use App\Events\VehicleDriver\VehicleDriverPermanentlyDeleted;
use Illuminate\Events\Dispatcher;

/**
 * Class VehicleDriverEventListener
 *
 * @package App\Listeners
 */
class VehicleDriverEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('VehicleDriver Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('VehicleDriver Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('VehicleDriver Deleted');
    }


    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        logger('VehicleDriver Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        logger('VehicleDriver Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        logger('VehicleDriver Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        logger('VehicleDriver Restored');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            VehicleDriverCreated::class,
            'App\Listeners\VehicleDriverEventListener@onCreated'
        );

        $events->listen(
            VehicleDriverUpdated::class,
            'App\Listeners\VehicleDriverEventListener@onUpdated'
        );

        $events->listen(
            VehicleDriverDeleted::class,
            'App\Listeners\VehicleDriverEventListener@onDeleted'
        );

        $events->listen(
            VehicleDriverDeactivated::class,
            'App\Listeners\VehicleDriverEventListener@onDeactivated'
        );

        $events->listen(
            VehicleDriverReactivated::class,
            'App\Listeners\VehicleDriverEventListener@onReactivated'
        );

        $events->listen(
            VehicleDriverPermanentlyDeleted::class,
            'App\Listeners\VehicleDriverEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            VehicleDriverRestored::class,
            'App\Listeners\VehicleDriverEventListener@onRestored'
        );
    }
}
