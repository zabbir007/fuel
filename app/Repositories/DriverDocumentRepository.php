<?php

namespace App\Repositories;

use App\Events\DriverDocument\DriverDocumentCreated;
use App\Events\DriverDocument\DriverDocumentDeactivated;
use App\Events\DriverDocument\DriverDocumentDeleted;
use App\Events\DriverDocument\DriverDocumentPermanentlyDeleted;
use App\Events\DriverDocument\DriverDocumentReactivated;
use App\Events\DriverDocument\DriverDocumentRestored;
use App\Events\DriverDocument\DriverDocumentUpdated;
use App\Exceptions\GeneralException;
use App\Models\DriverDocument;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class DriverDocumentRepository.
 */
class DriverDocumentRepository extends BaseRepository
{
    /**
     * DriverDocumentRepository constructor.
     *
     * @param  DriverDocument  $model
     */
    public function __construct(DriverDocument $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'user_id',
                'employee_id',
                'name',
                'designation',
                'active',
                'created_at',
                'updated_at'
            ])
        ->with(['user','user.branches']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the DriverDocumentScope trait
        return $dataTableQuery;
    }


    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return DriverDocument
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): DriverDocument
    {
        return DB::transaction(function () use ($data) {

            $module = $this->model::create([
                'driver_id' => $data['driver_id'],
                'document_type' => $data['document_type'],

                'document_no' => $data['document_no'],

                'issue_date' => isset($data['issue_date']) ?  Carbon::parse($data['issue_date'])->toDateString() : null,
                'expiry_date' => isset($data['expiry_date']) ?  Carbon::parse($data['expiry_date'])->toDateString() : null,

                'issuing_authority' => $data['issuing_authority'] ?? null,

                'order' => $data['order'] ?? null,
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

                event(new DriverDocumentCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return DriverDocument
     * @throws Throwable
     */
    public function update($id, array $data): DriverDocument
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'driver_id' => $data['driver_id'],
                'document_type' => $data['document_type'],

                'document_no' => $data['document_no'],

                'issue_date' => isset($data['issue_date']) ?  Carbon::parse($data['issue_date'])->toDateString() : null,
                'expiry_date' => isset($data['expiry_date']) ?  Carbon::parse($data['expiry_date'])->toDateString() : null,

                'issuing_authority' => $data['issuing_authority'] ?? null,

                'order' => $data['order'] ?? null,
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new DriverDocumentUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new DriverDocumentDeactivated($module));
                break;
            case 1:
                event(new DriverDocumentReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new DriverDocumentDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return DriverDocument
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): DriverDocument
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new DriverDocumentPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return DriverDocument
     * @throws GeneralException
     */
    public function restore($id): DriverDocument
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new DriverDocumentRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
