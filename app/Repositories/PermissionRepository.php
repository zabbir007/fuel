<?php

namespace App\Repositories;

use App\Events\Permission\PermissionCreated;
use App\Events\Permission\PermissionDeactivated;
use App\Events\Permission\PermissionReactivated;
use App\Events\Permission\PermissionUpdated;
use App\Exceptions\GeneralException;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends BaseRepository
{
    /**
     * PermissionRepository constructor.
     *
     * @param  Permission  $model
     */
    public function __construct(Permission $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model::with('permissions', 'permissions')
            ->select([
                'id',
                'name',
                'guard_name',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        // active() is a scope on the permissionScope trait
        return $dataTableQuery;
    }


    /**
     * @param  array  $data
     *
     * @return Permission
     * @throws GeneralException
     * @throws \Throwable
     */
    public function create(array $data): Permission
    {
        // Make sure it doesn't already exist
        if ($this->permissionExists($data['name'])) {
            throw new GeneralException('A permission already exists with the name '.e($data['name']));
        }

        return DB::transaction(function () use ($data) {
            $module = $this->model::create(['name' => Str::slug($data['name'])]);

            if ($module) {
                event(new PermissionCreated($module));

                return $module;
            }

            throw new GeneralException(trans('exceptions.access.roles.create_error'));
        });
    }

    /**
     * @param $id
     * @param  array  $data
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update($id, array $data)
    {
        $module = $this->getById($id);

        if ($module->isDefault()) {
            throw new GeneralException('You can not edit the default permission.');
        }

        // If the name is changing make sure it doesn't already exist
        if ($module->name !== strtolower($data['name'])) {
            if ($this->permissionExists($data['name'])) {
                throw new GeneralException('A permission already exists with the name '.$data['name']);
            }
        }

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'name' => Str::slug($data['name']),
            ])) {

                event(new PermissionUpdated($module));

                return $module;
            }

            throw new GeneralException(trans('exceptions.access.roles.update_error'));
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Permission
     * @throws GeneralException
     */
    public function mark($id, $status): Permission
    {
        $module = $this->getById($id);

        if ($status === 0 && auth()->id() === $module->id) {
            throw new GeneralException(__('exceptions.access.permissions.cant_deactivate_self'));
        }

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new PermissionDeactivated($module));
                break;
            case 1:
                event(new PermissionReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException(__('exceptions.access.permissions.mark_error'));
    }

    /**
     * @param $name
     *
     * @return bool
     */
    protected function permissionExists($name): bool
    {
        return $this->model
                ->where('name', strtolower($name))
                ->count() > 0;
    }
}
