<?php

namespace App\Repositories;

use App\Events\FuelRate\FuelRateCreated;
use App\Events\FuelRate\FuelRateDeactivated;
use App\Events\FuelRate\FuelRateDeleted;
use App\Events\FuelRate\FuelRatePermanentlyDeleted;
use App\Events\FuelRate\FuelRateReactivated;
use App\Events\FuelRate\FuelRateRestored;
use App\Events\FuelRate\FuelRateUpdated;
use App\Exceptions\GeneralException;
use App\Exceptions\RecordNotFoundException;
use App\Models\FuelRate;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class FuelRateRepository.
 */
class FuelRateRepository extends BaseRepository
{
    /**
     * FuelRateRepository constructor.
     *
     * @param FuelRate $model
     */
    public function __construct(FuelRate $model)
    {
        $this->model = $model;
    }

//    /**
//     * @param bool $status
//     * @param bool $trashed
//     * @return mixed
//     */
//    public function getForDataTable($status = true, $trashed = false)
//    {
//        $dataTableQuery = $this->model;
//
//        if ($status === 'true') {
//            return $dataTableQuery->active(true);
//        }
//
//        if ($status === 'false') {
//            return $dataTableQuery->active(false);
//        }
//
//        if ($trashed === 'true') {
//            return $dataTableQuery->onlyTrashed();
//        }
//
//        // active() is a scope on the FuelRateScope trait
//        return $dataTableQuery;
//    }

    /**
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getActive($orderBy = 'order', $orderType = 'asc')
    {
        return $this->model
            ->archived(0)
            ->active(true)
            ->latest()
            ->with(['branch', 'fuel'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->groupBy('branch_id');
    }

    /**
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function toSelect($orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->select(['id', 'name', 'license_plate_no'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('format_name', 'id');
    }

    /**
     * @param array $data
     *
     * @return FuelRate
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): FuelRate
    {
        return DB::transaction(function () use ($data) {

            $module = false;

            foreach ($data['rates'] as $rate) {
                $row = [
                    'company_id' => $data['company_id'] ?? request()->user()->company_id,
                    'branch_id' => $data['branch_id'],
                    'entry_date' => Carbon::parse($data['entry_date'])->toDateString(),
                    'fuel_id' => $rate['id'],
                    'rate' => $rate['rate'] == null ? 0 : $rate['rate'],
                    'order' => $rate['order'] ?? null,
                    'archived' => 0,
                    'active' => 1
                ];

                //set new value as active and previous as archived
                $this->model->update(['archived' => 1, 'active' => 0], [
                    'company_id' => $row['company_id'],
                    'branch_id' => $row['branch_id'],
                    'fuel_id' => $row['fuel_id']
                ]);

                $module = $this->model::create($row);
            }

            if ($module) {
                event(new FuelRateCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param array $data
     *
     * @return FuelRate
     * @throws Throwable
     */
    public function update(array $data): FuelRate
    {
        $modules = $this->where('branch_id', $data['branch_id'])
            ->where('entry_date', $data['date'])
            ->orderBy('created_at', 'desc')
            ->with(['branch', 'fuel'])
            ->get();

        if ($modules->count() == 0){
            throw new RecordNotFoundException('No Records found!', 404);
        }

        return DB::transaction(function () use ($modules, $data) {
            foreach ($data['rates'] as $rate) {
                $module = $modules->where('fuel_id', $rate['id'])->first();
                if ($module->update([
                    'branch_id' => $module['branch_id'],
                    'fuel_id' => $rate['id'],
                    'rate' => $rate['rate'],
                    'order' => $module['order'] ?? null,
                ])) {
                    event(new FuelRateUpdated($module));
                }
            }

            if ($module){
                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new FuelRateDeactivated($module));
                break;
            case 1:
                event(new FuelRateReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new FuelRateDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return FuelRate
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): FuelRate
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new FuelRatePermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return FuelRate
     * @throws GeneralException
     */
    public function restore($id): FuelRate
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new FuelRateRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
