<?php

namespace App\Repositories;

use App\Events\Employee\EmployeeCreated;
use App\Events\Employee\EmployeeDeactivated;
use App\Events\Employee\EmployeeDeleted;
use App\Events\Employee\EmployeePermanentlyDeleted;
use App\Events\Employee\EmployeeReactivated;
use App\Events\Employee\EmployeeRestored;
use App\Events\Employee\EmployeeUpdated;
use App\Exceptions\GeneralException;
use App\Models\Employee;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class EmployeeRepository.
 */
class EmployeeRepository extends BaseRepository
{
    /**
     * EmployeeRepository constructor.
     *
     * @param Employee $model
     */
    public function __construct(Employee $model)
    {
        $this->model = $model;
    }

    /**
     * @param bool $status
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'user_id',
                'employee_id',
                'name',
                'designation_id',
                'active',
                'created_at',
                'updated_at'
            ])
            ->with(['user', 'user.branches']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the EmployeeScope trait
        return $dataTableQuery;
    }


    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    public function getByBranch($branchId, $designationId = 2)
    {
        return Collection::make(DB::select(DB::raw("select * from `employees` where
exists (select * from `users` where `employees`.`user_id` = `users`.`id`and
exists (select * from `branches` inner join `user_branches` on `branches`.`id` = `user_branches`.`branch_id`
where `users`.`id` = `user_branches`.`user_id` and `branch_id` = ".$branchId." and `branches`.`deleted_at`
is null and `branch_id` in (".$branchId.")) and `users`.`deleted_at` is null)
and `designation_id` = ".$designationId." and `employees`.`deleted_at`
is null and `company_id` = 1 limit 1")))->first();

//        //designationID = 2 = NDM
//        return $this->model->whereHas('user.branches', function ($q) use ($branchId) {
//            $q->where('branch_id', $branchId);
//        })
//            ->where('designation_id', $designationId)
//            ->first();
    }

    /**
     * @param array $data
     *
     * @return Employee
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Employee
    {
        return DB::transaction(function () use ($data) {

            $userId = null;

            //if allow user create selected

            if (isset($data['allow_login']) && $data['allow_login'] != false) {
                //create user
                $data['roles'] = ['user'];

                $user = app(UserRepository::class)->create($data);

                if ($user) {
                    $userId = $user->id;
                }
            }

            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'user_id' => $userId,
                'employee_id' => $data['employee_code'],
                'name' => $data['first_name'] . ' ' . $data['last_name'],
                'designation_id' => $data['designation_id'],
                'department' => $data['department'] ?? null,
                'grade' => $data['grade'] ?? null,
                'email' => $data['email'] ?? null,

                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

                event(new EmployeeCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param array $data
     *
     * @return Employee
     * @throws Throwable
     */
    public function update($id, array $data): Employee
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'employee_id' => $data['employee_code'],
                'name' => $data['first_name'] . ' ' . $data['last_name'],
                'designation_id' => $data['designation_id'],
                'department' => $data['department'] ?? null,
                'grade' => $data['grade'] ?? null,
                'email' => $data['email'] ?? null,

                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new EmployeeUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new EmployeeDeactivated($module));
                break;
            case 1:
                event(new EmployeeReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new EmployeeDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Employee
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Employee
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new EmployeePermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Employee
     * @throws GeneralException
     */
    public function restore($id): Employee
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new EmployeeRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
