<?php

namespace App\Repositories;

use App\Exceptions\GeneralException;
use App\Models\DocumentType;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class DocumentTypeRepository.
 */
class DocumentTypeRepository extends BaseRepository
{
    /**
     * DocumentTypeRepository constructor.
     *
     * @param  DocumentType  $model
     */
    public function __construct(DocumentType $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'user_id',
                'employee_id',
                'name',
                'designation',
                'active',
                'created_at',
                'updated_at'
            ])
        ->with(['user','user.branches']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the DocumentTypeScope trait
        return $dataTableQuery;
    }


    /**
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function toSelectDriver($orderBy = 'order', $orderType ='asc')
    {
        return $this->model->active()
            ->driver()
            ->select(['id', 'name'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function toSelectVehicle($orderBy = 'order', $orderType ='asc')
    {
        return $this->model->active()
            ->vehicle()
            ->select(['id', 'name'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return DocumentType
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): DocumentType
    {
        return DB::transaction(function () use ($data) {

            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'type' => $data['type'],
                'name' => $data['name'],
                'order' => $data['order'],

                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

             //   event(new DocumentTypeCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return DocumentType
     * @throws Throwable
     */
    public function update($id, array $data): DocumentType
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'type' => $data['type'],
                'name' => $data['name'],
                'order' => $data['order'],

                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

            //    event(new DocumentTypeUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
               // event(new DocumentTypeDeactivated($module));
                break;
            case 1:
             //   event(new DocumentTypeReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
               // event(new DocumentTypeDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return DocumentType
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): DocumentType
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
              //  event(new DocumentTypePermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return DocumentType
     * @throws GeneralException
     */
    public function restore($id): DocumentType
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
          //  event(new DocumentTypeRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
