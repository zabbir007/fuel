<?php

namespace App\Repositories;

use App\Events\Driver\DriverCreated;
use App\Events\Driver\DriverDeactivated;
use App\Events\Driver\DriverDeleted;
use App\Events\Driver\DriverPermanentlyDeleted;
use App\Events\Driver\DriverReactivated;
use App\Events\Driver\DriverRestored;
use App\Events\Driver\DriverUpdated;
use App\Exceptions\GeneralException;
use App\Models\Driver;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class DriverRepository.
 */
class DriverRepository extends BaseRepository
{
    /**
     * DriverRepository constructor.
     *
     * @param  Driver  $model
     */
    public function __construct(Driver $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'employee_id',
                'name',
                'mobile',
                'dob',
                'doj',
                'active',
                'created_at',
                'updated_at'
            ])
        ->with(['branch','vehicle']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the DriverScope trait
        return $dataTableQuery;
    }


    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param $branchId
     * @param  string  $orderBy
     * @param  string  $orderType
     * @return mixed
     */
    public function toSelectByBranch($branchId, $orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->whereHas('branch', function ($q) use($branchId){
                $q->where('branch_id', $branchId);
            })
            ->select(['id', 'name', 'license_plate_no'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('format_name', 'id');
    }

    /**
     * @param $branchId
     * @param $vehicleId
     * @param  string  $orderBy
     * @param  string  $orderType
     * @return mixed
     */
    public function getByBranchVehicle($branchId, $vehicleId, $orderBy = 'order', $orderType = 'asc')
    {
       // $driverId = $driverId == null ? false : $driverId;

        return Collection::make(DB::select(DB::raw("SELECT `id`,`name`,`employee_id` FROM `drivers` WHERE `active` = 1
	AND EXISTS (
	SELECT * FROM `branches` INNER JOIN `vehicle_drivers` ON `branches`.`id` = `vehicle_drivers`.`branch_id`
	WHERE
		`drivers`.`id` = `vehicle_drivers`.`driver_id`
		AND `branch_id` = {$branchId}
		AND `archived` = 0
		AND `branches`.`deleted_at` IS NULL
	AND `branch_id` IN ( {$branchId} ))
	AND EXISTS ( SELECT * FROM `vehicle_drivers` WHERE `drivers`.`id` = `vehicle_drivers`.`driver_id` AND `vehicle_id` = {$vehicleId} AND `archived` = 0 AND `vehicle_drivers`.`deleted_at` IS NULL )
	AND `drivers`.`deleted_at` IS NULL
	AND `company_id` = 1
ORDER BY
	`{$orderBy}` {$orderType}")));

//        return $this->model->active()
//            ->whereHas('branch', function ($q) use($branchId){
//                $q->where('branch_id', $branchId);
//            })
//            ->whereHas('vehicle', function ($q) use($vehicleId){
//                $q->where('vehicle_id', $vehicleId);
//            })
////            ->when($driverId, function ($q) use($driverId){
////                $q->where('vehicle.driver_id', $driverId);
////            })
//            ->orderBy($orderBy, $orderType)
//            ->get();
    }

    /**
     * @param  array  $data
     *
     * @return Driver
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Driver
    {
        return DB::transaction(function () use ($data) {
            $photo = null;

            if (isset($data['photo'])){
                $photo = $data['photo']->getClientOriginalName();
            }

            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'employee_id' => $data['employee_id'],
                'name' => $data['name'],
                'mobile' => $data['mobile'] ?? null,
                'dob' => isset($data['dob']) ? Carbon::parse($data['dob'])->toDateString() : null,
                'doj' => isset($data['doj']) ? Carbon::parse($data['doj'])->toDateString() : null,

                'blood_group' => $data['blood_group'] ?? null,
                'em_mobile' => $data['em_mobile'] ?? null,
                'present_address' => $data['present_address'] ?? null,
                'permanent_address' => $data['permanent_address'] ?? null,

                'photo' => $photo,
                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {
                if (isset($data['photo'])){
                    $data['photo']->move(public_path('img/drivers'), $data['photo']->getClientOriginalName());
                }

                //add documents
                if (isset($data['documents'])){
                    foreach ($data['documents'] as $document){
                        if ($document['document_no'] != null){
                            $document['driver_id'] = $module->id;

                            app(DriverDocumentRepository::class)->create($document);
                        }
                    }
                }

                event(new DriverCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Driver
     * @throws Throwable
     */
    public function update($id, array $data): Driver
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            $photo = null;

            if (isset($data['photo'])){
                $photo = $data['photo']->getClientOriginalName();
            }

            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'employee_id' => $data['employee_id'],
                'name' => $data['name'],
                'mobile' => $data['mobile'] ?? null,
                'dob' => $data['dob'] ?? null,
                'doj' => $data['doj'] ?? null,
                'blood_group' => $data['blood_group'] ?? null,
                'em_mobile' => $data['em_mobile'] ?? null,
                'present_address' => $data['present_address'] ?? null,
                'permanent_address' => $data['permanent_address'] ?? null,

                'photo' => $photo,
                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {
                if (isset($data['photo'])){
                    $data['photo']->move(public_path('img/drivers'), $data['photo']->getClientOriginalName());
                }

                if (isset($data['docs'])){
                    //update documents
                    foreach ($data['docs'] as $document){
                        if ($document['document_no'] != null){
                            $document['driver_id'] = $module->id;

                            app(DriverDocumentRepository::class)->update($document['id'], $document);
                        }
                    }
                }

                //add documents
                if (isset($data['new_docs'])){
                    foreach ($data['new_docs'] as $document){
                        if ($document['document_no'] != null){
                            $document['driver_id'] = $module->id;

                            app(DriverDocumentRepository::class)->create($document);
                        }
                    }
                }

                event(new DriverUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new DriverDeactivated($module));
                break;
            case 1:
                event(new DriverReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new DriverDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Driver
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Driver
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new DriverPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Driver
     * @throws GeneralException
     */
    public function restore($id): Driver
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new DriverRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
