<?php

namespace App\Repositories;


use App\Exceptions\GeneralException;
use App\Models\Fuel;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class FuelRepository.
 */
class FuelRepository extends BaseRepository
{
    /**
     * FuelRepository constructor.
     *
     * @param  Fuel  $model
     */
    public function __construct(Fuel $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'employee_id',
                'name',
                'mobile',
                'dob',
                'doj',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the FuelScope trait
        return $dataTableQuery;
    }


    /**
     * @param  string  $orderBy
     * @param  string  $orderType
     * @return mixed
     */
    public function toSelect($orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param $branchId
     * @param  string  $orderBy
     * @param  string  $orderType
     * @return mixed
     */
    public function getByBranch($branchId, $orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->whereHas('branch', function ($q) use ($branchId) {
                $q->where('branch_id', $branchId);
            })
            ->with([
                'rates' => function ($q) use ($branchId) {
                    $q->where('branch_id', $branchId);
                }
            ])
            ->orderBy($orderBy, $orderType)
            ->get();
    }

    /**
     * @param  array  $data
     *
     * @return Fuel
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Fuel
    {
        return DB::transaction(function () use ($data) {

            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'name' => $data['name'],
                'order' => $data['order'],
                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

                //   event(new FuelCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Fuel
     * @throws Throwable
     */
    public function update($id, array $data): Fuel
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {

            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'name' => $data['name'],
                'order' => $data['order'],
                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {
                //event(new FuelUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                //  event(new FuelDeactivated($module));
                break;
            case 1:
                //   event(new FuelReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                // event(new FuelDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Fuel
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Fuel
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                // event(new FuelPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Fuel
     * @throws GeneralException
     */
    public function restore($id): Fuel
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            //  event(new FuelRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
