<?php

namespace App\Repositories;

use App\Events\VehicleHistory\VehicleHistoryCreated;
use App\Events\VehicleHistory\VehicleHistoryDeactivated;
use App\Events\VehicleHistory\VehicleHistoryDeleted;
use App\Events\VehicleHistory\VehicleHistoryPermanentlyDeleted;
use App\Events\VehicleHistory\VehicleHistoryReactivated;
use App\Events\VehicleHistory\VehicleHistoryRestored;
use App\Events\VehicleHistory\VehicleHistoryUpdated;
use App\Exceptions\GeneralException;
use App\Models\VehicleHistory;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class VehicleHistoryRepository.
 */
class VehicleHistoryRepository extends BaseRepository
{
    /**
     * VehicleHistoryRepository constructor.
     *
     * @param VehicleHistory $model
     */
    public function __construct(VehicleHistory $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        $dataTableQuery = $this->model::with(['roles', 'branches']);

        return $dataTableQuery->active(true)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function toSelect($orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->select(['id', 'name', 'license_plate_no'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('format_name', 'id');
    }

    /**
     * @param array $data
     *
     * @return VehicleHistory
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): VehicleHistory
    {
        return DB::transaction(function () use ($data) {

            //histories

            $histories = [[
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'entry_date' => isset($data['entry_date']) ? Carbon::parse($data['entry_date'])->toDateString() : Carbon::now()->toDateString(),
                'vehicle_id' => $data['vehicle_id'],

                'tanks' => isset($data['is_dual_tank']) ? 2 : 1,
                'tank_type' => 1,

                'fuel_id' => $data['main_tank_fuel_id'],
                'capacity' => $data['main_tank_fuel_capacity'],
                'millage' => $data['main_tank_millage'],

                'order' => $data['order'],

                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'active' => isset($data['active']) && $data['active'] === '1'
            ]
            ];

            if (isset($data['is_dual_tank'])) {
                $histories = Arr::prepend($histories,
                    [
                        'company_id' => $data['company_id'] ?? request()->user()->company_id,

                        'entry_date' => isset($data['entry_date']) ? Carbon::parse($data['entry_date'])->toDateString() : Carbon::now()->toDateString(),
                        'vehicle_id' => $data['vehicle_id'],

                        'tanks' => isset($data['is_dual_tank']) ? 2 : 1,
                        'tank_type' => 2,

                        'fuel_id' => $data['second_tank_fuel_id'],
                        'capacity' => $data['second_tank_fuel_capacity'],
                        'millage' => $data['second_tank_millage'],

                        'order' => $data['order'],

                        'custom1' => $data['custom1'] ?? null,
                        'custom2' => $data['custom2'] ?? null,
                        'custom3' => $data['custom3'] ?? null,

                        'active' => isset($data['active']) && $data['active'] === '1'
                    ]);
            }

            foreach ($histories as $history) {
                $module = $this->model::create($history);
            }

            if ($module) {

                event(new VehicleHistoryCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param array $data
     *
     * @return VehicleHistory
     * @throws Throwable
     */
    public function update($id, array $data): VehicleHistory
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {

            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'vehicle_type' => $data['vehicle_type'],
                'name' => $data['name'],
                'manufacturer' => $data['manufacturer'],
                'model' => $data['model'] ?? null,
                'manufacturer_year' => $data['manufacturer_year'],
                'weight' => $data['weight'] ?? null,
                'lifetime' => $data['lifetime'] ?? null,

                'chassis_no' => $data['chassis_no'],
                'engine_no' => $data['engine_no'],
                'vin_no' => $data['vin_no'] ?? null,
                'purchase_date' => isset($data['purchase_date']) ? Carbon::parse($data['purchase_date'])->toDateString() : null,

                'license_plate_no' => $data['license_plate_no'],
                'license_year' => $data['license_year'],
                'main_tank_fuel_id' => $data['main_tank_fuel_id'],
                'main_tank_fuel_capacity' => $data['main_tank_fuel_capacity'],
                'second_tank_fuel_id' => $data['second_tank_fuel_id'] ?? null,
                'second_tank_fuel_capacity' => $data['second_tank_fuel_capacity'] ?? null,

                'order' => $data['order'],

                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'is_dual_tank' => isset($data['is_dual_tank']) && $data['is_dual_tank'] === '1',
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                //update documents
                if (isset($data['docs'])) {
                    foreach ($data['docs'] as $document) {
                        if ($document['document_no'] != null) {
                            $document['vehicle_id'] = $module->id;

                            app(VehicleDocumentRepository::class)->update($document['id'], $document);
                        }
                    }
                }


                //add documents
                if (isset($data['new_docs'])) {
                    foreach ($data['new_docs'] as $document) {
                        if ($document['document_no'] != null) {
                            $document['vehicle_id'] = $module->id;

                            app(VehicleDocumentRepository::class)->create($document);
                        }
                    }
                }

                event(new VehicleHistoryUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new VehicleHistoryDeactivated($module));
                break;
            case 1:
                event(new VehicleHistoryReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new VehicleHistoryDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return VehicleHistory
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): VehicleHistory
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new VehicleHistoryPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return VehicleHistory
     * @throws GeneralException
     */
    public function restore($id): VehicleHistory
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new VehicleHistoryRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
