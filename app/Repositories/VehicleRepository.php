<?php

namespace App\Repositories;

use App\Events\Vehicle\VehicleCreated;
use App\Events\Vehicle\VehicleDeactivated;
use App\Events\Vehicle\VehicleDeleted;
use App\Events\Vehicle\VehiclePermanentlyDeleted;
use App\Events\Vehicle\VehicleReactivated;
use App\Events\Vehicle\VehicleRestored;
use App\Events\Vehicle\VehicleUpdated;
use App\Exceptions\GeneralException;
use App\Models\Vehicle;
use App\Models\VehicleDriver;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class VehicleRepository.
 */
class VehicleRepository extends BaseRepository
{
    /**
     * VehicleRepository constructor.
     *
     * @param Vehicle $model
     */
    public function __construct(Vehicle $model)
    {
        $this->model = $model;
    }

    /**
     * @param bool $status
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'vehicle_type',
                'name',
                'manufacturer',
                'manufacturer_year',
                'license_plate_no',
                'license_year',
                'is_dual_tank',
                'chassis_no',
                'engine_no',
                'active',
                'created_at',
                'updated_at'
            ])->with(['type', 'main_fuel']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the VehicleScope trait
        return $dataTableQuery;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        $dataTableQuery = $this->model::with(['roles', 'branches']);

        return $dataTableQuery->active(true)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function toSelect($orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->select(['id', 'name', 'license_plate_no'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('format_name', 'id');
    }

    /**
     * @param $branchId
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function toSelectByBranch($branchId, $orderBy = 'order', $orderType = 'asc')
    {
        return Collection::make(DB::select(DB::raw("select `id`, `name`, `license_plate_no`, CONCAT(`name`,' (',`license_plate_no`,')') as format_name from `vehicles`
where `active` = 1 and
exists (select * from `branches` inner join `vehicle_drivers`
on `branches`.`id` = `vehicle_drivers`.`branch_id`
where `vehicles`.`id` = `vehicle_drivers`.`vehicle_id` and
`branch_id` =  {$branchId} and `vehicle_drivers`.`archived` = 0 and
`branches`.`deleted_at` is null and
`branch_id` in ({$branchId})) and
`vehicles`.`deleted_at` is null and
`company_id` = 1 order by `{$orderBy}` {$orderType} ")))->pluck('format_name', 'id');
//        return $this->model->active()
//            ->whereHas('branches', function ($q) use ($branchId) {
//                $q->where('branch_id', $branchId);
//            })
//            ->select(['id', 'name', 'license_plate_no'])
//            ->orderBy($orderBy, $orderType)
//            ->get()
//            ->pluck('format_name', 'id');
    }

    /**
     * @param $branchId
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getByBranch($branchId, $orderBy = 'order', $orderType = 'asc')
    {
        $results = collect();

        $vehicles =  Collection::make(DB::select(DB::raw("select *, CONCAT(`name`,' (',`license_plate_no`,')') as format_name, YEAR(purchase_date) as purchase_year from `vehicles`
where `active` = 1 and
exists (select * from `branches` inner join `vehicle_drivers`
on `branches`.`id` = `vehicle_drivers`.`branch_id`
where `vehicles`.`id` = `vehicle_drivers`.`vehicle_id` and
`branch_id` =  {$branchId} and `vehicle_drivers`.`archived` = 0 and
`branches`.`deleted_at` is null and
`branch_id` in ({$branchId})) and
`vehicles`.`deleted_at` is null and
`company_id` = 1 order by `{$orderBy}` {$orderType} ")));

        $vehicles->map(function ($item) use($branchId,$orderBy, $orderType){
            $item->histories = VehicleDriver::where('branch_id', $branchId)->where('vehicle_id', $item->id)
                ->orderBy($orderBy, $orderType)
                ->get();
        });

        return $vehicles;

//        return $this->model->active()
//            ->whereHas('branches', function ($q) use ($branchId) {
//                $q->where('branch_id', $branchId);
//            })
////            ->with(['histories' => function ($q) {
//////                $q->where('trans_date', '')
////                //$q->limit(1);
////            }])
//            //  ->select(['id', 'name', 'license_plate_no'])
//            ->orderBy($orderBy, $orderType)
//            ->get();
    }

    /**
     * @param array $data
     *
     * @return Vehicle
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Vehicle
    {
        return DB::transaction(function () use ($data) {

            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'vehicle_type' => $data['vehicle_type'],
                'name' => $data['name'],
                'manufacturer' => $data['manufacturer'],
                'model' => $data['model'] ?? null,
                'manufacturer_year' => $data['manufacturer_year'],
                'weight' => $data['weight'] ?? null,
                'lifetime' => $data['lifetime'] ?? null,

                'chassis_no' => $data['chassis_no'],
                'engine_no' => $data['engine_no'],
                'vin_no' => $data['vin_no'] ?? null,
                'purchase_date' => isset($data['purchase_date']) ? Carbon::parse($data['purchase_date'])->toDateString() : null,

                'license_plate_no' => $data['license_plate_no'],
                'license_year' => $data['license_year'],
                'main_tank_fuel_id' => $data['main_tank_fuel_id'],
                'main_tank_fuel_capacity' => $data['main_tank_fuel_capacity'],

                'second_tank_fuel_id' => $data['second_tank_fuel_id'] ?? null,
                'second_tank_fuel_capacity' => $data['second_tank_fuel_capacity'] ?? null,

                'opening' => $data['opening_millage'],
                'order' => $data['order'],

                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'is_dual_tank' => isset($data['is_dual_tank']) && $data['is_dual_tank'] === '1',
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {
                event(new VehicleCreated($module));

                //add documents
                if (isset($data['docs'])) {
                    foreach ($data['docs'] as $document) {
                        if ($document['document_no'] != null) {
                            $document['vehicle_id'] = $module->id;

                            app(VehicleDocumentRepository::class)->create($document);
                        }
                    }
                }

                //add history
                $data['vehicle_id'] = $module->id;
                app(VehicleHistoryRepository::class)->create($data);

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param array $data
     *
     * @return Vehicle
     * @throws Throwable
     */
    public function update($id, array $data): Vehicle
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {

            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'vehicle_type' => $data['vehicle_type'],
                'name' => $data['name'],
                'manufacturer' => $data['manufacturer'],
                'model' => $data['model'] ?? null,
                'manufacturer_year' => $data['manufacturer_year'],
                'weight' => $data['weight'] ?? null,
                'lifetime' => $data['lifetime'] ?? null,

                'chassis_no' => $data['chassis_no'],
                'engine_no' => $data['engine_no'],
                'vin_no' => $data['vin_no'] ?? null,
                'purchase_date' => isset($data['purchase_date']) ? Carbon::parse($data['purchase_date'])->toDateString() : null,

                'license_plate_no' => $data['license_plate_no'],
                'license_year' => $data['license_year'],
                'main_tank_fuel_id' => $data['main_tank_fuel_id'],
                'main_tank_fuel_capacity' => $data['main_tank_fuel_capacity'],
                'main_tank_millage' => $data['main_tank_millage'],
                'second_tank_fuel_id' => $data['second_tank_fuel_id'] ?? null,
                'second_tank_fuel_capacity' => $data['second_tank_fuel_capacity'] ?? null,
                'second_tank_millage' => $data['second_tank_millage'],

                'order' => $data['order'],

                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'is_dual_tank' => isset($data['is_dual_tank']) && $data['is_dual_tank'] === '1',
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {
                event(new VehicleUpdated($module));

                //update documents
                if (isset($data['docs'])) {
                    foreach ($data['docs'] as $document) {
                        if ($document['document_no'] != null) {
                            $document['vehicle_id'] = $module->id;

                            app(VehicleDocumentRepository::class)->update($document['id'], $document);
                        }
                    }
                }

                //add documents
                if (isset($data['new_docs'])) {
                    if (isset($data['new_docs'])) {
                        foreach ($data['new_docs'] as $document) {
                            if ($document['document_no'] != null) {
                                $document['vehicle_id'] = $module->id;

                                app(VehicleDocumentRepository::class)->create($document);
                            }
                        }
                    }
                }

                //update history
                $data['vehicle_id'] = $module->id;

                app(VehicleHistoryRepository::class)->create($data);

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new VehicleDeactivated($module));
                break;
            case 1:
                event(new VehicleReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new VehicleDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Vehicle
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Vehicle
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new VehiclePermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Vehicle
     * @throws GeneralException
     */
    public function restore($id): Vehicle
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new VehicleRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
