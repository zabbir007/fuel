<?php

namespace App\Repositories;

use App\Exceptions\GeneralException;
use App\Models\Designation;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class DesignationRepository.
 */
class DesignationRepository extends BaseRepository
{
    /**
     * DesignationRepository constructor.
     *
     * @param  Designation  $model
     */
    public function __construct(Designation $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'user_id',
                'employee_id',
                'name',
                'designation',
                'active',
                'created_at',
                'updated_at'
            ])
        ->with(['user','user.branches']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the DesignationScope trait
        return $dataTableQuery;
    }


    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Designation
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Designation
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'name' => $data['name'],

                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

              //  event(new DesignationCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Designation
     * @throws Throwable
     */
    public function update($id, array $data): Designation
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'name' => $data['name'],

                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

             //   event(new DesignationUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
              //  event(new DesignationDeactivated($module));
                break;
            case 1:
            //    event(new DesignationReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
              //  event(new DesignationDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Designation
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Designation
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
               // event(new DesignationPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Designation
     * @throws GeneralException
     */
    public function restore($id): Designation
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
          //  event(new DesignationRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
