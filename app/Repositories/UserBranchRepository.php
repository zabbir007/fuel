<?php

namespace App\Repositories;

use App\Events\Branch\BranchCreated;
use App\Events\Branch\BranchDeactivated;
use App\Events\Branch\BranchDeleted;
use App\Events\Branch\BranchPermanentlyDeleted;
use App\Events\Branch\BranchReactivated;
use App\Events\Branch\BranchRestored;
use App\Events\Branch\BranchUpdated;
use App\Exceptions\GeneralException;
use App\Models\Branch;
use App\Models\User;
use App\Models\UserBranch;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class UserBranchRepository
 *
 * @package App\Repositories
 */
class UserBranchRepository extends BaseRepository
{
    /**
     * BranchRepository constructor.
     *
     * @param  UserBranch  $model
     */
    public function __construct(UserBranch $model)
    {
        $this->model = $model;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll(){
        return app(UserRepository::class)
            ->getActive()
        //    ->whereIn('id', $this->model->pluck('user_id'))
            ->load(['branches']);
    }

    /**
     * @param  array  $data
     *
     * @return Branch
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $module = app(User::class)->find($data['user_id'])->branches()->sync($data['branch_ids']);

            if ($module) {

                event(new BranchCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Branch
     * @throws Throwable
     */
    public function update($id, array $data)
    {
       // $module = $this->getById($id);

        return DB::transaction(function () use ($data) {
            $module = app(User::class)->find($data['user_id'])->branches()->sync($data['branch_ids']);

            if ($module) {

                event(new BranchUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

}
