<?php

namespace App\Repositories;

use App\Events\LogbookEntry\LogbookEntryCreated;
use App\Events\LogbookEntry\LogbookEntryDeleted;
use App\Events\LogbookEntry\LogbookEntryRestored;
use App\Events\LogbookEntry\LogbookEntryUpdated;
use App\Exceptions\GeneralException;
use App\Exceptions\RecordNotFoundException;
use App\Models\LogbookEntry;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Throwable;
use Yajra\DataTables\DataTables;

/**
 * Class LogbookEntryRepository.
 */
class LogbookEntryRepository extends BaseRepository
{
    /**
     * LogbookEntryRepository constructor.
     *
     * @param  LogbookEntry  $model
     */
    public function __construct(LogbookEntry $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model->latest()->with(['branch', 'vehicle','driver','fuel']);

//        if ($status === 'true') {
//            $dataTableQuery = $dataTableQuery->active(true);
//        }
//
//        if ($status === 'false') {
//            $dataTableQuery = $dataTableQuery->active(false);
//        }
//
//        if ($trashed === 'true') {
//            $dataTableQuery = $dataTableQuery->onlyTrashed();
//        }

        return DataTables::of($dataTableQuery)
            ->addIndexColumn()
            ->escapeColumns(['id'])
            ->addColumn('row_checkbox', function ($q){
                return !$q->isApproved() ? '<input type="checkbox" name="trans_id" value="'.$q->id.'" />' : '<input type="checkbox" name="trans_id" disabled readonly value="'.$q->id.'" />';
            })

//            ->editColumn('employee_id', function ($q) {
//                return $q->employee_id == null ? 'N/A' : $q->employee_id;
//            })
//            ->addColumn('designation', function ($q) {
//                return $q->designation->name;
//            })
//            ->addColumn('branches', function ($q) {
//                return $q->user->branches->pluck('name')->implode(', ');
//            })
            ->editColumn('logbook_opening', function ($q) {
                return number_format($q->logbook_opening, 2) . ' KM';
            })
            ->editColumn('logbook_closing', function ($q) {
                return number_format($q->logbook_closing, 2) . ' KM';
            })
            ->editColumn('fuel_consumption', function ($q) {
                return number_format($q->fuel_consumption, 2) . ' Ltr';
            })
            ->editColumn('fuel_rate', function ($q) {
                return number_format($q->fuel_rate, 2) . ' Tk.';
            })
            ->editColumn('fuel_cost', function ($q) {
                return number_format($q->fuel_cost, 2) . ' Tk.';
            })
            ->addColumn('status_label', function ($q) {
                return $q->status_label;
            })
            ->addColumn('approve_label', function ($q) {
                return $q->approve_label;
            })
            ->addColumn('actions', function ($q) {
                return $q->actions;
            })
            ->editColumn('created_at', function ($q) {
                return $q->created_at == null ? 'N/A' : $q->created_at->format('d M Y');
            })
            ->editColumn('updated_at', function ($q) {
                return $q->updated_at == null ? null : $q->updated_at->diffForHumans();
            })
            ->rawColumns(['row_checkbox','actions','approve_label'])
            ->make(true);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getById($id)
    {
        return parent::getById($id)->load(['branch','employee','vehicle','driver','fuel']);
    }

    /**
     * @param  string  $orderBy
     * @param  string  $orderType
     * @return mixed
     */
    public function toSelect($orderBy = 'order', $orderType = 'asc')
    {
        return $this->model->active()
            ->select(['id', 'name', 'license_plate_no'])
            ->orderBy($orderBy, $orderType)
            ->get()
            ->pluck('format_name', 'id');
    }

    /**
     * @param array $data
     * @return int
     */
    public function postTransactions(array $data){
        return $this->model
            ->whereIn('id', $data['transIds'])
            ->update(['status' => 1, 'posted' => 1]);
    }

    /**
     * @param array $data
     * @return int
     */
    public function approveTransactions(array $data){
        return $this->model
            ->whereIn('id', $data['transIds'])
            ->update(['status' => 2, 'approved' => 1]);
    }

    /**
     * @param  array  $data
     *
     * @return LogbookEntry
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): LogbookEntry
    {
        return DB::transaction(function () use ($data) {

            //branch
            $branchRepository = app(BranchRepository::class);

            $transSl = $this->count();

            $row = [
                'company_id' => $data['company_id'] ?? request()->user()->company_id,
                'sl' => $transSl + 1,
                'trans_date' => Carbon::parse($data['entry_date'])->toDateString(),

                'branch_id' => $data['branch_id'],
                'branch_name' => $branchRepository->getById($data['branch_id'])->name,

                'employee_id' => $data['employee_id'],
                'employee_name' => $data['employee_name'],

                'vehicle_id' => $data['vehicle_id'],
                'vehicle_name' => $data['vehicle_name'],
                'vehicle_license_no' => $data['vehicle_license_no'],
                'vehicle_mnf_yr' => $data['manufacturing_year'],
                'vehicle_purchase_yr' => $data['purchase_year'],

                'vehicle_lifetime' => $data['manufacturing_year'],
                'vehicle_used' => $data['purchase_year'],

                'driver_id' => $data['driver_id'],
                'driver_name' => $data['driver_name'],
                'driver_take_ovr_dt' => Carbon::parse($data['take_over_date']),
                'driver_hand_ovr_dt' => Carbon::parse($data['hand_over_date']),

                'rkm_data' => $data['vts_data'],
                'logbook_opening' => $data['opening_logbook'],
                'logbook_closing' => $data['closing_logbook'],
                'logbook_running' => $data['total_running'],

                'fuel_id' => $data['fuel_id'],
                'fuel_name' => $data['fuel_name'],
                'fuel_consumption' => $data['actual_consumption'],
                'fuel_rate' => $data['fuel_rate'],
                'fuel_cost' => $data['fuel_cost'],
                'std_fuel_consumption' => $data['standard_rkm'],

                'approved' => 0,
                'approved_by' => null,
                'status' => 0,
                'posted' => false,
                'order' => null,

                'year' => Carbon::parse($data['entry_date'])->year,
                'month' => Carbon::parse($data['entry_date'])->month,
                'month_name' => Carbon::parse($data['entry_date'])->format('m'),
            ];

            $module = $this->model::create($row);

            if ($module) {
                event(new LogbookEntryCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param  array  $data
     *
     * @return LogbookEntry
     * @throws Throwable
     */
    public function update($id, array $data): LogbookEntry
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            //branch
            $branchRepository = app(BranchRepository::class);

            $transSl = $this->count();

            $row = [
                'company_id' => $data['company_id'] ?? request()->user()->company_id,
//                'sl' => $transSl + 1,
//                'trans_date' => Carbon::parse($data['entry_date'])->toDateString(),

                'branch_id' => $data['branch_id'],
                'branch_name' => $branchRepository->getById($data['branch_id'])->name,

                'employee_id' => $data['employee_id'],
                'employee_name' => $data['employee_name'],

                'vehicle_id' => $data['vehicle_id'],
                'vehicle_name' => $data['vehicle_name'],
                'vehicle_license_no' => $data['vehicle_license_no'],
                'vehicle_mnf_yr' => $data['manufacturing_year'],
                'vehicle_purchase_yr' => $data['purchase_year'],

                'vehicle_lifetime' => $data['manufacturing_year'],
                'vehicle_used' => $data['purchase_year'],

                'driver_id' => $data['driver_id'],
                'driver_name' => $data['driver_name'],
                'driver_take_ovr_dt' => Carbon::parse($data['take_over_date']),
                'driver_hand_ovr_dt' => Carbon::parse($data['hand_over_date']),

                'rkm_data' => $data['vts_data'],
                'logbook_opening' => $data['opening_logbook'],
                'logbook_closing' => $data['closing_logbook'],
                'logbook_running' => $data['total_running'],

                'fuel_id' => $data['fuel_id'],
                'fuel_name' => $data['fuel_name'],
                'fuel_consumption' => $data['actual_consumption'],
                'fuel_rate' => $data['fuel_rate'],
                'fuel_cost' => $data['fuel_cost'],
                'std_fuel_consumption' => $data['standard_rkm'],

                'approved' => 0,
                'approved_by' => null,
                'status' => 0,
                'posted' => false,
                'order' => null,

//                'year' => Carbon::parse($data['entry_date'])->year,
//                'month' => Carbon::parse($data['entry_date'])->month,
//                'month_name' => Carbon::parse($data['entry_date'])->format('m'),
            ];

            $module = $module::update($row);

            if ($module) {
                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }


    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new LogbookEntryDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return LogbookEntry
     * @throws GeneralException
     */
    public function restore($id): LogbookEntry
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new LogbookEntryRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
