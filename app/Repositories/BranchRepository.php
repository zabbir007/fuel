<?php

namespace App\Repositories;

use App\Events\Branch\BranchCreated;
use App\Events\Branch\BranchDeactivated;
use App\Events\Branch\BranchDeleted;
use App\Events\Branch\BranchPermanentlyDeleted;
use App\Events\Branch\BranchReactivated;
use App\Events\Branch\BranchRestored;
use App\Events\Branch\BranchUpdated;
use App\Exceptions\GeneralException;
use App\Models\Branch;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class BranchRepository.
 */
class BranchRepository extends BaseRepository
{
    /**
     * BranchRepository constructor.
     *
     * @param  Branch  $model
     */
    public function __construct(Branch $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'code',
                'name',
                'short_name',
                'active',
                'created_at',
                'updated_at'
            ]);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the BranchGlobalScope trait
        return $dataTableQuery;
    }


    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return Branch
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): Branch
    {
        return DB::transaction(function () use ($data) {
            $module = $this->model::create([
                'code' => $data['code'],
                'name' => $data['name'],
                'short_name' => $data['short_name'],
                'address1' => $data['address1'],
                'address2' => $data['address2'] ?? null,
                'address3' => $data['address3'] ?? null,
                'address4' => $data['address4'] ?? null,
                'email' => $data['email'] ?? null,
                'location' => $data['location'] ?? null,
                'bic_id' => $data['bic_id'] ?? null,
                'dbic_id' => $data['dbic_id'] ?? null,
                'aic_id' => $data['aic_id'] ?? null,

                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

                event(new BranchCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return Branch
     * @throws Throwable
     */
    public function update($id, array $data): Branch
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'code' => $data['code'],
                'name' => $data['name'],
                'short_name' => $data['short_name'],
                'address1' => $data['address1'],
                'address2' => $data['address2'] ?? null,
                'address3' => $data['address3'] ?? null,
                'address4' => $data['address4'] ?? null,
                'email' => $data['email'] ?? null,
                'location' => $data['location'] ?? null,
                'bic_id' => $data['bic_id'] ?? null,
                'dbic_id' => $data['dbic_id'] ?? null,
                'aic_id' => $data['aic_id'] ?? null,

                'order' => $data['order'],
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new BranchUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new BranchDeactivated($module));
                break;
            case 1:
                event(new BranchReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new BranchDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Branch
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): Branch
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new BranchPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return Branch
     * @throws GeneralException
     */
    public function restore($id): Branch
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new BranchRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
