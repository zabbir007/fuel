<?php

namespace App\Repositories;

use App\Events\VehicleDocument\VehicleDocumentCreated;
use App\Events\VehicleDocument\VehicleDocumentDeactivated;
use App\Events\VehicleDocument\VehicleDocumentDeleted;
use App\Events\VehicleDocument\VehicleDocumentPermanentlyDeleted;
use App\Events\VehicleDocument\VehicleDocumentReactivated;
use App\Events\VehicleDocument\VehicleDocumentRestored;
use App\Events\VehicleDocument\VehicleDocumentUpdated;
use App\Exceptions\GeneralException;
use App\Models\VehicleDocument;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class VehicleDocumentRepository.
 */
class VehicleDocumentRepository extends BaseRepository
{
    /**
     * VehicleDocumentRepository constructor.
     *
     * @param  VehicleDocument  $model
     */
    public function __construct(VehicleDocument $model)
    {
        $this->model = $model;
    }

    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        $dataTableQuery = $this->model
            ->select([
                'id',
                'user_id',
                'employee_id',
                'name',
                'designation',
                'active',
                'created_at',
                'updated_at'
            ])
        ->with(['user','user.branches']);

        if ($status === 'true') {
            return $dataTableQuery->active(true);
        }

        if ($status === 'false') {
            return $dataTableQuery->active(false);
        }

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the VehicleDocumentScope trait
        return $dataTableQuery;
    }


    /**
     * @return mixed
     */
    public function toSelect()
    {
        return $this->model->active()
            ->select(['id', 'name'])
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param  array  $data
     *
     * @return VehicleDocument
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): VehicleDocument
    {
        return DB::transaction(function () use ($data) {

            $module = $this->model::create([
                'vehicle_id' => $data['vehicle_id'],
                'document_type' => $data['document_type'],

                'document_no' => $data['document_no'],

                'issue_date' => isset($data['issue_date']) ?  Carbon::parse($data['issue_date'])->toDateString() : null,
                'expiry_date' => isset($data['expiry_date']) ?  Carbon::parse($data['expiry_date'])->toDateString() : null,

                'issuing_authority' => $data['issuing_authority'] ?? null,

                'order' => $data['order'] ?? null,
                'active' => isset($data['active']) && $data['active'] === '1'
            ]);

            if ($module) {

                event(new VehicleDocumentCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return VehicleDocument
     * @throws Throwable
     */
    public function update($id, array $data): VehicleDocument
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($module, $data) {
            if ($module->update([
                'vehicle_id' => $data['vehicle_id'],
                'document_type' => $data['document_type'],

                'document_no' => $data['document_no'],

                'issue_date' => isset($data['issue_date']) ?  Carbon::parse($data['issue_date'])->toDateString() : null,
                'expiry_date' => isset($data['expiry_date']) ?  Carbon::parse($data['expiry_date'])->toDateString() : null,

                'issuing_authority' => $data['issuing_authority'] ?? null,

                'order' => $data['order'] ?? null,
                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new VehicleDocumentUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

    /**
     * @param      $id
     * @param      $status
     *
     * @return Model
     * @throws GeneralException
     */
    public function mark($id, $status): Model
    {
        $module = $this->getById($id);

        $module->active = $status;

        switch ($status) {
            case 0:
                event(new VehicleDocumentDeactivated($module));
                break;
            case 1:
                event(new VehicleDocumentReactivated($module));
                break;
        }

        if ($module->save()) {
            return $module;
        }

        throw new GeneralException('There was a problem updating this record. Please try again.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Throwable
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $module_name_singular = $this->getById($id);

            if ($this->deleteById($id)) {
                event(new VehicleDocumentDeleted($module_name_singular));
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return VehicleDocument
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id): VehicleDocument
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record must be deleted first before it can be destroyed permanently.');
        }

        return DB::transaction(function () use ($module) {
            if ($module->forceDelete()) {
                event(new VehicleDocumentPermanentlyDeleted($module));

                return $module;
            }

            throw new GeneralException('There was a problem deleting this record. Please try again.');
        });
    }

    /**
     * @param $id
     * @return VehicleDocument
     * @throws GeneralException
     */
    public function restore($id): VehicleDocument
    {
        $module = $this->model::withTrashed()->where('id', $id)->first();

        if ($module->deleted_at === null) {
            throw new GeneralException('This record is not deleted so it can not be restored.');
        }

        if ($module->restore()) {
            event(new VehicleDocumentRestored($module));

            return $module;
        }

        throw new GeneralException('There was a problem restoring this record. Please try again.');
    }
}
