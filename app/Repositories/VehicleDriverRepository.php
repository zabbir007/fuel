<?php

namespace App\Repositories;

use App\Events\VehicleDriver\VehicleDriverCreated;
use App\Events\VehicleDriver\VehicleDriverDeactivated;
use App\Events\VehicleDriver\VehicleDriverDeleted;
use App\Events\VehicleDriver\VehicleDriverPermanentlyDeleted;
use App\Events\VehicleDriver\VehicleDriverReactivated;
use App\Events\VehicleDriver\VehicleDriverRestored;
use App\Events\VehicleDriver\VehicleDriverUpdated;
use App\Exceptions\GeneralException;
use App\Models\VehicleDriver;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class VehicleDriverRepository
 *
 * @package App\Repositories
 */
class VehicleDriverRepository extends BaseRepository
{
    /**
     * VehicleDriverRepository constructor.
     *
     * @param  VehicleDriver  $model
     */
    public function __construct(VehicleDriver $model)
    {
        $this->model = $model;
    }


    /**
     * @return Builder[]|Collection
     */
    public function getAll(){
        return $this->model
            ->archived(0)
        //    ->whereIn('id', $this->model->pluck('user_id'))
            ->with(['vehicle','driver','branch'])
            //->groupBy('vehicle_id')
            ->get();
    }

    /**
     * @param  array  $data
     *
     * @return VehicleDriver
     * @throws Throwable
     * @throws Exception
     */
    public function create(array $data): VehicleDriver
    {
        return DB::transaction(function () use ($data) {

            //check if record already exists
            $exists = $this->model->where('vehicle_id', $data['vehicle_id'])
                ->where('branch_id', $data['branch_id'])
                ->where('driver_id', $data['driver_id'])
                ->exists();

            if ($exists){
                $archived = 1;
            }
            else{
                $archived = 0;
            }

            $module = $this->model::create([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'vehicle_id' => $data['vehicle_id'],
                'branch_id' => $data['branch_id'],
                'driver_id' => $data['driver_id'],

                'take_over' => isset($data['take_over']) ? Carbon::parse($data['take_over'])->toDateString() : null,
                'hand_over' => isset($data['hand_over']) ? Carbon::parse($data['hand_over'])->toDateString() : null,

                'order' => $data['order'] ?? null,

                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'archived' => $archived,
                'active' => 1
            ]);

            if ($module) {

                event(new VehicleDriverCreated($module));

                return $module;
            }

            throw new GeneralException('There was a problem creating this record. Please try again.');
        });
    }

    /**
     * @param         $id
     * @param  array  $data
     *
     * @return VehicleDriver
     * @throws Throwable
     */
    public function update($id, array $data)
    {
        $module = $this->getById($id);

        return DB::transaction(function () use ($data, $module) {
            if ($module->update([
                'company_id' => $data['company_id'] ?? request()->user()->company_id,

                'vehicle_id' => $data['vehicle_id'],
                'branch_id' => $data['branch_id'],
                'driver_id' => $data['driver_id'],

                'take_over' => isset($data['take_over']) ? Carbon::parse($data['take_over'])->toDateString() : null,
                'hand_over' => isset($data['hand_over']) ? Carbon::parse($data['hand_over'])->toDateString() : null,

                'order' => $data['order'] ?? null,

                'custom1' => $data['custom1'] ?? null,
                'custom2' => $data['custom2'] ?? null,
                'custom3' => $data['custom3'] ?? null,

                'active' => isset($data['active']) && $data['active'] === '1'
            ])) {

                event(new VehicleDriverUpdated($module));

                return $module;
            }

            throw new GeneralException('There was a problem updating this record. Please try again.');
        });
    }

}
