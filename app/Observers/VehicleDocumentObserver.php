<?php

namespace App\Observers;


use App\Models\VehicleDocument;
use App\Repositories\BranchRepository;
use App\Repositories\VehicleDocumentRepository;

/**
 * Class VehicleDocumentObserver.
 */
class VehicleDocumentObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(VehicleDocumentRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  VehicleDocument  $model
     */
    public function created(VehicleDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  VehicleDocument  $model
     */
    public function updated(VehicleDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  VehicleDocument  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  VehicleDocument  $model
     */
    public function deleted(VehicleDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  VehicleDocument  $model
     */
    public function restored(VehicleDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  VehicleDocument  $model
     */
    public function forceDeleted(VehicleDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
