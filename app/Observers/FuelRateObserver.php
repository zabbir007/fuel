<?php

namespace App\Observers;


use App\Models\FuelRate;
use App\Repositories\BranchRepository;
use App\Repositories\FuelRateRepository;

/**
 * Class FuelRateObserver.
 */
class FuelRateObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(FuelRateRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  FuelRate  $model
     */
    public function created(FuelRate $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  FuelRate  $model
     */
    public function updated(FuelRate $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  FuelRate  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  FuelRate  $model
     */
    public function deleted(FuelRate $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  FuelRate  $model
     */
    public function restored(FuelRate $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  FuelRate  $model
     */
    public function forceDeleted(FuelRate $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
