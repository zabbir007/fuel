<?php

namespace App\Observers;


use App\Models\LogbookEntry;
use App\Repositories\BranchRepository;
use App\Repositories\LogbookEntryRepository;

/**
 * Class LogbookEntryObserver.
 */
class LogbookEntryObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(LogbookEntryRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  LogbookEntry  $model
     */
    public function created(LogbookEntry $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  LogbookEntry  $model
     */
    public function updated(LogbookEntry $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  LogbookEntry  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  LogbookEntry  $model
     */
    public function deleted(LogbookEntry $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  LogbookEntry  $model
     */
    public function restored(LogbookEntry $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  LogbookEntry  $model
     */
    public function forceDeleted(LogbookEntry $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
