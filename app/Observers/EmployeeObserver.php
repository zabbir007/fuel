<?php

namespace App\Observers;


use App\Models\Employee;
use App\Repositories\BranchRepository;
use App\Repositories\EmployeeRepository;

/**
 * Class EmployeeObserver.
 */
class EmployeeObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(EmployeeRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Employee  $model
     */
    public function created(Employee $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Employee  $model
     */
    public function updated(Employee $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Employee  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Employee  $model
     */
    public function deleted(Employee $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Employee  $model
     */
    public function restored(Employee $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Employee  $model
     */
    public function forceDeleted(Employee $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
