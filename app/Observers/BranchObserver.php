<?php

namespace App\Observers;


use App\Models\Branch;
use App\Repositories\BranchRepository;

/**
 * Class BranchObserver.
 */
class BranchObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(BranchRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Branch  $model
     */
    public function created(Branch $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Branch  $model
     */
    public function updated(Branch $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Branch  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Branch  $model
     */
    public function deleted(Branch $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Branch  $model
     */
    public function restored(Branch $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Branch  $model
     */
    public function forceDeleted(Branch $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
