<?php

namespace App\Observers;


use App\Models\VehicleDriver;
use App\Repositories\BranchRepository;
use App\Repositories\VehicleDriverRepository;

/**
 * Class VehicleDriverObserver.
 */
class VehicleDriverObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(VehicleDriverRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  VehicleDriver  $model
     */
    public function created(VehicleDriver $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $lastOrder = app(VehicleDriverRepository::class)->count();

        $model->updated_by = auth()->id();
        $model->order = $model->order == null ? $lastOrder+1 :  $model->order;
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  VehicleDriver  $model
     */
    public function updated(VehicleDriver $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  VehicleDriver  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  VehicleDriver  $model
     */
    public function deleted(VehicleDriver $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  VehicleDriver  $model
     */
    public function restored(VehicleDriver $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  VehicleDriver  $model
     */
    public function forceDeleted(VehicleDriver $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
