<?php

namespace App\Observers;


use App\Models\VehicleHistory;
use App\Repositories\BranchRepository;
use App\Repositories\VehicleHistoryRepository;

/**
 * Class VehicleHistoryObserver.
 */
class VehicleHistoryObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(VehicleHistoryRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  VehicleHistory  $model
     */
    public function created(VehicleHistory $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  VehicleHistory  $model
     */
    public function updated(VehicleHistory $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  VehicleHistory  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  VehicleHistory  $model
     */
    public function deleted(VehicleHistory $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  VehicleHistory  $model
     */
    public function restored(VehicleHistory $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  VehicleHistory  $model
     */
    public function forceDeleted(VehicleHistory $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
