<?php

namespace App\Observers;


use App\Models\DriverDocument;
use App\Repositories\BranchRepository;
use App\Repositories\DriverDocumentRepository;

/**
 * Class DriverDocumentObserver.
 */
class DriverDocumentObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(DriverDocumentRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  DriverDocument  $model
     */
    public function created(DriverDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  DriverDocument  $model
     */
    public function updated(DriverDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  DriverDocument  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  DriverDocument  $model
     */
    public function deleted(DriverDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  DriverDocument  $model
     */
    public function restored(DriverDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  DriverDocument  $model
     */
    public function forceDeleted(DriverDocument $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
