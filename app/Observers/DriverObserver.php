<?php

namespace App\Observers;


use App\Models\Driver;
use App\Repositories\BranchRepository;
use App\Repositories\DriverRepository;

/**
 * Class DriverObserver.
 */
class DriverObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(DriverRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Driver  $model
     */
    public function created(Driver $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Driver  $model
     */
    public function updated(Driver $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Driver  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Driver  $model
     */
    public function deleted(Driver $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Driver  $model
     */
    public function restored(Driver $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Driver  $model
     */
    public function forceDeleted(Driver $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
