<?php

namespace App\Observers;


use App\Models\Vehicle;
use App\Repositories\BranchRepository;
use App\Repositories\VehicleRepository;

/**
 * Class VehicleObserver.
 */
class VehicleObserver
{
    /**
     * Handle the model "retrieved" event.
     *
     * @param $model
     */
    public function retrieved($model): void
    {
//        \Log::info('Retrieved By:'). \Auth::user()->id;
    }

    /**
     * Handle the model group "creating" event.
     *
     * @param $model
     * @throws Exception
     */
    public function creating($model): void
    {
        $lastOrder = app(VehicleRepository::class)->count();
        $model->order = $lastOrder+1;
        $model->created_by = auth()->id();
    }

    /**
     * Handle the model group "created" event.
     *
     * @param  Vehicle  $model
     */
    public function created(Vehicle $model): void
    {
        //
    }

    /**
     * Handle the model group "updating" event.
     *
     * @param $model
     */
    public function updating($model): void
    {
        $model->updated_by = auth()->id();
    }

    /**
     * Handle the model group "updated" event.
     *
     * @param  Vehicle  $model
     */
    public function updated(Vehicle $model): void
    {
        //
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Vehicle  $model
     */
    public function deleting($model): void
    {
        $model->deleted_by = auth()->id();
    }

    /**
     * Handle the model group "deleted" event.
     *
     * @param  Vehicle  $model
     */
    public function deleted(Vehicle $model): void
    {
        //
    }

    /**
     * Handle the model group "restored" event.
     *
     * @param  Vehicle  $model
     */
    public function restored(Vehicle $model): void
    {
        //
    }

    /**
     * Handle the model group "force deleted" event.
     *
     * @param  Vehicle  $model
     */
    public function forceDeleted(Vehicle $model): void
    {
        //
    }

    /**
     * Handle the model group "removing" event.
     *
     * @param $model
     */
    public function removing($model): void
    {

    }

    /**
     * Handle the model group "removed" event.
     *
     * @param $model
     */
    public function removed($model): void
    {
        //
    }
}
