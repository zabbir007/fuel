-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2021 at 08:02 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fuel`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'system', 'created', 1, 'App\\Models\\User', NULL, NULL, '[]', '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(2, 'system', 'created', 2, 'App\\Models\\User', NULL, NULL, '[]', '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(3, 'system', 'created', 3, 'App\\Models\\User', NULL, NULL, '[]', '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(4, 'system', 'created', 1, 'App\\Models\\Company', NULL, NULL, '[]', '2020-08-03 16:41:45', '2020-08-03 16:41:45'),
(5, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 16:41:58', '2020-08-03 16:41:58'),
(6, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 16:41:59', '2020-08-03 16:41:59'),
(7, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 16:41:59', '2020-08-03 16:41:59'),
(8, 'system', 'updated', 1, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-08-03 17:24:24', '2020-08-03 17:24:24'),
(10, 'system', 'created', 5, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 18:44:21', '2020-08-03 18:44:21'),
(11, 'system', 'created', 1, 'App\\Models\\Employee', 1, 'App\\Models\\User', '[]', '2020-08-03 18:44:21', '2020-08-03 18:44:21'),
(12, 'system', 'created', 1, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-03 20:07:06', '2020-08-03 20:07:06'),
(13, 'system', 'updated', 1, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-03 20:12:57', '2020-08-03 20:12:57'),
(14, 'system', 'updated', 1, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-03 20:14:09', '2020-08-03 20:14:09'),
(17, 'system', 'created', 4, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-03 22:36:10', '2020-08-03 22:36:10'),
(18, 'system', 'created', 1, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-03 22:36:10', '2020-08-03 22:36:10'),
(19, 'system', 'created', 2, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-03 22:36:10', '2020-08-03 22:36:10'),
(20, 'system', 'created', 3, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-03 22:49:19', '2020-08-03 22:49:19'),
(21, 'system', 'created', 4, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-03 22:49:19', '2020-08-03 22:49:19'),
(22, 'system', 'updated', 1, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-03 22:55:38', '2020-08-03 22:55:38'),
(23, 'system', 'updated', 2, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-03 22:55:38', '2020-08-03 22:55:38'),
(24, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 16:33:57', '2020-08-04 16:33:57'),
(25, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 16:34:03', '2020-08-04 16:34:03'),
(26, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 16:34:03', '2020-08-04 16:34:03'),
(28, 'system', 'created', 2, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-04 18:30:19', '2020-08-04 18:30:19'),
(29, 'system', 'created', 1, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 18:51:58', '2020-08-04 18:51:58'),
(30, 'system', 'updated', 1, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-08-04 21:17:02', '2020-08-04 21:17:02'),
(31, 'system', 'created', 1, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(32, 'system', 'created', 2, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(33, 'system', 'created', 3, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(34, 'system', 'created', 4, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:28:25', '2020-08-05 16:28:25'),
(35, 'system', 'created', 5, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(36, 'system', 'created', 6, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(37, 'system', 'created', 7, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(38, 'system', 'created', 8, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(47, 'system', 'updated', 5, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(48, 'system', 'updated', 6, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(49, 'system', 'updated', 7, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(50, 'system', 'updated', 8, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(51, 'system', 'created', 3, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-05 22:16:07', '2020-08-05 22:16:07'),
(52, 'system', 'created', 2, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-08-05 22:16:33', '2020-08-05 22:16:33'),
(53, 'system', 'created', 3, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-08-05 22:17:48', '2020-08-05 22:17:48'),
(54, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:36:23', '2020-08-05 22:36:23'),
(55, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:36:29', '2020-08-05 22:36:29'),
(56, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:37:42', '2020-08-05 22:37:42'),
(57, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:37:51', '2020-08-05 22:37:51'),
(58, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:37:51', '2020-08-05 22:37:51'),
(59, 'system', 'created', 6, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:41:05', '2020-08-05 22:41:05'),
(60, 'system', 'created', 59, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-05 22:43:48', '2020-08-05 22:43:48'),
(61, 'system', 'created', 60, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-05 22:45:25', '2020-08-05 22:45:25'),
(62, 'system', 'created', 61, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-05 22:45:35', '2020-08-05 22:45:35'),
(63, 'system', 'created', 62, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-05 22:45:44', '2020-08-05 22:45:44'),
(64, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-05 22:46:47', '2020-08-05 22:46:47'),
(65, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-05 22:46:54', '2020-08-05 22:46:54'),
(66, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-05 22:46:54', '2020-08-05 22:46:54'),
(67, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-05 23:00:24', '2020-08-05 23:00:24'),
(68, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-05 23:00:34', '2020-08-05 23:00:34'),
(69, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-05 23:00:35', '2020-08-05 23:00:35'),
(70, 'system', 'created', 9, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(71, 'system', 'created', 10, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(72, 'system', 'created', 11, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(73, 'system', 'created', 12, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(74, 'system', 'updated', 9, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(75, 'system', 'updated', 10, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(76, 'system', 'updated', 11, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(77, 'system', 'updated', 12, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(78, 'system', 'created', 13, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(79, 'system', 'created', 14, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(80, 'system', 'created', 15, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(81, 'system', 'created', 16, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(82, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 17:52:58', '2020-08-07 17:52:58'),
(83, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 17:53:11', '2020-08-07 17:53:11'),
(84, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 17:53:11', '2020-08-07 17:53:11'),
(85, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 18:43:03', '2020-08-07 18:43:03'),
(86, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-07 18:43:11', '2020-08-07 18:43:11'),
(87, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-07 18:43:11', '2020-08-07 18:43:11'),
(88, 'system', 'created', 64, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-07 18:43:37', '2020-08-07 18:43:37'),
(89, 'system', 'created', 65, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-07 18:43:51', '2020-08-07 18:43:51'),
(90, 'system', 'created', 66, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-07 18:44:01', '2020-08-07 18:44:01'),
(91, 'system', 'created', 67, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-07 18:44:10', '2020-08-07 18:44:10'),
(92, 'system', 'created', 68, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-07 18:44:18', '2020-08-07 18:44:18'),
(93, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-07 18:46:23', '2020-08-07 18:46:23'),
(94, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 18:46:29', '2020-08-07 18:46:29'),
(95, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 18:46:29', '2020-08-07 18:46:29'),
(96, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 18:55:34', '2020-08-07 18:55:34'),
(97, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-07 18:55:40', '2020-08-07 18:55:40'),
(98, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-07 18:55:40', '2020-08-07 18:55:40'),
(99, 'system', 'created', 69, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-07 18:55:57', '2020-08-07 18:55:57'),
(100, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-07 18:56:26', '2020-08-07 18:56:26'),
(101, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 18:56:34', '2020-08-07 18:56:34'),
(102, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 18:56:34', '2020-08-07 18:56:34'),
(103, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 18:51:27', '2020-08-08 18:51:27'),
(104, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 18:51:48', '2020-08-08 18:51:48'),
(105, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 18:51:49', '2020-08-08 18:51:49'),
(108, 'system', 'created', 6, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-08 23:23:50', '2020-08-08 23:23:50'),
(109, 'system', 'created', 1, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-08 23:23:50', '2020-08-08 23:23:50'),
(110, 'system', 'created', 2, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-08 23:23:50', '2020-08-08 23:23:50'),
(111, 'system', 'updated', 1, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-08 23:51:50', '2020-08-08 23:51:50'),
(112, 'system', 'created', 3, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-08 23:51:50', '2020-08-08 23:51:50'),
(113, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-11 21:39:06', '2020-08-11 21:39:06'),
(114, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-11 21:39:06', '2020-08-11 21:39:06'),
(115, 'system', 'created', 7, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-15 16:47:42', '2020-08-15 16:47:42'),
(116, 'system', 'created', 2, 'App\\Models\\Employee', 1, 'App\\Models\\User', '[]', '2020-08-15 16:47:42', '2020-08-15 16:47:42'),
(117, 'system', 'created', 17, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(118, 'system', 'created', 18, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(119, 'system', 'created', 19, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(120, 'system', 'created', 20, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(121, 'system', 'updated', 17, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(122, 'system', 'updated', 18, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(123, 'system', 'updated', 19, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(124, 'system', 'updated', 20, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(125, 'system', 'created', 7, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(126, 'system', 'created', 2, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(127, 'system', 'created', 3, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(128, 'system', 'created', 4, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(129, 'system', 'created', 4, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(132, 'system', 'created', 21, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(133, 'system', 'created', 22, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(134, 'system', 'created', 23, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(135, 'system', 'created', 24, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(136, 'system', 'updated', 2, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-15 17:33:15', '2020-08-15 17:33:15'),
(137, 'system', 'created', 5, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-15 17:33:15', '2020-08-15 17:33:15'),
(138, 'system', 'created', 1, 'App\\Models\\LogbookEntry', 1, 'App\\Models\\User', '[]', '2020-08-15 17:42:22', '2020-08-15 17:42:22'),
(139, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:33:03', '2020-09-07 22:33:03'),
(140, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:33:03', '2020-09-07 22:33:03'),
(141, 'system', 'updated', 5, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:34:45', '2020-09-07 22:34:45'),
(142, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:34:54', '2020-09-07 22:34:54'),
(143, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-07 22:34:59', '2020-09-07 22:34:59'),
(144, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-07 22:34:59', '2020-09-07 22:34:59'),
(145, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-07 22:35:33', '2020-09-07 22:35:33'),
(146, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:35:37', '2020-09-07 22:35:37'),
(147, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:35:37', '2020-09-07 22:35:37'),
(148, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:38:07', '2020-09-07 22:38:07'),
(149, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:38:11', '2020-09-07 22:38:11'),
(150, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:38:11', '2020-09-07 22:38:11'),
(151, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-07 22:38:13', '2020-09-07 22:38:13'),
(152, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-07 22:38:19', '2020-09-07 22:38:19'),
(153, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-07 22:38:19', '2020-09-07 22:38:19'),
(154, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:09:27', '2020-09-08 02:09:27'),
(155, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:09:27', '2020-09-08 02:09:27'),
(156, 'system', 'created', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:12:52', '2020-09-08 02:12:52'),
(157, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 02:14:09', '2020-09-08 02:14:09'),
(158, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:14:11', '2020-09-08 02:14:11'),
(159, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:14:11', '2020-09-08 02:14:11'),
(160, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:14:16', '2020-09-08 02:14:16'),
(161, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:14:16', '2020-09-08 02:14:16'),
(162, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:14:20', '2020-09-08 02:14:20'),
(163, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 02:14:34', '2020-09-08 02:14:34'),
(164, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 02:14:35', '2020-09-08 02:14:35'),
(165, 'system', 'created', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 02:17:54', '2020-09-08 02:17:54'),
(166, 'system', 'updated', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:18:46', '2020-09-08 02:18:46'),
(167, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 02:18:52', '2020-09-08 02:18:52'),
(168, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 02:19:03', '2020-09-08 02:19:03'),
(169, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 02:19:26', '2020-09-08 02:19:26'),
(172, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 02:20:18', '2020-09-08 02:20:18'),
(173, 'system', 'updated', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:21:09', '2020-09-08 02:21:09'),
(174, 'system', 'updated', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:21:26', '2020-09-08 02:21:26'),
(175, 'system', 'deleted', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:21:51', '2020-09-08 02:21:51'),
(176, 'system', 'created', 4, 'App\\Models\\Role', 1, 'App\\Models\\User', '[]', '2020-09-08 02:25:06', '2020-09-08 02:25:06'),
(177, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 02:28:42', '2020-09-08 02:28:42'),
(178, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:28:47', '2020-09-08 02:28:47'),
(179, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 02:28:47', '2020-09-08 02:28:47'),
(180, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:00:54', '2020-09-08 04:00:54'),
(181, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:00:55', '2020-09-08 04:00:55'),
(182, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:24:14', '2020-09-08 04:24:14'),
(183, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:24:27', '2020-09-08 04:24:27'),
(184, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:24:27', '2020-09-08 04:24:27'),
(185, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-11 02:38:13', '2020-09-11 02:38:13'),
(186, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-11 02:38:14', '2020-09-11 02:38:14'),
(187, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 02:20:41', '2020-09-19 02:20:41'),
(188, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 02:20:42', '2020-09-19 02:20:42'),
(189, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 02:21:17', '2020-09-19 02:21:17'),
(190, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 02:21:34', '2020-09-19 02:21:34'),
(191, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 02:21:34', '2020-09-19 02:21:34'),
(192, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 02:39:02', '2020-09-19 02:39:02'),
(193, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 22:21:53', '2020-09-19 22:21:53'),
(194, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 22:21:56', '2020-09-19 22:21:56'),
(195, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 22:21:57', '2020-09-19 22:21:57'),
(196, 'system', 'created', 6, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-10-10 22:20:15', '2020-10-10 22:20:15'),
(197, 'system', 'updated', 6, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:22:00', '2020-10-10 22:22:00'),
(198, 'system', 'updated', 6, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:24:38', '2020-10-10 22:24:38'),
(199, 'system', 'updated', 6, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:24:53', '2020-10-10 22:24:53'),
(200, 'system', 'created', 10, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:25:36', '2020-10-10 22:25:36'),
(201, 'system', 'created', 3, 'App\\Models\\Employee', 1, 'App\\Models\\User', '[]', '2020-10-10 22:25:37', '2020-10-10 22:25:37'),
(202, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:27:07', '2020-10-10 22:27:07'),
(203, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-10 22:27:17', '2020-10-10 22:27:17'),
(204, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-10 22:27:17', '2020-10-10 22:27:17'),
(205, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:28:07', '2020-10-10 22:28:07'),
(206, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:28:07', '2020-10-10 22:28:07'),
(207, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:29:13', '2020-10-10 22:29:13'),
(208, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-10 22:29:18', '2020-10-10 22:29:18'),
(209, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-10 22:29:18', '2020-10-10 22:29:18'),
(210, 'system', 'created', 25, 'App\\Models\\FuelRate', 10, 'App\\Models\\User', '[]', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(211, 'system', 'created', 26, 'App\\Models\\FuelRate', 10, 'App\\Models\\User', '[]', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(212, 'system', 'created', 27, 'App\\Models\\FuelRate', 10, 'App\\Models\\User', '[]', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(213, 'system', 'created', 28, 'App\\Models\\FuelRate', 10, 'App\\Models\\User', '[]', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(214, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-10 22:54:20', '2020-10-10 22:54:20'),
(215, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:54:25', '2020-10-10 22:54:25'),
(216, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-10 22:54:26', '2020-10-10 22:54:26'),
(217, 'system', 'created', 2, 'App\\Models\\LogbookEntry', 10, 'App\\Models\\User', '[]', '2020-10-10 22:57:21', '2020-10-10 22:57:21'),
(218, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-11 02:07:04', '2020-10-11 02:07:04'),
(219, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-11 02:07:58', '2020-10-11 02:07:58'),
(220, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-11 02:07:58', '2020-10-11 02:07:58'),
(221, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-10-17 00:54:32', '2020-10-17 00:54:32'),
(222, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-10-17 00:54:33', '2020-10-17 00:54:33'),
(223, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-10-17 00:54:57', '2020-10-17 00:54:57'),
(224, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-17 00:55:02', '2020-10-17 00:55:02'),
(225, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-17 00:55:02', '2020-10-17 00:55:02'),
(226, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-17 00:55:43', '2020-10-17 00:55:43'),
(227, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-10-17 00:55:46', '2020-10-17 00:55:46'),
(228, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-10-17 00:55:47', '2020-10-17 00:55:47'),
(229, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-10-17 00:59:52', '2020-10-17 00:59:52'),
(230, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-17 00:59:57', '2020-10-17 00:59:57'),
(231, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-17 00:59:57', '2020-10-17 00:59:57'),
(232, 'system', 'created', 3, 'App\\Models\\LogbookEntry', 10, 'App\\Models\\User', '[]', '2020-10-17 01:03:24', '2020-10-17 01:03:24'),
(233, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-10-17 01:10:04', '2020-10-17 01:10:04'),
(234, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-17 01:10:09', '2020-10-17 01:10:09'),
(235, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-10-17 01:10:09', '2020-10-17 01:10:09'),
(236, 'system', 'created', 7, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-10-17 01:14:42', '2020-10-17 01:14:42'),
(237, 'system', 'created', 8, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-10-17 01:15:02', '2020-10-17 01:15:02'),
(238, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-11-02 21:22:56', '2020-11-02 21:22:56'),
(239, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-11-02 21:22:56', '2020-11-02 21:22:56'),
(240, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-11-02 21:26:38', '2020-11-02 21:26:38'),
(241, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:26:42', '2020-11-02 21:26:42'),
(242, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:26:42', '2020-11-02 21:26:42'),
(243, 'system', 'created', 4, 'App\\Models\\LogbookEntry', 1, 'App\\Models\\User', '[]', '2020-11-02 21:28:23', '2020-11-02 21:28:23'),
(244, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:30:24', '2020-11-02 21:30:24'),
(245, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:30:29', '2020-11-02 21:30:29'),
(246, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:30:29', '2020-11-02 21:30:29'),
(247, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:30:47', '2020-11-02 21:30:47'),
(248, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-11-02 21:30:51', '2020-11-02 21:30:51'),
(249, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-11-02 21:30:51', '2020-11-02 21:30:51'),
(250, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-11-02 21:32:06', '2020-11-02 21:32:06'),
(251, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:32:14', '2020-11-02 21:32:14'),
(252, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:32:14', '2020-11-02 21:32:14'),
(253, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 21:49:51', '2020-11-02 21:49:51'),
(254, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 23:11:24', '2020-11-02 23:11:24'),
(255, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 23:11:24', '2020-11-02 23:11:24'),
(256, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-02 23:11:33', '2020-11-02 23:11:33'),
(257, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-11-02 23:11:38', '2020-11-02 23:11:38'),
(258, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-11-02 23:11:38', '2020-11-02 23:11:38'),
(259, 'system', 'updated', 10, 'App\\Models\\User', 10, 'App\\Models\\User', '[]', '2020-11-02 23:11:46', '2020-11-02 23:11:46'),
(260, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-03 00:03:51', '2020-11-03 00:03:51'),
(261, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-03 00:03:52', '2020-11-03 00:03:52'),
(262, 'system', 'created', 5, 'App\\Models\\LogbookEntry', 1, 'App\\Models\\User', '[]', '2020-11-03 00:04:57', '2020-11-03 00:04:57'),
(263, 'system', 'created', 5, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-11-03 11:45:03', '2020-11-03 11:45:03'),
(264, 'system', 'created', 4, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-11-03 11:45:59', '2020-11-03 11:45:59'),
(265, 'system', 'created', 3, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-11-03 12:03:56', '2020-11-03 12:03:56'),
(266, 'system', 'updated', 3, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-11-03 12:05:33', '2020-11-03 12:05:33'),
(267, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-11-03 12:05:41', '2020-11-03 12:05:41'),
(268, 'system', 'created', 4, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-11-03 12:06:31', '2020-11-03 12:06:31'),
(269, 'system', 'created', 11, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-04 04:00:16', '2020-11-04 04:00:16'),
(270, 'system', 'updated', 11, 'App\\Models\\User', 11, 'App\\Models\\User', '[]', '2020-11-04 04:04:39', '2020-11-04 04:04:39'),
(271, 'system', 'updated', 11, 'App\\Models\\User', 11, 'App\\Models\\User', '[]', '2020-11-04 04:12:03', '2020-11-04 04:12:03'),
(272, 'system', 'created', 5, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-11-04 04:15:33', '2020-11-04 04:15:33'),
(273, 'system', 'created', 8, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-11-04 05:04:56', '2020-11-04 05:04:56'),
(274, 'system', 'created', 9, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-11-04 05:04:56', '2020-11-04 05:04:56'),
(275, 'system', 'created', 6, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-11-04 05:10:21', '2020-11-04 05:10:21'),
(276, 'system', 'created', 6, 'App\\Models\\LogbookEntry', 11, 'App\\Models\\User', '[]', '2020-11-04 05:14:35', '2020-11-04 05:14:35'),
(277, 'system', 'updated', 11, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-04 05:38:51', '2020-11-04 05:38:51'),
(278, 'system', 'updated', 11, 'App\\Models\\User', 11, 'App\\Models\\User', '[]', '2020-11-04 05:39:36', '2020-11-04 05:39:36'),
(279, 'system', 'updated', 11, 'App\\Models\\User', 11, 'App\\Models\\User', '[]', '2020-11-04 05:41:39', '2020-11-04 05:41:39'),
(280, 'system', 'updated', 17, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(281, 'system', 'updated', 18, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(282, 'system', 'updated', 19, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(283, 'system', 'updated', 20, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(284, 'system', 'updated', 17, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(285, 'system', 'updated', 18, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(286, 'system', 'updated', 19, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(287, 'system', 'updated', 20, 'App\\Models\\FuelRate', 11, 'App\\Models\\User', '[]', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(288, 'system', 'created', 7, 'App\\Models\\LogbookEntry', 1, 'App\\Models\\User', '[]', '2020-11-05 16:23:05', '2020-11-05 16:23:05'),
(289, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-05 16:24:02', '2020-11-05 16:24:02'),
(290, 'system', 'created', 12, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-11-18 04:29:44', '2020-11-18 04:29:44'),
(291, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 04:29:52', '2020-11-18 04:29:52'),
(292, 'system', 'created', 7, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-11-18 04:46:46', '2020-11-18 04:46:46'),
(293, 'system', 'updated', 1, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-11-18 04:47:15', '2020-11-18 04:47:15'),
(294, 'system', 'created', 8, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-11-18 04:47:39', '2020-11-18 04:47:39'),
(295, 'system', 'created', 8, 'App\\Models\\LogbookEntry', 1, 'App\\Models\\User', '[]', '2020-11-18 04:56:58', '2020-11-18 04:56:58'),
(296, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-11-18 05:06:36', '2020-11-18 05:06:36'),
(297, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 05:08:51', '2020-11-18 05:08:51'),
(298, 'system', 'created', 33, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-18 05:14:22', '2020-11-18 05:14:22'),
(299, 'system', 'created', 13, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 05:19:15', '2020-11-18 05:19:15'),
(300, 'system', 'created', 4, 'App\\Models\\Employee', 12, 'App\\Models\\User', '[]', '2020-11-18 05:19:15', '2020-11-18 05:19:15'),
(301, 'system', 'updated', 4, 'App\\Models\\Employee', 12, 'App\\Models\\User', '[]', '2020-11-18 05:20:00', '2020-11-18 05:20:00'),
(302, 'system', 'updated', 4, 'App\\Models\\Employee', 12, 'App\\Models\\User', '[]', '2020-11-18 05:21:32', '2020-11-18 05:21:32'),
(303, 'system', 'created', 5, 'App\\Models\\Company', 12, 'App\\Models\\User', '[]', '2020-11-18 05:34:14', '2020-11-18 05:34:14'),
(304, 'system', 'created', 6, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-18 05:38:28', '2020-11-18 05:38:28'),
(305, 'system', 'created', 7, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-18 05:40:29', '2020-11-18 05:40:29'),
(306, 'system', 'created', 8, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-18 05:41:29', '2020-11-18 05:41:29'),
(307, 'system', 'updated', 8, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-11-18 05:44:50', '2020-11-18 05:44:50'),
(308, 'system', 'created', 9, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-18 05:53:52', '2020-11-18 05:53:52'),
(309, 'system', 'created', 9, 'App\\Models\\VehicleDriver', 12, 'App\\Models\\User', '[]', '2020-11-18 05:55:47', '2020-11-18 05:55:47'),
(310, 'system', 'created', 29, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(311, 'system', 'created', 30, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(312, 'system', 'created', 31, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(313, 'system', 'created', 32, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(314, 'system', 'created', 33, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(315, 'system', 'created', 34, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(316, 'system', 'created', 35, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(317, 'system', 'created', 36, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(318, 'system', 'created', 37, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(319, 'system', 'created', 38, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(320, 'system', 'created', 39, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(321, 'system', 'created', 40, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(322, 'system', 'created', 41, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(323, 'system', 'created', 42, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(324, 'system', 'created', 43, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(325, 'system', 'created', 44, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(326, 'system', 'created', 14, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 11:18:53', '2020-11-18 11:18:53'),
(327, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 11:19:16', '2020-11-18 11:19:16'),
(328, 'system', 'updated', 14, 'App\\Models\\User', 14, 'App\\Models\\User', '[]', '2020-11-18 11:19:24', '2020-11-18 11:19:24'),
(329, 'system', 'updated', 14, 'App\\Models\\User', 14, 'App\\Models\\User', '[]', '2020-11-18 11:20:10', '2020-11-18 11:20:10'),
(330, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 11:23:28', '2020-11-18 11:23:28'),
(331, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-18 12:18:17', '2020-11-18 12:18:17'),
(332, 'system', 'created', 10, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-22 05:32:21', '2020-11-22 05:32:21'),
(333, 'system', 'created', 9, 'App\\Models\\Vehicle', 12, 'App\\Models\\User', '[]', '2020-11-22 05:55:30', '2020-11-22 05:55:30'),
(334, 'system', 'created', 10, 'App\\Models\\VehicleHistory', 12, 'App\\Models\\User', '[]', '2020-11-22 05:55:30', '2020-11-22 05:55:30'),
(335, 'system', 'created', 11, 'App\\Models\\VehicleHistory', 12, 'App\\Models\\User', '[]', '2020-11-22 05:55:30', '2020-11-22 05:55:30'),
(336, 'system', 'updated', 2, 'App\\Models\\Vehicle', 12, 'App\\Models\\User', '[]', '2020-11-22 05:58:02', '2020-11-22 05:58:02'),
(337, 'system', 'created', 12, 'App\\Models\\VehicleHistory', 12, 'App\\Models\\User', '[]', '2020-11-22 05:58:02', '2020-11-22 05:58:02'),
(338, 'system', 'created', 13, 'App\\Models\\VehicleHistory', 12, 'App\\Models\\User', '[]', '2020-11-22 05:58:02', '2020-11-22 05:58:02'),
(339, 'system', 'created', 45, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(340, 'system', 'created', 46, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(341, 'system', 'created', 47, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(342, 'system', 'created', 48, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(343, 'system', 'created', 49, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(344, 'system', 'created', 50, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(345, 'system', 'created', 51, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(346, 'system', 'created', 52, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(347, 'system', 'created', 9, 'App\\Models\\LogbookEntry', 12, 'App\\Models\\User', '[]', '2020-11-22 06:20:31', '2020-11-22 06:20:31'),
(348, 'system', 'created', 11, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-22 07:30:00', '2020-11-22 07:30:00'),
(349, 'system', 'updated', 11, 'App\\Models\\Driver', 12, 'App\\Models\\User', '[]', '2020-11-22 07:30:31', '2020-11-22 07:30:31'),
(350, 'system', 'created', 10, 'App\\Models\\VehicleDriver', 12, 'App\\Models\\User', '[]', '2020-11-22 07:43:47', '2020-11-22 07:43:47'),
(351, 'system', 'created', 10, 'App\\Models\\Vehicle', 12, 'App\\Models\\User', '[]', '2020-11-22 08:39:45', '2020-11-22 08:39:45'),
(352, 'system', 'created', 14, 'App\\Models\\VehicleHistory', 12, 'App\\Models\\User', '[]', '2020-11-22 08:39:45', '2020-11-22 08:39:45'),
(353, 'system', 'created', 15, 'App\\Models\\VehicleHistory', 12, 'App\\Models\\User', '[]', '2020-11-22 08:39:45', '2020-11-22 08:39:45'),
(354, 'system', 'created', 11, 'App\\Models\\VehicleDriver', 12, 'App\\Models\\User', '[]', '2020-11-22 08:57:12', '2020-11-22 08:57:12'),
(355, 'system', 'created', 12, 'App\\Models\\VehicleDriver', 12, 'App\\Models\\User', '[]', '2020-11-22 09:02:32', '2020-11-22 09:02:32'),
(356, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-22 09:17:57', '2020-11-22 09:17:57'),
(357, 'system', 'created', 53, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(358, 'system', 'created', 54, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(359, 'system', 'created', 55, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(360, 'system', 'created', 56, 'App\\Models\\FuelRate', 12, 'App\\Models\\User', '[]', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(361, 'system', 'created', 10, 'App\\Models\\LogbookEntry', 12, 'App\\Models\\User', '[]', '2020-11-22 09:54:20', '2020-11-22 09:54:20'),
(362, 'system', 'created', 11, 'App\\Models\\LogbookEntry', 12, 'App\\Models\\User', '[]', '2020-11-22 09:57:49', '2020-11-22 09:57:49'),
(363, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-22 11:10:53', '2020-11-22 11:10:53'),
(364, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-22 11:13:18', '2020-11-22 11:13:18'),
(365, 'system', 'updated', 2, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-22 12:06:37', '2020-11-22 12:06:37'),
(366, 'system', 'updated', 2, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-22 12:07:05', '2020-11-22 12:07:05'),
(367, 'system', 'updated', 1, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-22 12:07:43', '2020-11-22 12:07:43'),
(368, 'system', 'updated', 1, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-22 12:08:14', '2020-11-22 12:08:14'),
(371, 'system', 'updated', 2, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-22 12:11:18', '2020-11-22 12:11:18'),
(373, 'system', 'updated', 2, 'App\\Models\\Branch', 12, 'App\\Models\\User', '[]', '2020-11-22 12:11:50', '2020-11-22 12:11:50'),
(374, 'system', 'updated', 12, 'App\\Models\\User', 12, 'App\\Models\\User', '[]', '2020-11-29 05:07:23', '2020-11-29 05:07:23'),
(375, 'system', 'updated', 2, 'App\\Models\\User', 2, 'App\\Models\\User', '[]', '2021-01-24 00:01:19', '2021-01-24 00:01:19'),
(376, 'system', 'updated', 2, 'App\\Models\\User', 2, 'App\\Models\\User', '[]', '2021-01-24 00:05:40', '2021-01-24 00:05:40'),
(377, 'system', 'updated', 2, 'App\\Models\\User', 2, 'App\\Models\\User', '[]', '2021-01-24 00:07:02', '2021-01-24 00:07:02');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bic_id` int(11) DEFAULT NULL,
  `dbic_id` int(11) DEFAULT NULL,
  `aic_id` int(11) DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `code`, `name`, `short_name`, `address1`, `address2`, `address3`, `address4`, `email`, `location`, `bic_id`, `dbic_id`, `aic_id`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '000', 'Corporate', 'COR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, 12, NULL, '2020-08-03 23:13:57', '2020-11-22 12:08:14', NULL),
(2, '110', 'Motijheel', 'MOT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, 12, NULL, '2020-08-03 23:13:57', '2020-11-22 12:11:50', NULL),
(3, '115', 'Mirpur', 'MIR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(4, '120', 'Mohakhali', 'MOH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(5, '130', 'Narayangonj', 'NAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(6, '140', 'Mymensingh', 'MYM', 'MYM', NULL, NULL, NULL, 'mymensingh@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(7, '150', 'Tangail', 'TAN', 'TNG', NULL, NULL, NULL, 'tangail@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(8, '160', 'Gazipur', 'GAZ', 'GZP', NULL, NULL, NULL, 'gazipur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(9, '170', 'Savar', 'SAV', 'SVR', NULL, NULL, NULL, 'savar@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(10, '180', 'Bhairab', 'BHA', 'abc', NULL, NULL, NULL, 'b.baria@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(11, '190', 'Kishorgonj', 'KIS', 'KSR', NULL, NULL, NULL, 'kisoregonj@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(12, '280', 'Keranigonj', 'KER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(13, '210', 'Chattogram', 'CTGS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(14, '220', 'Cumilla', 'CUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(15, '230', 'Noakhali', 'NOA', 'NOK', NULL, NULL, NULL, 'noakhali@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(16, '240', 'Cox\'s bazar', 'COX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(17, '250', 'Chandpur', 'CHA', 'CHD', NULL, NULL, NULL, 'chandpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(18, '260', 'CTG-N', 'CTGN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(19, '270', 'Feni', 'FEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(20, '310', 'Rajshahi', 'RAJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(21, '320', 'Bogura', 'BOG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(22, '330', 'Rangpur', 'RAN', 'RNG', NULL, NULL, NULL, 'rangpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(23, '340', 'Dinajpur', 'DIN', 'DNJ', NULL, NULL, NULL, 'dinajpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(24, '350', 'Pabna', 'PAB', 'PBN', NULL, NULL, NULL, 'pabna@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(25, '410', 'Khulna', 'KHU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(26, '420', 'Faridpur', 'FAR', 'FRD', NULL, NULL, NULL, 'faridpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(27, '430', 'Kushtia', 'KUS', 'KUS', NULL, NULL, NULL, 'kushtia@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(28, '440', 'Jashore', 'JAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(29, '510', 'Barishal', 'BAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(30, '520', 'Patuakhali', 'PAT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(31, '610', 'Sylhet', 'SYL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(32, '620', 'Mowlovibazar', 'MOW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-03 23:13:57', '2020-08-03 23:14:29', NULL),
(33, 'Test1', 'Test Dhaka', 'TestDhaka', 'Dhaka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '33', 1, 12, NULL, NULL, '2020-11-18 05:14:22', '2020-11-18 05:14:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_suffix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Asia/Dhaka',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `code`, `name`, `short_name`, `address`, `logo`, `email_suffix`, `time_zone`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '100', 'Transcom Distribution Company Limited', 'TDCL', 'Default Company Address', 'logo.png', '@tdcl.transcombd.com', 'Asia/Dhaka', 1, 1, 1, NULL, '2020-08-03 16:41:45', '2020-08-03 17:24:24', NULL),
(2, '2222', 'brix', 'R soft', 'dhaka', 'hiclipart.com.png', 'great', 'Asia/Dhaka', 0, 1, 1, NULL, '2020-09-08 02:17:54', '2020-11-03 12:05:41', NULL),
(3, '34', 'test', 'te', 'dhaka', 'Transcom-logo.png', '@transcombd.com', 'Asia/Dhaka', 0, 1, 1, NULL, '2020-11-03 12:03:56', '2020-11-03 12:05:33', NULL),
(4, '667', 'test', 'te', 'dhaka', 'Transcom-logo.png', '@transcombd.com', 'Asia/Dhaka', 1, 1, NULL, NULL, '2020-11-03 12:06:31', '2020-11-03 12:06:31', NULL),
(5, 'TCPL-23', 'Transcom Consumer Products Limited', 'TCPL', 'Gulshan Tower', 'TCPL.jpg', 'tcpl@transcombd.com', 'Asia/Dhaka', 1, 12, NULL, NULL, '2020-11-18 05:34:14', '2020-11-18 05:34:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=driver, 2=vehicle',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `company_id`, `type`, `name`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'NID Card', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, 'Driving License', 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(3, 1, 2, 'Fitness Paper', 3, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, 1, 2, 'Insurance Paper', 4, 1, 1, NULL, NULL, NULL, NULL, NULL),
(5, 1, 2, 'Tax Token', 5, 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'driver payroll id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `blood_group` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `present_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'emergency mobile number',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `company_id`, `employee_id`, `name`, `mobile`, `dob`, `doj`, `blood_group`, `present_address`, `permanent_address`, `em_mobile`, `photo`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1234567890', 'Hasan', '0172222222', NULL, NULL, 'A-', 'test present addresss', 'test parmanent address', '0172222222', NULL, 1, NULL, NULL, NULL, 1, 1, 1, NULL, '2020-08-03 20:07:06', '2020-08-03 20:14:09', NULL),
(4, 1, '11111111111', 'Parvez', '0172222222', '2020-08-04', '2020-08-04', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-03 22:36:10', '2020-08-03 22:36:10', NULL),
(5, 1, '1234567891', 'arif', '01613722564', '1981-01-07', NULL, NULL, 'dhaka', 'Dhaka', '01613722564', NULL, 3, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-11-03 11:45:03', '2020-11-03 11:45:03', NULL),
(6, 5, 'X-0001', 'Mr. X', '01711111111', '1990-11-01', '2020-11-01', 'A+', 'Dhaka', 'Barisal', '01788888888', NULL, 4, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-18 05:38:28', '2020-11-18 05:38:28', NULL),
(7, 5, 'X-0001', 'Mr. X', '01711111111', '1990-11-01', '2020-11-11', NULL, NULL, 'Barisal', '01788888888', NULL, 4, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-18 05:40:29', '2020-11-18 05:40:29', NULL),
(8, 5, 'X-0002', 'Mr. Y', '01711111112', '1990-11-01', '2020-11-04', 'A+', 'Dhaka', 'Barisal', NULL, NULL, 4, NULL, NULL, NULL, 1, 12, 1, NULL, '2020-11-18 05:41:29', '2020-11-18 05:44:50', NULL),
(9, 1, 'X-0001', 'Mr. X', '01711111111', '1990-11-01', '2020-11-01', 'A+', 'Dhaka', NULL, '01788888888', NULL, 4, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-18 05:53:52', '2020-11-18 05:53:52', NULL),
(10, 1, 'TDCL-003', 'Abu Musa', '01780101012', '1988-11-01', '2020-11-01', 'O-', 'Badda, Dhaka', 'Barisal', '01788888888', NULL, 5, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 05:32:21', '2020-11-22 05:32:21', NULL),
(11, 1, 'TDCL-004', 'Abu Yunus', '01711111111', '1988-11-01', '2020-11-01', 'A+', 'Dhaka', NULL, '01788888888', NULL, 6, NULL, NULL, NULL, 1, 12, 12, NULL, '2020-11-22 07:30:00', '2020-11-22 07:30:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `driver_documents`
--

CREATE TABLE `driver_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_type` tinyint(4) NOT NULL COMMENT '1 = NID card, 2=license',
  `document_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `issuing_authority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_documents`
--

INSERT INTO `driver_documents` (`id`, `driver_id`, `document_type`, `document_no`, `issue_date`, `expiry_date`, `issuing_authority`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 2, 'AAAAAAefff', '2020-08-04', '2020-08-04', 'BRTA', NULL, NULL, NULL, NULL, 0, 1, 1, NULL, '2020-08-03 22:36:10', '2020-08-03 22:55:38', NULL),
(2, 4, 1, '1000211', '2020-08-04', '2020-08-04', 'EC', NULL, NULL, NULL, NULL, 0, 1, 1, NULL, '2020-08-03 22:36:10', '2020-08-03 22:55:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_id` int(10) UNSIGNED NOT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `company_id`, `user_id`, `employee_id`, `name`, `designation_id`, `grade`, `mobile_no`, `email`, `department`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 5, NULL, 'Almas Estiak', 1, '1', NULL, 'almas.estiak@transcombd.com', 'ISA', NULL, 1, 1, NULL, NULL, '2020-08-03 18:44:21', '2020-08-03 18:44:21', NULL),
(2, 1, 7, '123456789', 'Bashir Uddin', 2, NULL, NULL, 'bashir@bashir.com', NULL, '2', 1, 1, NULL, NULL, '2020-08-15 16:47:42', '2020-08-15 16:47:42', NULL),
(3, 1, 10, NULL, 'AIC MOT', 2, '1', NULL, 'aicmot@tdcl.transcombd.com', 'IS', '3', 1, 1, NULL, NULL, '2020-10-10 22:25:37', '2020-10-10 22:25:37', NULL),
(4, 1, 13, 'TDCL-01', 'Din Mohammad', 2, 'NDM', NULL, 'din.mohammad@transcombd.com', 'Sales', '4', 1, 12, 12, NULL, '2020-11-18 05:19:15', '2020-11-18 05:21:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_designations`
--

CREATE TABLE `employee_designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_designations`
--

INSERT INTO `employee_designations` (`id`, `company_id`, `name`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Jr. System Analyst', '1', 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'NDM', '1', 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fuels`
--

CREATE TABLE `fuels` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fuels`
--

INSERT INTO `fuels` (`id`, `company_id`, `name`, `short_name`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Deisel', NULL, 1, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'Petrol', NULL, 2, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL),
(3, 1, 'Octane', NULL, 3, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, 1, 'CNG', NULL, 4, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fuel_rates`
--

CREATE TABLE `fuel_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `fuel_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `rate` decimal(10,6) NOT NULL DEFAULT 0.000000,
  `order` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fuel_rates`
--

INSERT INTO `fuel_rates` (`id`, `company_id`, `entry_date`, `fuel_id`, `branch_id`, `rate`, `order`, `archived`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2020-08-06', 1, 4, '65.000000', 1, 0, 1, 1, NULL, NULL, '2020-08-05 16:28:24', '2020-08-05 16:28:24', NULL),
(2, 1, '2020-08-06', 2, 4, '86.000000', 2, 0, 1, 1, NULL, NULL, '2020-08-05 16:28:24', '2020-08-05 16:28:24', NULL),
(3, 1, '2020-08-06', 3, 4, '89.000000', 3, 0, 1, 1, NULL, NULL, '2020-08-05 16:28:24', '2020-08-05 16:28:24', NULL),
(4, 1, '2020-08-06', 4, 4, '43.000000', 4, 0, 1, 1, NULL, NULL, '2020-08-05 16:28:24', '2020-08-05 16:28:24', NULL),
(5, 1, '2020-08-06', 1, 5, '65.000000', 5, 0, 1, 1, 1, NULL, '2020-08-05 16:56:06', '2020-08-05 17:58:30', NULL),
(6, 1, '2020-08-06', 2, 5, '86.000000', 6, 0, 1, 1, 1, NULL, '2020-08-05 16:56:06', '2020-08-05 17:58:30', NULL),
(7, 1, '2020-08-06', 3, 5, '89.000000', 7, 0, 1, 1, 1, NULL, '2020-08-05 16:56:06', '2020-08-05 17:58:30', NULL),
(8, 1, '2020-08-06', 4, 5, '430.000000', 8, 0, 1, 1, 1, NULL, '2020-08-05 16:56:06', '2020-08-05 17:58:30', NULL),
(9, 1, '2020-08-06', 1, 21, '0.000000', 1, 0, 1, 6, 6, NULL, '2020-08-05 23:18:58', '2020-08-05 23:25:22', NULL),
(10, 1, '2020-08-06', 2, 21, '86.300000', 2, 0, 1, 6, 6, NULL, '2020-08-05 23:18:58', '2020-08-05 23:25:22', NULL),
(11, 1, '2020-08-06', 3, 21, '89.300000', 3, 0, 1, 6, 6, NULL, '2020-08-05 23:18:58', '2020-08-05 23:25:22', NULL),
(12, 1, '2020-08-06', 4, 21, '43.000000', 4, 0, 1, 6, 6, NULL, '2020-08-05 23:18:58', '2020-08-05 23:25:22', NULL),
(13, 1, '2020-08-06', 1, 21, '0.000000', 5, 0, 1, 6, NULL, NULL, '2020-08-05 23:35:17', '2020-08-05 23:35:17', NULL),
(14, 1, '2020-08-06', 2, 21, '89.000000', 6, 0, 1, 6, NULL, NULL, '2020-08-05 23:35:17', '2020-08-05 23:35:17', NULL),
(15, 1, '2020-08-06', 3, 21, '84.000000', 7, 0, 1, 6, NULL, NULL, '2020-08-05 23:35:17', '2020-08-05 23:35:17', NULL),
(16, 1, '2020-08-06', 4, 21, '60.000000', 8, 0, 1, 6, NULL, NULL, '2020-08-05 23:35:17', '2020-08-05 23:35:17', NULL),
(17, 1, '2020-08-16', 1, 6, '88.000000', 17, 0, 1, 1, 11, NULL, '2020-08-15 16:57:25', '2020-11-04 09:08:15', NULL),
(18, 1, '2020-08-16', 2, 6, '60.000000', 18, 0, 1, 1, 11, NULL, '2020-08-15 16:57:25', '2020-11-04 09:08:15', NULL),
(19, 1, '2020-08-16', 3, 6, '65.000000', 19, 0, 1, 1, 11, NULL, '2020-08-15 16:57:25', '2020-11-04 09:08:15', NULL),
(20, 1, '2020-08-16', 4, 6, '86.000000', 20, 0, 1, 1, 11, NULL, '2020-08-15 16:57:25', '2020-11-04 09:08:15', NULL),
(21, 1, '2020-08-01', 1, 2, '86.000000', 21, 0, 1, 1, NULL, NULL, '2020-08-15 17:26:56', '2020-08-15 17:26:56', NULL),
(22, 1, '2020-08-01', 2, 2, '65.000000', 22, 0, 1, 1, NULL, NULL, '2020-08-15 17:26:56', '2020-08-15 17:26:56', NULL),
(23, 1, '2020-08-01', 3, 2, '60.000000', 23, 0, 1, 1, NULL, NULL, '2020-08-15 17:26:56', '2020-08-15 17:26:56', NULL),
(24, 1, '2020-08-01', 4, 2, '75.000000', 24, 0, 1, 1, NULL, NULL, '2020-08-15 17:26:56', '2020-08-15 17:26:56', NULL),
(25, 1, '2020-10-11', 1, 2, '60.000000', 5, 0, 1, 10, NULL, NULL, '2020-10-10 22:48:58', '2020-10-10 22:48:58', NULL),
(26, 1, '2020-10-11', 2, 2, '88.000000', 6, 0, 1, 10, NULL, NULL, '2020-10-10 22:48:58', '2020-10-10 22:48:58', NULL),
(27, 1, '2020-10-11', 3, 2, '89.000000', 7, 0, 1, 10, NULL, NULL, '2020-10-10 22:48:58', '2020-10-10 22:48:58', NULL),
(28, 1, '2020-10-11', 4, 2, '75.000000', 8, 0, 1, 10, NULL, NULL, '2020-10-10 22:48:58', '2020-10-10 22:48:58', NULL),
(29, 1, '2020-11-18', 1, 2, '50.000000', 29, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:08', '2020-11-18 06:22:08', NULL),
(30, 1, '2020-11-18', 2, 2, '0.000000', 30, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:08', '2020-11-18 06:22:08', NULL),
(31, 1, '2020-11-18', 3, 2, '0.000000', 31, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:08', '2020-11-18 06:22:08', NULL),
(32, 1, '2020-11-18', 4, 2, '0.000000', 32, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:08', '2020-11-18 06:22:08', NULL),
(33, 1, '2020-11-18', 1, 6, '45.000000', 33, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:53', '2020-11-18 06:22:53', NULL),
(34, 1, '2020-11-18', 2, 6, '0.000000', 34, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:53', '2020-11-18 06:22:53', NULL),
(35, 1, '2020-11-18', 3, 6, '0.000000', 35, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:53', '2020-11-18 06:22:53', NULL),
(36, 1, '2020-11-18', 4, 6, '70.000000', 36, 0, 1, 12, NULL, NULL, '2020-11-18 06:22:53', '2020-11-18 06:22:53', NULL),
(37, 1, '2020-11-18', 1, 3, '44.000000', 37, 0, 1, 12, NULL, NULL, '2020-11-18 06:23:46', '2020-11-18 06:23:46', NULL),
(38, 1, '2020-11-18', 2, 3, '0.000000', 38, 0, 1, 12, NULL, NULL, '2020-11-18 06:23:46', '2020-11-18 06:23:46', NULL),
(39, 1, '2020-11-18', 3, 3, '45.000000', 39, 0, 1, 12, NULL, NULL, '2020-11-18 06:23:46', '2020-11-18 06:23:46', NULL),
(40, 1, '2020-11-18', 4, 3, '0.000000', 40, 0, 1, 12, NULL, NULL, '2020-11-18 06:23:46', '2020-11-18 06:23:46', NULL),
(41, 1, '2020-11-18', 1, 6, '0.000000', 41, 0, 1, 12, NULL, NULL, '2020-11-18 06:24:13', '2020-11-18 06:24:13', NULL),
(42, 1, '2020-11-18', 2, 6, '60.000000', 42, 0, 1, 12, NULL, NULL, '2020-11-18 06:24:13', '2020-11-18 06:24:13', NULL),
(43, 1, '2020-11-18', 3, 6, '0.000000', 43, 0, 1, 12, NULL, NULL, '2020-11-18 06:24:13', '2020-11-18 06:24:13', NULL),
(44, 1, '2020-11-18', 4, 6, '0.000000', 44, 0, 1, 12, NULL, NULL, '2020-11-18 06:24:13', '2020-11-18 06:24:13', NULL),
(45, 1, '2020-11-22', 1, 21, '60.000000', 45, 0, 1, 12, NULL, NULL, '2020-11-22 06:09:32', '2020-11-22 06:09:32', NULL),
(46, 1, '2020-11-22', 2, 21, '90.000000', 46, 0, 1, 12, NULL, NULL, '2020-11-22 06:09:32', '2020-11-22 06:09:32', NULL),
(47, 1, '2020-11-22', 3, 21, '85.000000', 47, 0, 1, 12, NULL, NULL, '2020-11-22 06:09:32', '2020-11-22 06:09:32', NULL),
(48, 1, '2020-11-22', 4, 21, '55.000000', 48, 0, 1, 12, NULL, NULL, '2020-11-22 06:09:32', '2020-11-22 06:09:32', NULL),
(49, 1, '2020-11-22', 1, 3, '50.000000', 49, 0, 1, 12, NULL, NULL, '2020-11-22 06:11:26', '2020-11-22 06:11:26', NULL),
(50, 1, '2020-11-22', 2, 3, '50.000000', 50, 0, 1, 12, NULL, NULL, '2020-11-22 06:11:26', '2020-11-22 06:11:26', NULL),
(51, 1, '2020-11-22', 3, 3, '45.000000', 51, 0, 1, 12, NULL, NULL, '2020-11-22 06:11:26', '2020-11-22 06:11:26', NULL),
(52, 1, '2020-11-22', 4, 3, '52.000000', 52, 0, 1, 12, NULL, NULL, '2020-11-22 06:11:26', '2020-11-22 06:11:26', NULL),
(53, 1, '2020-11-22', 1, 3, '60.000000', 53, 0, 1, 12, NULL, NULL, '2020-11-22 09:32:20', '2020-11-22 09:32:20', NULL),
(54, 1, '2020-11-22', 2, 3, '65.000000', 54, 0, 1, 12, NULL, NULL, '2020-11-22 09:32:20', '2020-11-22 09:32:20', NULL),
(55, 1, '2020-11-22', 3, 3, '70.000000', 55, 0, 1, 12, NULL, NULL, '2020-11-22 09:32:20', '2020-11-22 09:32:20', NULL),
(56, 1, '2020-11-22', 4, 3, '55.000000', 56, 0, 1, 12, NULL, NULL, '2020-11-22 09:32:20', '2020-11-22 09:32:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ledgers`
--

CREATE TABLE `ledgers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `recordable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recordable_id` bigint(20) UNSIGNED NOT NULL,
  `context` tinyint(3) UNSIGNED NOT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pivot` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ledgers`
--

INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$duQ\\/Luo3Fp\\/vyBSWVv62ruiQ692QTgy31p5slAREJcflK\\/je.NE\\/q\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"zMVJrptTKQXe4ZeR51xAhX6WivIZK3KwxTKLFiSM8IpA1dXiRZz2H0CNFHjX\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-04 04:41:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9623856ca43f3a76f3413996de077db8ccfa6341b392bdd727aaee8a44564385b0fdf8d107b929b346355adf2dfa647247e2f58d72a4b09ae167bf26cbf824dd', '2020-08-03 16:41:58', '2020-08-03 16:41:58'),
(2, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$duQ\\/Luo3Fp\\/vyBSWVv62ruiQ692QTgy31p5slAREJcflK\\/je.NE\\/q\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-04 04:41:58\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"zMVJrptTKQXe4ZeR51xAhX6WivIZK3KwxTKLFiSM8IpA1dXiRZz2H0CNFHjX\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-04 04:41:59\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '782b58b09bb5510dad366f855530dacf6af54d5d88e0495e57b1740e487c387987a22e10c99d7b803945d313f4f99c7128b5e372936390d6993f6580646ae54f', '2020-08-03 16:41:59', '2020-08-03 16:41:59'),
(3, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-04 04:41:58\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"zMVJrptTKQXe4ZeR51xAhX6WivIZK3KwxTKLFiSM8IpA1dXiRZz2H0CNFHjX\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-04 04:41:59\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '832ab600dd47553c2a088efc8923254c6a8079973f7062f686b2af67412959bf4f1b6cfd98246dc2678a64490773aacf24a8e24616a2a1b7c4f5d499c2b173ff', '2020-08-03 16:41:59', '2020-08-03 16:41:59'),
(4, 'App\\Models\\User', 1, 'App\\Models\\Company', 1, 4, 'updated', '{\"id\":1,\"code\":\"100\",\"name\":\"Transcom Distribution Company Limited\",\"short_name\":\"TDCL\",\"address\":\"Default Company Address\",\"logo\":\"logo.png\",\"email_suffix\":\"@tdcl.transcombd.com\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-04 04:41:45\",\"updated_at\":\"2020-08-04 05:24:24\",\"deleted_at\":null}', '[\"name\",\"short_name\",\"email_suffix\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/company/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '3ff1507901e614510eba634fa268f57674fba167135b0c9dea5224b862cea7e083d494b30005b35ab29d70e091d3a3658cab8b2592013bd925d425b7dfdf38e6', '2020-08-03 17:24:24', '2020-08-03 17:24:24'),
(6, 'App\\Models\\User', 1, 'App\\Models\\User', 5, 4, 'created', '{\"first_name\":\"Almas\",\"last_name\":\"Estiak\",\"email\":\"almas.estiak@transcombd.com\",\"username\":\"almas\",\"password\":\"$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL\\/cdyHSOdnbrDXc8O5cUh8kG\",\"active\":true,\"confirmation_code\":\"29b575c492a20aafaad3a0aa89807a5f\",\"confirmed\":false,\"updated_at\":\"2020-08-04 06:44:21\",\"created_at\":\"2020-08-04 06:44:21\",\"id\":5}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/employee', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ced7d25ec6d0b943e829041fdc4d8e54afe5142378bc9c796d956c491781388613776a7067aa933424debb344195060ae8dd4e7b69b1e629fc0d63b63bbc5b82', '2020-08-03 18:44:21', '2020-08-03 18:44:21'),
(7, 'App\\Models\\User', 1, 'App\\Models\\Employee', 1, 4, 'created', '{\"company_id\":\"1\",\"user_id\":5,\"employee_id\":null,\"name\":\"Almas Estiak\",\"designation\":\"Jr. System Analyst\",\"department\":\"ISA\",\"grade\":\"1\",\"email\":\"almas.estiak@transcombd.com\",\"order\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-04 06:44:21\",\"created_at\":\"2020-08-04 06:44:21\",\"id\":1}', '[\"company_id\",\"user_id\",\"employee_id\",\"name\",\"designation\",\"department\",\"grade\",\"email\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/employee', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '08e0821a9e5da144e63ba70bc32376e6a981cc1d9b294936e97183cca15c8fe868d23239be565da02bb9e8c8f11406382b79d2cc795a1ac861cf53af64019fda', '2020-08-03 18:44:21', '2020-08-03 18:44:21'),
(8, 'App\\Models\\User', 1, 'App\\Models\\Driver', 1, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"1234567890\",\"name\":\"Hasan\",\"mobile\":\"0172222222\",\"dob\":null,\"doj\":null,\"blood_group\":\"A-\",\"em_mobile\":\"0172222222\",\"present_address\":\"test present addresss\",\"permanent_address\":\"test parmanent address\",\"photo\":\"bcb319b817317a6416f9f726bc96747f.jpg\",\"order\":1,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-04 08:07:06\",\"created_at\":\"2020-08-04 08:07:06\",\"id\":1}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'bbe479ea25379918eb837899e61cde9cd3f46228da4a383eaf2e8eb2d0f91074e8cf178208158bfae76768367129f36dcc63a157c14fd2e24938dad95ef1c80f', '2020-08-03 20:07:06', '2020-08-03 20:07:06'),
(9, 'App\\Models\\User', 1, 'App\\Models\\Driver', 1, 4, 'updated', '{\"id\":1,\"company_id\":\"1\",\"employee_id\":null,\"name\":\"Hasan\",\"mobile\":\"0172222222\",\"dob\":null,\"doj\":null,\"blood_group\":\"A-\",\"present_address\":\"test present addresss\",\"permanent_address\":\"test parmanent address\",\"em_mobile\":\"0172222222\",\"photo\":null,\"order\":\"1\",\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-04 08:07:06\",\"updated_at\":\"2020-08-04 08:12:57\",\"deleted_at\":null}', '[\"employee_id\",\"photo\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5ff6fbb93a33c9cc22ca19a20fdc158405e12bfcb790ec6f6f77add006698f2f3565a1f7108b18468a88e67a8991c839bb7ed54fff80bda52a42248006bab6de', '2020-08-03 20:12:57', '2020-08-03 20:12:57'),
(10, 'App\\Models\\User', 1, 'App\\Models\\Driver', 1, 4, 'updated', '{\"id\":1,\"company_id\":\"1\",\"employee_id\":\"1234567890\",\"name\":\"Hasan\",\"mobile\":\"0172222222\",\"dob\":null,\"doj\":null,\"blood_group\":\"A-\",\"present_address\":\"test present addresss\",\"permanent_address\":\"test parmanent address\",\"em_mobile\":\"0172222222\",\"photo\":null,\"order\":\"1\",\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-04 08:07:06\",\"updated_at\":\"2020-08-04 08:14:09\",\"deleted_at\":null}', '[\"employee_id\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '7f8647f09b9b4d8575a954de31f5592c7e93b7d7237dedb3f74d94ddc884fa3cb2dc8a9adb6142defe854673adaaf38f95e158094b787245c2fec08bae1d4fd1', '2020-08-03 20:14:09', '2020-08-03 20:14:09'),
(13, 'App\\Models\\User', 1, 'App\\Models\\Driver', 4, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"11111111111\",\"name\":\"Parvez\",\"mobile\":\"0172222222\",\"dob\":\"2020-08-04\",\"doj\":\"2020-08-04\",\"blood_group\":null,\"em_mobile\":null,\"present_address\":null,\"permanent_address\":null,\"photo\":null,\"order\":2,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-04 10:36:10\",\"created_at\":\"2020-08-04 10:36:10\",\"id\":4}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a1aeef4fccdbfd0b2848ea93ad098de70b330e9cb611d6514ac04f6b82d06f81dcd197e9ee09416b9be2e7b94621ee1b1c06815fc3a71b938460ac0fcc307201', '2020-08-03 22:36:10', '2020-08-03 22:36:10'),
(14, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 1, 4, 'created', '{\"driver_id\":4,\"document_type\":\"2\",\"document_no\":\"AAAAAAe\",\"issue_date\":\"2020-08-04\",\"expiry_date\":\"2020-08-04\",\"issuing_authority\":\"BRTA\",\"order\":1,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-04 10:36:10\",\"created_at\":\"2020-08-04 10:36:10\",\"id\":1}', '[\"driver_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '560cc306cad11ad869691e1ae99856321c3363cc3630dfd1222f09142902b6853c4ec37e6ede369c8d3d294464f6c967aa4e6a550e27f311a4249334ee4b0d14', '2020-08-03 22:36:10', '2020-08-03 22:36:10'),
(15, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 2, 4, 'created', '{\"driver_id\":4,\"document_type\":\"1\",\"document_no\":\"10002\",\"issue_date\":\"2020-08-04\",\"expiry_date\":\"2020-08-04\",\"issuing_authority\":\"EC\",\"order\":2,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-04 10:36:10\",\"created_at\":\"2020-08-04 10:36:10\",\"id\":2}', '[\"driver_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '06f1e5725dbbf840e2ce87333eed9f908bb4675bccbd92a26ffa84ccd2c49d318cb97d77d8bb3dd11af3baff35599c349951e25b9252124cd10743233a1f699c', '2020-08-03 22:36:10', '2020-08-03 22:36:10'),
(16, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 3, 4, 'created', '{\"driver_id\":4,\"document_type\":\"2\",\"document_no\":\"AAAAAAeff\",\"issue_date\":\"2020-08-04\",\"expiry_date\":\"2020-08-04\",\"issuing_authority\":\"BRTA\",\"order\":3,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-04 10:49:19\",\"created_at\":\"2020-08-04 10:49:19\",\"id\":3}', '[\"driver_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '617ef270a5b9ab4eb04a0bdcb87d329016a5d7a8cbd0bd7457aa937636125909fc62bfc92db710ce4867286b38c89cd89f47267ba5a5615b00dcad2f068b24b7', '2020-08-03 22:49:19', '2020-08-03 22:49:19'),
(17, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 4, 4, 'created', '{\"driver_id\":4,\"document_type\":\"1\",\"document_no\":\"10002q\",\"issue_date\":\"2020-08-04\",\"expiry_date\":\"2020-08-04\",\"issuing_authority\":\"EC\",\"order\":4,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-04 10:49:19\",\"created_at\":\"2020-08-04 10:49:19\",\"id\":4}', '[\"driver_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'bd311d5c66cf888040c42ae194c4229a4a49fa9d038e44d695668280f2881d890df121759695555624f457deb83d24aec3fb9c29ce5dc789672d81d52260d850', '2020-08-03 22:49:19', '2020-08-03 22:49:19'),
(18, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 1, 4, 'updated', '{\"id\":1,\"driver_id\":4,\"document_type\":\"2\",\"document_no\":\"AAAAAAefff\",\"issue_date\":\"2020-08-04\",\"expiry_date\":\"2020-08-04\",\"issuing_authority\":\"BRTA\",\"order\":null,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":false,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-04 10:36:10\",\"updated_at\":\"2020-08-04 10:55:38\",\"deleted_at\":null}', '[\"document_no\",\"order\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c9b8b221307efcda7faa45f25e3c35633240c03de8a3551fd6131d5627423aba179a89bb0d5bd60c9232ffdf240d34968b9860de98baad47dee0ac5cb904b7b7', '2020-08-03 22:55:38', '2020-08-03 22:55:38'),
(19, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 2, 4, 'updated', '{\"id\":2,\"driver_id\":4,\"document_type\":\"1\",\"document_no\":\"1000211\",\"issue_date\":\"2020-08-04\",\"expiry_date\":\"2020-08-04\",\"issuing_authority\":\"EC\",\"order\":null,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":false,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-04 10:36:10\",\"updated_at\":\"2020-08-04 10:55:38\",\"deleted_at\":null}', '[\"document_no\",\"order\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '96661135ce475f96543ac43511ae055f8be42a2cd6ab746a5dfa74392a959592af79cde0bf1c6c9a30785327dc465fc6ca7d6e1c587f91524062900190316ee8', '2020-08-03 22:55:38', '2020-08-03 22:55:38'),
(20, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-04 04:41:58\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"nLbYNSMQgA7XkXpJzKq3m4n7mQLKUyIqTY3ni4g5cVl4cIGyb8l8SjBPOFkB\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-04 04:41:59\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'cfb705e2e7cacf93767fa628aeb9e785421fc722971119531d2177a5119410f834de3d66833c60ed9b099f5d6b1a38b134811005475fd9c8678eb2bde3579210', '2020-08-04 16:33:57', '2020-08-04 16:33:57'),
(21, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-05 04:34:02\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"nLbYNSMQgA7XkXpJzKq3m4n7mQLKUyIqTY3ni4g5cVl4cIGyb8l8SjBPOFkB\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-05 04:34:02\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8c6294a7e69a0b6b55caf531da4333330114521691fea25b863b729b8ccd6f1a99beb27e99623ac9cce465cb139810dc385d7d6310e8d55950f7a06c125c1c17', '2020-08-04 16:34:02', '2020-08-04 16:34:02'),
(22, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-05 04:34:02\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"nLbYNSMQgA7XkXpJzKq3m4n7mQLKUyIqTY3ni4g5cVl4cIGyb8l8SjBPOFkB\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-05 04:34:03\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9cd45ad4bf1e931ce02f50c84ea7ab3bdd9a44d317522b5de604d016b1da4290f62e68e7d5919fe74e879c1dafbbda81da09e01e4dffb2a2063c146017f36485', '2020-08-04 16:34:03', '2020-08-04 16:34:03'),
(24, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 2, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"1\",\"name\":\"Semi Truck\",\"manufacturer\":\"Runner\",\"model\":\"T51\",\"manufacturer_year\":\"2019\",\"weight\":\"120 pound\",\"lifetime\":null,\"chassis_no\":\"LEZZWAAADDTTTAAS\",\"engine_no\":\"E11AAATT1\",\"vin_no\":null,\"purchase_date\":\"2017-11-08\",\"license_plate_no\":\"DHAKA-METRO-HA-244855\",\"license_year\":\"2017\",\"main_tank_fuel_id\":\"1\",\"main_tank_fuel_capacity\":\"9\",\"second_tank_fuel_id\":\"1\",\"second_tank_fuel_capacity\":null,\"order\":1,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":false,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-05 06:30:19\",\"created_at\":\"2020-08-05 06:30:19\",\"id\":2}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c8f1751db7febfacf2154168b1c6605592db81c9657e896d8afb2993c3c71205c5537994a8de6edd3b7d2ab0635b29f641b29e47abaaf6856ee4d9ea34bd40e6', '2020-08-04 18:30:19', '2020-08-04 18:30:19'),
(25, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 1, 4, 'created', '{\"vehicle_id\":2,\"document_type\":\"3\",\"document_no\":\"aaaaa\",\"issue_date\":\"2020-08-05\",\"expiry_date\":\"2020-08-05\",\"issuing_authority\":\"BRTA\",\"order\":1,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-05 06:51:58\",\"created_at\":\"2020-08-05 06:51:58\",\"id\":1}', '[\"vehicle_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle/2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '676efae9e9bc0ffdfdd4c04d2bf6e55a920adde7c77058433c6a8e07e863d6f9536ddddb334767ca4632b9e61c82b1be25ebee72c9b0022aae53a16994f6e0a1', '2020-08-04 18:51:58', '2020-08-04 18:51:58'),
(26, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"vehicle_id\":\"2\",\"branch_id\":\"2\",\"driver_id\":\"4\",\"take_over\":\"2020-08-05\",\"hand_over\":null,\"order\":2,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":false,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-05 07:48:17\",\"updated_at\":\"2020-08-05 09:17:02\",\"deleted_at\":null}', '[\"order\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle_driver/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '107e63d7bf7c88a42cfe3112440d4516647f36845617c888a1e071898870278adb2dc418cf28dfa9e62b9f24e10394c544e9cc1670c14b161136d4cbd16f810f', '2020-08-04 21:17:02', '2020-08-04 21:17:02'),
(27, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 1, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"4\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"1\",\"rate\":\"65\",\"order\":1,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:28:24\",\"created_at\":\"2020-08-06 04:28:24\",\"id\":1}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'e367dfeeaac5e2d588ab9cc80803f4f21567732bfb6a93730adc34484d479da223ad147f521881ca531a20674d30633f2c0ad29ee5a98cd8bb5ba1db4d870b8b', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(28, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 2, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"4\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"2\",\"rate\":\"86\",\"order\":2,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:28:24\",\"created_at\":\"2020-08-06 04:28:24\",\"id\":2}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'b16c0a185c1e1ce3be8ccea09f52be8aa0c9d7914ff3b05ed7df127ed22196011dac1d649e0107db6fd2345237b1519104fd31f6caba19a18b5e7abda1af690d', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(29, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 3, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"4\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"3\",\"rate\":\"89\",\"order\":3,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:28:24\",\"created_at\":\"2020-08-06 04:28:24\",\"id\":3}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '2f074f79841417cfc2cb900e2aebb4ef115aedbe536051697b34cf89aa601f261bd0c29af87170fafab183b01754d33a371da921634bae931c994aa4a041ba56', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(30, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 4, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"4\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"4\",\"rate\":\"43\",\"order\":4,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:28:24\",\"created_at\":\"2020-08-06 04:28:24\",\"id\":4}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ce930738fe4e1ce55fa94d261bcd16e089f17eb135194a1ee7f75cd6af3f8485010429814b078378f57471741ede6740a54ccb03291a57ef871147b4473833d9', '2020-08-05 16:28:24', '2020-08-05 16:28:24'),
(31, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 5, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"5\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"1\",\"rate\":\"65\",\"order\":5,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:56:06\",\"created_at\":\"2020-08-06 04:56:06\",\"id\":5}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '047afdc6a68f8be472440d9737577e1cf25cf4892555eed573661672bceab5d49ee42b20bbcc340c26c563b564ef7c35142562fadd751149ad562213aaa22747', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(32, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 6, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"5\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"2\",\"rate\":\"86\",\"order\":6,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:56:06\",\"created_at\":\"2020-08-06 04:56:06\",\"id\":6}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '86e2ecdbd5d1b9ed8fb688d4f96ee6b4b938fc617b3cca386b9122e4e75bcf12e581cc56e4fd31002acf87170d30506906ab8c6b10b5e2cd7eb2e690530b0ff7', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(33, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 7, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"5\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"3\",\"rate\":\"89\",\"order\":7,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:56:06\",\"created_at\":\"2020-08-06 04:56:06\",\"id\":7}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '4fab3b85afab9e68f4b394798669ab7aa70f2765259eac3394b8bb99312b46fa26b8f7edfff80d3c8ae9c0f58470f9ca98776c7c9e41672c5f8fac96ac9b2502', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(34, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 8, 4, 'created', '{\"company_id\":\"1\",\"branch_id\":\"5\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"4\",\"rate\":\"43\",\"order\":8,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 04:56:06\",\"created_at\":\"2020-08-06 04:56:06\",\"id\":8}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8a9e020b6f29a0c57f32037c32a10eab6ee83854b3e96a72b06cba8fd47ca23241ef3edac8681be4dce27a2e9f79bf6fb3c26ed3dbac918e4ec6901f9dfd9152', '2020-08-05 16:56:06', '2020-08-05 16:56:06'),
(43, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 5, 4, 'updated', '{\"id\":5,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"1\",\"branch_id\":5,\"rate\":\"65.00\",\"order\":5,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-06 04:56:06\",\"updated_at\":\"2020-08-06 05:58:30\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c492703b8e68ca9e61108dab645644339d5c1858ad02e2ff032a01ddad25aa73f001a9c49f2b83278ae949d129d737b08c16cfa3343b530a4c52cb95b89b76a6', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(44, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"2\",\"branch_id\":5,\"rate\":\"86.00\",\"order\":6,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-06 04:56:06\",\"updated_at\":\"2020-08-06 05:58:30\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '69528175c43deed997f8b232e630de8413cb084fc7ca1951b7cdc531d556c1e6ce272b72d9e4129e90756e0a66a512a9c8185e2cb638c1215420871d0141d010', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(45, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 7, 4, 'updated', '{\"id\":7,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"3\",\"branch_id\":5,\"rate\":\"89.00\",\"order\":7,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-06 04:56:06\",\"updated_at\":\"2020-08-06 05:58:30\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '86a9c2273e63613ec835a4b3c35519104414d4c5d8c8eba28718e18e4f2277f3b0a3e9cc90a3cbf2599a00164fb51504b7bbb406167196c514974b84a4241d54', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(46, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 8, 4, 'updated', '{\"id\":8,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"4\",\"branch_id\":5,\"rate\":\"430.00\",\"order\":8,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-06 04:56:06\",\"updated_at\":\"2020-08-06 05:58:30\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '4a90b5493bd1007adc27619d76ebd6b3655cfc589f1d65304cf22575fdf9206b92d97be3e9fbfdf867b59c98fab4e8c635495321dc44e549e8991ebb02cfd84e', '2020-08-05 17:58:30', '2020-08-05 17:58:30'),
(47, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 3, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"4\",\"name\":\"Semi Van\",\"manufacturer\":\"Runner\",\"model\":\"H51\",\"manufacturer_year\":\"2009\",\"weight\":null,\"lifetime\":null,\"chassis_no\":\"LEZZWAAADDTTTAAS1\",\"engine_no\":\"E11AAATT12\",\"vin_no\":null,\"purchase_date\":\"2017-08-01\",\"license_plate_no\":\"MTL-DM-MA-51-3755\",\"license_year\":\"2019\",\"main_tank_fuel_id\":\"1\",\"main_tank_fuel_capacity\":\"9\",\"second_tank_fuel_id\":\"4\",\"second_tank_fuel_capacity\":\"5\",\"order\":2,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":false,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-06 10:16:07\",\"created_at\":\"2020-08-06 10:16:07\",\"id\":3}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '49a8d36f29c60e4b00c3bdb9d96086e23276785d88d46ea010b0059b4ae7935e0e8e0e89f434a3e6f76d259a1cfe226fbc5a788f73fdf712b03195a93adc33cc', '2020-08-05 22:16:07', '2020-08-05 22:16:07'),
(48, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 2, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"3\",\"branch_id\":\"2\",\"driver_id\":\"4\",\"take_over\":\"2020-08-06\",\"hand_over\":null,\"order\":2,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 10:16:33\",\"created_at\":\"2020-08-06 10:16:33\",\"id\":2}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle_driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f733feff2161560a877937c7f2a70be349ae557fe8675364d89260cc61d96547c029ba1d416e4bc297b6a2e93eeb2b34f596dd6d37bdd116d74318b556085051', '2020-08-05 22:16:33', '2020-08-05 22:16:33'),
(49, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 3, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"3\",\"branch_id\":\"2\",\"driver_id\":\"1\",\"take_over\":\"2020-08-06\",\"hand_over\":null,\"order\":3,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-06 10:17:48\",\"created_at\":\"2020-08-06 10:17:48\",\"id\":3}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle_driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'b1c05b185ef0cb1bbf6ef73c0ed3ceb66d50207170a8810024352767616207f65e8ba41b00cf001bb5ae1f4c53f2f0628bd3f81c4834dbe8b59b3e2be8ec605b', '2020-08-05 22:17:48', '2020-08-05 22:17:48'),
(50, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-05 04:34:02\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"z5D97oysdUL6NSopMRiabNz0KWC0AnpWqGWcol99sKKAZfzO2lDBJR9hzOQA\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-05 04:34:03\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '01278e7599b1a361bdc09f67465814ee77940156373318781810d778be815953b7c3dd8d27c4b293c224afba6033f8f2b54d67524b8a7164bb98e4b0f5be0001', '2020-08-05 22:36:23', '2020-08-05 22:36:23'),
(51, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:36:29\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"z5D97oysdUL6NSopMRiabNz0KWC0AnpWqGWcol99sKKAZfzO2lDBJR9hzOQA\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-06 10:36:29\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'd9b085fce7f7900c378121e51098308a094d713c1bb72efade866c74f7d2a9fbf9d0c59ff2ca15d805f910102055cb264ea2112c8a9cf2a8ba032e8127011242', '2020-08-05 22:36:29', '2020-08-05 22:36:29'),
(52, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:36:29\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"qCluGq2LCqKjKQi03nkgwzt23TnsrROwrOdoygv8vJcos0ybpCBjy41p7vcX\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-06 10:36:29\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '693a44310fe4364f4cc86a67c56f20bc017d54442c4ce93dbdf0b40df9bb05ba68f8842e16db09a3ab49af71999a33679b20212246f16db6a99fb9bae84adb93', '2020-08-05 22:37:42', '2020-08-05 22:37:42'),
(53, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:37:50\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"qCluGq2LCqKjKQi03nkgwzt23TnsrROwrOdoygv8vJcos0ybpCBjy41p7vcX\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-06 10:37:50\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '903e49f46a43daa0ffde64277c95c35f47271bd07bace11679da5a3087f1d1a7a6243fc538e2ee25e46c878760bcee5907afed7ac70d3f9ad72c30427a0b7b84', '2020-08-05 22:37:51', '2020-08-05 22:37:51'),
(54, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms\\/8qOmPS0g7wb9QPB1WUVmCOC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:37:50\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"qCluGq2LCqKjKQi03nkgwzt23TnsrROwrOdoygv8vJcos0ybpCBjy41p7vcX\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-06 10:37:51\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '45e5238b72bf3070aad4ebf3225f20e606dd7c9df1fba6cf8be11fcbbf6bf07f1c8b3d7baa402debf918b7acabe9afadf7f679cf7c170f26d70431f1eaec8c8b', '2020-08-05 22:37:51', '2020-08-05 22:37:51'),
(55, 'App\\Models\\User', 1, 'App\\Models\\User', 6, 4, 'created', '{\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"password\":\"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW\",\"active\":true,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":true,\"updated_at\":\"2020-08-06 10:41:05\",\"created_at\":\"2020-08-06 10:41:05\",\"id\":6}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/user', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '782690e87b1afc261b237a354b30bc85e18f2ca3eb9fa9c958cc041ffdf18f46f55985a1fb5f717a6d253972c1ee63b5e27de03943ca1f1f3f9da2bc56f6519c', '2020-08-05 22:41:05', '2020-08-05 22:41:05'),
(56, 'App\\Models\\User', 1, 'App\\Models\\Permission', 59, 4, 'created', '{\"name\":\"transaction-fuel-rate-index\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-06 10:43:48\",\"created_at\":\"2020-08-06 10:43:48\",\"id\":59}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'eb7fb8a1c8de4ef89230b17be591345412352abd47434160845de8bb21897525644832d7d5b4e65b28a103479f19f898b10aa532e7a37b2c48ac59a7363fe884', '2020-08-05 22:43:48', '2020-08-05 22:43:48'),
(57, 'App\\Models\\User', 1, 'App\\Models\\Permission', 60, 4, 'created', '{\"name\":\"transaction-fuel-rate-create\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-06 10:45:25\",\"created_at\":\"2020-08-06 10:45:25\",\"id\":60}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'b6c901a43ccf34e31206717d7305557a4b0c61ceda34b2f521d6cb2207ba1e876f0edd33ba071d02ff422995357c8b00cfa0b0317d0a31cb8dc9c89bd99a975f', '2020-08-05 22:45:25', '2020-08-05 22:45:25'),
(58, 'App\\Models\\User', 1, 'App\\Models\\Permission', 61, 4, 'created', '{\"name\":\"transaction-fuel-rate-update\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-06 10:45:35\",\"created_at\":\"2020-08-06 10:45:35\",\"id\":61}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '321a652234c5c1f7093e933fedd66ea35b5f2c233d0282e9743f3c30b9b0316cd720e6453ceede88025c814ffed814f368cfcfbacbb041e3b809478c3f0743d6', '2020-08-05 22:45:35', '2020-08-05 22:45:35'),
(59, 'App\\Models\\User', 1, 'App\\Models\\Permission', 62, 4, 'created', '{\"name\":\"transaction-fuel-rate-edit\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-06 10:45:44\",\"created_at\":\"2020-08-06 10:45:44\",\"id\":62}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6df917bdd172944574ac80a33eba19bed818e3d2ec053bccaa6bc0f4b39d9ccc5b71e1f1fca0175c2ada0f141dfadc4029f97917c7f0ea51a99a726d143a0903', '2020-08-05 22:45:44', '2020-08-05 22:45:44'),
(60, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms\\/8qOmPS0g7wb9QPB1WUVmCOC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:37:50\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"OoqdZlemSru6nmzANMk56f3KKLaOcu4fVZESZEIAAe68lxwAWrmeanXmfXJQ\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-06 10:37:51\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f039dac3f7388ab89cc6c9817b3bb980bb02a9c06540b1984b865d06901dabe687d1538c897a6b8f50d86f2aa626a873ec6df9105a54cd070cf7e7bb8d88fa15', '2020-08-05 22:46:47', '2020-08-05 22:46:47'),
(61, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"lxtynmxb3qms7zTa66EUfRp9gaGPuRCmhn9s1Cw5IzOi8GMNSpuUk02tcn6M\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-06 10:41:05\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c8992e7a93b6514055cf574752733a91278539b721ca8a18bd6f580ad975394e331926ef20a5fb0768f1ee1d4dba8c2be056320bd927b83b602503489536580c', '2020-08-05 22:46:54', '2020-08-05 22:46:54'),
(62, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:46:54\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"lxtynmxb3qms7zTa66EUfRp9gaGPuRCmhn9s1Cw5IzOi8GMNSpuUk02tcn6M\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-06 10:46:54\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '1979aae09fc66bf1ccd8da9b0bbbac4eb837516c2b1ef1d426c8bd72a2a94be931c1ee79b60775fe32fe515a841c2a06fde7632bec87d6429d42036af4c90cd6', '2020-08-05 22:46:54', '2020-08-05 22:46:54'),
(63, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 10:46:54\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"5taMkqNPAyMadShZ6JzBdKzblLZ6mPgm95H1IPKRpUftKfP8RgfHw0JO2wUK\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-06 10:46:54\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6154df8beb1988e23dc879e8f999c09f0ac8ca6f8965df3234bba9bc173ee00e28607eaa43b66f47d102f729836149ff32b12c5e51543ec06c6f2e03675170cd', '2020-08-05 23:00:24', '2020-08-05 23:00:24');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(64, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 11:00:34\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"5taMkqNPAyMadShZ6JzBdKzblLZ6mPgm95H1IPKRpUftKfP8RgfHw0JO2wUK\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-06 11:00:34\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5fb4cd1a0c0620c219ec6748a1b07b7ce360c40d49dc77af53edc586152ae795601f48080461adcaf0a581eaf5c15935da720cb14f02b9a76d94447211629464', '2020-08-05 23:00:34', '2020-08-05 23:00:34'),
(65, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg\\/qNpTlmCFfgN9nJtAhrq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 11:00:34\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"5taMkqNPAyMadShZ6JzBdKzblLZ6mPgm95H1IPKRpUftKfP8RgfHw0JO2wUK\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-06 11:00:35\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ae5724e0c764934243ba0c1ca67addd52a3a887230931297616ce7ded69e032c6610da326615a2e70eed38d6fe6b90321ea227512a5ff13d1facf7e0da93a305', '2020-08-05 23:00:35', '2020-08-05 23:00:35'),
(66, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 9, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"1\",\"rate\":0,\"order\":1,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:18:58\",\"created_at\":\"2020-08-06 11:18:58\",\"id\":9}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8cd1c02adceb65fcb279a129f8d242535a1ccc2964023626d59473d2620cd1a62b5174c974caf22bbfa3cb5a1067e94eeea934fde7495b67121835618571dd76', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(67, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 10, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"2\",\"rate\":\"86.3\",\"order\":2,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:18:58\",\"created_at\":\"2020-08-06 11:18:58\",\"id\":10}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '7528951bb46a783179901e47e5d3af59af3ca142f12aa4a7466faf23f990a8d231b8740b886ff0650d7e9679d8833841e8cfad58c7e5751dc6ef860c866432f6', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(68, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 11, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"3\",\"rate\":\"89.3\",\"order\":3,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:18:58\",\"created_at\":\"2020-08-06 11:18:58\",\"id\":11}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'e1ba14c33e0ca31e2107221a063d00d9d0f3ad9ce698c9d059fc36ab13964a09d7a6b989f473c8ea660501835359ef8e77404b189e976b6c8fcc6cb99b8453bf', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(69, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 12, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06\",\"fuel_id\":\"4\",\"rate\":\"43\",\"order\":4,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:18:58\",\"created_at\":\"2020-08-06 11:18:58\",\"id\":12}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '4f5d21bea6d365c99723bcb4c289ad3ad435078f0a1de706c0bfd3cf8bb3ebbb39b9645bc387fd9f3156c8ba7ac45e81fa7d110168493585706a42cfc273024a', '2020-08-05 23:18:58', '2020-08-05 23:18:58'),
(70, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 9, 4, 'updated', '{\"id\":9,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"1\",\"branch_id\":21,\"rate\":\"0.00\",\"order\":1,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_by\":6,\"deleted_by\":null,\"created_at\":\"2020-08-06 11:18:58\",\"updated_at\":\"2020-08-06 11:25:22\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9c9e29a0191c6e940e1ecb1ef798f8a90be748ac18405454a4705c1818a73f6180553a60ed888b220d1219e4aa3a73f489caede9d4894975193d6ad691879b36', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(71, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"2\",\"branch_id\":21,\"rate\":\"86.30\",\"order\":2,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_by\":6,\"deleted_by\":null,\"created_at\":\"2020-08-06 11:18:58\",\"updated_at\":\"2020-08-06 11:25:22\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c97010916b11f08f1fee2cfacd1af7dd50303c29d5990e0929dab31e7f9e82739c3feaa338afb31cc6fc121bf83e120329f08ecaf58d62f636e47cb57f75cf5a', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(72, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 11, 4, 'updated', '{\"id\":11,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"3\",\"branch_id\":21,\"rate\":\"89.30\",\"order\":3,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_by\":6,\"deleted_by\":null,\"created_at\":\"2020-08-06 11:18:58\",\"updated_at\":\"2020-08-06 11:25:22\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '212e0cc91808768a392bbb51a02802e498c5577b5132f46b8e32702051ff47c5bfe96c5f4c30b0c41bc3bd8d48b93b564b19e24c82551eb5841ca4e18ecdd42b', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(73, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"entry_date\":\"2020-08-06\",\"fuel_id\":\"4\",\"branch_id\":21,\"rate\":\"43.00\",\"order\":4,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_by\":6,\"deleted_by\":null,\"created_at\":\"2020-08-06 11:18:58\",\"updated_at\":\"2020-08-06 11:25:22\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5cab55726e82f942a5138034998f48e471b70b6b87de0a3edfb93dfdf605e99c4fee164cb200d7b0acf8df686ade33e8b5edb57bbeec04525195fee297661453', '2020-08-05 23:25:22', '2020-08-05 23:25:22'),
(74, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 13, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"0\",\"order\":5,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:35:17\",\"created_at\":\"2020-08-06 11:35:17\",\"id\":13}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a409bd3c55390b77216d1519470e1e1ce3bdc7a56794dc75baa608e538ae2d22f73f9a72045424df84a29ae971b8ac7648e4c4d3c5447d2bc0012cb24b410b12', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(75, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 14, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"89\",\"order\":6,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:35:17\",\"created_at\":\"2020-08-06 11:35:17\",\"id\":14}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a74a196bf7787a95928d73fcd3fa473a162ce4da3cd5cf6eeb91d4ad9121b81c0c147c5302876164c6d6b34c6f402b82dff1db837fdf87298bf6036ce9bcddeb', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(76, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 15, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"84\",\"order\":7,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:35:17\",\"created_at\":\"2020-08-06 11:35:17\",\"id\":15}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '80268ab6036b52e388518f7e5d0a2e6b08ebe2bb3cb263f54a33ec2e7e13d30db81d1fc5980ba1f6e954b7658a2e1d6143eff1b843aed75473612a35de602519', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(77, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 16, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-08-06 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"60\",\"order\":8,\"archived\":0,\"active\":1,\"created_by\":6,\"updated_at\":\"2020-08-06 11:35:17\",\"created_at\":\"2020-08-06 11:35:17\",\"id\":16}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'be0e73a2f03ff276cd41104b3bd7126f25132e80f44ba2412ba5666147e51911c7d61b9b463326a3f692294e6ecff872ba9188f068403ab3f0ee3d0c9528f112', '2020-08-05 23:35:17', '2020-08-05 23:35:17'),
(78, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg\\/qNpTlmCFfgN9nJtAhrq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-06 11:00:34\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"wtx4sISe7kGGI1f34JuhNbXGmTE4FUoHPEJ406JwVYQEB7OxjCz5EuKdMr4B\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-06 11:00:35\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9b170d7df1060d9f6d30bd8e03997b2eb9c430e487040c0908b4f7de713cac5e27ea542cbd634d56378d9a95691dcb6a26e9d0b4daa5f4d184dbe83d90cc203e', '2020-08-07 17:52:58', '2020-08-07 17:52:58'),
(79, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg\\/qNpTlmCFfgN9nJtAhrq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 05:53:10\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"wtx4sISe7kGGI1f34JuhNbXGmTE4FUoHPEJ406JwVYQEB7OxjCz5EuKdMr4B\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 05:53:11\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '98e7849d7f908cad42e17e3548b046a3317ec77b7fa16b7a67d4c416d03b7fdf897b48a62314d870958cc90cbd1e8aee9c59cd1f1b8d66ac065222a84d889c67', '2020-08-07 17:53:11', '2020-08-07 17:53:11'),
(80, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$\\/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 05:53:10\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"wtx4sISe7kGGI1f34JuhNbXGmTE4FUoHPEJ406JwVYQEB7OxjCz5EuKdMr4B\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 05:53:11\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5abecf0649e5b4b12026db229bb3698ba19b9f3b3ac72cbc91f1ff98fd1eda2502e11aa3418d7c089a3e483b6a98724dbcdbcbd3f8cfbca418138fcfac971e44', '2020-08-07 17:53:11', '2020-08-07 17:53:11'),
(81, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$\\/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 05:53:10\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"guleyUwINfU8nLtfz1et6gCtzxgISyZuL8rpzsinzj3oqu8pNzzSSHF2ojpP\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 05:53:11\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '202bd9311c4f9373e1b36994d058982a05bba9bbed7800a8c900137623445282a5a3a4810bc855190383647656c8ee8cdf12c3befefdc27a4b5fde386a926bb4', '2020-08-07 18:43:03', '2020-08-07 18:43:03'),
(82, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms\\/8qOmPS0g7wb9QPB1WUVmCOC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:43:11\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"OoqdZlemSru6nmzANMk56f3KKLaOcu4fVZESZEIAAe68lxwAWrmeanXmfXJQ\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-08 06:43:11\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6dbcbb70b8b9b5f988df911ca315114c2f7007bde7b5c9b5589db8fd2470ef95801e7d70a6d2fca0ed757cef140ff5ab68823a1d8c29b9870e0fc3426a97ce85', '2020-08-07 18:43:11', '2020-08-07 18:43:11'),
(83, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$CNB\\/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:43:11\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"OoqdZlemSru6nmzANMk56f3KKLaOcu4fVZESZEIAAe68lxwAWrmeanXmfXJQ\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-08 06:43:11\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '31b304b6a0e54f74ce3beeafa00657ccd870e6260c838cf108e6476846240647a056ae428f4a651d46c16cabe09b923579b3d37e1a9ef19e63c4250d02b71388', '2020-08-07 18:43:11', '2020-08-07 18:43:11'),
(84, 'App\\Models\\User', 1, 'App\\Models\\Permission', 64, 4, 'created', '{\"name\":\"transaction-logbook-entry-create\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-08 06:43:37\",\"created_at\":\"2020-08-08 06:43:37\",\"id\":64}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c1f525ff74c7ab448a614ad9adb0390735b644d9b9916c09c3c246a34aee6176d8feccff69620def4b4d4e7456ad141fec72d1b1c9510d0a3b886389ad14c1dd', '2020-08-07 18:43:37', '2020-08-07 18:43:37'),
(85, 'App\\Models\\User', 1, 'App\\Models\\Permission', 65, 4, 'created', '{\"name\":\"transaction-logbook-entry-store\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-08 06:43:51\",\"created_at\":\"2020-08-08 06:43:51\",\"id\":65}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '023c943dff30ec6b709e55a9f4f83a72ac4abcd89dc70a2572fdafa76df440aad4f0dc91c39cad5d8d964fda669c69c9a6f1a757330cbb36a47272a47fbd703e', '2020-08-07 18:43:51', '2020-08-07 18:43:51'),
(86, 'App\\Models\\User', 1, 'App\\Models\\Permission', 66, 4, 'created', '{\"name\":\"transaction-logbook-entry-edit\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-08 06:44:01\",\"created_at\":\"2020-08-08 06:44:01\",\"id\":66}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5ddbf1d7884986455314866cc945ab977d60a894b895c99fc268a8515d6845a3c8bf7a9e8373c08fdc1cdc3f1b5b508a21c6f76c040c7afa25140e9c46451e8f', '2020-08-07 18:44:01', '2020-08-07 18:44:01'),
(87, 'App\\Models\\User', 1, 'App\\Models\\Permission', 67, 4, 'created', '{\"name\":\"transaction-logbook-entry-update\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-08 06:44:10\",\"created_at\":\"2020-08-08 06:44:10\",\"id\":67}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8c916662dbb93f4ae84ba41629a9ff3fe89a4432a58d1f702fcdd2df36c1d59bcf3ef8bbc670789e7c95610f8d9316c8454e5c038889cf5a9e04331f4574279e', '2020-08-07 18:44:10', '2020-08-07 18:44:10'),
(88, 'App\\Models\\User', 1, 'App\\Models\\Permission', 68, 4, 'created', '{\"name\":\"transaction-logbook-entry-delete\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-08 06:44:18\",\"created_at\":\"2020-08-08 06:44:18\",\"id\":68}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '7c4c549f43d040321206dbd12b6c40bce3943e73a60b1d55c8f6208b1084ca97feabc937cbc54231ab2532cf3a482412ef07746704f80ec952b2730c821f2bbf', '2020-08-07 18:44:18', '2020-08-07 18:44:18'),
(89, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$CNB\\/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:43:11\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"CNOlmWGObCe7eCzgQsXMrv0qM8KwJkLWbvmOsnbXtujlo1XrwTpmqbFojQrv\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-08 06:43:11\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a7b02e333a85f872257be41e434864b431f50a9578136551220fc210737db4cd273807fd882a1f3c58bf3c4b23789fbe4d30ba1deb2ad688447e742abf38ea5f', '2020-08-07 18:46:23', '2020-08-07 18:46:23'),
(90, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$\\/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:46:29\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"guleyUwINfU8nLtfz1et6gCtzxgISyZuL8rpzsinzj3oqu8pNzzSSHF2ojpP\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 06:46:29\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6b9a485f6338caaa005cc151899788f08e5bb805e79e2940becd1798e03d3eea4577f79c14ebf4bc406411e4398eae0724e5c069e1f892734dea2ad2c1af100f', '2020-08-07 18:46:29', '2020-08-07 18:46:29'),
(91, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:46:29\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"guleyUwINfU8nLtfz1et6gCtzxgISyZuL8rpzsinzj3oqu8pNzzSSHF2ojpP\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 06:46:29\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '83b0608ee24c5395fa2758e835a428038e50849d9b87c53782b29c5211a42b6f89a9f2680760e3cc98ae111689c585ddec596d8aad93af1477fceef009c1979f', '2020-08-07 18:46:29', '2020-08-07 18:46:29'),
(92, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:46:29\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"j6cdrAAFJyl3Ki0wIPzmlyhCToLM8JOpvN2Mz4Q8L5Jo0Ot09FZutFybor0Y\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 06:46:29\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '0543b2565469b2584662d49b315f8608c0d5b58c3a3eefc55666c3aefb40f1efb65406485e91c9a446abac4489781c7c291e9ffbe068b104e4212a394570cbe1', '2020-08-07 18:55:34', '2020-08-07 18:55:34'),
(93, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$CNB\\/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:55:39\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"CNOlmWGObCe7eCzgQsXMrv0qM8KwJkLWbvmOsnbXtujlo1XrwTpmqbFojQrv\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-08 06:55:40\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'd488518eeef199ccc2f5150bcc741f72b50e3c13335f6eb63ae21287dc6817d264f3d0f1833cc8f7fef106c31be2a01c47bf1167c819c25a77a3867db5918f81', '2020-08-07 18:55:40', '2020-08-07 18:55:40'),
(94, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$ONvL9H\\/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:55:39\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"CNOlmWGObCe7eCzgQsXMrv0qM8KwJkLWbvmOsnbXtujlo1XrwTpmqbFojQrv\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-08 06:55:40\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '79df3600e4cd5fe066c267e9f65298877339c82543a3068f0f9d8c5ce329baa788499ee7d542fbcc9f0743e174f8db1fbbedb1cd320e9f281decbbb133454934', '2020-08-07 18:55:40', '2020-08-07 18:55:40'),
(95, 'App\\Models\\User', 1, 'App\\Models\\Permission', 69, 4, 'created', '{\"name\":\"transaction-logbook-entry-index\",\"guard_name\":\"web\",\"updated_at\":\"2020-08-08 06:55:57\",\"created_at\":\"2020-08-08 06:55:57\",\"id\":69}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '1e94a08ad9cba66492490c6aa634225ff086f48539d5d5feef7587af49ea218431929bf9f3d9e692805f82b831dad3ec4ce2b63319b0f4434fdcb5880d18ab94', '2020-08-07 18:55:57', '2020-08-07 18:55:57'),
(96, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$ONvL9H\\/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:55:39\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-08 06:55:40\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a5b083e8940ba4acd916f08cddb741d1333879a8be9950a63a2e26caa68c45b41ee897bf3f01a7ecee8fbc303eac229e142698ab30b935f1c62473816142b7f4', '2020-08-07 18:56:26', '2020-08-07 18:56:26'),
(97, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:56:33\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"j6cdrAAFJyl3Ki0wIPzmlyhCToLM8JOpvN2Mz4Q8L5Jo0Ot09FZutFybor0Y\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 06:56:34\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '2833372ad59544713e8754c1de199460046f94c9dba879356af7cfc42b69d0cb7004f3ebca30b5bf23c60572c3faf54108fde572ed748d867ba7eff73c941b3b', '2020-08-07 18:56:34', '2020-08-07 18:56:34'),
(98, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:56:33\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"j6cdrAAFJyl3Ki0wIPzmlyhCToLM8JOpvN2Mz4Q8L5Jo0Ot09FZutFybor0Y\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 06:56:34\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '81d7445258bae999460b0268cdee59349adc90a90bda1f66660e9ec079c8bc887b9a0ca83818536640ae9163cb198bd5cb41a154fd53353ae1001b75305e078f', '2020-08-07 18:56:34', '2020-08-07 18:56:34'),
(99, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-08 06:56:33\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"w0htWJxoVGfRWEwRd4vBxrsw27WkGPLbqDdoHJZe195UuLpENfw4svjIW7YD\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-08-08 06:56:34\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f3322568971b2a40e5d99b2271148e3d4d62ff23ec93fa1436c4671945508046947dfe121f33c72a5194ff5d97d6450e1eddaaa9e8ab94502a2d575f876364f4', '2020-08-08 18:51:27', '2020-08-08 18:51:27'),
(100, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$ONvL9H\\/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-09 06:51:48\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-09 06:51:48\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c694418df80e063851f92e5c2541b9f6a9764d434e3cab602971962d22b91ece01c555869dff5b8792dc42ab157058abc2722037f03be14dcb3f609801200fc0', '2020-08-08 18:51:48', '2020-08-08 18:51:48'),
(101, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Eqn4puXstk0B7kXpKHiJWuvg\\/xMVaa08HjaMe32vdCuQ5lWjx7b9.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-09 06:51:48\",\"last_login_ip\":\"::1\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-09 06:51:48\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '814f096dba942fd11435d08cebba934be09f292c4579df18e6151140283d1c24945f0a6910e9622d3a7796610810cae9df240edbeb229baf460b7cb6a5ce7f1b', '2020-08-08 18:51:49', '2020-08-08 18:51:49'),
(104, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 6, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"3\",\"name\":\"Freezer Truck\",\"manufacturer\":\"Ace\",\"model\":\"AW22\",\"manufacturer_year\":\"2017\",\"weight\":\"120 pound\",\"lifetime\":null,\"chassis_no\":\"LEZZWAAADDTTTAAS11\",\"engine_no\":\"E11AAATT121\",\"vin_no\":null,\"purchase_date\":\"2019-08-01\",\"license_plate_no\":\"DHAKA-METRO-HA-124522\",\"license_year\":\"2017\",\"main_tank_fuel_id\":\"1\",\"main_tank_fuel_capacity\":\"12\",\"second_tank_fuel_id\":\"4\",\"second_tank_fuel_capacity\":\"15\",\"opening\":\"14123\",\"order\":3,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":false,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-09 11:23:50\",\"created_at\":\"2020-08-09 11:23:50\",\"id\":6}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"opening\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '835a6c5ea8b4aa40a7a96bd7f13f180ab067f4a8e0045423074728e22b3cfe1505421d6ef87a11eddbe6b3f0f02345d8ff6beca355f65ac41f24152885225768', '2020-08-08 23:23:50', '2020-08-08 23:23:50'),
(105, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 1, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-08-09\",\"vehicle_id\":6,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"12\",\"millage\":\"9\",\"order\":1,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-09 11:23:50\",\"created_at\":\"2020-08-09 11:23:50\",\"id\":1}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '10c9270b0633d7f165975c7d160829e68cb3ecdfe1ccef27634622e082348aed7176d022f609987c0c56c41b379c6c4dfd7e8c2c07f6a05c8bc59020fd5766ca', '2020-08-08 23:23:50', '2020-08-08 23:23:50'),
(106, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 2, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-08-09\",\"vehicle_id\":6,\"tanks\":1,\"tank_type\":2,\"fuel_id\":\"4\",\"capacity\":\"15\",\"millage\":\"12\",\"order\":2,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-09 11:23:50\",\"created_at\":\"2020-08-09 11:23:50\",\"id\":2}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ee23dec0593d8e1e8024aa6e6dab5275b1c9fbd002a810100f315e1b907630ddbaa9af23529a353649f5ece82ffb5335f5e7cca5d2b2c33a8db86bde5607bd66', '2020-08-08 23:23:50', '2020-08-08 23:23:50'),
(107, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 1, 4, 'updated', '{\"id\":1,\"vehicle_id\":2,\"document_type\":\"3\",\"document_no\":\"aaaaa\",\"issue_date\":\"2020-08-05\",\"expiry_date\":\"2020-08-05\",\"issuing_authority\":\"BRTA\",\"order\":null,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":false,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-05 06:51:58\",\"updated_at\":\"2020-08-09 11:51:50\",\"deleted_at\":null}', '[\"order\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle/2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a17b076ad3e3c602b1004e467771f097ca380a185f0b9073aec16f5a2dfdf2fc61e16e572bff6c1630e56335a315f15a0ee05c6ecd354296f269a2544381c981', '2020-08-08 23:51:50', '2020-08-08 23:51:50'),
(108, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 3, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-08-09\",\"vehicle_id\":2,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"9\",\"millage\":\"12\",\"order\":3,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-09 11:51:50\",\"created_at\":\"2020-08-09 11:51:50\",\"id\":3}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle/2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '69962bb1e3d503d986fee27abded6eadea4a75952aab458412b198cff9caf038e5ec2ca02f52e5af118aa77823a17a82e84ce16707dc16b60aad3710757711e1', '2020-08-08 23:51:50', '2020-08-08 23:51:50'),
(109, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Eqn4puXstk0B7kXpKHiJWuvg\\/xMVaa08HjaMe32vdCuQ5lWjx7b9.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-12 09:39:05\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-12 09:39:05\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '43a8820320ef227c82e215ce54b19b81c8121e273975080f7b7bff62c175b31e117254fa91a8dff9e45869320fe9dc799876538ffc786c41b6e25296c9c330a6', '2020-08-11 21:39:06', '2020-08-11 21:39:06'),
(110, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$TFz5OpeJVpDmaqhNCDSc5uiov6m0C30lw6ngWc6raurxaCwkk0cbq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-08-12 09:39:05\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-08-12 09:39:06\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ea4f08944184f1ea1c8fe8313569bacb439442df6bc848c242c0d1af151921d41fed96ab0fc297c1a132c7bc8c6c4ef54ae3f93b71c137f0a90e26096281d3d2', '2020-08-11 21:39:06', '2020-08-11 21:39:06'),
(111, 'App\\Models\\User', 1, 'App\\Models\\User', 7, 4, 'created', '{\"first_name\":\"Bashir\",\"last_name\":\"Uddin\",\"email\":\"bashir@bashir.com\",\"username\":\"bashir\",\"password\":\"$2y$10$XFe6dqOEiYVO0QLk9O8oqOxEOD50\\/uGlOVaa1fA9ruzdbqvokPwyK\",\"active\":true,\"confirmation_code\":\"7aa9cdfb31ece02418f281eaf4d86869\",\"confirmed\":false,\"updated_at\":\"2020-08-16 04:47:42\",\"created_at\":\"2020-08-16 04:47:42\",\"id\":7}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/employee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'db844e34494a72452380fae421e835cf05fd5d5134e98fb215f83a698654f1c4ea1017b43ea92e2e5aff31831a496f299ccf859aff7fc84546b239fb345638f1', '2020-08-15 16:47:42', '2020-08-15 16:47:42'),
(112, 'App\\Models\\User', 1, 'App\\Models\\Employee', 2, 4, 'created', '{\"company_id\":\"1\",\"user_id\":7,\"employee_id\":\"123456789\",\"name\":\"Bashir Uddin\",\"designation_id\":\"2\",\"department\":null,\"grade\":null,\"email\":\"bashir@bashir.com\",\"order\":2,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-16 04:47:42\",\"created_at\":\"2020-08-16 04:47:42\",\"id\":2}', '[\"company_id\",\"user_id\",\"employee_id\",\"name\",\"designation_id\",\"department\",\"grade\",\"email\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/employee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6e26a25b2d31dd0a434bb3d8bef5fb9fb37a93d175f6c53ae6f31289ac44d06c10cb41ee46d55dce0169263081719a16806cef3e8af2d4cd7f9e7951dc14dd6f', '2020-08-15 16:47:42', '2020-08-15 16:47:42'),
(113, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 17, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-08-16 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"86\",\"order\":17,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 04:57:25\",\"created_at\":\"2020-08-16 04:57:25\",\"id\":17}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'eb289d8f1494396420cba398cbb8e79cf99ae0b142120756735bdeba42aff492e01946fd48be8bfe597f8e1529c02086bfbc49245c1b1247a1d89505e0e76313', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(114, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 18, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-08-16 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"60\",\"order\":18,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 04:57:25\",\"created_at\":\"2020-08-16 04:57:25\",\"id\":18}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5eef908c16fbd755e23960c10eeac44522c205f636332743437e7b42253086d9d3c1bc1bd516712289dd81e53dbb9864c0c85a376bb597233b7b8f677c65f094', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(115, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 19, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-08-16 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"65\",\"order\":19,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 04:57:25\",\"created_at\":\"2020-08-16 04:57:25\",\"id\":19}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '11cea66f5ae2cd746efcf7cd9a12e76a1393a06ded5b61f9ebec220a5e824f1b264f37bd16389f7ce85f6cc0fbe4690159462f35cb8eaa83ba122ccc7dc1eab0', '2020-08-15 16:57:25', '2020-08-15 16:57:25'),
(116, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 20, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-08-16 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"86\",\"order\":20,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 04:57:25\",\"created_at\":\"2020-08-16 04:57:25\",\"id\":20}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9e1c93a926cd582fc471c82b307b8a93bf41149195c124d2b59c88e5ad8bbda2639b43d13eb9fafdeea65dd41bac35a87ee18151d955eeb0df2327613b24aaec', '2020-08-15 16:57:25', '2020-08-15 16:57:25');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(117, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 17, 4, 'updated', '{\"id\":17,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"1\",\"branch_id\":6,\"rate\":\"86.00\",\"order\":17,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-16 04:57:25\",\"updated_at\":\"2020-08-16 04:57:32\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'acfdc6e6d5ba2c204b69fd735d6348cf9627bce240ab021a6327be13010dc4d5f8c0100f034dd3900d359dec12a38768b92f95cd2b3d0283b7a2cb2acfae9d90', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(118, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 18, 4, 'updated', '{\"id\":18,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"2\",\"branch_id\":6,\"rate\":\"60.00\",\"order\":18,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-16 04:57:25\",\"updated_at\":\"2020-08-16 04:57:32\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ab73525b65b142b87dc64fdda18abccf76e15272c15894b5461f7a0785b35170161e2696b8a023985d3f03afadd9addbbcdffb43b6a3b989a26b29b4c183b589', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(119, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 19, 4, 'updated', '{\"id\":19,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"3\",\"branch_id\":6,\"rate\":\"65.00\",\"order\":19,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-16 04:57:25\",\"updated_at\":\"2020-08-16 04:57:32\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '406fb14709280dddf29e17bbe60ef820c1bae574bd979143efd68a9e5fbb00ff225cebe870d348a2e0982658d5897fb47d34d16218b3133b3d0548b36e288e51', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(120, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 20, 4, 'updated', '{\"id\":20,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"4\",\"branch_id\":6,\"rate\":\"86.00\",\"order\":20,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-16 04:57:25\",\"updated_at\":\"2020-08-16 04:57:32\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'd5ca585d48ae241913549c5608d708de7918d77c06bbe6c9503b1f224c2c6f570a056203757d5bfa48ff83f63efea353b61396cf5e36c2f8bcb21ddee140f8a7', '2020-08-15 16:57:32', '2020-08-15 16:57:32'),
(121, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 7, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"3\",\"name\":\"Semi Freezar Van Van\",\"manufacturer\":\"Toyota\",\"model\":\"AW22\",\"manufacturer_year\":\"2009\",\"weight\":\"120 pound\",\"lifetime\":\"12\",\"chassis_no\":\"LEZZWAAADDTTTAAS11\",\"engine_no\":\"E11AAATT121\",\"vin_no\":null,\"purchase_date\":\"2020-08-15\",\"license_plate_no\":\"DHAKA-METRO-HA-124522\",\"license_year\":\"2019\",\"main_tank_fuel_id\":\"3\",\"main_tank_fuel_capacity\":\"35\",\"second_tank_fuel_id\":\"4\",\"second_tank_fuel_capacity\":\"34\",\"opening\":\"12000\",\"order\":4,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":false,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-16 05:09:19\",\"created_at\":\"2020-08-16 05:09:19\",\"id\":7}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"opening\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c7a6e83e233e2b195659827d9a9c19f6c622060ecee35a9d19e56e176ca37fdfe0d54f61ff5898a05a5fb356e2593a75bcbaab2ad5e7e6f0d48e2a82167b73d4', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(122, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 2, 4, 'created', '{\"vehicle_id\":7,\"document_type\":\"3\",\"document_no\":\"1213123\",\"issue_date\":\"2020-08-16\",\"expiry_date\":\"2020-08-16\",\"issuing_authority\":\"1231\",\"order\":2,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-16 05:09:19\",\"created_at\":\"2020-08-16 05:09:19\",\"id\":2}', '[\"vehicle_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '65a0f9165b4626f41d682f03f4c5020dbc84884ebe7906b719df12cf307279c3f404e4915b7085c0b387b957d91098cf3b766e2adb50cbf655c04df32b468e33', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(123, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 3, 4, 'created', '{\"vehicle_id\":7,\"document_type\":\"4\",\"document_no\":\"1qwqe\",\"issue_date\":\"2020-08-15\",\"expiry_date\":\"2020-08-15\",\"issuing_authority\":\"qweqwe\",\"order\":3,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-16 05:09:19\",\"created_at\":\"2020-08-16 05:09:19\",\"id\":3}', '[\"vehicle_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'fa2ae6fea4dd3aa71c866007cf767b7a50766755b0f40eaa4d0d93946072a913d113ffcb2fd563be1fd51865a3e4d5f6c3270af54fc40c864099d31a7a0a79b1', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(124, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 4, 4, 'created', '{\"vehicle_id\":7,\"document_type\":\"5\",\"document_no\":\"1213\",\"issue_date\":\"2020-08-14\",\"expiry_date\":\"2020-08-16\",\"issuing_authority\":\"qweqe\",\"order\":4,\"active\":false,\"created_by\":1,\"updated_at\":\"2020-08-16 05:09:19\",\"created_at\":\"2020-08-16 05:09:19\",\"id\":4}', '[\"vehicle_id\",\"document_type\",\"document_no\",\"issue_date\",\"expiry_date\",\"issuing_authority\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'bbc81f2b32797d7635af414449ff22a80ce0e2f68315df465cf4f86b7cba597a020b2a7f3e23490b4a401d90909031d3d6bc1361fd78d6515e1a1b1b28ba690d', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(125, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 4, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-08-13\",\"vehicle_id\":7,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"3\",\"capacity\":\"35\",\"millage\":\"9\",\"order\":4,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-16 05:09:19\",\"created_at\":\"2020-08-16 05:09:19\",\"id\":4}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a3f495cfa518a1a575e4632c7d3a106f788861857241733464f094d2e301253a7bf266d80559d88bc2a6f1db38825340c9bba1df89c7303fcf2e930142734a79', '2020-08-15 17:09:19', '2020-08-15 17:09:19'),
(128, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 21, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-08-01 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"86\",\"order\":21,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 05:26:56\",\"created_at\":\"2020-08-16 05:26:56\",\"id\":21}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '3b122d095f10df7976a1d696f49fede3fdb559257acd645664629c4344ab6c45b3ac98a343d71c6a87ce1e4e22b92470bce7be5a6c352bb09fb821550b9f3fbe', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(129, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 22, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-08-01 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"65\",\"order\":22,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 05:26:56\",\"created_at\":\"2020-08-16 05:26:56\",\"id\":22}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'cfe1f4afc96a8b579c01d2c0ffaafbf99e02c22253840805bac74ee313b5e046cac8c7bfa6fd5e610bb8e2e0741cb977f3d46d31c6593ca645a2e7d8b23e2362', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(130, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 23, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-08-01 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"60\",\"order\":23,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 05:26:56\",\"created_at\":\"2020-08-16 05:26:56\",\"id\":23}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '353310c0fab2564198d3f30a3ac1c1e92d8a784c9ae753ee6c9d14130f3ef322cb222bd236e30c37e566dcb47d9ded278000629513b3305e1881d80e28d286d4', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(131, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 24, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-08-01 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"75\",\"order\":24,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-08-16 05:26:56\",\"created_at\":\"2020-08-16 05:26:56\",\"id\":24}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f395ca03e83cae3abad26d726299f47c1495f1437f4ed7be52e4f07e03ab75667a7bbeab411e973f1474563cd0699ed28b50ac3d3e93e727b30fa00b4457395e', '2020-08-15 17:26:56', '2020-08-15 17:26:56'),
(132, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 2, 4, 'updated', '{\"id\":2,\"company_id\":\"1\",\"vehicle_type\":\"4\",\"name\":\"Semi Truck\",\"manufacturer\":\"Runner\",\"model\":\"T51\",\"manufacturer_year\":\"2019\",\"weight\":\"120 pound\",\"lifetime\":null,\"license_plate_no\":\"DHAKA-METRO-HA-244855\",\"license_year\":\"2017\",\"is_dual_tank\":false,\"main_tank_fuel_id\":\"1\",\"main_tank_fuel_capacity\":\"50\",\"second_tank_fuel_id\":\"4\",\"second_tank_fuel_capacity\":\"25\",\"chassis_no\":\"LEZZWAAADDTTTAAS\",\"engine_no\":\"E11AAATT1\",\"vin_no\":null,\"opening\":\"12980.0000\",\"purchase_date\":\"2017-11-08\",\"order\":\"1\",\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-05 06:30:19\",\"updated_at\":\"2020-08-16 05:33:15\",\"deleted_at\":null}', '[\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5b43f314ab395eacd04850de9a6c6e84ae11540b98337d7172e2254dee480aed1bb2c0ef01b2d6ea52072e18edbeff5aee16c86e1aa2336da490b89ec374d13e', '2020-08-15 17:33:15', '2020-08-15 17:33:15'),
(133, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 5, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-08-01\",\"vehicle_id\":2,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"50\",\"millage\":\"15\",\"order\":5,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-08-16 05:33:15\",\"created_at\":\"2020-08-16 05:33:15\",\"id\":5}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '967c49b71d424ece8d638186165340ea4ddbb8a60cf9c05d3a89e7debe47302229d00d922efa330f999760c1f3acc82bd18ae11d99b8d237be0e5488996b5683', '2020-08-15 17:33:15', '2020-08-15 17:33:15'),
(134, 'App\\Models\\User', 1, 'App\\Models\\LogbookEntry', 1, 4, 'created', '{\"company_id\":1,\"sl\":1,\"trans_date\":\"2020-08-01\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"4\",\"driver_name\":\"Parvez\",\"driver_take_ovr_dt\":\"2020-08-01T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-08-01T00:00:00.000000Z\",\"rkm_data\":\"30\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13015\",\"logbook_running\":\"35\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"45\",\"fuel_rate\":\"86.000000\",\"fuel_cost\":\"3870\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":1,\"year\":2020,\"month\":8,\"month_name\":\"08\",\"created_by\":1,\"updated_at\":\"2020-08-16 05:42:22\",\"created_at\":\"2020-08-16 05:42:22\",\"id\":1}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/transactions/logbook_entry', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '06c4492825a7d946ada3bb6595b6ea6c0c9142317de8864922cc877fde4eee3aff22384d11c0da63fc16551e0a204ce69e0ad2feeba48a5e39b431deefab1257', '2020-08-15 17:42:22', '2020-08-15 17:42:22'),
(135, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$TFz5OpeJVpDmaqhNCDSc5uiov6m0C30lw6ngWc6raurxaCwkk0cbq\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:33:03\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:33:03\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'fb2189abc7cfedac02518dc7876d2d805da8b9d9a7645e5a41585658fc2afa4c3e7d9eaff004a5712461297d75f19e3c838b6591ff922333714b2a87698a65fc', '2020-09-07 22:33:03', '2020-09-07 22:33:03'),
(136, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$9SMbOJqey\\/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD\\/wI5vlNUZEe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:33:03\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:33:03\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '05642bfb77f88324c46117da5fed708901cda9f155eb42139d6022d536f6c451754fb9dca54ac8d54d31abf71e6d69531db6e254e560731c0847b6b4efb1c2b1', '2020-09-07 22:33:03', '2020-09-07 22:33:03'),
(137, 'App\\Models\\User', 1, 'App\\Models\\User', 5, 4, 'updated', '{\"id\":5,\"company_id\":1,\"first_name\":\"Rejaul Islam\",\"last_name\":\"Royel\",\"email\":\"rejaul.islam@transcombd.com\",\"username\":\"almas\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL\\/cdyHSOdnbrDXc8O5cUh8kG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"29b575c492a20aafaad3a0aa89807a5f\",\"confirmed\":0,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":null,\"created_at\":\"2020-08-04 00:44:21\",\"updated_at\":\"2020-09-08 04:34:45\",\"deleted_at\":null}', '[\"first_name\",\"last_name\",\"email\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/user/5', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '2320e175077dcf0231871469dfc1527f2788340f5d0a75cf5faa0257a53319162166f722a75450dcc92a12664b3124da7aea5f199d9f9e6cadb1a7a5b8cf10c4', '2020-09-07 22:34:45', '2020-09-07 22:34:45'),
(138, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$9SMbOJqey\\/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD\\/wI5vlNUZEe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:33:03\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Ei5sQ2PSFny7Iuv6yFWYQr4I8jyh6wzkr5xEL9S5tj0CnssGY5G7RoRGUwtI\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:33:03\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'a68f12489b723fe931f9b5779525200068ff1f030ffff6cc75276a075d36fb964eca0fd6207b17e5dea6ad10ae59bbbb1c538da5456fc91941d18b66cfbb52b8', '2020-09-07 22:34:54', '2020-09-07 22:34:54'),
(139, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:34:59\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"w0htWJxoVGfRWEwRd4vBxrsw27WkGPLbqDdoHJZe195UuLpENfw4svjIW7YD\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 04:34:59\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '05078867269122aa43cd338fa248222298ca2e5475b6cf25e9caa7c920a4de026eb33ae3e812a3b372cf76482152c2e9903519189f8c6f217beec9a0ca093694', '2020-09-07 22:34:59', '2020-09-07 22:34:59'),
(140, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L\\/wSMS3bNURqZJNefOnW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:34:59\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"w0htWJxoVGfRWEwRd4vBxrsw27WkGPLbqDdoHJZe195UuLpENfw4svjIW7YD\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 04:34:59\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '6e20cd115074f0226ec3c213c5a33865e893103e5484b20cc60c8adea6dee83f50f6e0ae054e2312ca6a112e26ba22f1478a2d01118a328cd4bc9d7d04eb8086', '2020-09-07 22:34:59', '2020-09-07 22:34:59'),
(141, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L\\/wSMS3bNURqZJNefOnW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:34:59\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"XN84dBdUmjHmLRutVrPEYWgwDq4rTY7VbbtnClijzWa6d0VKSrKReDwbs7oz\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 04:34:59\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'f971b31031167892e7d1bf6645018dbd5a93cfcea3385377f13ad7a29626e7af65cfa5402bfd217841d7866b80ee2c441c5601076965c204399a6392f6c095ee', '2020-09-07 22:35:33', '2020-09-07 22:35:33'),
(142, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$9SMbOJqey\\/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD\\/wI5vlNUZEe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:35:37\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Ei5sQ2PSFny7Iuv6yFWYQr4I8jyh6wzkr5xEL9S5tj0CnssGY5G7RoRGUwtI\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:35:37\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'dcbd81bf9ebe4b5eee3494911c3564556a24f24a1e67738c429ef447e32e9dff9c0d8f784b0e3442548cf3302de0105648ab849e63c4dc4fe3057060b9c13de0', '2020-09-07 22:35:37', '2020-09-07 22:35:37'),
(143, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Dst.N445dreP4kNI\\/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:35:37\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Ei5sQ2PSFny7Iuv6yFWYQr4I8jyh6wzkr5xEL9S5tj0CnssGY5G7RoRGUwtI\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:35:37\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '3e8fe17b800dd334ecd44b61ee820c52d9f285d44d20e3cd3581bad0e11161235213f67f1fb854a726d9e5820b8daec350e95587667213421a50c69378103a0e', '2020-09-07 22:35:37', '2020-09-07 22:35:37'),
(144, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Dst.N445dreP4kNI\\/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:35:37\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"7ICzAUyQFRNtu0px45oaWvmD483J1ATRhb8uZmSlfVl2Iqt1aiK476hY1FYN\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:35:37\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'fa137a0a0e6896cd55cec93f69fd92a50c85f1ff176976cc4c808904188a0158f88cf4579d041a10f37ec86cf7d9638cb591420525393f81e35e73d0f077b7c5', '2020-09-07 22:38:07', '2020-09-07 22:38:07'),
(145, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Dst.N445dreP4kNI\\/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:38:11\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"7ICzAUyQFRNtu0px45oaWvmD483J1ATRhb8uZmSlfVl2Iqt1aiK476hY1FYN\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:38:11\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '34dc29fe135d2f746f360b02f4e9fc0f9471037367a21ff5a6daf7cc4af98a528c5113ba4cd2b43001bba60bc9ca6e86e5e06f12e51c057dedcd1622713c0c9e', '2020-09-07 22:38:11', '2020-09-07 22:38:11'),
(146, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:38:11\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"7ICzAUyQFRNtu0px45oaWvmD483J1ATRhb8uZmSlfVl2Iqt1aiK476hY1FYN\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:38:11\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'd551a882db615efb2335ee7b5db1846ad768dd298c1c9e8b70dd376d3335efa5611f851090d335a7f236fd49039d59715aa03cfd1917e8c445416818a8c68576', '2020-09-07 22:38:11', '2020-09-07 22:38:11'),
(147, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:38:11\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 04:38:11\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'dea8845d8393e95755c6d724761e5e3d8a6724864a55a2fa087bb8e2d3cd7a39fc90f7a4d799a0fca91427e8f4d5c3408623b7098caf0cb4d112162da7da9a0f', '2020-09-07 22:38:13', '2020-09-07 22:38:13'),
(148, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L\\/wSMS3bNURqZJNefOnW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:38:19\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"XN84dBdUmjHmLRutVrPEYWgwDq4rTY7VbbtnClijzWa6d0VKSrKReDwbs7oz\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 04:38:19\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'd5338b336ffc3347ba3edf994e65cb7adb634ea9cad21f0ffb769755956c6c26139207383c8bc15bd08ab06c4cff7914a681c64bca608393fde20a08c573a75f', '2020-09-07 22:38:19', '2020-09-07 22:38:19'),
(149, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:38:19\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"XN84dBdUmjHmLRutVrPEYWgwDq4rTY7VbbtnClijzWa6d0VKSrKReDwbs7oz\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 04:38:19\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '47de98a8b3914caeb04611933882ca3da75d2f017b603ad9cf76cedb36f719557ec5403df59e06f4fd6eca7a3e10ac754482dca755510a8f1f937bdcf017699f', '2020-09-07 22:38:19', '2020-09-07 22:38:19'),
(150, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:09:26\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:09:26\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '40fe110aae5f6a7e5ee9d9185c5d6cc30d9bc13a38ad3e67c4bf5f73ff66982607fea58143403865624e99a7735346a7207bc33cf33ac3ab8a3e89716b832083', '2020-09-08 02:09:26', '2020-09-08 02:09:26'),
(151, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$\\/43fgsfG48meVrs6WxOqIejm5KytE0qcCtG8.38j.4RprD1ttR5PC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:09:26\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:09:27\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '74a32e56aa2d6abcde17be6ff2d7f261c91be874b72719195651b51fe513abb4e0de634ac28adee82ce94b84c281089532e42bc36fe4deabdaef4b7bc0789407', '2020-09-08 02:09:27', '2020-09-08 02:09:27'),
(152, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'created', '{\"first_name\":\"john\",\"last_name\":\"due\",\"email\":\"john@gmail.com\",\"username\":\"john\",\"password\":\"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6\",\"active\":true,\"confirmation_code\":\"3e09600c3bec3dde2016d82ce64d0499\",\"confirmed\":true,\"updated_at\":\"2020-09-08 08:12:52\",\"created_at\":\"2020-09-08 08:12:52\",\"id\":8}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.erprnd.com/user', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '34692baf835b179fa733da45c2a2529080655391ae1ea943d550b289a74b0e22937d6fcf22b39830c23436a4e835feda6426d063a78c6ba2c8872212c81ac1d8', '2020-09-08 02:12:52', '2020-09-08 02:12:52'),
(153, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 04:38:19\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"8eEkyPpzhiUz2uOTpxiZCJvi4Y8FEuc4sFCfLvYDLTYFYVvKOQvTd2DZJ3Uz\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 04:38:19\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'df512d3ff244e658f474cb60cf4225f434ecd5ef5beebab5f467f891fafee466662fdc1946c13d4f914ae53620aaa6615c40368a0ca35cbfd503bce9e142cba3', '2020-09-08 02:14:09', '2020-09-08 02:14:09'),
(154, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$\\/43fgsfG48meVrs6WxOqIejm5KytE0qcCtG8.38j.4RprD1ttR5PC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:10\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:14:11\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '4972699a922b2b47ad73e3f14142ec847f8fc0468794f1abc1f7a3d52a9d1f8d35669730a5da0ce774d7e89f5fd96da1d28ced6c0f27882d66d83a4d702345cf', '2020-09-08 02:14:11', '2020-09-08 02:14:11'),
(155, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$rReBoyduZYkwcdrQrfeR1OdONlMYQoQ90XgCo6oRFvgHEkSSxLnty\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:10\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:14:11\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'ec7e882c7dc78e9e6b5744960144d74b867a55b098c2bd5dc99bfc96491f03c87ee6ce243bb8f8bd3ba83ce119c148fc7eac1c08e339ba8c5a6f30a6f51be0cd', '2020-09-08 02:14:11', '2020-09-08 02:14:11'),
(156, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$rReBoyduZYkwcdrQrfeR1OdONlMYQoQ90XgCo6oRFvgHEkSSxLnty\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:16\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:14:16\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'f0827c94687b4380a49bd364be06fc90f748cc110340758df22a7157e51de6774c66157c57f1256085c5cc14267cbbae79d7210deb5890855c478bdbf3aae8d6', '2020-09-08 02:14:16', '2020-09-08 02:14:16'),
(157, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:16\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:14:16\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '7f89770bd664b9724ed9e7ab55abf9eb7814179fc24882dbb9a87beed1414f16d1ec0cf8334a41b297f375c52943a2e66e25f49247ce41b0ee87a07be5050104', '2020-09-08 02:14:16', '2020-09-08 02:14:16'),
(158, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:16\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"BGdW1CrteXQwhyifuBdHzdfPkryDG0vCyWAWrMBwkhMiiuRvJD2OWCO2SX6Y\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:14:16\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '699861ac81fe613b4c0c24fdba6492faa5508a57056fb11ce1971082b54c9ed2d18199a477840479b4be1ceee1c0a460920456373f33306eac4a0c5f5fb5dbc9', '2020-09-08 02:14:20', '2020-09-08 02:14:20'),
(159, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:34\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"8eEkyPpzhiUz2uOTpxiZCJvi4Y8FEuc4sFCfLvYDLTYFYVvKOQvTd2DZJ3Uz\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 08:14:34\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '49e8c5f422e1e31eb341d2c163127057418b9499cb71f9bb7c86c3f80f6ae8977b522d22f6b2c12e4cb844a3994e6bacf0e808b117f43837ca60bddccbb0f9ea', '2020-09-08 02:14:34', '2020-09-08 02:14:34'),
(160, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU\\/MmKKHR95SsTYCyS\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:34\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"8eEkyPpzhiUz2uOTpxiZCJvi4Y8FEuc4sFCfLvYDLTYFYVvKOQvTd2DZJ3Uz\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 08:14:35\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '44efaee2aeb8d3d64e9fd25e817f6678a35d5d01bcdc9fb5378f45e00e741dae663e07e68403a0131153220280463c7d8818214d3fb06b2c7dc6fc3961cd10ef', '2020-09-08 02:14:35', '2020-09-08 02:14:35'),
(161, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'created', '{\"code\":\"2222\",\"name\":\"Rabby soft\",\"short_name\":\"R soft\",\"address\":\"dhaka\",\"email_suffix\":\"great\",\"logo\":\"pngwing.com (1).png\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":1,\"created_at\":\"2020-09-08 08:17:54\",\"updated_at\":\"2020-09-08 08:17:54\",\"id\":2}', '[\"code\",\"name\",\"short_name\",\"address\",\"email_suffix\",\"logo\",\"time_zone\",\"active\",\"created_by\",\"created_at\",\"updated_at\",\"id\"]', '[]', '[]', 'http://odometer.erprnd.com/master/company', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'b75c02e6954228aa69f9947ec5440a02c3b2555de598db827f3efa712737ab69fe26ca68857bdf9d171f8f99a74b9a16c02e0f8e17809e8dd046e57b7dfdcd2a', '2020-09-08 02:17:54', '2020-09-08 02:17:54'),
(162, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'updated', '{\"id\":8,\"company_id\":1,\"first_name\":\"john1\",\"last_name\":\"due\",\"email\":\"john@gmail.com\",\"username\":\"john\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3e09600c3bec3dde2016d82ce64d0499\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":null,\"created_at\":\"2020-09-08 08:12:52\",\"updated_at\":\"2020-09-08 08:18:46\",\"deleted_at\":null}', '[\"first_name\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/user/8', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '481236c511b309114c1e89156e2307aaabc2d26e6d63aae9d9eb244cc14fcadde894e87c1ee4ed0da0dbcaa2d0218d677655a470867acad1cc15f78e9691ca54', '2020-09-08 02:18:46', '2020-09-08 02:18:46'),
(163, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{\"id\":2,\"code\":\"2222\",\"name\":\"brix\",\"short_name\":\"R soft\",\"address\":\"dhaka\",\"logo\":\"pngwing.com (1).png\",\"email_suffix\":\"great\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-09-08 08:17:54\",\"updated_at\":\"2020-09-08 08:18:52\",\"deleted_at\":null}', '[\"name\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '29f808f5ef3b11ad2e6a8e79f6204b9afb8af6af32f837af4d2f1969d00b5c5dabab311e2165b258cdbcabf1ca9661e636eafb6e24724092e8ee55e48858ac78', '2020-09-08 02:18:52', '2020-09-08 02:18:52');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(164, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{\"id\":2,\"code\":\"2222\",\"name\":\"brix\",\"short_name\":\"R soft\",\"address\":\"dhaka\",\"logo\":\"pngwing.com (1).png\",\"email_suffix\":\"great\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":0,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-09-08 08:17:54\",\"updated_at\":\"2020-09-08 08:19:03\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '11ca21a3a29eb0cdcd85f3af583de617e025bc753fd8adc777aa9b1f5ce2080fb0e7ee91281e81164b87d6b6a798c8824c210d8be7756762bbcfd7f291ca3552', '2020-09-08 02:19:03', '2020-09-08 02:19:03'),
(165, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{\"id\":2,\"code\":\"2222\",\"name\":\"brix\",\"short_name\":\"R soft\",\"address\":\"dhaka\",\"logo\":\"pngwing.com (1).png\",\"email_suffix\":\"great\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":1,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-09-08 08:17:54\",\"updated_at\":\"2020-09-08 08:19:26\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '63426853a60323dd2aba38f4dc60561939543b2bc71de687b36a109c5056bc1b9983910eef2ffd9fd1c94655993d724a7d7fa617ba381bd6b1c761932c46467d', '2020-09-08 02:19:26', '2020-09-08 02:19:26'),
(168, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{\"id\":2,\"code\":\"2222\",\"name\":\"brix\",\"short_name\":\"R soft\",\"address\":\"dhaka\",\"logo\":\"hiclipart.com.png\",\"email_suffix\":\"great\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-09-08 08:17:54\",\"updated_at\":\"2020-09-08 08:20:18\",\"deleted_at\":null}', '[\"logo\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'd422367006a162a31b8a8a2f5952144bb179ca89c18bf16390e83abc9dac3020d7c1d9b80c2a0ea8d485c9fc8e48e6ecc1a38f63e399481d208ea21c07f3402a', '2020-09-08 02:20:18', '2020-09-08 02:20:18'),
(169, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'updated', '{\"id\":8,\"company_id\":1,\"first_name\":\"john1\",\"last_name\":\"due\",\"email\":\"john@gmail.com\",\"username\":\"john\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6\",\"password_changed_at\":null,\"active\":0,\"confirmation_code\":\"3e09600c3bec3dde2016d82ce64d0499\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":null,\"created_at\":\"2020-09-08 08:12:52\",\"updated_at\":\"2020-09-08 08:21:09\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/user/8/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '78d38c315485803aef5b8a21ccae1beecfbafbde110cd0635a8f431d8f6bf16b55890ac6b9a7ca1c4a6aac77f1ff10f2247fb6160506488f574299c09ea5f6d2', '2020-09-08 02:21:09', '2020-09-08 02:21:09'),
(170, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'updated', '{\"id\":8,\"company_id\":1,\"first_name\":\"john1\",\"last_name\":\"due\",\"email\":\"john@gmail.com\",\"username\":\"john\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3e09600c3bec3dde2016d82ce64d0499\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":null,\"created_at\":\"2020-09-08 08:12:52\",\"updated_at\":\"2020-09-08 08:21:26\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/user/8/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '3a9c03cb5a64969c10b10930748838afb1e7df268a335b8d48998b82046110efd8bd7235cf9d8ec773bd44425527422c16b6ae807a72dfcd7f9fb182658be3b5', '2020-09-08 02:21:26', '2020-09-08 02:21:26'),
(171, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'deleted', '{\"id\":8,\"company_id\":1,\"first_name\":\"john1\",\"last_name\":\"due\",\"email\":\"john@gmail.com\",\"username\":\"john\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3e09600c3bec3dde2016d82ce64d0499\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":null,\"created_at\":\"2020-09-08 08:12:52\",\"updated_at\":\"2020-09-08 08:21:51\",\"deleted_at\":\"2020-09-08 08:21:51\"}', '[]', '[]', '[]', 'http://odometer.erprnd.com/user/8', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'a290d75a5e20f01af614cf8c500042264aa27316eed9e738203acd2d75dbff105221ef904d99566c6ef0f594eaa4d7209412b8223074a14bf41e61210c439c8c', '2020-09-08 02:21:51', '2020-09-08 02:21:51'),
(172, 'App\\Models\\User', 1, 'App\\Models\\Role', 4, 4, 'created', '{\"name\":\"facerole\",\"guard_name\":\"web\",\"updated_at\":\"2020-09-08 08:25:06\",\"created_at\":\"2020-09-08 08:25:06\",\"id\":4}', '[\"name\",\"guard_name\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.erprnd.com/role', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '9233c120b974f489c6daa5c819385aff8c10032601301d3855ea6208002a146356c5fefe77cd387d0bbe73ee63d291e52e889e2baa397fdd85b29303cde1ca60', '2020-09-08 02:25:06', '2020-09-08 02:25:06'),
(173, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU\\/MmKKHR95SsTYCyS\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:14:34\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 08:14:35\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'd7fa44dd383a1b447c519882e8d9fbf788a067bd3027d504cb2a67f9f47b1177e2a18840ad988b12ab644279eaa08715b7e0f6b2b221caeeadb1ba09b0af8329', '2020-09-08 02:28:42', '2020-09-08 02:28:42'),
(174, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:28:47\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"BGdW1CrteXQwhyifuBdHzdfPkryDG0vCyWAWrMBwkhMiiuRvJD2OWCO2SX6Y\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:28:47\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '3ba11a2db1081a9f6742b33fe4e535ea219d7ccbfaad3461506eca4098e32597e4ed48294da4d80cbd79682a3950a7c9f324f1d9db1af3dc430ed074c170b852', '2020-09-08 02:28:47', '2020-09-08 02:28:47'),
(175, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:28:47\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"BGdW1CrteXQwhyifuBdHzdfPkryDG0vCyWAWrMBwkhMiiuRvJD2OWCO2SX6Y\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:28:47\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '821f5b97818f685131a3669cfd268dbdf83a4f26ee16df3daf56b94a217523d0a58980c4a3879acb94ff2367f1549fd14cf8976f7f805ab84f848f1392cdd9b9', '2020-09-08 02:28:47', '2020-09-08 02:28:47'),
(176, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU\\/MmKKHR95SsTYCyS\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 10:00:53\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 10:00:54\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '7b19b800812e78dd971c63d70acc77adfedd30476b53c7918650312d58a5456aac45acbd0a2869dc90961127c8ef59fe433928eb4e358cd8dd7ea4ed31f3a8cd', '2020-09-08 04:00:54', '2020-09-08 04:00:54'),
(177, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$3kCpwukGAxvU5UBSp.h6MO8asDFLi\\/zTwY.dUqi\\/fFsHB\\/ZTy1ay6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 10:00:53\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 10:00:55\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'bba63675c083e604e4d07471d35538f6779a3803bd4ea7c40cb802d5d788d6b8e35feb0395225f16a21d8432597fda73074f0213696861c5558ee151ba46f889', '2020-09-08 04:00:55', '2020-09-08 04:00:55'),
(178, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 08:28:47\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-08 08:28:47\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '27bfcde8dc38594ccef03f21f1908e98cf1e8578b4e1da3aa193b4d7c247d7629d8e635bd5ac5e1f1ece0a849741d652ac2e5c2017403b04dd6253d811160c7f', '2020-09-08 04:24:14', '2020-09-08 04:24:14'),
(179, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$3kCpwukGAxvU5UBSp.h6MO8asDFLi\\/zTwY.dUqi\\/fFsHB\\/ZTy1ay6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 10:24:27\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 10:24:27\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'a8b5fa9b053c27c107ff5fca78714de1a01568ffbf29e43ebd694882ea04dea67b521ee7f4b3fb742e7941b544a5b9fa0af2e37303b5fd09ea1320dae9ccac84', '2020-09-08 04:24:27', '2020-09-08 04:24:27'),
(180, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$OLCCEDtdSjvfeSFxeh\\/mI.Uzg61kOKWIiICF9KmKGIA\\/Mz26BhD4m\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"Asia\\/Dhaka\",\"is_logged\":0,\"last_login_at\":\"2020-09-08 10:24:27\",\"last_login_ip\":\"182.16.157.232\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-08 10:24:27\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '2fd70ec2f43995985b2f774c2abb05159396c16cbe61a9b4289753bdf95c152392c2e234ff64c23ccbf6996a181d38abdf8a9c84f3dacc03bc37dec5edf866cd', '2020-09-08 04:24:27', '2020-09-08 04:24:27'),
(181, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-11 14:38:12\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-09-11 14:38:12\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '10e8be6c41851bb9b04b516a7b38d0e92059e13d30da943d8112765267ea0e90d97acb863c61d87ae6477f0100cf039c0009904eb99c4fbf5fff5a16d8750464', '2020-09-11 02:38:12', '2020-09-11 02:38:12'),
(182, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$e59TfnIdHX5szbAnl4YP2e9QOvGn3vv7zvqPcoGYsnZEXUOCWGxxK\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-11 14:38:12\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-09-11 14:38:13\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'dfc58d04b9d2982f92ea136309201c5dec26e34e383564ec94f8a446b71214bc1898e7798ccd4c69a8a674d8d97ad66c15d22ec8ed4aafd9268170467fc6b795', '2020-09-11 02:38:13', '2020-09-11 02:38:13'),
(183, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$e59TfnIdHX5szbAnl4YP2e9QOvGn3vv7zvqPcoGYsnZEXUOCWGxxK\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 14:20:40\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-09-19 14:20:40\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '9d56893768dff4c8e1ed74c420df339dd3b1c50b374de3d7e33ce6f8e04274d0f5656ce5c7fe2f0bec5364ba21d29c4c79a042a41697b3f2e2a8e5e335e79b19', '2020-09-19 02:20:41', '2020-09-19 02:20:41'),
(184, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$HYbe34AnTZ8W\\/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 14:20:40\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-09-19 14:20:42\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '1329551886a6360cadd5a01a07c25922f84874c9c2dfe55207e95fbdfd449864729a5c9d7d5cc8a3d6e8146594fe1d906433e7129ae93eeaf85940d09cbcf303', '2020-09-19 02:20:42', '2020-09-19 02:20:42'),
(185, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$HYbe34AnTZ8W\\/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 14:20:40\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"yiRGOdmgsQTwhNjACGuzte2MPThbB3S1Uw40JjUmkWzpLep6uJiVUxi5XQJb\",\"created_at\":\"2020-08-04 04:41:44\",\"updated_at\":\"2020-09-19 14:20:42\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '7153f49c5fa51b6404179f3b3ba1c1dd2ea9cdcffc170ec8d92f83c6e4918aadb3f352527a21479628913a3c6b1e05c307ba93a05c174d4cd416e980dacddd70', '2020-09-19 02:21:17', '2020-09-19 02:21:17'),
(186, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$OLCCEDtdSjvfeSFxeh\\/mI.Uzg61kOKWIiICF9KmKGIA\\/Mz26BhD4m\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 14:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-09-19 14:21:33\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '0dcbe20af6a2a21a9f56206a21ac9fe4033794ad1f521bf7b351f632b55c8e1c6b4c3f957eca0d2b49df83d8fd0a6236b5bc82626d3111a45c76c02e7f9d1a9e', '2020-09-19 02:21:34', '2020-09-19 02:21:34'),
(187, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 14:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-09-19 14:21:34\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '007d569979a58818a77c723d9efa40466e2b04daff0d1fe8b3c8c6666f362331360e61023f1d8fa30c5043b55d255ea183bde66ee0d8174d02b2828ae0f588d8', '2020-09-19 02:21:34', '2020-09-19 02:21:34'),
(188, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 14:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"3Oo7ITWXMOS9QLnP6aIUIe0smx3JvWHgJLF6WsGWHVkITGSJz4GyFWGso2Hn\",\"created_at\":\"2020-08-06 10:41:05\",\"updated_at\":\"2020-09-19 14:21:34\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '9239d63488d12e85eaedd316495a352f9cf070c74db3ae42584973fadf4ec1909c4136f613d171d1b54c0630ef5163805c26c80e1c3ec16ed82958b49c18586d', '2020-09-19 02:39:02', '2020-09-19 02:39:02'),
(189, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 08:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"aKvrCKiIDP8Qlg263qZoAAA9UYAI3LDd7cUKG5vz1RpX7MVHcPKyUZmH5QvH\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-09-19 08:21:34\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '3364eac9fa3701de4c7af3b78d8b962570d4dc655e0f070ad5fc0f09ee5255c481c471663c544cf9e62d2fb2aa6758abc33a3d11c42d586888677db62d7b80fc', '2020-09-19 22:21:52', '2020-09-19 22:21:52'),
(190, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$HYbe34AnTZ8W\\/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-20 04:21:56\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"yiRGOdmgsQTwhNjACGuzte2MPThbB3S1Uw40JjUmkWzpLep6uJiVUxi5XQJb\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-20 04:21:56\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '50f70f9b2e96a7f7ec1ee18db499ad6b7999f98b1e2553d93f1d607d3743a9389d0731753aa08dbf9022c77fd14512609dacf40e28f8076f4dc7a9c9783704ee', '2020-09-19 22:21:56', '2020-09-19 22:21:56'),
(191, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$y0EEOBdl\\/vcnlI5kwPfMjuCdAMmZUfpYPyDI.MB1hj.MZBf.AXVLW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-20 04:21:56\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"yiRGOdmgsQTwhNjACGuzte2MPThbB3S1Uw40JjUmkWzpLep6uJiVUxi5XQJb\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-20 04:21:56\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'b8b8a192fafbe8d191acd3f368bd0dd79869de1992a04a91ee2c5d7ea60cc581a37fc9af9f7ae41e241283e9ff5b0e67aff539a449548077f6b2178c80b4081f', '2020-09-19 22:21:57', '2020-09-19 22:21:57'),
(192, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 6, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-10-11\",\"vehicle_id\":2,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"50\",\"millage\":\"9\",\"order\":6,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-10-11 04:20:14\",\"created_at\":\"2020-10-11 04:20:14\",\"id\":6}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '33187557110dae03100f5bc0feb1dca9d8c933a3e4319a604ef60ffb85a76d069141b39d3b409900367c5c4905b5d4b36195da02d38d93e9d7c1ad23edecae71', '2020-10-10 22:20:15', '2020-10-10 22:20:15'),
(193, 'App\\Models\\User', 1, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 08:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"aKvrCKiIDP8Qlg263qZoAAA9UYAI3LDd7cUKG5vz1RpX7MVHcPKyUZmH5QvH\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-11 04:22:00\",\"deleted_at\":null}', '[\"last_name\",\"email\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/user/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'd382131f22cb55ab83dfcc6ccb2d7b3ae3dfec92b7b258ed343a06293cad1006d5b01d85f96c7376392a0aa3bb46e1533561c335b3aa02c79fc025983e18c489', '2020-10-10 22:22:00', '2020-10-10 22:22:00'),
(194, 'App\\Models\\User', 1, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 08:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"aKvrCKiIDP8Qlg263qZoAAA9UYAI3LDd7cUKG5vz1RpX7MVHcPKyUZmH5QvH\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-11 04:24:38\",\"deleted_at\":null}', '[\"last_name\",\"email\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/user/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '445c97bb6927e3f8082b154e1307bb0781af5f68b4b3ef95a747332c9666733dead1f08c36c6156c1a02aa6ef5d3a2204ae0a020fe17d419d8bfe02b1ae2d1e3', '2020-10-10 22:24:38', '2020-10-10 22:24:38'),
(195, 'App\\Models\\User', 1, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-19 08:21:33\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"aKvrCKiIDP8Qlg263qZoAAA9UYAI3LDd7cUKG5vz1RpX7MVHcPKyUZmH5QvH\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-11 04:24:53\",\"deleted_at\":null}', '[\"last_name\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/user/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '0fd4db8d52ee83d53b8153c4978f0d415f311c6602a90be0497e82e3384e89c415d6ab73c5d960dc1e23fd44391b9a377911afe9236654b1a0abf36d86522a0a', '2020-10-10 22:24:53', '2020-10-10 22:24:53'),
(196, 'App\\Models\\User', 1, 'App\\Models\\User', 10, 4, 'created', '{\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"password\":\"$2y$10$z51zs0gbRwpWQQMR0Dh5m.jD9OYUEZKd3PMO.A2Ckl9tHwE9krqUy\",\"active\":true,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":false,\"updated_at\":\"2020-10-11 04:25:36\",\"created_at\":\"2020-10-11 04:25:36\",\"id\":10}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/master/employee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '83b3f806b78213752722edbbc3bb1b3018a575130a66b7fc762f9b7c4ed984fd2362195bf49df37e17c4a945266b6f5d29d780f88217a04704b219ff016416aa', '2020-10-10 22:25:36', '2020-10-10 22:25:36'),
(197, 'App\\Models\\User', 1, 'App\\Models\\Employee', 3, 4, 'created', '{\"company_id\":\"1\",\"user_id\":10,\"employee_id\":null,\"name\":\"AIC MOT\",\"designation_id\":\"2\",\"department\":\"IS\",\"grade\":\"1\",\"email\":\"aicmot@tdcl.transcombd.com\",\"order\":3,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-10-11 04:25:37\",\"created_at\":\"2020-10-11 04:25:37\",\"id\":3}', '[\"company_id\",\"user_id\",\"employee_id\",\"name\",\"designation_id\",\"department\",\"grade\",\"email\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/master/employee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'baf2ed3859c0f97bb160d1b92ddfad03d4b2690d938007ff6a94db61c352c9abb0e274f487df3b791b7a016a80eb3f46984cb21a1159526d9a66c0f2290c400a', '2020-10-10 22:25:37', '2020-10-10 22:25:37'),
(198, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$y0EEOBdl\\/vcnlI5kwPfMjuCdAMmZUfpYPyDI.MB1hj.MZBf.AXVLW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-09-20 04:21:56\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ezyQAPUMLwaWAiQXs3xEudZJWd9tYsVohEWcZIe8P9roy3LTvYvM3BSeDdq9\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-09-20 04:21:56\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'cebc71d50ac6aad80b4ed1e4c877b16aa14d6bfab7fe40217b082b7e9dc28519cf72bf3bf4c8ef102974db046f9c3890229145e2d682235af362e2bcd2571781', '2020-10-10 22:27:06', '2020-10-10 22:27:06'),
(199, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$z51zs0gbRwpWQQMR0Dh5m.jD9OYUEZKd3PMO.A2Ckl9tHwE9krqUy\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":0,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"ObIVn3LPBPq49Nbo7aoKq9fat7cSGYMYal8nsLasrXruV1YYt7C457wxoysy\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-11 04:25:36\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '88519e9c6d53e54bac3a9b4cc72b6718b81baa69459e6565200b194c239f3c67798fe614f91dea1413881ef8f17d0a795a5d20207532991a73c1d4cb457f282c', '2020-10-10 22:27:17', '2020-10-10 22:27:17'),
(200, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$z51zs0gbRwpWQQMR0Dh5m.jD9OYUEZKd3PMO.A2Ckl9tHwE9krqUy\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":0,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"DFaVHT9hYotP3AvGLF8PqjVPL3hY5o4GSA1v28rvanroKO8Yq7oaVRTY5R6F\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-11 04:25:36\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '2c813e6ba7849ae26949a5e86272ecd11a68e2d06e2687ab1616b6f090e9344b965e12f04b0517d14adcd7f771dbc3f02678b3e0d016d23b062f1217e37bd722', '2020-10-10 22:27:17', '2020-10-10 22:27:17'),
(201, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$y0EEOBdl\\/vcnlI5kwPfMjuCdAMmZUfpYPyDI.MB1hj.MZBf.AXVLW\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:28:07\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ezyQAPUMLwaWAiQXs3xEudZJWd9tYsVohEWcZIe8P9roy3LTvYvM3BSeDdq9\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 04:28:07\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'd3e2196f20feb83ebc4636d62d3d3eb097c4569af8d900e07ee01657b888a6e3338f6755616cc1a734e92a7f02e8d3a63daf67bd5e41cca38759b23a7744befa', '2020-10-10 22:28:07', '2020-10-10 22:28:07'),
(202, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$o3tupCcnnS6wcvTASFh1WegYNzR1l8W8HLZWvLlFuiyl\\/Khs7crWi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:28:07\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ezyQAPUMLwaWAiQXs3xEudZJWd9tYsVohEWcZIe8P9roy3LTvYvM3BSeDdq9\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 04:28:07\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'f47cb378edea6bb2e53584f25940c427f73511d0902da7a8e686c73d58923e76da2dc2155db839135c0fe708432c9f26d98b4ab9e8695450d18c3d0f81b90ae6', '2020-10-10 22:28:07', '2020-10-10 22:28:07'),
(203, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$o3tupCcnnS6wcvTASFh1WegYNzR1l8W8HLZWvLlFuiyl\\/Khs7crWi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:28:07\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"xKNsh4NJLf4dZQ8AgSZJHizjOMO3hRQ8S9Gj6ApiEElSdjH0Qnd7qMRtkt78\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 04:28:07\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '15370acb76a1663d969db44104bc779b68be99ca34ed9ea6fa081ff10f08a7cf6b30df9b27832b9b572d968152d31041f028dc3f8770a904b767c290ef22ae60', '2020-10-10 22:29:13', '2020-10-10 22:29:13'),
(204, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$z51zs0gbRwpWQQMR0Dh5m.jD9OYUEZKd3PMO.A2Ckl9tHwE9krqUy\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:29:17\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"DFaVHT9hYotP3AvGLF8PqjVPL3hY5o4GSA1v28rvanroKO8Yq7oaVRTY5R6F\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-11 04:29:17\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'a58add230a5d8d1ec6260143de2962618b670b8318f3da5ae2dcafd33455a933cd082ec05ec395f2f8b4e3570d7fe6a5e791e188ba0218b5618c8a70ce7148fe', '2020-10-10 22:29:18', '2020-10-10 22:29:18'),
(205, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$avTVhcsaMw9bnPSolwuea.8aoq72nIclejd\\/1Ik5IgKACt7.61h.O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:29:17\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"DFaVHT9hYotP3AvGLF8PqjVPL3hY5o4GSA1v28rvanroKO8Yq7oaVRTY5R6F\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-11 04:29:18\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '9b55d11c41d617fc2c9107e9369decdd577b941decfeb4259491fc00299a749090b52e62d0df319c668e22ad76cc4ebe639ec682e7e95b0df0344e80fc674a14', '2020-10-10 22:29:18', '2020-10-10 22:29:18'),
(206, 'App\\Models\\User', 10, 'App\\Models\\FuelRate', 25, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-10-11 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"60\",\"order\":5,\"archived\":0,\"active\":1,\"created_by\":10,\"updated_at\":\"2020-10-11 04:48:58\",\"created_at\":\"2020-10-11 04:48:58\",\"id\":25}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '553a469b753099a5f4f8067154958ee4d5703fae64906e0f4e082d7a74c86da58bd6af1616096bc77a2673e46715e351d6e43c61d7be616a501b0cf953bd805f', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(207, 'App\\Models\\User', 10, 'App\\Models\\FuelRate', 26, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-10-11 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"88\",\"order\":6,\"archived\":0,\"active\":1,\"created_by\":10,\"updated_at\":\"2020-10-11 04:48:58\",\"created_at\":\"2020-10-11 04:48:58\",\"id\":26}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'd40a98d6386018ae1c717300a043abb5be59c1e3b945aa412b7f043038bb4977448d6fc9327e62edf3c24c275849418785de357598352d755ebd1562377808a8', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(208, 'App\\Models\\User', 10, 'App\\Models\\FuelRate', 27, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-10-11 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"89\",\"order\":7,\"archived\":0,\"active\":1,\"created_by\":10,\"updated_at\":\"2020-10-11 04:48:58\",\"created_at\":\"2020-10-11 04:48:58\",\"id\":27}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '121eaea318d2dafc100df957039d1adb3ed0c02fbc0cff896696ca2ea0ae7db04a897fedb6a9fb52b8b72689ccd932a59489c9d9028baa4f2e43f6f3c80f2aca', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(209, 'App\\Models\\User', 10, 'App\\Models\\FuelRate', 28, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-10-11 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"75\",\"order\":8,\"archived\":0,\"active\":1,\"created_by\":10,\"updated_at\":\"2020-10-11 04:48:58\",\"created_at\":\"2020-10-11 04:48:58\",\"id\":28}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '2c39042aba98f83d6d0e3e559ca86e1c96c3b1dbf2fe1845312864d515dd1206ff06c95a21d7dd336ca5a74af097f406313a0df9f8bf48e25cfde6915e9d0f86', '2020-10-10 22:48:58', '2020-10-10 22:48:58'),
(210, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$avTVhcsaMw9bnPSolwuea.8aoq72nIclejd\\/1Ik5IgKACt7.61h.O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:29:17\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"YwzCdOjs4hTEAev1LvXWCp1ZxS4Axgm67KLkW5sf9psWbHnf3d2awOCScR09\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-11 04:29:18\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '26f4d4e4ca28fcf1630c60c22a1094da16be4c0b8b6bb438fef31d5cc8eda35817d142b1781df98b94f7ad4b86e755f507ccce1f8ebee232259c0ccb49bba27d', '2020-10-10 22:54:20', '2020-10-10 22:54:20');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(211, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$o3tupCcnnS6wcvTASFh1WegYNzR1l8W8HLZWvLlFuiyl\\/Khs7crWi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:54:25\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"xKNsh4NJLf4dZQ8AgSZJHizjOMO3hRQ8S9Gj6ApiEElSdjH0Qnd7qMRtkt78\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 04:54:25\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'ce018c051f6b3ff2565083a203202a225b0dff1fbc72bc9f609ad3df07d640fbae14224594e15071f0ac95cfc61d5a6a15586f4adbe3be9d9cd21d82e001b4b6', '2020-10-10 22:54:25', '2020-10-10 22:54:25'),
(212, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$0bENg7WFGPZ.MtgDx6HZs.H6bO8MXBCOp.PTzg\\/RDQ7zZZQMJfPYe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:54:25\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"xKNsh4NJLf4dZQ8AgSZJHizjOMO3hRQ8S9Gj6ApiEElSdjH0Qnd7qMRtkt78\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 04:54:26\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'ae8994bec3829072d072026ab2d0c0cb458049b54a47a15d03b7e43b090691b9a389b7bc7ee8b9231d37f649c7c7cbb1c2284f5069b321de357cbe8934e51328', '2020-10-10 22:54:26', '2020-10-10 22:54:26'),
(213, 'App\\Models\\User', 10, 'App\\Models\\LogbookEntry', 2, 4, 'created', '{\"company_id\":1,\"sl\":2,\"trans_date\":\"2020-10-11\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"4\",\"driver_name\":\"Parvez\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-10-10T00:00:00.000000Z\",\"rkm_data\":\"234\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13500\",\"logbook_running\":\"520\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"9\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"540\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":2,\"year\":2020,\"month\":10,\"month_name\":\"10\",\"created_by\":10,\"updated_at\":\"2020-10-11 04:57:21\",\"created_at\":\"2020-10-11 04:57:21\",\"id\":2}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/logbook_entry', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'a0ee3c877a2904fc82592fa1e3983ff2d823d29726aaf859140273627d5596abdc909368e5643c348493db929551de5a793ff43f472149edd77f4f81d0cc1d4d', '2020-10-10 22:57:21', '2020-10-10 22:57:21'),
(214, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$0bENg7WFGPZ.MtgDx6HZs.H6bO8MXBCOp.PTzg\\/RDQ7zZZQMJfPYe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 04:54:25\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Txq3BLolU37jQ7IP5GtVk7eggvMhH9pFROes8Pkrr5MaNrmGxYrPkEUap8sQ\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 04:54:26\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '48eb57e1aa0678b4b3639079e52800453f7618620dfe1ded8dce8b9fc5269da72322443291fcdd29ce94d9bbb545145dd240f02853586b6830d187a08b602294', '2020-10-11 02:07:04', '2020-10-11 02:07:04'),
(215, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$0bENg7WFGPZ.MtgDx6HZs.H6bO8MXBCOp.PTzg\\/RDQ7zZZQMJfPYe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 08:07:58\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Txq3BLolU37jQ7IP5GtVk7eggvMhH9pFROes8Pkrr5MaNrmGxYrPkEUap8sQ\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 08:07:58\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '67f645313f5331d5a70578f964e6adb91ab6ec3718f6e5faa45f975d16eeeed67c9ed2de7fa14b0eee65ef7c8933c9a5ec366d5e2d4a9562a9437e00278b2d66', '2020-10-11 02:07:58', '2020-10-11 02:07:58'),
(216, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$STOkgyCLFQT7wuAgWhGBq.eLuzVa1fSNm5Sr3O5AlWWa16ghSFK3C\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-11 08:07:58\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Txq3BLolU37jQ7IP5GtVk7eggvMhH9pFROes8Pkrr5MaNrmGxYrPkEUap8sQ\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-11 08:07:58\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '525cd374ec1ee8b0c49d3ea02f47370473272235f9d1d347cec9e1de6915bb19388c5a0d03de0245bc7692cd27da451ddb90dcb2caba7221a32787dddf8e9ada', '2020-10-11 02:07:58', '2020-10-11 02:07:58'),
(217, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:54:32\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"aKvrCKiIDP8Qlg263qZoAAA9UYAI3LDd7cUKG5vz1RpX7MVHcPKyUZmH5QvH\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-17 06:54:32\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '9e96b9c9615caa025449cb60f6cb24d9caa9c517860835397e919cb968eee339814e515f20ec733212de987e87c472846e19a452c76f594afb1e9a2f1f048e6e', '2020-10-17 00:54:32', '2020-10-17 00:54:32'),
(218, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$AQ\\/VbfTD4TOs53\\/SFsM55uRsVqTjPXYx71AGLBJoc1UzhgWidcYNm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:54:32\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"aKvrCKiIDP8Qlg263qZoAAA9UYAI3LDd7cUKG5vz1RpX7MVHcPKyUZmH5QvH\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-17 06:54:33\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'd0bcf306878456c8033ae2d826fc1140d155e8849ef894a07bb32725fc5eb3fc65fd3260016b6b1bd07f5ac98c491599e03209652187ef85c0214480fa68ec7b', '2020-10-17 00:54:33', '2020-10-17 00:54:33'),
(219, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$AQ\\/VbfTD4TOs53\\/SFsM55uRsVqTjPXYx71AGLBJoc1UzhgWidcYNm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:54:32\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"AKxZJA3WZ6E9yLC1e6BDjggV6wdVxgzqB422gS5SsoFj47YR4w6XTzrQZrU7\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-17 06:54:33\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '376255a65c381a730b4c08f09d25564455875a041b487618f84142db92076f526e09cf49d2383cb10a0823515ed3286aff816d8cab5d20b7d44a7d367a167941', '2020-10-17 00:54:57', '2020-10-17 00:54:57'),
(220, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$STOkgyCLFQT7wuAgWhGBq.eLuzVa1fSNm5Sr3O5AlWWa16ghSFK3C\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:55:01\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Txq3BLolU37jQ7IP5GtVk7eggvMhH9pFROes8Pkrr5MaNrmGxYrPkEUap8sQ\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-17 06:55:01\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '08c176f5e0afca093aee5793e031180821e6dfbd349dbe7aba438b6241a3071f04af6595d96266c52874d892b5cf83537f1f61e73d129196656643c3d617e07e', '2020-10-17 00:55:01', '2020-10-17 00:55:01'),
(221, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$FPBFdOUZrdh7M\\/e1iUB8COJaMSl0Pwggf5lGiCi1GqaQX.10PJO8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:55:01\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"Txq3BLolU37jQ7IP5GtVk7eggvMhH9pFROes8Pkrr5MaNrmGxYrPkEUap8sQ\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-17 06:55:02\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '7904cbb7b3db843b6984bd51ceae5f47b41192efbd3c4de87ff86bc4e0f4a12ea2a9c6ca52dc43168545ab082768e7cb96165e3d5064369cfd7f3ed618cbef43', '2020-10-17 00:55:02', '2020-10-17 00:55:02'),
(222, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$FPBFdOUZrdh7M\\/e1iUB8COJaMSl0Pwggf5lGiCi1GqaQX.10PJO8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:55:01\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ul4KWGGNSmu0KfYEX6yVrlyzezXiW01AWmNxUtQYdwFLxZOG3cwZ6byNcpod\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-17 06:55:02\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '98644edfd580f02970939a2e14e42ec5b2e5d3eb18efc63916af41fe50bfe09a17bdcb1859cb731ae21f0cc5bffa567fef8b9aac587d581edca5d2ce150bb3bc', '2020-10-17 00:55:43', '2020-10-17 00:55:43'),
(223, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$AQ\\/VbfTD4TOs53\\/SFsM55uRsVqTjPXYx71AGLBJoc1UzhgWidcYNm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:55:46\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"AKxZJA3WZ6E9yLC1e6BDjggV6wdVxgzqB422gS5SsoFj47YR4w6XTzrQZrU7\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-17 06:55:46\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '11c6b81d9e2f7f44eec5eb1796f31bfe39eb8415ac3d5e337b5f36dc7fda537040a7fc3ea09afef584b24b9d3751e5167c666ebc81a20b50cf20b610d86ed011', '2020-10-17 00:55:46', '2020-10-17 00:55:46'),
(224, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$aECfJlRWGo7YAWJbr1yP5OHkiqdLiI\\/tMv7qJ6cC8pTf2GNLJTsqC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:55:46\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"AKxZJA3WZ6E9yLC1e6BDjggV6wdVxgzqB422gS5SsoFj47YR4w6XTzrQZrU7\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-17 06:55:46\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'f44250a1b8d8ecb4de55fb3b23de3c2eb2213860a4eb424a3ecf7d39bf413f3d9f18573b01bc7e8f8d7ccf3d001c5ff3bb081721bc461600ab50a36da444d5ca', '2020-10-17 00:55:47', '2020-10-17 00:55:47'),
(225, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$aECfJlRWGo7YAWJbr1yP5OHkiqdLiI\\/tMv7qJ6cC8pTf2GNLJTsqC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:55:46\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ahEc5cJMBLTv5fUDZOf1LQ2qejXJJlBTKAj07KNuG16QpmA5z7EMkN1EqPP9\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-10-17 06:55:46\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '7b1f332e7e2a9489f725eef28919496ef82eb5095c91a924fd80a1127bae09ade8d3844dbbe90806b9b20256d903f4ecc71a6d1a93e1aaee29505c757544cc57', '2020-10-17 00:59:52', '2020-10-17 00:59:52'),
(226, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$avTVhcsaMw9bnPSolwuea.8aoq72nIclejd\\/1Ik5IgKACt7.61h.O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:59:57\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"YwzCdOjs4hTEAev1LvXWCp1ZxS4Axgm67KLkW5sf9psWbHnf3d2awOCScR09\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-17 06:59:57\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'a19ca72558320c0feb9dd82a71740a707cf860d508a7023f02c79e870595fb8830ddbe3971706cb6f642f21cf4b573dcd8689f98c934149c9f13deff05e1cd7a', '2020-10-17 00:59:57', '2020-10-17 00:59:57'),
(227, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bn6H23w8WTkxGzY7.3VBfuAGm9\\/SLEFHqPjZFO5blTDTZpod85H8y\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:59:57\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"YwzCdOjs4hTEAev1LvXWCp1ZxS4Axgm67KLkW5sf9psWbHnf3d2awOCScR09\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-17 06:59:57\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '9958b54e43bbb685b23accf2f9117085c18b969eb4265c0083d3c52155e51f759ecf694f6c67e8348d20fc25c8db11d8a0f1868c1ec541a55afd3c117eceec66', '2020-10-17 00:59:57', '2020-10-17 00:59:57'),
(228, 'App\\Models\\User', 10, 'App\\Models\\LogbookEntry', 3, 4, 'created', '{\"company_id\":1,\"sl\":3,\"trans_date\":\"2020-10-17\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"4\",\"driver_name\":\"Parvez\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-10-15T00:00:00.000000Z\",\"rkm_data\":\"123456\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13500\",\"logbook_running\":\"520\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"500\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"30000\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":3,\"year\":2020,\"month\":10,\"month_name\":\"10\",\"created_by\":10,\"updated_at\":\"2020-10-17 07:03:24\",\"created_at\":\"2020-10-17 07:03:24\",\"id\":3}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/logbook_entry', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'c288d277cda57c8c47408dae7269260874f1a43cbefa91c92048cab85fc57e9c0821303861eefea95471596c1f629e4c6c4720701a1a293ae4fc71a0b4e40bbf', '2020-10-17 01:03:24', '2020-10-17 01:03:24'),
(229, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bn6H23w8WTkxGzY7.3VBfuAGm9\\/SLEFHqPjZFO5blTDTZpod85H8y\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 06:59:57\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"vkaY3rxI88UlS07DLsx8Tb01aSest8XA7afntiiBpUlfLjlEufepKqBK3tit\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-10-17 06:59:57\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'f7d9a1a8f92adfa89179a06bb099d2c435822c734cbdbf1605e589568a623541d40331b003f27c6a89332128d875d2f3a9ac16a46cd44f611e70816621b212af', '2020-10-17 01:10:04', '2020-10-17 01:10:04'),
(230, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$FPBFdOUZrdh7M\\/e1iUB8COJaMSl0Pwggf5lGiCi1GqaQX.10PJO8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 07:10:09\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ul4KWGGNSmu0KfYEX6yVrlyzezXiW01AWmNxUtQYdwFLxZOG3cwZ6byNcpod\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-17 07:10:09\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'b1dbe290dcb3f21903f06d39b8780a10825b0b3347b289ea22361358958ca25ec4bc2ba27b57cfbf4aa3425b8423a944d770c2440dd8186ec9b249ffda06fceb', '2020-10-17 01:10:09', '2020-10-17 01:10:09'),
(231, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$VOOs1CJIQ78aKtW5MBmX8.Ml.L8OqyLoZmXneoPF1mbDLjZ9QxGFm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-10-17 07:10:09\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ul4KWGGNSmu0KfYEX6yVrlyzezXiW01AWmNxUtQYdwFLxZOG3cwZ6byNcpod\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-10-17 07:10:09\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'e5f472ff0ecd9513ccbb422c2673258e91e7667757eb7a3614c7d32fad08cf4e95d80f9a6b10ef12de3a41d93210787ab57e43114c4f0681ab09b9d1dcc5004b', '2020-10-17 01:10:09', '2020-10-17 01:10:09'),
(232, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 7, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-10-17\",\"vehicle_id\":2,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"50\",\"millage\":\"9\",\"order\":7,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-10-17 07:14:42\",\"created_at\":\"2020-10-17 07:14:42\",\"id\":7}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', 'e1bc643142880af823ca8e4d73dfdd8b2dd8fda02725766163594fe07a4a90b24201ba42069763c28e1794a88c1a7ccc38049e33c3980fe2f153c063e88155df', '2020-10-17 01:14:42', '2020-10-17 01:14:42'),
(233, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 8, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-10-17\",\"vehicle_id\":2,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"50\",\"millage\":\"9\",\"order\":8,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-10-17 07:15:02\",\"created_at\":\"2020-10-17 07:15:02\",\"id\":8}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '82c3ef306d017841050c5b6f9af1f6f027ad49d9a720adcbd56f2071b956431f28c0ecb781f5baff44e61766440197d761d3c80af7fd4ee8b2fb9d6d95579b35', '2020-10-17 01:15:02', '2020-10-17 01:15:02'),
(234, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$aECfJlRWGo7YAWJbr1yP5OHkiqdLiI\\/tMv7qJ6cC8pTf2GNLJTsqC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:22:55\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ahEc5cJMBLTv5fUDZOf1LQ2qejXJJlBTKAj07KNuG16QpmA5z7EMkN1EqPP9\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-11-03 03:22:55\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '1cee0ccbcb0181ae808fcbf350bd12c32a492842c476a734c91eea46fb6b8e524e383bb7dcbca82d5d42cbd43ba4f24e46e717cb7b75220dcff662fed08ce871', '2020-11-02 21:22:55', '2020-11-02 21:22:55'),
(235, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$QQ0YulniXHfB\\/4nQNmcl2uYUsHElPd9wPuEU.4UXaCK7VvhLBuBqG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:22:55\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ahEc5cJMBLTv5fUDZOf1LQ2qejXJJlBTKAj07KNuG16QpmA5z7EMkN1EqPP9\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-11-03 03:22:56\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '2949798039fe780e4af7b065334d3ccffc5dc130e353e8003e141f3a45c6803924ed814beca87c19e679db6b7ba054ab0745e38a4acdbcc185e0dba14d91526f', '2020-11-02 21:22:56', '2020-11-02 21:22:56'),
(236, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$QQ0YulniXHfB\\/4nQNmcl2uYUsHElPd9wPuEU.4UXaCK7VvhLBuBqG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:22:55\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"9Qcno1sG5ne0vIDrVSVMIrDTSBHSbKPPitlIzhmtRIMKJIRD0COXmKtnAi9W\",\"created_at\":\"2020-08-06 04:41:05\",\"updated_at\":\"2020-11-03 03:22:56\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '60595462e19ccb22899a25a712c282acfdc8318c755399b0b993280c30dae8f1cf3782192d43b0b9e08d5eb9643a91ac9d17f400baa59935d34b03958e97119f', '2020-11-02 21:26:37', '2020-11-02 21:26:37'),
(237, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$VOOs1CJIQ78aKtW5MBmX8.Ml.L8OqyLoZmXneoPF1mbDLjZ9QxGFm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:26:42\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ul4KWGGNSmu0KfYEX6yVrlyzezXiW01AWmNxUtQYdwFLxZOG3cwZ6byNcpod\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:26:42\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '7d77f5dde595c4c1ec90a0ea1dcfef243adb72c98c7da033b3e0be12de4297468ca2efa8bd4153f89feeec59754ba556f85b11773b0f0c2ba94f872c32780f84', '2020-11-02 21:26:42', '2020-11-02 21:26:42'),
(238, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Qb1ZZpxWV1b6t5qTgqqSr.iMfOpr1UVzxQMcCffC9D4AL1CDUFg5K\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:26:42\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"ul4KWGGNSmu0KfYEX6yVrlyzezXiW01AWmNxUtQYdwFLxZOG3cwZ6byNcpod\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:26:42\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'ef0266a0fdbb5fe0ca6826568ad5fa09429039cb81c0fbf4f2d3638efb42ee5e2ef84909b4ac95446bfe4d6a5f8f7a906497d5a2c174a2f4df3d38d48fe7e9e3', '2020-11-02 21:26:42', '2020-11-02 21:26:42'),
(239, 'App\\Models\\User', 1, 'App\\Models\\LogbookEntry', 4, 4, 'created', '{\"company_id\":1,\"sl\":4,\"trans_date\":\"2020-11-03\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"4\",\"driver_name\":\"Parvez\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-03T00:00:00.000000Z\",\"rkm_data\":\"123\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13500\",\"logbook_running\":\"520\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"123\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"7380\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":4,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":1,\"updated_at\":\"2020-11-03 03:28:23\",\"created_at\":\"2020-11-03 03:28:23\",\"id\":4}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/logbook_entry', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'd1dec123e7bcefeaef232b8bd6ae4355336615232de5613b3514a2db01f0e19b387548075da52ed8f17831f00945c2322aa6303ced26c4b3a6d3a8aa45913fda', '2020-11-02 21:28:23', '2020-11-02 21:28:23'),
(240, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Qb1ZZpxWV1b6t5qTgqqSr.iMfOpr1UVzxQMcCffC9D4AL1CDUFg5K\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:26:42\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"4R2STgDyg4F6kgStdENSN3ghqQqrrbWM8wm7xV296VrnuZLzt51kDCREytDP\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:26:42\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '024c181ece429f4d34616f3a62f60fd669eb0c022e690d796a94593d10c1632457f21c0ffbb2a62c86d03f8dc6302b39087417d8ffe955533f33c0245fc9acfb', '2020-11-02 21:30:24', '2020-11-02 21:30:24'),
(241, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$Qb1ZZpxWV1b6t5qTgqqSr.iMfOpr1UVzxQMcCffC9D4AL1CDUFg5K\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:30:29\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"4R2STgDyg4F6kgStdENSN3ghqQqrrbWM8wm7xV296VrnuZLzt51kDCREytDP\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:30:29\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '934addd98f72641f87c67c31dc65538bb1a2efc8096115105a948fb898a37e4034b48979a3e17a8455cca9e1c6ccc662eb6e82c34b954c18c2bb1139fd5628a9', '2020-11-02 21:30:29', '2020-11-02 21:30:29'),
(242, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$NdjwGpDkYa.OlykHfUXqMOJBp36Oudxh8MD9kExpXvElf8TaK4YPi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:30:29\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"4R2STgDyg4F6kgStdENSN3ghqQqrrbWM8wm7xV296VrnuZLzt51kDCREytDP\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:30:29\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '199071d85bd60b1c90c1f4e621969d19cc85a166f1014f2f8f4668ea6c89a42327deebc3211886de55115c447bc1c6c42bd267c7ba1758d5d0a7cc177405bd50', '2020-11-02 21:30:29', '2020-11-02 21:30:29'),
(243, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$NdjwGpDkYa.OlykHfUXqMOJBp36Oudxh8MD9kExpXvElf8TaK4YPi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:30:29\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"6eGHAQQFkSCYT6o6hY4LbvJe1sauB0itBBc0yRv4RR0MVCUG6c0lGoqwhE9Z\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:30:29\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '1e13512585153e00ce0c76a3cc793de0ac6145c79214bb62e8e7efed151a78a5f8ca31a3586bd5e94039804a2301962574cf034582f24122394b818ce7247702', '2020-11-02 21:30:47', '2020-11-02 21:30:47'),
(244, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bn6H23w8WTkxGzY7.3VBfuAGm9\\/SLEFHqPjZFO5blTDTZpod85H8y\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:30:51\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"vkaY3rxI88UlS07DLsx8Tb01aSest8XA7afntiiBpUlfLjlEufepKqBK3tit\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-11-03 03:30:51\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'ac8d477b41596cd67b586f6fb3ac3eefbe8fca161e50c8938b22022997ea6400bb8e59ae5e706ba7bd5dc1414a6d4267e2a875641bcde236d1a185a47e302fdc', '2020-11-02 21:30:51', '2020-11-02 21:30:51'),
(245, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$U0Zz8ZVySwnNjbLMPD6TL.fZeRDJWkQSTtfcbxZ11cvxR8qG8\\/Lxe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:30:51\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"vkaY3rxI88UlS07DLsx8Tb01aSest8XA7afntiiBpUlfLjlEufepKqBK3tit\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-11-03 03:30:51\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'eb1b10882f28e066137347ac16f0ac752536c50ff0b3d1f80e8f3f9d17908a50b73e83d719ccaa0b3ceded199590ae275f75f301f568942d40de3e31a2552bba', '2020-11-02 21:30:51', '2020-11-02 21:30:51'),
(246, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$U0Zz8ZVySwnNjbLMPD6TL.fZeRDJWkQSTtfcbxZ11cvxR8qG8\\/Lxe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:30:51\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"SJd3AOvpmvM0EZZX82EkO0xlTE3TkLzKfwFJEtHkM4JXwX2MMH9sFv7fQYqh\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-11-03 03:30:51\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '4532c4e0140f0a106f856b5b82a42c42ecd9218a34bc27e3f19ddf61d4674478df8e9433e5be63865e81fd140075136c1efb3d6a78180d9813ea87bfe6eb0389', '2020-11-02 21:32:06', '2020-11-02 21:32:06'),
(247, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$NdjwGpDkYa.OlykHfUXqMOJBp36Oudxh8MD9kExpXvElf8TaK4YPi\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:32:13\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"6eGHAQQFkSCYT6o6hY4LbvJe1sauB0itBBc0yRv4RR0MVCUG6c0lGoqwhE9Z\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:32:13\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'aea3f6e5627b40c565bb216852914e4ced8fb6b32baea83dfe6fce2b44a74a117e7cd8ea0b2c492529beb4872085902df4344c0fca2416174dc593d8a560f7a8', '2020-11-02 21:32:13', '2020-11-02 21:32:13'),
(248, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$jHkXldM47.VINYw8FlfJvuArVSE9qTmu\\/fZDU6arvBZ15bEP\\/0owm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:32:13\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"6eGHAQQFkSCYT6o6hY4LbvJe1sauB0itBBc0yRv4RR0MVCUG6c0lGoqwhE9Z\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:32:14\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '2b27bbafb45997b6f235a41b8ccda72a49867d2e0fc9d1e45e800eaf7496c7ce7fa2260c304b23a81026b38b5bd975f2254ecb9167bcff5a319019adca11964e', '2020-11-02 21:32:14', '2020-11-02 21:32:14'),
(249, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$jHkXldM47.VINYw8FlfJvuArVSE9qTmu\\/fZDU6arvBZ15bEP\\/0owm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 03:32:13\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"G3slFG847CKYkc0bEMjlkOAFqINz5j21OHLm8SD5WALjZGNC4FUhisEFCNts\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 03:32:14\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '4955989e18ded331905ad6b0f0b8b493621c0f2300fa7357b89de3028aa96b77c48e50b8e0860f97a7d3a9824758d31853e7ac941ea5b4a0dcfc44f2343899a8', '2020-11-02 21:49:51', '2020-11-02 21:49:51'),
(250, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$jHkXldM47.VINYw8FlfJvuArVSE9qTmu\\/fZDU6arvBZ15bEP\\/0owm\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 05:11:24\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"G3slFG847CKYkc0bEMjlkOAFqINz5j21OHLm8SD5WALjZGNC4FUhisEFCNts\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 05:11:24\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'b569a2fc801e03955914ba30cca350957895d1c94a54cc4a07ebd30e39115871f36d5c9f503860731eee4e8d06c984fa10d64319f89d1e1ad42499012425bd65', '2020-11-02 23:11:24', '2020-11-02 23:11:24');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(251, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$HDqRVV2lt2xQipbeR2LVeuh5IUa2bPl\\/7YPm1FGIEe7GnMqGAujh2\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 05:11:24\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"G3slFG847CKYkc0bEMjlkOAFqINz5j21OHLm8SD5WALjZGNC4FUhisEFCNts\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 05:11:24\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'a272c78987c0e0c6c4d78d155f8868742d2ba2d30ef94fe235380476fe01fe01e4000c7fd008b3a1c82edcca5b085a16e3c83010d90563a4d137c63ee80a5cf7', '2020-11-02 23:11:24', '2020-11-02 23:11:24'),
(252, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$HDqRVV2lt2xQipbeR2LVeuh5IUa2bPl\\/7YPm1FGIEe7GnMqGAujh2\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 05:11:24\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"KtWrX29bbPjIlH2LevVJUrwKECd0eL2l4JVT1imM5txacCgjWhczvMI6s6Dj\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 05:11:24\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '9e9e431a1faa79b05d0d9a32ad3d58f8548e93a2f53a26e2c908aafe354c141cd78999db12706ca628d83c47becb673c2bfd0a3009ac56368b09038ee754c06f', '2020-11-02 23:11:33', '2020-11-02 23:11:33'),
(253, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$U0Zz8ZVySwnNjbLMPD6TL.fZeRDJWkQSTtfcbxZ11cvxR8qG8\\/Lxe\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 05:11:38\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"SJd3AOvpmvM0EZZX82EkO0xlTE3TkLzKfwFJEtHkM4JXwX2MMH9sFv7fQYqh\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-11-03 05:11:38\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'd36b83e7b04479e063263476329394bd076af262df77535117fd502e2bc083a627b1ae7d5f03f6d5be66dc17500b662dc02737234c224bdc3fab759c11354c25', '2020-11-02 23:11:38', '2020-11-02 23:11:38'),
(254, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$5cbxn\\/Fm2xZpjySuXdZOWuzaoBHCnl2wy9oPwpFdXQd\\/9cVy.zZee\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 05:11:38\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"SJd3AOvpmvM0EZZX82EkO0xlTE3TkLzKfwFJEtHkM4JXwX2MMH9sFv7fQYqh\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-11-03 05:11:38\",\"deleted_at\":null}', '[\"password\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '96c19f21758ce3f8b22106501ffd659e8cfeb403a91b7c49702128d3abc65925b3ac62748a38ac0e2ea6de702ad18a782b46d655932fb02025ba3718fddadd6a', '2020-11-02 23:11:38', '2020-11-02 23:11:38'),
(255, 'App\\Models\\User', 10, 'App\\Models\\User', 10, 4, 'updated', '{\"id\":10,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"MOT\",\"email\":\"aicmot@tdcl.transcombd.com\",\"username\":\"AICMOT\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$5cbxn\\/Fm2xZpjySuXdZOWuzaoBHCnl2wy9oPwpFdXQd\\/9cVy.zZee\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"1fa4475ffdf9ce336b3418c9fa432cc8\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 05:11:38\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"VwGkiKPWi7FQqJm84O6KP8hqAA706764jLnrbFjotUQ0fEPpjurYSWgfOHjH\",\"created_at\":\"2020-10-11 04:25:36\",\"updated_at\":\"2020-11-03 05:11:38\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://odometer.test:8080/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '60d58ed38b2897ec28a8f9efd88177b16fe83033ad742c06063113fdaf431e7e671b911a7c0736c08ecd573403fcb4c8b2d0c0b76865f1a8c33508f7dfeea529', '2020-11-02 23:11:46', '2020-11-02 23:11:46'),
(256, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$HDqRVV2lt2xQipbeR2LVeuh5IUa2bPl\\/7YPm1FGIEe7GnMqGAujh2\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 06:03:51\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"KtWrX29bbPjIlH2LevVJUrwKECd0eL2l4JVT1imM5txacCgjWhczvMI6s6Dj\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 06:03:51\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'c5b1745dc63cde40854a817832ee10e7edea2f6b40c011da2807073c4080d80edbd7e68c67886881701b93bc1df124995c6cb5f595d6ffd50dc8c64b26788202', '2020-11-03 00:03:51', '2020-11-03 00:03:51'),
(257, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$o1mOGeu5AQTzRfWfX8HB2eIORWXF\\/YPlHnM7mukCFvfpK7t\\/8iQ8G\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 06:03:51\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"KtWrX29bbPjIlH2LevVJUrwKECd0eL2l4JVT1imM5txacCgjWhczvMI6s6Dj\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-11-03 06:03:52\",\"deleted_at\":null}', '[\"password\",\"updated_at\"]', '[]', '[]', 'http://odometer.test:8080/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '430a76a9f23df849b2db3d4b4ddcb2d6bf4b1a7312d0b0e1219e122d7cbdbc18d7554d73957c5e7bfe5551670c4c944e43d8e827c964b23856f9d429b034939a', '2020-11-03 00:03:52', '2020-11-03 00:03:52'),
(258, 'App\\Models\\User', 1, 'App\\Models\\LogbookEntry', 5, 4, 'created', '{\"company_id\":1,\"sl\":5,\"trans_date\":\"2020-11-03\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"4\",\"driver_name\":\"Parvez\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-03T00:00:00.000000Z\",\"rkm_data\":\"600\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13500\",\"logbook_running\":\"520\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"12\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"720\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":5,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":1,\"updated_at\":\"2020-11-03 06:04:57\",\"created_at\":\"2020-11-03 06:04:57\",\"id\":5}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://odometer.test:8080/public/transactions/logbook_entry', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '5c23253786192442530b7a3e128ce2a0544ab7ec446b30d0d3155d2dc06527abefeef5b6b79ad520001e86c194e185c9a481a7fa865215f314a4254e82c3a771', '2020-11-03 00:04:57', '2020-11-03 00:04:57'),
(259, 'App\\Models\\User', 1, 'App\\Models\\Driver', 5, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"1234567891\",\"name\":\"arif\",\"mobile\":\"01613722564\",\"dob\":\"1981-01-07\",\"doj\":null,\"blood_group\":null,\"em_mobile\":\"01613722564\",\"present_address\":\"dhaka\",\"permanent_address\":\"Dhaka\",\"photo\":null,\"order\":3,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-11-03 11:45:03\",\"created_at\":\"2020-11-03 11:45:03\",\"id\":5}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '086cd36f3595bbd6583ec2c79f7cb8af1411c92a428f74c9c5c2adeacf84230e5aa258e55679d5f79d857c6d592347638fd66297a6b2309d3ff674f2d7510a8e', '2020-11-03 11:45:03', '2020-11-03 11:45:03'),
(260, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 4, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"3\",\"branch_id\":\"2\",\"driver_id\":\"5\",\"take_over\":\"2020-11-03\",\"hand_over\":null,\"order\":4,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-11-03 11:45:59\",\"created_at\":\"2020-11-03 11:45:59\",\"id\":4}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'a001e9f691827d7e23712de2c9c6a579d7e3615a7a60379771271c9de09296f93f4a7aceef9f655adab8186ac0f0559bda365bee409ce0a3c2aba78a0a313d17', '2020-11-03 11:45:59', '2020-11-03 11:45:59'),
(261, 'App\\Models\\User', 1, 'App\\Models\\Company', 3, 4, 'created', '{\"code\":\"34\",\"name\":\"test\",\"short_name\":\"te\",\"address\":\"dhaka\",\"email_suffix\":\"@transcombd.com\",\"logo\":\"Transcom-logo.png\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":1,\"created_at\":\"2020-11-03 12:03:56\",\"updated_at\":\"2020-11-03 12:03:56\",\"id\":3}', '[\"code\",\"name\",\"short_name\",\"address\",\"email_suffix\",\"logo\",\"time_zone\",\"active\",\"created_by\",\"created_at\",\"updated_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/company', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '770b2e19cbf4ea876fd6cf6d86d2e525a107b33f2661c32bb7fea531e845c23a41eb57f333c947db9dbd0319baf871eda549cbda941424d511e6245750317fe7', '2020-11-03 12:03:56', '2020-11-03 12:03:56'),
(262, 'App\\Models\\User', 1, 'App\\Models\\Company', 3, 4, 'updated', '{\"id\":3,\"code\":\"34\",\"name\":\"test\",\"short_name\":\"te\",\"address\":\"dhaka\",\"logo\":\"Transcom-logo.png\",\"email_suffix\":\"@transcombd.com\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":0,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-11-03 12:03:56\",\"updated_at\":\"2020-11-03 12:05:33\",\"deleted_at\":null}', '[\"active\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/company/3/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '32cb39be4f781c21a196cbe0d9aca5ff4e8022472c6a96b246f1cdfbc7302c5d8bd9f925fcced12fdcf84b4f046b6effb429066d6fdd77083bdb7ee36ab12f4a', '2020-11-03 12:05:33', '2020-11-03 12:05:33'),
(263, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{\"id\":2,\"code\":\"2222\",\"name\":\"brix\",\"short_name\":\"R soft\",\"address\":\"dhaka\",\"logo\":\"hiclipart.com.png\",\"email_suffix\":\"great\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":0,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-09-08 02:17:54\",\"updated_at\":\"2020-11-03 12:05:41\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/company/2/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'bcc0f3f5dea36c3a87c27017e3618908ae3e5f9f979370f3c674c0d243f6dc6ec51ab784b3bbedf33576c7aff2d990813f5bde2af8f7922d4403597976ba657a', '2020-11-03 12:05:41', '2020-11-03 12:05:41'),
(264, 'App\\Models\\User', 1, 'App\\Models\\Company', 4, 4, 'created', '{\"code\":\"667\",\"name\":\"test\",\"short_name\":\"te\",\"address\":\"dhaka\",\"email_suffix\":\"@transcombd.com\",\"logo\":\"Transcom-logo.png\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":1,\"created_at\":\"2020-11-03 12:06:31\",\"updated_at\":\"2020-11-03 12:06:31\",\"id\":4}', '[\"code\",\"name\",\"short_name\",\"address\",\"email_suffix\",\"logo\",\"time_zone\",\"active\",\"created_by\",\"created_at\",\"updated_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/company', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '9e929dcf86ca29ff446ded6f1e347374ca9d4f60f5794e3fdc9874a644823a1bf51207056b6f6a08322c00bbc6dae695f3eb9a52718558e771b7fd417d815491', '2020-11-03 12:06:31', '2020-11-03 12:06:31'),
(265, 'App\\Models\\User', 1, 'App\\Models\\User', 11, 4, 'created', '{\"first_name\":\"MOM\",\"last_name\":\"AIC\",\"email\":\"aicmom@tdcl.transcombd.com\",\"username\":\"AICMOM\",\"password\":\"$2y$10$bT8rJ.co\\/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC\",\"active\":true,\"confirmation_code\":\"8c1d0e3449d59bbac9259f85525763d2\",\"confirmed\":true,\"updated_at\":\"2020-11-04 04:00:16\",\"created_at\":\"2020-11-04 04:00:16\",\"id\":11}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/user', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '7fd526616997effa33aa3d172db57f3ff6dc5763b9957ba9a5443b90a89c1d1609c72bcb57fd240f1ca6b4857bf33885b39b5bb0d7cf1fef64cf367fe2b0abe6', '2020-11-04 04:00:16', '2020-11-04 04:00:16'),
(266, 'App\\Models\\User', 11, 'App\\Models\\User', 11, 4, 'updated', '{\"id\":11,\"company_id\":1,\"first_name\":\"MOM\",\"last_name\":\"AIC\",\"email\":\"aicmom@tdcl.transcombd.com\",\"username\":\"AICMOM\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bT8rJ.co\\/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"8c1d0e3449d59bbac9259f85525763d2\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"gKr8FOHt0yUTVnzljPgfzJJXjBDD3n7hhHwSbahOdGpDFGXeQ7uCcTAxOY68\",\"created_at\":\"2020-11-04 04:00:16\",\"updated_at\":\"2020-11-04 04:00:16\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'a1e14c25d682f375ea24fa26cec0560d9d4b9d7782911e8dc45586f1fe263ec3f964261183917d0d4c7916f2d2de5e5f15705b3c183482ebf3e3830fc3a060a3', '2020-11-04 04:04:39', '2020-11-04 04:04:39'),
(267, 'App\\Models\\User', 11, 'App\\Models\\User', 11, 4, 'updated', '{\"id\":11,\"company_id\":1,\"first_name\":\"MOM\",\"last_name\":\"AIC\",\"email\":\"aicmom@tdcl.transcombd.com\",\"username\":\"AICMOM\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bT8rJ.co\\/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"8c1d0e3449d59bbac9259f85525763d2\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"a2vsT1dNP0Iu5jxB78hpbXc8bWS4Yzw2ZPigrRwAIp7qXmDrrPZ23zEnu8CU\",\"created_at\":\"2020-11-04 04:00:16\",\"updated_at\":\"2020-11-04 04:00:16\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '532de9ec81148e82d890030f366bafcb7f3d3510961e496fd8bec5a0654998b7c5bdec86c8e14ebfdc71309ab76c7c8e1ab2b5e85c421d3f6c63d84db741b0a4', '2020-11-04 04:12:03', '2020-11-04 04:12:03'),
(268, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 5, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"2\",\"branch_id\":\"6\",\"driver_id\":\"1\",\"take_over\":\"2020-11-04\",\"hand_over\":null,\"order\":5,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-11-04 04:15:33\",\"created_at\":\"2020-11-04 04:15:33\",\"id\":5}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '448727dd040b5b3eb5d2910427d23e14d45a060cb8fcf352c8751858236f55a41b97b57dbe4de48ecd680e4cc371cfe5a6256bda4baea8023b3d6b53a141e84f', '2020-11-04 04:15:33', '2020-11-04 04:15:33'),
(269, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 8, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"3\",\"name\":\"Demo Truck\",\"manufacturer\":\"Ace\",\"model\":\"AW33\",\"manufacturer_year\":\"2018\",\"weight\":\"130 pound\",\"lifetime\":null,\"chassis_no\":\"LEZZ11111111112222222\",\"engine_no\":\"E11AAATT111\",\"vin_no\":null,\"purchase_date\":\"2020-11-02\",\"license_plate_no\":\"DHAKA-METRO-KA-124987\",\"license_year\":\"2018\",\"main_tank_fuel_id\":\"2\",\"main_tank_fuel_capacity\":\"14\",\"second_tank_fuel_id\":\"1\",\"second_tank_fuel_capacity\":\"13\",\"opening\":\"10970\",\"order\":5,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":false,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-11-04 05:04:56\",\"created_at\":\"2020-11-04 05:04:56\",\"id\":8}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"opening\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '5fa80a610f81ad8e5373fd298c157af4922b2818715b69e853dce884122f3c9ac13520653c11d992c83f87b5be278439428e8d75c600b37a31d28c2d9d317c54', '2020-11-04 05:04:56', '2020-11-04 05:04:56'),
(270, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 9, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-04\",\"vehicle_id\":8,\"tanks\":1,\"tank_type\":1,\"fuel_id\":\"2\",\"capacity\":\"14\",\"millage\":\"5\",\"order\":9,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_at\":\"2020-11-04 05:04:56\",\"created_at\":\"2020-11-04 05:04:56\",\"id\":9}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'd5fa0c6ad9633611d824019ce0c064e79d980ab8bf07450db6475ccbaf85d75b1906b43d3db00ca6637c4d7ff3e0309e44622db50c7b5ade8efc1786dae80c50', '2020-11-04 05:04:56', '2020-11-04 05:04:56'),
(271, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 6, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"8\",\"branch_id\":\"6\",\"driver_id\":\"1\",\"take_over\":\"2020-11-03\",\"hand_over\":null,\"order\":6,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-11-04 05:10:21\",\"created_at\":\"2020-11-04 05:10:21\",\"id\":6}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'bb666012bb8e767825bfc029995c77c805c50584b2273b5fbea7198000bd0c5210bca3a8da399a970eef333e72753d06c911d62bc5170041628b2104b11a04b5', '2020-11-04 05:10:21', '2020-11-04 05:10:21'),
(272, 'App\\Models\\User', 11, 'App\\Models\\LogbookEntry', 6, 4, 'created', '{\"company_id\":1,\"sl\":1,\"trans_date\":\"2020-11-04\",\"branch_id\":\"6\",\"branch_name\":\"Mymensingh\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"8\",\"vehicle_name\":\"Demo Truck\",\"vehicle_license_no\":\"DHAKA-METRO-KA-124987\",\"vehicle_mnf_yr\":\"2018\",\"vehicle_purchase_yr\":\"2020\",\"vehicle_lifetime\":\"2018\",\"vehicle_used\":\"2020\",\"driver_id\":\"1\",\"driver_name\":\"Hasan\",\"driver_take_ovr_dt\":\"2020-11-03T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-04T00:00:00.000000Z\",\"rkm_data\":\"30\",\"logbook_opening\":\"10970.0000\",\"logbook_closing\":\"11000\",\"logbook_running\":\"30\",\"fuel_id\":\"2\",\"fuel_name\":\"Petrol\",\"fuel_consumption\":\"30\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"1800\",\"std_fuel_consumption\":\"14\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":1,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":11,\"updated_at\":\"2020-11-04 05:14:35\",\"created_at\":\"2020-11-04 05:14:35\",\"id\":6}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/logbook_entry', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '472deff32c8b81cdb97754117e1345d68d5a21730ac39d2b2cbeafb65062c8b643549fa5a7d186465fe0b368a2c9a7a5f3d673f68d0e9092d51459f73986a7f5', '2020-11-04 05:14:35', '2020-11-04 05:14:35'),
(273, 'App\\Models\\User', 1, 'App\\Models\\User', 11, 4, 'updated', '{\"id\":11,\"company_id\":1,\"first_name\":\"MYM\",\"last_name\":\"AIC\",\"email\":\"aicmym@tdcl.transcombd.com\",\"username\":\"AICMOM\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bT8rJ.co\\/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"8c1d0e3449d59bbac9259f85525763d2\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"a2vsT1dNP0Iu5jxB78hpbXc8bWS4Yzw2ZPigrRwAIp7qXmDrrPZ23zEnu8CU\",\"created_at\":\"2020-11-04 04:00:16\",\"updated_at\":\"2020-11-04 05:38:51\",\"deleted_at\":null}', '[\"first_name\",\"email\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/user/11', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'affe0c67ea687c53230459ed4cd842a42d13c243c0e224dffa3c32b3ad289293778308b862fd27d9357f27b6e6b12be9d21fa78066deaa34dcae0827d7587f04', '2020-11-04 05:38:51', '2020-11-04 05:38:51'),
(274, 'App\\Models\\User', 11, 'App\\Models\\User', 11, 4, 'updated', '{\"id\":11,\"company_id\":1,\"first_name\":\"MYM\",\"last_name\":\"AIC\",\"email\":\"aicmym@tdcl.transcombd.com\",\"username\":\"AICMOM\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bT8rJ.co\\/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"8c1d0e3449d59bbac9259f85525763d2\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"Y82OPZ1gJhNXVPKtLLB50XjBCWofISDfz7uqm1joeiw5JTLw79CYPukY8BKE\",\"created_at\":\"2020-11-04 04:00:16\",\"updated_at\":\"2020-11-04 05:38:51\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'aeb705df3e5d56a801f5f2c2cbb0fb00ed882f0872645c82d0654ac24551f6f9daf7607edb889ea12a49767e5e76edab1da19c017e34210f8b88a16ae4ea7e01', '2020-11-04 05:39:36', '2020-11-04 05:39:36'),
(275, 'App\\Models\\User', 11, 'App\\Models\\User', 11, 4, 'updated', '{\"id\":11,\"company_id\":1,\"first_name\":\"MYM\",\"last_name\":\"AIC\",\"email\":\"aicmym@tdcl.transcombd.com\",\"username\":\"AICMOM\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$bT8rJ.co\\/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"8c1d0e3449d59bbac9259f85525763d2\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"2pfmwNbXGY1yFiOdBeh3jMkLWoeSGFakOveITRaW2l441sVNNps2dvR1woO4\",\"created_at\":\"2020-11-04 04:00:16\",\"updated_at\":\"2020-11-04 05:38:51\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '60643f191a40a53bebf844ad63933aa0c9c92d8eda4cd7379d9838350e0936dc0dde9d91e2914035ea004967634205c5dccb71ba79f9bbed2c2c5a72964a9d09', '2020-11-04 05:41:39', '2020-11-04 05:41:39'),
(276, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 17, 4, 'updated', '{\"id\":17,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"1\",\"branch_id\":6,\"rate\":\"87.00\",\"order\":17,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 06:05:25\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'cb7b236b0757706f99601f9332e6f8086398607c488b8e772ba4ae91a93aa78d3271882276baee7b5a50bf7cf4215ee55a36eabe86886fbe0433a48f5dde4c96', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(277, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 18, 4, 'updated', '{\"id\":18,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"2\",\"branch_id\":6,\"rate\":\"60.00\",\"order\":18,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 06:05:25\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', 'fc6714f10c92a7c63f8122aedaaee2f4bf7cfe2c92c25205d1c52cf46d3935cb2bbb2f5efeb1f990af8bfedb041fca86743cbfec0f8ad386c09659fefc974d01', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(278, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 19, 4, 'updated', '{\"id\":19,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"3\",\"branch_id\":6,\"rate\":\"65.00\",\"order\":19,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 06:05:25\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '9e4bb4007579b3b1b6389f8ce9a21dfed84dca5294fcdc79bb66559ffbf055179758b6e7abaa9bcbba081a077d00a10b1172ae5cc8682376eb9bb61de71bab77', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(279, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 20, 4, 'updated', '{\"id\":20,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"4\",\"branch_id\":6,\"rate\":\"86.00\",\"order\":20,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 06:05:25\",\"deleted_at\":null}', '[\"rate\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.58', '82061d1df28632c7d363a18029cf5bf6e39ae6b6e32f7e238ce1725e349540ae70fefe0eacbdc8cb6145aaeb286b102fb4d3d5133280f974ac3468b97c4aae16', '2020-11-04 06:05:25', '2020-11-04 06:05:25'),
(280, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 17, 4, 'updated', '{\"id\":17,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"1\",\"branch_id\":6,\"rate\":\"88.00\",\"order\":17,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 09:08:15\",\"deleted_at\":null}', '[\"rate\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.61', '58185115af0f709d52ec3532dcdc65bee3d1d99c3c09a6dd05c384202b9be680df838893f94d9fe04a196b4d87397534d8c93854371fcc7a2c0a82e777086754', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(281, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 18, 4, 'updated', '{\"id\":18,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"2\",\"branch_id\":6,\"rate\":\"60.00\",\"order\":18,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 09:08:15\",\"deleted_at\":null}', '[\"rate\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.61', '0f74cc54f4364e4ec3cd4196091918f8f44042629d5b352eabf4c9fed6b16ed2a668e0b3b6873839b62926d9d96f3dd239a67732177b71f9ceb8ada64f7b6ef2', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(282, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 19, 4, 'updated', '{\"id\":19,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"3\",\"branch_id\":6,\"rate\":\"65.00\",\"order\":19,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 09:08:15\",\"deleted_at\":null}', '[\"rate\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.61', 'ab44719233b6292715d9720c876372369b06a73f367df2ada7987f127432f1143f2d97badfc7ff574d30896b7d92709dac6acd6abeeec7792e224aad5f31bec5', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(283, 'App\\Models\\User', 11, 'App\\Models\\FuelRate', 20, 4, 'updated', '{\"id\":20,\"company_id\":1,\"entry_date\":\"2020-08-16\",\"fuel_id\":\"4\",\"branch_id\":6,\"rate\":\"86.00\",\"order\":20,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_by\":11,\"deleted_by\":null,\"created_at\":\"2020-08-15 16:57:25\",\"updated_at\":\"2020-11-04 09:08:15\",\"deleted_at\":null}', '[\"rate\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate?branch_id=6&date=2020-08-16', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 Edg/86.0.622.61', '3607a0635f358367479046058e5f472f0c11ad951e56e41cfa2fc7322e9ad96801c90705ee1c64a0f962898cdae135df78b889f7f1cb55ebdab70ac2cc8d840a', '2020-11-04 09:08:15', '2020-11-04 09:08:15'),
(284, 'App\\Models\\User', 1, 'App\\Models\\LogbookEntry', 7, 4, 'created', '{\"company_id\":1,\"sl\":7,\"trans_date\":\"2020-11-05\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"1\",\"driver_name\":\"Hasan\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-05T00:00:00.000000Z\",\"rkm_data\":\"120\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13000\",\"logbook_running\":\"20\",\"fuel_id\":\"2\",\"fuel_name\":\"Petrol\",\"fuel_consumption\":\"120\",\"fuel_rate\":\"88.000000\",\"fuel_cost\":\"10560\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":7,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":1,\"updated_at\":\"2020-11-05 16:23:05\",\"created_at\":\"2020-11-05 16:23:05\",\"id\":7}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/logbook_entry', '103.91.232.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', '2fcca5be0191f2b558c6c880ca2c296d8a4d871fa3b63363c9ed6a2c7b4e61eb3d60d1c5b62d391ad51a5aef369690278d817d09184b9602c9100f4a9848fa36', '2020-11-05 16:23:05', '2020-11-05 16:23:05'),
(285, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"root@admin.com\",\"username\":\"root\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$o1mOGeu5AQTzRfWfX8HB2eIORWXF\\/YPlHnM7mukCFvfpK7t\\/8iQ8G\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"3886ede9f760a9238679af97c75e41b3\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-03 00:03:51\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"N38rRsVtuWfx77EkJzLE54CNmIXdHiJGxY2M3qHQhuLwk46roSHn1U4d0Dux\",\"created_at\":\"2020-08-03 16:41:44\",\"updated_at\":\"2020-11-03 00:03:52\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '103.91.232.36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 'bdf156dced154e642470af109cb8e2fc1b805721a87c8f1b7f93bf21728005a881d96bbe4d35ac25fd36a74789de16df31285c1e6bafc7d6e723e071da0361df', '2020-11-05 16:24:02', '2020-11-05 16:24:02'),
(286, 'App\\Models\\User', 1, 'App\\Models\\User', 12, 4, 'created', '{\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"active\":true,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":true,\"updated_at\":\"2020-11-18 04:29:44\",\"created_at\":\"2020-11-18 04:29:44\",\"id\":12}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/user', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', 'cee4077e6a229293ecbf914a3d6ceccfc22cd227ad19b4c39cbd7415d70780c3c6e607ed4c51fda4bf3991c0ea5bda2b81aea87af651caca3cf00ff487aeb20d', '2020-11-18 04:29:44', '2020-11-18 04:29:44'),
(287, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"TUzNLc6gJaBsJMCfFqrfmlr7c1nKzCabbKNjpX8FLZ6ojyRQMyNLcqoxs7zm\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '6dbbd40386c3769dda9eef2a99594fd3e93cdcd82f0f183174dd44b11e052e135d4ee066d10296a6276979e782dd5bfc29e5e0f00ba84ddeeaf0d47314edb6c7', '2020-11-18 04:29:52', '2020-11-18 04:29:52'),
(288, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 7, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"7\",\"branch_id\":\"2\",\"driver_id\":\"1\",\"take_over\":\"2020-11-01\",\"hand_over\":null,\"order\":7,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-11-18 04:46:46\",\"created_at\":\"2020-11-18 04:46:46\",\"id\":7}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '33861650ca9a30db960d22a7565c846f692fe755affe8faaf5127d3c5cf69ff5bdfaf13f4b6ff14fa9fad76a877ef6ed544d72c9b45ee6c511db499a3bc54581', '2020-11-18 04:46:46', '2020-11-18 04:46:46'),
(289, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 1, 4, 'updated', '{\"id\":1,\"company_id\":1,\"vehicle_id\":\"2\",\"branch_id\":\"2\",\"driver_id\":\"4\",\"take_over\":\"2020-08-05\",\"hand_over\":\"2020-11-18\",\"order\":8,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":false,\"created_by\":1,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-08-04 19:48:17\",\"updated_at\":\"2020-11-18 04:47:15\",\"deleted_at\":null}', '[\"hand_over\",\"order\",\"active\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '4442f035f2cbb1801d7a062c1741700b01de2383a7e43fc5c5e64aa67c7bf22fe9e7739b04d331c2ef743ab4d542aaf514b6adf56dad351c603709b68f14ff76', '2020-11-18 04:47:15', '2020-11-18 04:47:15'),
(290, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 8, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"6\",\"branch_id\":\"2\",\"driver_id\":\"1\",\"take_over\":\"2020-11-18\",\"hand_over\":null,\"order\":8,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":1,\"updated_at\":\"2020-11-18 04:47:39\",\"created_at\":\"2020-11-18 04:47:39\",\"id\":8}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '3d535a5c411174d845b353cc28f927b6492e99e794af4a0051edd22abeac4d0d0683066c6d667f241ab554214ca94ab3253b5dbad1a438c8efbd5b8cc3915fc1', '2020-11-18 04:47:39', '2020-11-18 04:47:39'),
(291, 'App\\Models\\User', 1, 'App\\Models\\LogbookEntry', 8, 4, 'created', '{\"company_id\":1,\"sl\":8,\"trans_date\":\"2020-11-18\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"1\",\"driver_name\":\"Hasan\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-18T00:00:00.000000Z\",\"rkm_data\":\"150\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13160\",\"logbook_running\":\"180\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"50\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"3000\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":8,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":1,\"updated_at\":\"2020-11-18 04:56:58\",\"created_at\":\"2020-11-18 04:56:58\",\"id\":8}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/logbook_entry', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '06166785d45940194e903719f0bcc5798fa189fe7df96d4e630ad2197869656b12723053a053f006b7e1c9002a43876df14ff6bdaf6ea9483afff3ce68505913', '2020-11-18 04:56:58', '2020-11-18 04:56:58'),
(292, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{\"id\":6,\"company_id\":1,\"first_name\":\"AIC\",\"last_name\":\"BOG\",\"email\":\"aicbog@tdcl.transcombd.com\",\"username\":\"AICBOG\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$QQ0YulniXHfB\\/4nQNmcl2uYUsHElPd9wPuEU.4UXaCK7VvhLBuBqG\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"80369097ebdd7d606a5bbf629cbf2565\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"is_logged\":0,\"last_login_at\":\"2020-11-02 21:22:55\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"remember_token\":\"DLAI8KM1k2yHiSiQj6SNr12SeoesuiyByqJOEhYGd6xGloK6iknAUvuu1E2V\",\"created_at\":\"2020-08-05 22:41:05\",\"updated_at\":\"2020-11-02 21:22:56\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '92a77f853c033f385050a46cd597f9de11e1e25bc862b87af522c851d199e4d2f059f7cdbd9a66b146777e5d866969299df6e0de0c2c58f636918e30c7e647c5', '2020-11-18 05:06:36', '2020-11-18 05:06:36'),
(293, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"wm3KWu9bjvgIK26oYRvgpsU4iV7Tf88GHMjPu0qgTMDVphwmAcqWqM1bvMx9\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '2dc9dc624ce7d66fdbde77c1246204c1f0446964b8fb97954b5be2e589aa2a1909d073e87ae5ca4c9cb736bc3b91d97895fed84ea232a8273db66b16501418b3', '2020-11-18 05:08:51', '2020-11-18 05:08:51');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(294, 'App\\Models\\User', 12, 'App\\Models\\Branch', 33, 4, 'created', '{\"code\":\"Test1\",\"name\":\"Test Dhaka\",\"short_name\":\"TestDhaka\",\"address1\":\"Dhaka\",\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":33,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-18 05:14:22\",\"created_at\":\"2020-11-18 05:14:22\",\"id\":33}', '[\"code\",\"name\",\"short_name\",\"address1\",\"address2\",\"address3\",\"address4\",\"email\",\"location\",\"bic_id\",\"dbic_id\",\"aic_id\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'c3d947dcd40167aebe091dc4ac9c0699bde156f49a2fa3039e7fc2874703c12338e2fbc1741ac7af1793533a1863a30fcd79531fc1c48d83c9ca839919293a1b', '2020-11-18 05:14:22', '2020-11-18 05:14:22'),
(295, 'App\\Models\\User', 12, 'App\\Models\\User', 13, 4, 'created', '{\"first_name\":\"Din\",\"last_name\":\"Mohammad\",\"email\":\"din.mohammad@transcombd.com\",\"username\":\"sajib\",\"password\":\"$2y$10$xVkThvbHS3x4fpoW.C65vu02qQB.5F\\/DoGbJJR\\/CUrmsSO7mM7XDS\",\"active\":true,\"confirmation_code\":\"3ce448bd789ea0a603f083afa4f6087a\",\"confirmed\":false,\"updated_at\":\"2020-11-18 05:19:15\",\"created_at\":\"2020-11-18 05:19:15\",\"id\":13}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/employee', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'a1c35a8d38a504a1803a43ee1811c58ec0595e19e61f4a5646254bdecfae7b12745515b01d8c8b4ce64f4db3235e244cfdde80452cb3574aceabbb926d9af548', '2020-11-18 05:19:15', '2020-11-18 05:19:15'),
(296, 'App\\Models\\User', 12, 'App\\Models\\Employee', 4, 4, 'created', '{\"company_id\":\"1\",\"user_id\":13,\"employee_id\":\"TDCL-01\",\"name\":\"Din Mohammad\",\"designation_id\":\"1\",\"department\":\"Sales\",\"grade\":\"NDM\",\"email\":\"din.mohammad@transcombd.com\",\"order\":4,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-18 05:19:15\",\"created_at\":\"2020-11-18 05:19:15\",\"id\":4}', '[\"company_id\",\"user_id\",\"employee_id\",\"name\",\"designation_id\",\"department\",\"grade\",\"email\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/employee', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '69e7c418566540e99304d69878cc44a2b513dc262d9bf7b0117cf80a355015c88a55528d9128e59c34f6e7e3fb04c6f3ec3a5729a6bcb454dc126db0b1602d34', '2020-11-18 05:19:15', '2020-11-18 05:19:15'),
(297, 'App\\Models\\User', 12, 'App\\Models\\Employee', 4, 4, 'updated', '{\"id\":4,\"company_id\":\"1\",\"user_id\":13,\"employee_id\":null,\"name\":\"Din Mohammad\",\"designation_id\":\"2\",\"grade\":\"NDM\",\"mobile_no\":null,\"email\":\"din.mohammad@transcombd.com\",\"department\":\"Sales\",\"order\":\"4\",\"active\":true,\"created_by\":12,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-11-18 05:19:15\",\"updated_at\":\"2020-11-18 05:20:00\",\"deleted_at\":null}', '[\"employee_id\",\"designation_id\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/employee/4', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '0a03185f082ba5dc2358cfb737dc16733408e2c0c6328323cec53044d7541faa4ec521a98cc32e62de87d2736fa410d4b81646335cfb31bd2ee94cace19f6836', '2020-11-18 05:20:00', '2020-11-18 05:20:00'),
(298, 'App\\Models\\User', 12, 'App\\Models\\Employee', 4, 4, 'updated', '{\"id\":4,\"company_id\":\"1\",\"user_id\":13,\"employee_id\":\"TDCL-01\",\"name\":\"Din Mohammad\",\"designation_id\":\"2\",\"grade\":\"NDM\",\"mobile_no\":null,\"email\":\"din.mohammad@transcombd.com\",\"department\":\"Sales\",\"order\":\"4\",\"active\":true,\"created_by\":12,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-11-18 05:19:15\",\"updated_at\":\"2020-11-18 05:21:32\",\"deleted_at\":null}', '[\"employee_id\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/employee/4', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '05b201362ba2315dcba30c7047175ca76f3b71b39568287e72b43e490374af8243daec93602974cff138f827f06a1a5887d8c4e9b5f954bfebac3627ca873a30', '2020-11-18 05:21:32', '2020-11-18 05:21:32'),
(299, 'App\\Models\\User', 12, 'App\\Models\\Company', 5, 4, 'created', '{\"code\":\"TCPL-23\",\"name\":\"Transcom Consumer Products Limited\",\"short_name\":\"TCPL\",\"address\":\"Gulshan Tower\",\"email_suffix\":\"tcpl@transcombd.com\",\"logo\":\"TCPL.jpg\",\"time_zone\":\"Asia\\/Dhaka\",\"active\":true,\"created_by\":12,\"created_at\":\"2020-11-18 05:34:14\",\"updated_at\":\"2020-11-18 05:34:14\",\"id\":5}', '[\"code\",\"name\",\"short_name\",\"address\",\"email_suffix\",\"logo\",\"time_zone\",\"active\",\"created_by\",\"created_at\",\"updated_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/company', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'da6bda7722295478d692bbad26c1d8f235be4bb3f9755f9c70f2448cbb26ad28b3514ccefd999a2943b9fb82f861f3498fa158c0d8b7d048dc404fa1c03f72b4', '2020-11-18 05:34:14', '2020-11-18 05:34:14'),
(300, 'App\\Models\\User', 12, 'App\\Models\\Driver', 6, 4, 'created', '{\"company_id\":\"5\",\"employee_id\":\"X-0001\",\"name\":\"Mr. X\",\"mobile\":\"01711111111\",\"dob\":\"1990-11-01\",\"doj\":\"2020-11-01\",\"blood_group\":\"A+\",\"em_mobile\":\"01788888888\",\"present_address\":\"Dhaka\",\"permanent_address\":\"Barisal\",\"photo\":null,\"order\":4,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-18 05:38:28\",\"created_at\":\"2020-11-18 05:38:28\",\"id\":6}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '5ea911d309c8406f7c206abf5271d5f77b4af2a312eb72fd981bd572286fcdbbe6f83434e573c59d0542c15fe85734ef35adce9c0723e7e08320b8aa363bbb8c', '2020-11-18 05:38:28', '2020-11-18 05:38:28'),
(301, 'App\\Models\\User', 12, 'App\\Models\\Driver', 7, 4, 'created', '{\"company_id\":\"5\",\"employee_id\":\"X-0001\",\"name\":\"Mr. X\",\"mobile\":\"01711111111\",\"dob\":\"1990-11-01\",\"doj\":\"2020-11-11\",\"blood_group\":null,\"em_mobile\":\"01788888888\",\"present_address\":null,\"permanent_address\":\"Barisal\",\"photo\":null,\"order\":4,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-18 05:40:29\",\"created_at\":\"2020-11-18 05:40:29\",\"id\":7}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'f2ee69a25bf31b0210e0c6987b57101e81ef0748f02524a40a2a74c7262c1a38e7c8a049f588f3dadebd46faf5cc418d9275fc889e612bf599ed81e992b7a9f5', '2020-11-18 05:40:29', '2020-11-18 05:40:29'),
(302, 'App\\Models\\User', 12, 'App\\Models\\Driver', 8, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"X-0002\",\"name\":\"Mr. Y\",\"mobile\":\"01711111112\",\"dob\":\"1990-11-01\",\"doj\":\"2020-11-04\",\"blood_group\":\"A+\",\"em_mobile\":null,\"present_address\":\"Dhaka\",\"permanent_address\":\"Barisal\",\"photo\":null,\"order\":4,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-18 05:41:29\",\"created_at\":\"2020-11-18 05:41:29\",\"id\":8}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'baacfe1ef8e86f13eaf46a3516b83661320936441139dd1de918de20b557d6b057735eb7508969b78881df74d3c9bebbd575dca00cd175851013ace48fcb4a7a', '2020-11-18 05:41:29', '2020-11-18 05:41:29'),
(303, 'App\\Models\\User', 1, 'App\\Models\\Driver', 8, 4, 'updated', '{\"id\":8,\"company_id\":\"5\",\"employee_id\":\"X-0002\",\"name\":\"Mr. Y\",\"mobile\":\"01711111112\",\"dob\":\"1990-11-01\",\"doj\":\"2020-11-04\",\"blood_group\":\"A+\",\"present_address\":\"Dhaka\",\"permanent_address\":\"Barisal\",\"em_mobile\":null,\"photo\":null,\"order\":\"4\",\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_by\":1,\"deleted_by\":null,\"created_at\":\"2020-11-18 05:41:29\",\"updated_at\":\"2020-11-18 05:44:50\",\"deleted_at\":null}', '[\"company_id\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver/8', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 Edg/86.0.622.69', 'e209d5501a0e7df93779636cf1d31a899dd6f382eae2d4794d39decf92841b111a7046bf2d552099ef914b7fcfb35aeb0f1ce299856796388ea4a4baff75cb4e', '2020-11-18 05:44:50', '2020-11-18 05:44:50'),
(304, 'App\\Models\\User', 12, 'App\\Models\\Driver', 9, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"X-0001\",\"name\":\"Mr. X\",\"mobile\":\"01711111111\",\"dob\":\"1990-11-01\",\"doj\":\"2020-11-01\",\"blood_group\":\"A+\",\"em_mobile\":\"01788888888\",\"present_address\":\"Dhaka\",\"permanent_address\":null,\"photo\":null,\"order\":4,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-18 05:53:52\",\"created_at\":\"2020-11-18 05:53:52\",\"id\":9}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'ad67a8e570200a13ca17d39a2db00ff0153f75b6c565ccfa48f16367ad84a14eaf7e1be04880778b466f60990ca0f25a8f62f5aa561035948fe844169bfe386b', '2020-11-18 05:53:52', '2020-11-18 05:53:52'),
(305, 'App\\Models\\User', 12, 'App\\Models\\VehicleDriver', 9, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"6\",\"branch_id\":\"1\",\"driver_id\":\"9\",\"take_over\":\"2020-11-01\",\"hand_over\":null,\"order\":9,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 05:55:47\",\"created_at\":\"2020-11-18 05:55:47\",\"id\":9}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'e6592d50a336228e14ee9cd037b6f381da64351a519b01060c1920fdeda893867bbb9e15eca2ebec3af74efdc9fd444c4ecfabaf252f64fdccc57613a6b20318', '2020-11-18 05:55:47', '2020-11-18 05:55:47'),
(306, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 29, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"50\",\"order\":29,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:08\",\"created_at\":\"2020-11-18 06:22:08\",\"id\":29}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '8656cc6f2d46cb77fe07de2c595976e62cbf0076acd1997c377268e72fac2efcf3b6fac7c2e61dd64a1c2d2f6601092533f9b95ef3c3184270b95e01562e9184', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(307, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 30, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"2\",\"rate\":0,\"order\":30,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:08\",\"created_at\":\"2020-11-18 06:22:08\",\"id\":30}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'a7598b5fc9f02dcbb128783acab51a68a1c75905ff067863acc29c4e263a364702abb15d87851e579b2f6ea580c204604d4d8a2face9a5c3a3831289870b9729', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(308, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 31, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"3\",\"rate\":0,\"order\":31,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:08\",\"created_at\":\"2020-11-18 06:22:08\",\"id\":31}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'fafcd35cc09825091809d47910d9b37a47c59833c269044ff954b58cd31785c915350185c17a913b4343509f236e7f818071440002072cd71d48181d5d559803', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(309, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 32, 4, 'created', '{\"company_id\":1,\"branch_id\":\"2\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"4\",\"rate\":0,\"order\":32,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:08\",\"created_at\":\"2020-11-18 06:22:08\",\"id\":32}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '53d0f148d81d834cae1f94e9efb7bee7af60166b8739d36310d12c8a08178b66da0cfcfd79d54010dc44c3b990a5f44cb31bd4093674f43494beae5b874918ef', '2020-11-18 06:22:08', '2020-11-18 06:22:08'),
(310, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 33, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"45\",\"order\":33,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:53\",\"created_at\":\"2020-11-18 06:22:53\",\"id\":33}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '848592167a4cfa6f19bcd34dc638b5f6913fcb6fdcd2d5447abbb2f79b9d738577aa1e3b5ecb1262c60fd7dd40753d93f66f9f53795d95c7be1bec5ba0471b3e', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(311, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 34, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"2\",\"rate\":0,\"order\":34,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:53\",\"created_at\":\"2020-11-18 06:22:53\",\"id\":34}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'd2fa3830026897a915ec71e7ec672bdfc33609c044fb5add80308530acf9e639b3529a02373a0a5aecc087dfb2721382c4e035390462a567a7545a4cca820064', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(312, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 35, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"3\",\"rate\":0,\"order\":35,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:53\",\"created_at\":\"2020-11-18 06:22:53\",\"id\":35}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '8114f2c2ea1a873ad5a12b0f375d1ee9bd85d2179de92085a2dfaeca0fc750bf458b76f4bd5e3863129c4dd663f7defd48eb3833ee94ef26c6b0353ce8370929', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(313, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 36, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"70\",\"order\":36,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:22:53\",\"created_at\":\"2020-11-18 06:22:53\",\"id\":36}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '441485e5c7a1e58c2ad704590cb97fbaa2123aa58daecf83a5416fa8b4399cf4f9c2dc2c96457d41ffcc84f129ef43e137c3825d9214a3199b6f76dea999cb8b', '2020-11-18 06:22:53', '2020-11-18 06:22:53'),
(314, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 37, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"44\",\"order\":37,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:23:46\",\"created_at\":\"2020-11-18 06:23:46\",\"id\":37}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'c3e126fddd07702e503185eb964acb2c2edb3ce6385786d90dda2d014383940949876bbe1b3c78a83690299ae19111b9906584861d4c754893ad7ba7df49b8d9', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(315, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 38, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"2\",\"rate\":0,\"order\":38,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:23:46\",\"created_at\":\"2020-11-18 06:23:46\",\"id\":38}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '02e50dea5f5eabe859b4090a60dcdde05cec5e289ec6bb28c21257fdfe7429d0d1230c6f668754be1e5a4e63c8214a14b0cd3d817147c94578c54ca33b357f01', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(316, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 39, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"45\",\"order\":39,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:23:46\",\"created_at\":\"2020-11-18 06:23:46\",\"id\":39}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '52fed9e86db59570f59c872f8ea2ef31505c0729f6ce997263135111c4772a47eff3b46c70a90f3e8432e139038e2adb6fb7b03ecb583a3d512f96501a687db1', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(317, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 40, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"4\",\"rate\":0,\"order\":40,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:23:46\",\"created_at\":\"2020-11-18 06:23:46\",\"id\":40}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '09c8fa1e3aa540f46ef1db6772d43c0eae5041c418b5671bb06461342d35791e8c53a25355e7ffb71e09dc22f88281097be5227252600dc31bac247b9c7ad0d6', '2020-11-18 06:23:46', '2020-11-18 06:23:46'),
(318, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 41, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"1\",\"rate\":0,\"order\":41,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:24:13\",\"created_at\":\"2020-11-18 06:24:13\",\"id\":41}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'd36beeef794d6c0e90fe29cbb3c1182ca811641b8c6ec714e4c1e81f024cdb7a222bba1547006987e9c7a47bfbe06d1cbf330d5949a1a26c45564179d99820d5', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(319, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 42, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"60\",\"order\":42,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:24:13\",\"created_at\":\"2020-11-18 06:24:13\",\"id\":42}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '56667e309830652935542c9f05208d0a23ce74a8183f440cfcd77a20ac2ec11180d3fbbd79817d4c337d1126a1e95c350d6a811be73702a330d74217ef21da59', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(320, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 43, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"3\",\"rate\":0,\"order\":43,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:24:13\",\"created_at\":\"2020-11-18 06:24:13\",\"id\":43}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'd235deb0678e26c7119396e82d4ebf3765ecab527956ad6a556d207b553649b5a652544cd9f5d3766cbb6b5ea2ad51a11010c255285423483dd1f05c86e17184', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(321, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 44, 4, 'created', '{\"company_id\":1,\"branch_id\":\"6\",\"entry_date\":\"2020-11-18 00:00:00\",\"fuel_id\":\"4\",\"rate\":0,\"order\":44,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-18 06:24:13\",\"created_at\":\"2020-11-18 06:24:13\",\"id\":44}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', '577c5a43c73e352b45bf322ef128d3da859437b31015750009f6e3adddb079fd1120ea6b5e47d6b892e615fbdac526b4ffb0f512a8cd038e577ab232feda6582', '2020-11-18 06:24:13', '2020-11-18 06:24:13'),
(322, 'App\\Models\\User', 12, 'App\\Models\\User', 14, 4, 'created', '{\"first_name\":\"Hamza Bin\",\"last_name\":\"Habib\",\"email\":\"hamza.habib@transcombd.com\",\"username\":\"Hamza\",\"password\":\"$2y$10$zeOv35sEDls2\\/X8ZDlO7SOd.aLLfN8a4HC\\/JUxuSYOqilQ6DcnUG6\",\"active\":true,\"confirmation_code\":\"52dd55e5af7bd1e1f3851da03a2a554b\",\"confirmed\":true,\"updated_at\":\"2020-11-18 11:18:53\",\"created_at\":\"2020-11-18 11:18:53\",\"id\":14}', '[\"first_name\",\"last_name\",\"email\",\"username\",\"password\",\"active\",\"confirmation_code\",\"confirmed\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/user', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'ec726dae3e5bc59278c96ee5375c7f81881f00696a0c1ea847280765c898b501b6bbd1818ee0f7f4d2ab0a2803419305495b53006fe909c1ef3c2c7388bb9869', '2020-11-18 11:18:53', '2020-11-18 11:18:53'),
(323, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"W1FsRNvY5hHWYB0f4PYMO6ZYONxQUvIHAY5Z0Kd9E9I8iPmys2XAQ9rurjXM\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'dd9b905543ccb635c355029a3a755bf9189604a50453a50ed705ac1e40aa807f888c7d8ae43d44bb05a074d762608464cbe3da009e759fbf7d13357e180c3a3d', '2020-11-18 11:19:16', '2020-11-18 11:19:16'),
(324, 'App\\Models\\User', 14, 'App\\Models\\User', 14, 4, 'updated', '{\"id\":14,\"company_id\":1,\"first_name\":\"Hamza Bin\",\"last_name\":\"Habib\",\"email\":\"hamza.habib@transcombd.com\",\"username\":\"Hamza\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zeOv35sEDls2\\/X8ZDlO7SOd.aLLfN8a4HC\\/JUxuSYOqilQ6DcnUG6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"52dd55e5af7bd1e1f3851da03a2a554b\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"ddCyvJsv4uCG42evuRfMQmG17qmex7j5k97YU5ugryZMQYVEVKnT8gNd88rw\",\"created_at\":\"2020-11-18 11:18:53\",\"updated_at\":\"2020-11-18 11:18:53\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '706c6e144857cab19179b659a6783829b3f888334004160019f7d700c2e7399b98da3bf265992b1c47873c5f8f26aa32a69d75574dea2ad56be57a71acb63ff9', '2020-11-18 11:19:24', '2020-11-18 11:19:24'),
(325, 'App\\Models\\User', 14, 'App\\Models\\User', 14, 4, 'updated', '{\"id\":14,\"company_id\":1,\"first_name\":\"Hamza Bin\",\"last_name\":\"Habib\",\"email\":\"hamza.habib@transcombd.com\",\"username\":\"Hamza\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$zeOv35sEDls2\\/X8ZDlO7SOd.aLLfN8a4HC\\/JUxuSYOqilQ6DcnUG6\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"52dd55e5af7bd1e1f3851da03a2a554b\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"azrwx09AJ34mAKpZ44FTrlhrxjdWYYwm8RNUVLLTY9BgzFmOwYVR3tThDpwV\",\"created_at\":\"2020-11-18 11:18:53\",\"updated_at\":\"2020-11-18 11:18:53\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'a824a67970471bbb4e77290ae0d39b56b738505fcf8915a0ec228b7eacc1b9818677a1ec24a46db8c332e2663403f2c399bf3d838bb688f63851c7782d9fbc2b', '2020-11-18 11:20:10', '2020-11-18 11:20:10'),
(326, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"XAOuKjEfjrR74kv41yY0O1W6eL24RQccyYuXd3Szxu4UXpCcRJxGajGubhCW\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '0bd62f666abe86e0584ad487b6dff8d468fb5e1e592c7746ff0471be4535cb5eb22c9b0a7d0814f0051eeba600c23dfcc8b454153172286ef261164132c4f36a', '2020-11-18 11:23:28', '2020-11-18 11:23:28'),
(327, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"Q4v7VfCTQxPqGuyQremguBverzVhibmUjt04wZ432x7nSxqV17nT7uODueDI\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '5b7f222d6938db8bf940489edb6337780165ba21cfd9a0cabc7b62cbe2c6d6aa6d215b6cb2b1b2fde58315ef68454029e772da29937fc1ac77668754d02e3332', '2020-11-18 12:18:17', '2020-11-18 12:18:17'),
(328, 'App\\Models\\User', 12, 'App\\Models\\Driver', 10, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"TDCL-003\",\"name\":\"Abu Musa\",\"mobile\":\"01780101012\",\"dob\":\"1988-11-01\",\"doj\":\"2020-11-01\",\"blood_group\":\"O-\",\"em_mobile\":\"01788888888\",\"present_address\":\"Badda, Dhaka\",\"permanent_address\":\"Barisal\",\"photo\":null,\"order\":5,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 05:32:21\",\"created_at\":\"2020-11-22 05:32:21\",\"id\":10}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'd308364da65c59917198c5ff2b9e7aeb075e6e1c6e1fad88e81c52273b34a4785ab8579776fceabadc62a550850dde0d7daa6e817bdf8cc3d6aff17480b3e87f', '2020-11-22 05:32:21', '2020-11-22 05:32:21'),
(329, 'App\\Models\\User', 12, 'App\\Models\\Vehicle', 9, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"3\",\"name\":\"Toyota\",\"manufacturer\":\"Toyota\",\"model\":\"TYTA-003\",\"manufacturer_year\":\"1988\",\"weight\":\"102\",\"lifetime\":null,\"chassis_no\":\"TYTA-1099\",\"engine_no\":\"TYTA-2323\",\"vin_no\":\"TYTA-002\",\"purchase_date\":\"2011-01-01\",\"license_plate_no\":\"DHK-Kha-20\",\"license_year\":\"2019\",\"main_tank_fuel_id\":\"1\",\"main_tank_fuel_capacity\":\"50\",\"second_tank_fuel_id\":\"3\",\"second_tank_fuel_capacity\":\"60\",\"opening\":\"20000\",\"order\":6,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":true,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 05:55:29\",\"created_at\":\"2020-11-22 05:55:29\",\"id\":9}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"opening\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'eefc79919a87c7075e1c90c5bf849213da6be7397f1ac1838e3eb68ca3c28a3555be8b93e41fb36427f4506c569863753c7270a5e9cd8e1e0e55413274fbd85f', '2020-11-22 05:55:29', '2020-11-22 05:55:29'),
(330, 'App\\Models\\User', 12, 'App\\Models\\VehicleHistory', 10, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-22\",\"vehicle_id\":9,\"tanks\":2,\"tank_type\":2,\"fuel_id\":\"3\",\"capacity\":\"60\",\"millage\":\"55\",\"order\":10,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 05:55:30\",\"created_at\":\"2020-11-22 05:55:30\",\"id\":10}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '11b7977ad4169e416b86d192856781f2e25f2280e63fd711d095ae788d8e5e17f3265893bfdf329f8df91cb2b3528825b2ec3fa4a3b2fa5102095653cdc1ec36', '2020-11-22 05:55:30', '2020-11-22 05:55:30'),
(331, 'App\\Models\\User', 12, 'App\\Models\\VehicleHistory', 11, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-22\",\"vehicle_id\":9,\"tanks\":2,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"50\",\"millage\":\"45\",\"order\":11,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 05:55:30\",\"created_at\":\"2020-11-22 05:55:30\",\"id\":11}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '7b460f2e13180391d74b7ad4710bfdb4046e91ac7d7f47badfa05b8ee043307af391881a2e92d1a9b945363bb3a147debe3ce46129b0be09a3644c67e3e8cd15', '2020-11-22 05:55:30', '2020-11-22 05:55:30'),
(332, 'App\\Models\\User', 12, 'App\\Models\\Vehicle', 2, 4, 'updated', '{\"id\":2,\"company_id\":\"1\",\"vehicle_type\":\"4\",\"name\":\"Semi Truck\",\"manufacturer\":\"Runner\",\"model\":\"T51\",\"manufacturer_year\":\"2019\",\"weight\":\"120 pound\",\"lifetime\":null,\"license_plate_no\":\"DHAKA-METRO-HA-244855\",\"license_year\":\"2017\",\"is_dual_tank\":true,\"main_tank_fuel_id\":\"1\",\"main_tank_fuel_capacity\":\"50\",\"second_tank_fuel_id\":\"4\",\"second_tank_fuel_capacity\":\"25\",\"chassis_no\":\"LEZZWAAADDTTTAAS\",\"engine_no\":\"E11AAATT1\",\"vin_no\":null,\"opening\":\"12980.0000\",\"purchase_date\":\"2017-11-08\",\"order\":\"1\",\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":1,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-04 18:30:19\",\"updated_at\":\"2020-11-22 05:58:02\",\"deleted_at\":null}', '[\"is_dual_tank\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '8c3d9322299dc2f9d780cba41bac69f03fdc27317af4fd0f36acd223a29a1972362ff9649dfde2f00d156b1f88449ee4ee71015b5d597a06aa0fe67ffc5220fa', '2020-11-22 05:58:02', '2020-11-22 05:58:02'),
(333, 'App\\Models\\User', 12, 'App\\Models\\VehicleHistory', 12, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-21\",\"vehicle_id\":2,\"tanks\":2,\"tank_type\":2,\"fuel_id\":\"4\",\"capacity\":\"25\",\"millage\":\"45\",\"order\":12,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 05:58:02\",\"created_at\":\"2020-11-22 05:58:02\",\"id\":12}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'b795ee7e576371654f9a834484f8b7cd74534a31c2f4ddcf2a804a507d94f0fcee626a9cbe23e4fa8b73364be86794603f3a9cc70ad3c85d3b6fed287f4866b2', '2020-11-22 05:58:02', '2020-11-22 05:58:02'),
(334, 'App\\Models\\User', 12, 'App\\Models\\VehicleHistory', 13, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-21\",\"vehicle_id\":2,\"tanks\":2,\"tank_type\":1,\"fuel_id\":\"1\",\"capacity\":\"50\",\"millage\":\"55\",\"order\":13,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 05:58:02\",\"created_at\":\"2020-11-22 05:58:02\",\"id\":13}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '689c19b9b9f3c4d7c0b531953c9f4be9c2d0afab3304584bad4d763510a5086ab79417d4ee57d553c98f34ac47dce631fba1d9c03986cce3646f9cbadfcf839c', '2020-11-22 05:58:02', '2020-11-22 05:58:02'),
(335, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 45, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"60\",\"order\":45,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:09:32\",\"created_at\":\"2020-11-22 06:09:32\",\"id\":45}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '3a959e6ed2e015b0e3a76efdd039913fe5e6cc6db607e1d335baf3403e7999246cfcfafebf6679b5ec03a28fdada12d3366085778b948cceff213ec2a9496f16', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(336, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 46, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"90\",\"order\":46,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:09:32\",\"created_at\":\"2020-11-22 06:09:32\",\"id\":46}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '502dfbec49d1af233297ceb92b2f49fc7d506c5c25a9c0a85a6231470feddff8662b18c2fc57deab89973128e038bb05d8afc5f3916054e3cd43f48ca1586993', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(337, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 47, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"85\",\"order\":47,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:09:32\",\"created_at\":\"2020-11-22 06:09:32\",\"id\":47}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '84c6ee18314138f78dd7b35f12894e302cf807287455751b8e257424865fc0749be307bcd06d88463c0e7cea49425df3062b83697f63693f0821c33cdb4cf1f7', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(338, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 48, 4, 'created', '{\"company_id\":1,\"branch_id\":\"21\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"55\",\"order\":48,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:09:32\",\"created_at\":\"2020-11-22 06:09:32\",\"id\":48}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'dba121d512b9731d6f01c78c6a97819647bac7b5fa3aa9f11158e789f5672fe8b5c88d64ece653005a12b9f763623a8a0b5fe32653daa6f59f3b0d1f7b52f649', '2020-11-22 06:09:32', '2020-11-22 06:09:32'),
(339, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 49, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"50\",\"order\":49,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:11:26\",\"created_at\":\"2020-11-22 06:11:26\",\"id\":49}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'db88aec5358d0ef318192b7143bff37aa48e83c4ef0562bd769012b784783cf3ff06a32573eb06be4000977f36214911960512b2e814c5b52324b6972352c99c', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(340, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 50, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"50\",\"order\":50,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:11:26\",\"created_at\":\"2020-11-22 06:11:26\",\"id\":50}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '048a378deb7321f7f53d4ca83d68a7098767399d434317784a6c3b03f3ac2bb23aade288dedcf5fe3615a40a476e2d22a76e88931cba1051b390afc64c229aeb', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(341, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 51, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"45\",\"order\":51,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:11:26\",\"created_at\":\"2020-11-22 06:11:26\",\"id\":51}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '255af2a160d51eb86263d1862ed6cfbee3b5c07dfe2877e1729dddebb99e21669c3e1bbaa09224aba9a34f1272259115e1956784ccca2ab636135582ef9d1c45', '2020-11-22 06:11:26', '2020-11-22 06:11:26'),
(342, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 52, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"52\",\"order\":52,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 06:11:26\",\"created_at\":\"2020-11-22 06:11:26\",\"id\":52}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'd46aeefbbfcfbcef53dc2bfc026b030afefcbadaf632a3e3800ed8d515af77a05eed6594d07b4fa45d8f326c155c013065bb54c383ca8d1cb3539fae2300797b', '2020-11-22 06:11:26', '2020-11-22 06:11:26');
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(343, 'App\\Models\\User', 12, 'App\\Models\\LogbookEntry', 9, 4, 'created', '{\"company_id\":1,\"sl\":9,\"trans_date\":\"2020-11-22\",\"branch_id\":\"2\",\"branch_name\":\"Motijheel\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"1\",\"driver_name\":\"Hasan\",\"driver_take_ovr_dt\":\"2020-08-05T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-22T00:00:00.000000Z\",\"rkm_data\":\"45\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13100\",\"logbook_running\":\"120\",\"fuel_id\":\"2\",\"fuel_name\":\"Petrol\",\"fuel_consumption\":\"45\",\"fuel_rate\":\"0.000000\",\"fuel_cost\":\"0\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":9,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":12,\"updated_at\":\"2020-11-22 06:20:31\",\"created_at\":\"2020-11-22 06:20:31\",\"id\":9}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/logbook_entry', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '4f0ebd2756d089d1028b902dba7b3a99452055bc476021cb34cc153bd3d73456f5ffa08f7f2b7beab5f3d3865eb4e0f8a36bdb96f7e3e5abbf54e6e1b020935c', '2020-11-22 06:20:31', '2020-11-22 06:20:31'),
(344, 'App\\Models\\User', 12, 'App\\Models\\Driver', 11, 4, 'created', '{\"company_id\":\"1\",\"employee_id\":\"TDCL-003\",\"name\":\"Abu Yunus\",\"mobile\":\"01711111111\",\"dob\":\"1988-11-01\",\"doj\":\"2020-11-01\",\"blood_group\":\"A+\",\"em_mobile\":\"01788888888\",\"present_address\":\"Dhaka\",\"permanent_address\":null,\"photo\":null,\"order\":6,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 07:30:00\",\"created_at\":\"2020-11-22 07:30:00\",\"id\":11}', '[\"company_id\",\"employee_id\",\"name\",\"mobile\",\"dob\",\"doj\",\"blood_group\",\"em_mobile\",\"present_address\",\"permanent_address\",\"photo\",\"order\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '0f526bd47729e6dac81ed6d8034173c2af18a8e99ddd8d76783e108a1d43afa6a842694e0138020b93b5fd3702e830dbe68f417a14ee98938a8bd39238779f94', '2020-11-22 07:30:00', '2020-11-22 07:30:00'),
(345, 'App\\Models\\User', 12, 'App\\Models\\Driver', 11, 4, 'updated', '{\"id\":11,\"company_id\":\"1\",\"employee_id\":\"TDCL-004\",\"name\":\"Abu Yunus\",\"mobile\":\"01711111111\",\"dob\":\"1988-11-01\",\"doj\":\"2020-11-01\",\"blood_group\":\"A+\",\"present_address\":\"Dhaka\",\"permanent_address\":null,\"em_mobile\":\"01788888888\",\"photo\":null,\"order\":\"6\",\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-11-22 07:30:00\",\"updated_at\":\"2020-11-22 07:30:31\",\"deleted_at\":null}', '[\"employee_id\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/driver/11', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '757ea489d04bd74139ba95c0e67fa77a61738737785424b62b8c88ab5e95b43a2cdacd64769bb5468a66aa4e184f3ec616356f535e62f3a4bc1e9ca4aa4571b7', '2020-11-22 07:30:31', '2020-11-22 07:30:31'),
(346, 'App\\Models\\User', 12, 'App\\Models\\VehicleDriver', 10, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"2\",\"branch_id\":\"3\",\"driver_id\":\"1\",\"take_over\":\"2020-11-22\",\"hand_over\":null,\"order\":10,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 07:43:47\",\"created_at\":\"2020-11-22 07:43:47\",\"id\":10}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '94a78d3295779a07d9b85a8cb0551854ba5b80027205c456bec7d35fea8695914711dd5d4024fe32584f2618d97f635801266e1a5a7104076f7855b3881609e1', '2020-11-22 07:43:47', '2020-11-22 07:43:47'),
(347, 'App\\Models\\User', 12, 'App\\Models\\Vehicle', 10, 4, 'created', '{\"company_id\":\"1\",\"vehicle_type\":\"4\",\"name\":\"1_Demo Frezer Van\",\"manufacturer\":\"TATA\",\"model\":\"TATA-234\",\"manufacturer_year\":\"2000\",\"weight\":null,\"lifetime\":null,\"chassis_no\":\"TATA-2345\",\"engine_no\":\"TATA-253\",\"vin_no\":\"TATA-4343\",\"purchase_date\":\"2020-11-21\",\"license_plate_no\":\"123\",\"license_year\":\"2019\",\"main_tank_fuel_id\":\"2\",\"main_tank_fuel_capacity\":\"60\",\"second_tank_fuel_id\":\"4\",\"second_tank_fuel_capacity\":\"100\",\"opening\":\"12000\",\"order\":7,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"is_dual_tank\":true,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 08:39:45\",\"created_at\":\"2020-11-22 08:39:45\",\"id\":10}', '[\"company_id\",\"vehicle_type\",\"name\",\"manufacturer\",\"model\",\"manufacturer_year\",\"weight\",\"lifetime\",\"chassis_no\",\"engine_no\",\"vin_no\",\"purchase_date\",\"license_plate_no\",\"license_year\",\"main_tank_fuel_id\",\"main_tank_fuel_capacity\",\"second_tank_fuel_id\",\"second_tank_fuel_capacity\",\"opening\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"is_dual_tank\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'db6fe54f35fbc02f1278c365b38dae93c89aa3a5f651ed940b6f67758bdfa4485e3ae9085835ae6c3695f4b6d15df6b45606613d2642ba52fa777829136aa572', '2020-11-22 08:39:45', '2020-11-22 08:39:45'),
(348, 'App\\Models\\User', 12, 'App\\Models\\VehicleHistory', 14, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-22\",\"vehicle_id\":10,\"tanks\":2,\"tank_type\":2,\"fuel_id\":\"4\",\"capacity\":\"100\",\"millage\":\"60\",\"order\":14,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 08:39:45\",\"created_at\":\"2020-11-22 08:39:45\",\"id\":14}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '422aca65af88d2090c70371dcc9cd5a87be8747918164b890a66185dc7a740a3a0b7c3981b1a355c0ddc348a8a79ebcaae1b2b3a38b963b6c62e0814e4189cd5', '2020-11-22 08:39:45', '2020-11-22 08:39:45'),
(349, 'App\\Models\\User', 12, 'App\\Models\\VehicleHistory', 15, 4, 'created', '{\"company_id\":\"1\",\"entry_date\":\"2020-11-22\",\"vehicle_id\":10,\"tanks\":2,\"tank_type\":1,\"fuel_id\":\"2\",\"capacity\":\"60\",\"millage\":\"50\",\"order\":15,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"active\":true,\"created_by\":12,\"updated_at\":\"2020-11-22 08:39:45\",\"created_at\":\"2020-11-22 08:39:45\",\"id\":15}', '[\"company_id\",\"entry_date\",\"vehicle_id\",\"tanks\",\"tank_type\",\"fuel_id\",\"capacity\",\"millage\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'e386ed92c1947df880517c708766801968b0568158b162db2c695d2bad433aaa210614ae4f2e10d43a4bb5ae7540f923f1761ac174fd8d3bc9f3781e90483158', '2020-11-22 08:39:45', '2020-11-22 08:39:45'),
(350, 'App\\Models\\User', 12, 'App\\Models\\VehicleDriver', 11, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"2\",\"branch_id\":\"3\",\"driver_id\":\"1\",\"take_over\":\"2020-05-08\",\"hand_over\":\"2020-11-22\",\"order\":11,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":1,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 08:57:12\",\"created_at\":\"2020-11-22 08:57:12\",\"id\":11}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '6e87483f68d6e1aebcc9024fc7bfcc1283f169ec5645801208e90be08f2705ca3cadac7749170a8a735a7bcff40f341e950a1c498a0358efd8b0e3d1bdde38f0', '2020-11-22 08:57:12', '2020-11-22 08:57:12'),
(351, 'App\\Models\\User', 12, 'App\\Models\\VehicleDriver', 12, 4, 'created', '{\"company_id\":1,\"vehicle_id\":\"6\",\"branch_id\":\"1\",\"driver_id\":\"9\",\"take_over\":\"2020-11-01\",\"hand_over\":\"2020-11-21\",\"order\":12,\"custom1\":null,\"custom2\":null,\"custom3\":null,\"archived\":1,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 09:02:32\",\"created_at\":\"2020-11-22 09:02:32\",\"id\":12}', '[\"company_id\",\"vehicle_id\",\"branch_id\",\"driver_id\",\"take_over\",\"hand_over\",\"order\",\"custom1\",\"custom2\",\"custom3\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/vehicle_driver', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '0306e537e2c95cafd80de7312c512d3cc215d93bc87375d1350406a3505613c8db4c923e64d00254f5e1edadb1f2a2a4bd9778f696016190ab75d0801dfa93dd', '2020-11-22 09:02:32', '2020-11-22 09:02:32'),
(352, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"JeJckInYWHQGEMXPoNLX7A0qXm7cSdbBi00K7hNj1koOlHEI0nq5hlUJaiUp\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'f443df3a900428c6adfb27af852534fca08ea9735fc60ed3899009394f0b3f8e20ccb6bd599e9c4505e4137faed72e05695c268d0d26a02482e46a6f178c3cd3', '2020-11-22 09:17:57', '2020-11-22 09:17:57'),
(353, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 53, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"1\",\"rate\":\"60\",\"order\":53,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 09:32:20\",\"created_at\":\"2020-11-22 09:32:20\",\"id\":53}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '78f282204b19de669c2e93ba5149a98b1139464fdde2b4670ff24113b62e9d4ed0b7a4d5ad4ab57a893e7cea69643fa785f3e4b75d005915a0dac6c066f6d656', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(354, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 54, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"2\",\"rate\":\"65\",\"order\":54,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 09:32:20\",\"created_at\":\"2020-11-22 09:32:20\",\"id\":54}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '48d59000beecc0253c456f71d6a2480814083b10073820072b9af8272205a63e1ed151f3eec34d18a0263e09735750472a273569a48839ce523148dd669c160a', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(355, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 55, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"3\",\"rate\":\"70\",\"order\":55,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 09:32:20\",\"created_at\":\"2020-11-22 09:32:20\",\"id\":55}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '70917dfc1e5f1f14e15dadf7cc3373ccdca8543a8c1cfc225dd0fb78dd8aabb315046d98d1b718d58bfa3c5475a7b109ec4b1452004e7b125864c61446f4480b', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(356, 'App\\Models\\User', 12, 'App\\Models\\FuelRate', 56, 4, 'created', '{\"company_id\":1,\"branch_id\":\"3\",\"entry_date\":\"2020-11-22 00:00:00\",\"fuel_id\":\"4\",\"rate\":\"55\",\"order\":56,\"archived\":0,\"active\":1,\"created_by\":12,\"updated_at\":\"2020-11-22 09:32:20\",\"created_at\":\"2020-11-22 09:32:20\",\"id\":56}', '[\"company_id\",\"branch_id\",\"entry_date\",\"fuel_id\",\"rate\",\"order\",\"archived\",\"active\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/fuel_rate', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '908b7fbc4ffd671a0e86220c2aa0957026a66992ea6d06829cdd768d3eaf8129ca7f5e1c32eaca5dc23bb0e38090d2e7f7b2a87721c2d1d3b11888866c643606', '2020-11-22 09:32:20', '2020-11-22 09:32:20'),
(357, 'App\\Models\\User', 12, 'App\\Models\\LogbookEntry', 10, 4, 'created', '{\"company_id\":1,\"sl\":10,\"trans_date\":\"2020-11-22\",\"branch_id\":\"3\",\"branch_name\":\"Mirpur\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"1\",\"driver_name\":\"Hasan\",\"driver_take_ovr_dt\":\"2020-11-22T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-22T00:00:00.000000Z\",\"rkm_data\":\"200\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"14000\",\"logbook_running\":\"1020\",\"fuel_id\":\"1\",\"fuel_name\":\"Deisel\",\"fuel_consumption\":\"120\",\"fuel_rate\":\"60.000000\",\"fuel_cost\":\"7200\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":10,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":12,\"updated_at\":\"2020-11-22 09:54:20\",\"created_at\":\"2020-11-22 09:54:20\",\"id\":10}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/logbook_entry', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '76a14e288bbef263b71bb658d79c25f7a3dbfe619cda678cb54505ecefc0b9f9e8a78cd68e68942c0d09988d665ee92265661ebfa35ccec14dce5e1080f58486', '2020-11-22 09:54:20', '2020-11-22 09:54:20'),
(358, 'App\\Models\\User', 12, 'App\\Models\\LogbookEntry', 11, 4, 'created', '{\"company_id\":1,\"sl\":11,\"trans_date\":\"2020-11-22\",\"branch_id\":\"3\",\"branch_name\":\"Mirpur\",\"employee_id\":\"2\",\"employee_name\":\"Bashir Uddin\",\"vehicle_id\":\"2\",\"vehicle_name\":\"Semi Truck\",\"vehicle_license_no\":\"DHAKA-METRO-HA-244855\",\"vehicle_mnf_yr\":\"2019\",\"vehicle_purchase_yr\":\"2017\",\"vehicle_lifetime\":\"2019\",\"vehicle_used\":\"2017\",\"driver_id\":\"1\",\"driver_name\":\"Hasan\",\"driver_take_ovr_dt\":\"2020-11-22T00:00:00.000000Z\",\"driver_hand_ovr_dt\":\"2020-11-22T00:00:00.000000Z\",\"rkm_data\":\"110\",\"logbook_opening\":\"12980.0000\",\"logbook_closing\":\"13100\",\"logbook_running\":\"120\",\"fuel_id\":\"3\",\"fuel_name\":\"Octane\",\"fuel_consumption\":\"20\",\"fuel_rate\":\"70.000000\",\"fuel_cost\":\"1400\",\"std_fuel_consumption\":\"50\",\"approved\":0,\"approved_by\":null,\"status\":0,\"posted\":false,\"order\":11,\"year\":2020,\"month\":11,\"month_name\":\"11\",\"created_by\":12,\"updated_at\":\"2020-11-22 09:57:49\",\"created_at\":\"2020-11-22 09:57:49\",\"id\":11}', '[\"company_id\",\"sl\",\"trans_date\",\"branch_id\",\"branch_name\",\"employee_id\",\"employee_name\",\"vehicle_id\",\"vehicle_name\",\"vehicle_license_no\",\"vehicle_mnf_yr\",\"vehicle_purchase_yr\",\"vehicle_lifetime\",\"vehicle_used\",\"driver_id\",\"driver_name\",\"driver_take_ovr_dt\",\"driver_hand_ovr_dt\",\"rkm_data\",\"logbook_opening\",\"logbook_closing\",\"logbook_running\",\"fuel_id\",\"fuel_name\",\"fuel_consumption\",\"fuel_rate\",\"fuel_cost\",\"std_fuel_consumption\",\"approved\",\"approved_by\",\"status\",\"posted\",\"order\",\"year\",\"month\",\"month_name\",\"created_by\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/transactions/logbook_entry', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '9940662315c6156ee81127fc53ac01689f318545a96fc75f73658e24d7811260ae418a4620d333925a3d8ca3f8c38181a8215bbf2d3fcf8dbeda1416f5d57eab', '2020-11-22 09:57:49', '2020-11-22 09:57:49'),
(359, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"faGZe2f17G6Oo53JWnkNAbw0f8Z952rvgVXQKYrQmux2WetCMBTpgQq16BMk\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'fce7b6e3a2d29d0e17cb9f4705ab5ad261e7d1449cd29c4b5bb048e33ffe5bec57d99c582be967d169eca837a26bf471ed6d625ba2f482769d64d990af715d49', '2020-11-22 11:10:53', '2020-11-22 11:10:53'),
(360, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"ghRanTBdu3UeA8c1hhszL6lODmNImvrreHJYPH7DVz02il9lZYKAo8JEPpvZ\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'bda2ec343ec6f268a5583c6efc0018ee24dc414af7f95faf5b346d2a4b93543b7719938f652725a890c0caf55bb942e55983dcf0044a7afdded10163e5b7d6e7', '2020-11-22 11:13:18', '2020-11-22 11:13:18'),
(361, 'App\\Models\\User', 12, 'App\\Models\\Branch', 2, 4, 'updated', '{\"id\":2,\"code\":\"110\",\"name\":\"Motijheel\",\"short_name\":\"MOT\",\"address1\":null,\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":\"\",\"active\":0,\"created_by\":0,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-03 23:13:57\",\"updated_at\":\"2020-11-22 12:06:37\",\"deleted_at\":null}', '[\"active\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch/2/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '5a3e595552a755f6aa019c52151a6adf81fa53787ae55ec7e28ff4234d5f158559f9c4793ab9d8d2e03eddb6874a9a6e393978b6a12b27f0a083036c3a0096bd', '2020-11-22 12:06:37', '2020-11-22 12:06:37'),
(362, 'App\\Models\\User', 12, 'App\\Models\\Branch', 2, 4, 'updated', '{\"id\":2,\"code\":\"110\",\"name\":\"Motijheel\",\"short_name\":\"MOT\",\"address1\":null,\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":\"\",\"active\":1,\"created_by\":0,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-03 23:13:57\",\"updated_at\":\"2020-11-22 12:07:05\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch/2/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '313f9d5ec2d253927080fb822567d7c07991a785bdbb4eb4f705b2cb79f1244ae9325fcafd47e81dbe6ca786d67da105288934850406bb19b0c2e0d8c7c5aaea', '2020-11-22 12:07:05', '2020-11-22 12:07:05'),
(363, 'App\\Models\\User', 12, 'App\\Models\\Branch', 1, 4, 'updated', '{\"id\":1,\"code\":\"000\",\"name\":\"Corporate\",\"short_name\":\"COR\",\"address1\":null,\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":\"\",\"active\":0,\"created_by\":0,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-03 23:13:57\",\"updated_at\":\"2020-11-22 12:07:43\",\"deleted_at\":null}', '[\"active\",\"updated_by\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch/1/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '118b0fbac4f753a76b6d1d79103a4ead4b5c62d1ecffe05f2e85de38ba1fa746daf78d6c274c6200949d2607ec5bcd09b191deaf871d0b5c83baf5e65889b89e', '2020-11-22 12:07:43', '2020-11-22 12:07:43'),
(364, 'App\\Models\\User', 12, 'App\\Models\\Branch', 1, 4, 'updated', '{\"id\":1,\"code\":\"000\",\"name\":\"Corporate\",\"short_name\":\"COR\",\"address1\":null,\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":\"\",\"active\":1,\"created_by\":0,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-03 23:13:57\",\"updated_at\":\"2020-11-22 12:08:14\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch/1/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '7e13d0821db7b6ca864742680f44e5200fdc843471738238b74e330e7a5d99c3a2d9f989de31c9b81a73c623cc06b6da3ef402abf1ea49d81b0a6ec067636338', '2020-11-22 12:08:14', '2020-11-22 12:08:14'),
(367, 'App\\Models\\User', 12, 'App\\Models\\Branch', 2, 4, 'updated', '{\"id\":2,\"code\":\"110\",\"name\":\"Motijheel\",\"short_name\":\"MOT\",\"address1\":null,\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":\"\",\"active\":0,\"created_by\":0,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-03 23:13:57\",\"updated_at\":\"2020-11-22 12:11:18\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch/2/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'bffcf46bec1e2bfbc9d35574674d536d7e324339f4a4ba4af122ab8dc5b0650e1b7c5935f27f761b9bccda602fbfdbcebcc668fdd47a79c7b139e6a72eda3d0f', '2020-11-22 12:11:18', '2020-11-22 12:11:18'),
(369, 'App\\Models\\User', 12, 'App\\Models\\Branch', 2, 4, 'updated', '{\"id\":2,\"code\":\"110\",\"name\":\"Motijheel\",\"short_name\":\"MOT\",\"address1\":null,\"address2\":null,\"address3\":null,\"address4\":null,\"email\":null,\"location\":null,\"bic_id\":null,\"dbic_id\":null,\"aic_id\":null,\"order\":\"\",\"active\":1,\"created_by\":0,\"updated_by\":12,\"deleted_by\":null,\"created_at\":\"2020-08-03 23:13:57\",\"updated_at\":\"2020-11-22 12:11:50\",\"deleted_at\":null}', '[\"active\",\"updated_at\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/master/branch/2/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'c6151108824a154f595757221c3051fb2b4eb54fd66362e88b8fcd63d2de54a7bbf71eeef8a5d8b54dc98f8e9bc8561d83dccf36812b477344e962012d1f73b9', '2020-11-22 12:11:50', '2020-11-22 12:11:50'),
(370, 'App\\Models\\User', 12, 'App\\Models\\User', 12, 4, 'updated', '{\"id\":12,\"company_id\":1,\"first_name\":\"Saiful\",\"last_name\":\"Islam\",\"email\":\"saiful.islam@transcombd.com\",\"username\":\"saiful\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$P8LaVa9JfIok4Bc\\/kgkqWuP2Bubkpkxl\\/RauFx4vWk9dMUXwkZD4.\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"83ec5c2a94a8ecbfc0df31c70670f87a\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"62zAEGHZNuDXHeHEIphfH5RRc5K3M8Nma4862qqGzOfVdZCXHmLXwPrXoanH\",\"created_at\":\"2020-11-18 04:29:44\",\"updated_at\":\"2020-11-18 04:29:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://fuelmeter.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'fd333a83c96dccc34606a5345ae2ba0b88edebc5270937e5317400bfd7e9408c776b1ce2c24cb037785b729c7bb4111f645d80fad3a3bdeced9e5aaf77f0ea2e', '2020-11-29 05:07:23', '2020-11-29 05:07:23'),
(371, 'App\\Models\\User', 2, 'App\\Models\\User', 2, 4, 'updated', '{\"id\":2,\"company_id\":1,\"first_name\":\"Admin\",\"last_name\":\"Istator\",\"email\":\"admin@admin.com\",\"username\":\"admin\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$8AOTWzxUh3qK2ji7Y7E.Y.M\\/PC9p06heJWwJjZ2eHUZnQ4j1\\/UiTy\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"5b81902f59b14f18b943def25f90bf57\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"4OBB5sOsM2Se9oSmC2TXIdbnSXboUsvn0qNiKNN0e8d2jg5cF4ezJxLM8hdA\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-08-03 22:41:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.50', 'cf0af1070dd48d59b0f3ffedd73faceb3d2f86696b1e4bae30e841e9d1cd7f4038839e72ab8e08f8d238c612a94b3a168ac3bd5b1d8130c2fd52ecacf8688f83', '2021-01-24 00:01:16', '2021-01-24 00:01:16'),
(372, 'App\\Models\\User', 2, 'App\\Models\\User', 2, 4, 'updated', '{\"id\":2,\"company_id\":1,\"first_name\":\"Admin\",\"last_name\":\"Istator\",\"email\":\"admin@admin.com\",\"username\":\"admin\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$8AOTWzxUh3qK2ji7Y7E.Y.M\\/PC9p06heJWwJjZ2eHUZnQ4j1\\/UiTy\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"5b81902f59b14f18b943def25f90bf57\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"K6JLqMgE8fu5f0LaGitekyuseiMnOME9gxjcMeZWQoRnUB3UTGuq0U9DEPbK\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-08-03 22:41:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.50', 'a0f75b0bb726655559fcde98a54edf166a92cf9b85abec1c37edc0f34b354f6e30666c153efc5adfb2182b6091141e4c904134a82acb4a5bd0a2ffa4cde168df', '2021-01-24 00:05:40', '2021-01-24 00:05:40'),
(373, 'App\\Models\\User', 2, 'App\\Models\\User', 2, 4, 'updated', '{\"id\":2,\"company_id\":1,\"first_name\":\"Admin\",\"last_name\":\"Istator\",\"email\":\"admin@admin.com\",\"username\":\"admin\",\"email_verified_at\":null,\"avatar_type\":\"photo\",\"avatar_location\":null,\"password\":\"$2y$10$8AOTWzxUh3qK2ji7Y7E.Y.M\\/PC9p06heJWwJjZ2eHUZnQ4j1\\/UiTy\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"5b81902f59b14f18b943def25f90bf57\",\"confirmed\":1,\"timezone\":null,\"is_logged\":0,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"jv06h2kXZWnVbW9UZQouygMjBM7cSzmhWE4XimXWvbxv9bYI9PL6Tc8bvFqJ\",\"created_at\":\"2020-08-03 22:41:44\",\"updated_at\":\"2020-08-03 22:41:44\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36 Edg/88.0.705.50', '20bf982a71c6af3242bf15478239ec092a9b93967705dcc396a961330c136b40ddf1a13b0366d3a191dffb007d7fc421ba9e86074f66fcb8331042a26d3124a6', '2021-01-24 00:07:02', '2021-01-24 00:07:02');

-- --------------------------------------------------------

--
-- Table structure for table `logbook_entries`
--

CREATE TABLE `logbook_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `sl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'transaction no',
  `trans_date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT 'NDM id',
  `employee_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `vehicle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_license_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_mnf_yr` int(11) NOT NULL COMMENT 'vehicle manufacturing year',
  `vehicle_purchase_yr` int(11) NOT NULL COMMENT 'vehicle purchase year',
  `vehicle_lifetime` int(11) NOT NULL COMMENT 'vehicle lifetime till date',
  `vehicle_used` int(11) NOT NULL COMMENT 'vehicle used (in days) as per action plan',
  `driver_id` int(11) NOT NULL,
  `driver_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_take_ovr_dt` date NOT NULL COMMENT 'vehicle key take over date',
  `driver_hand_ovr_dt` date NOT NULL COMMENT 'vehicle key hand over date',
  `rkm_data` decimal(10,4) NOT NULL COMMENT 'RKM VTS Data (KM)',
  `logbook_opening` decimal(10,4) NOT NULL COMMENT 'KM of Logbook opening',
  `logbook_closing` decimal(10,4) NOT NULL COMMENT 'KM of Logbook closing',
  `logbook_running` decimal(10,4) NOT NULL COMMENT 'KM of total running',
  `fuel_id` int(11) NOT NULL,
  `fuel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fuel_consumption` decimal(10,4) NOT NULL,
  `fuel_rate` decimal(10,4) NOT NULL,
  `fuel_cost` decimal(10,4) NOT NULL,
  `std_fuel_consumption` decimal(10,4) NOT NULL COMMENT 'standard_fuel_consumption',
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `approved_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `posted` tinyint(4) NOT NULL DEFAULT 0,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `month_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logbook_entries`
--

INSERT INTO `logbook_entries` (`id`, `company_id`, `sl`, `trans_date`, `branch_id`, `branch_name`, `employee_id`, `employee_name`, `vehicle_id`, `vehicle_name`, `vehicle_license_no`, `vehicle_mnf_yr`, `vehicle_purchase_yr`, `vehicle_lifetime`, `vehicle_used`, `driver_id`, `driver_name`, `driver_take_ovr_dt`, `driver_hand_ovr_dt`, `rkm_data`, `logbook_opening`, `logbook_closing`, `logbook_running`, `fuel_id`, `fuel_name`, `fuel_consumption`, `fuel_rate`, `fuel_cost`, `std_fuel_consumption`, `approved`, `approved_by`, `status`, `posted`, `year`, `month`, `month_name`, `order`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1', '2020-08-01', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 4, 'Parvez', '2020-08-01', '2020-08-01', '30.0000', '12980.0000', '13015.0000', '35.0000', 1, 'Deisel', '45.0000', '86.0000', '3870.0000', '50.0000', 1, NULL, 2, 0, 2020, 8, '08', 1, 1, NULL, NULL, '2020-08-15 17:42:22', '2020-11-02 21:28:57', NULL),
(2, 1, '2', '2020-10-11', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 4, 'Parvez', '2020-08-05', '2020-10-10', '234.0000', '12980.0000', '13500.0000', '520.0000', 1, 'Deisel', '9.0000', '60.0000', '540.0000', '50.0000', 1, NULL, 2, 0, 2020, 10, '10', 2, 10, NULL, NULL, '2020-10-10 22:57:21', '2020-11-04 04:35:30', NULL),
(3, 1, '3', '2020-10-17', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 4, 'Parvez', '2020-08-05', '2020-10-15', '123456.0000', '12980.0000', '13500.0000', '520.0000', 1, 'Deisel', '500.0000', '60.0000', '30000.0000', '50.0000', 1, NULL, 2, 0, 2020, 10, '10', 3, 10, NULL, NULL, '2020-10-17 01:03:24', '2020-11-04 04:35:30', NULL),
(4, 1, '4', '2020-11-03', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 4, 'Parvez', '2020-08-05', '2020-11-03', '123.0000', '12980.0000', '13500.0000', '520.0000', 1, 'Deisel', '123.0000', '60.0000', '7380.0000', '50.0000', 1, NULL, 2, 0, 2020, 11, '11', 4, 1, NULL, NULL, '2020-11-02 21:28:23', '2020-11-04 04:35:30', NULL),
(5, 1, '5', '2020-11-03', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 4, 'Parvez', '2020-08-05', '2020-11-03', '600.0000', '12980.0000', '13500.0000', '520.0000', 1, 'Deisel', '12.0000', '60.0000', '720.0000', '50.0000', 1, NULL, 2, 0, 2020, 11, '11', 5, 1, NULL, NULL, '2020-11-03 00:04:57', '2020-11-04 04:35:30', NULL),
(6, 1, '1', '2020-11-04', 6, 'Mymensingh', 2, 'Bashir Uddin', 8, 'Demo Truck', 'DHAKA-METRO-KA-124987', 2018, 2020, 2018, 2020, 1, 'Hasan', '2020-11-03', '2020-11-04', '30.0000', '10970.0000', '11000.0000', '30.0000', 2, 'Petrol', '30.0000', '60.0000', '1800.0000', '14.0000', 1, NULL, 2, 0, 2020, 11, '11', 1, 11, NULL, NULL, '2020-11-04 05:14:35', '2020-11-04 05:15:56', NULL),
(7, 1, '7', '2020-11-05', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 1, 'Hasan', '2020-08-05', '2020-11-05', '120.0000', '12980.0000', '13000.0000', '20.0000', 2, 'Petrol', '120.0000', '88.0000', '10560.0000', '50.0000', 1, NULL, 2, 0, 2020, 11, '11', 7, 1, NULL, NULL, '2020-11-05 16:23:05', '2020-11-18 08:02:39', NULL),
(8, 1, '8', '2020-11-18', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 1, 'Hasan', '2020-08-05', '2020-11-18', '150.0000', '12980.0000', '13160.0000', '180.0000', 1, 'Deisel', '50.0000', '60.0000', '3000.0000', '50.0000', 1, NULL, 2, 0, 2020, 11, '11', 8, 1, NULL, NULL, '2020-11-18 04:56:58', '2020-11-18 04:57:46', NULL),
(9, 1, '9', '2020-11-22', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 1, 'Hasan', '2020-08-05', '2020-11-22', '45.0000', '12980.0000', '13100.0000', '120.0000', 2, 'Petrol', '45.0000', '0.0000', '0.0000', '50.0000', 1, NULL, 2, 0, 2020, 11, '11', 9, 12, NULL, NULL, '2020-11-22 06:20:31', '2020-11-22 06:21:05', NULL),
(10, 1, '10', '2020-11-22', 3, 'Mirpur', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 1, 'Hasan', '2020-11-22', '2020-11-22', '200.0000', '12980.0000', '14000.0000', '1020.0000', 1, 'Deisel', '120.0000', '60.0000', '7200.0000', '50.0000', 1, NULL, 2, 1, 2020, 11, '11', 10, 12, NULL, NULL, '2020-11-22 09:54:20', '2020-11-22 09:55:38', NULL),
(11, 1, '11', '2020-11-22', 3, 'Mirpur', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 1, 'Hasan', '2020-11-22', '2020-11-22', '110.0000', '12980.0000', '13100.0000', '120.0000', 3, 'Octane', '20.0000', '70.0000', '1400.0000', '50.0000', 0, NULL, 0, 0, 2020, 11, '11', 11, 12, NULL, NULL, '2020-11-22 09:57:49', '2020-11-22 09:57:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_09_03_144628_create_permission_tables', 1),
(9, '2017_09_26_140332_create_cache_table', 1),
(10, '2017_09_26_140528_create_sessions_table', 1),
(11, '2017_09_26_140609_create_jobs_table', 1),
(12, '2018_04_08_033256_create_password_histories_table', 1),
(13, '2018_11_21_000001_create_ledgers_table', 1),
(14, '2019_07_24_150207_setup_users_table', 1),
(15, '2019_08_19_000000_create_failed_jobs_table', 1),
(16, '2019_10_18_075255_create_companies_table', 1),
(17, '2020_07_30_095141_create_activity_log_table', 1),
(18, '2020_08_03_052921_create_branches_table', 1),
(19, '2020_08_03_053025_create_user_branches_table', 1),
(20, '2020_08_04_055416_create_employees_table', 2),
(23, '2020_08_04_072329_create_drivers_table', 3),
(24, '2020_08_04_072617_create_driver_documents_table', 3),
(25, '2020_08_04_111420_create_fuels_table', 4),
(26, '2020_08_04_115832_create_vehicles_table', 5),
(27, '2020_08_05_050021_create_vehicle_types_table', 5),
(28, '2020_08_05_051823_create_documents_types_table', 6),
(29, '2020_08_05_062735_create_vehicle_documents_table', 7),
(30, '2020_08_05_071436_create_vehicle_drivers_table', 8),
(31, '2020_08_05_104908_create_fuel_rates_table', 9),
(32, '2020_08_09_060246_create_logbook_entries', 10),
(33, '2020_08_09_065317_add_columns_in_vehicle_tables', 11),
(34, '2020_08_09_065341_create_vehicle_history_table', 11),
(35, '2020_08_10_075022_create_employee_designations_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 6),
(1, 'App\\Models\\User', 8),
(1, 'App\\Models\\User', 12),
(7, 'App\\Models\\User', 8);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 5),
(1, 'App\\Models\\User', 12),
(2, 'App\\Models\\User', 2),
(3, 'App\\Models\\User', 3),
(3, 'App\\Models\\User', 6),
(3, 'App\\Models\\User', 7),
(3, 'App\\Models\\User', 8),
(3, 'App\\Models\\User', 10),
(3, 'App\\Models\\User', 11),
(3, 'App\\Models\\User', 13),
(3, 'App\\Models\\User', 14);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_histories`
--

CREATE TABLE `password_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_histories`
--

INSERT INTO `password_histories` (`id`, `user_id`, `password`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$duQ/Luo3Fp/vyBSWVv62ruiQ692QTgy31p5slAREJcflK/je.NE/q', '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(2, 2, '$2y$10$tFLTbB5ODvY6beF3M9cBNOoppQbGh0n6NWge.jzgwcdAFW7cPuFai', '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(3, 3, '$2y$10$HNAJwDaVyBR5.aK9t.FWMuyhfnk3RMbEh8ZjWYh9UlHWSbitSsV9C', '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(4, 1, '$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG', '2020-08-03 16:41:59', '2020-08-03 16:41:59'),
(6, 5, '$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL/cdyHSOdnbrDXc8O5cUh8kG', '2020-08-03 18:44:21', '2020-08-03 18:44:21'),
(7, 1, '$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq', '2020-08-04 16:34:03', '2020-08-04 16:34:03'),
(8, 1, '$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms/8qOmPS0g7wb9QPB1WUVmCOC', '2020-08-05 22:37:51', '2020-08-05 22:37:51'),
(9, 6, '$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW', '2020-08-05 22:41:05', '2020-08-05 22:41:05'),
(10, 6, '$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg/qNpTlmCFfgN9nJtAhrq', '2020-08-05 23:00:35', '2020-08-05 23:00:35'),
(11, 6, '$2y$10$/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW', '2020-08-07 17:53:11', '2020-08-07 17:53:11'),
(12, 1, '$2y$10$CNB/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G', '2020-08-07 18:43:11', '2020-08-07 18:43:11'),
(13, 6, '$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW', '2020-08-07 18:46:29', '2020-08-07 18:46:29'),
(14, 1, '$2y$10$ONvL9H/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa', '2020-08-07 18:55:40', '2020-08-07 18:55:40'),
(15, 6, '$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q', '2020-08-07 18:56:34', '2020-08-07 18:56:34'),
(16, 1, '$2y$10$Eqn4puXstk0B7kXpKHiJWuvg/xMVaa08HjaMe32vdCuQ5lWjx7b9.', '2020-08-08 18:51:49', '2020-08-08 18:51:49'),
(17, 1, '$2y$10$TFz5OpeJVpDmaqhNCDSc5uiov6m0C30lw6ngWc6raurxaCwkk0cbq', '2020-08-11 21:39:06', '2020-08-11 21:39:06'),
(18, 7, '$2y$10$XFe6dqOEiYVO0QLk9O8oqOxEOD50/uGlOVaa1fA9ruzdbqvokPwyK', '2020-08-15 16:47:42', '2020-08-15 16:47:42'),
(19, 1, '$2y$10$9SMbOJqey/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD/wI5vlNUZEe', '2020-09-07 22:33:03', '2020-09-07 22:33:03'),
(20, 6, '$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L/wSMS3bNURqZJNefOnW', '2020-09-07 22:34:59', '2020-09-07 22:34:59'),
(21, 1, '$2y$10$Dst.N445dreP4kNI/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi', '2020-09-07 22:35:37', '2020-09-07 22:35:37'),
(22, 1, '$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W', '2020-09-07 22:38:11', '2020-09-07 22:38:11'),
(23, 6, '$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O', '2020-09-07 22:38:19', '2020-09-07 22:38:19'),
(24, 1, '$2y$10$/43fgsfG48meVrs6WxOqIejm5KytE0qcCtG8.38j.4RprD1ttR5PC', '2020-09-08 02:09:27', '2020-09-08 02:09:27'),
(25, 8, '$2y$10$5Wmsnn/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6', '2020-09-08 02:12:52', '2020-09-08 02:12:52'),
(26, 1, '$2y$10$rReBoyduZYkwcdrQrfeR1OdONlMYQoQ90XgCo6oRFvgHEkSSxLnty', '2020-09-08 02:14:11', '2020-09-08 02:14:11'),
(27, 1, '$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi', '2020-09-08 02:14:16', '2020-09-08 02:14:16'),
(28, 6, '$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU/MmKKHR95SsTYCyS', '2020-09-08 02:14:35', '2020-09-08 02:14:35'),
(29, 1, '$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa', '2020-09-08 02:28:47', '2020-09-08 02:28:47'),
(30, 6, '$2y$10$3kCpwukGAxvU5UBSp.h6MO8asDFLi/zTwY.dUqi/fFsHB/ZTy1ay6', '2020-09-08 04:00:55', '2020-09-08 04:00:55'),
(31, 6, '$2y$10$OLCCEDtdSjvfeSFxeh/mI.Uzg61kOKWIiICF9KmKGIA/Mz26BhD4m', '2020-09-08 04:24:27', '2020-09-08 04:24:27'),
(32, 1, '$2y$10$e59TfnIdHX5szbAnl4YP2e9QOvGn3vv7zvqPcoGYsnZEXUOCWGxxK', '2020-09-11 02:38:14', '2020-09-11 02:38:14'),
(33, 1, '$2y$10$HYbe34AnTZ8W/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe', '2020-09-19 02:20:42', '2020-09-19 02:20:42'),
(34, 6, '$2y$10$zN5nvSvDg0JEdD2/m0MfAu/7yW..hm2JPr7GtgUSB3LvooLeaYg0e', '2020-09-19 02:21:34', '2020-09-19 02:21:34'),
(35, 1, '$2y$10$y0EEOBdl/vcnlI5kwPfMjuCdAMmZUfpYPyDI.MB1hj.MZBf.AXVLW', '2020-09-19 22:21:57', '2020-09-19 22:21:57'),
(36, 10, '$2y$10$z51zs0gbRwpWQQMR0Dh5m.jD9OYUEZKd3PMO.A2Ckl9tHwE9krqUy', '2020-10-10 22:25:36', '2020-10-10 22:25:36'),
(37, 1, '$2y$10$o3tupCcnnS6wcvTASFh1WegYNzR1l8W8HLZWvLlFuiyl/Khs7crWi', '2020-10-10 22:28:07', '2020-10-10 22:28:07'),
(38, 10, '$2y$10$avTVhcsaMw9bnPSolwuea.8aoq72nIclejd/1Ik5IgKACt7.61h.O', '2020-10-10 22:29:18', '2020-10-10 22:29:18'),
(39, 1, '$2y$10$0bENg7WFGPZ.MtgDx6HZs.H6bO8MXBCOp.PTzg/RDQ7zZZQMJfPYe', '2020-10-10 22:54:26', '2020-10-10 22:54:26'),
(40, 1, '$2y$10$STOkgyCLFQT7wuAgWhGBq.eLuzVa1fSNm5Sr3O5AlWWa16ghSFK3C', '2020-10-11 02:07:58', '2020-10-11 02:07:58'),
(41, 6, '$2y$10$AQ/VbfTD4TOs53/SFsM55uRsVqTjPXYx71AGLBJoc1UzhgWidcYNm', '2020-10-17 00:54:33', '2020-10-17 00:54:33'),
(42, 1, '$2y$10$FPBFdOUZrdh7M/e1iUB8COJaMSl0Pwggf5lGiCi1GqaQX.10PJO8O', '2020-10-17 00:55:02', '2020-10-17 00:55:02'),
(43, 6, '$2y$10$aECfJlRWGo7YAWJbr1yP5OHkiqdLiI/tMv7qJ6cC8pTf2GNLJTsqC', '2020-10-17 00:55:47', '2020-10-17 00:55:47'),
(44, 10, '$2y$10$bn6H23w8WTkxGzY7.3VBfuAGm9/SLEFHqPjZFO5blTDTZpod85H8y', '2020-10-17 00:59:57', '2020-10-17 00:59:57'),
(45, 1, '$2y$10$VOOs1CJIQ78aKtW5MBmX8.Ml.L8OqyLoZmXneoPF1mbDLjZ9QxGFm', '2020-10-17 01:10:09', '2020-10-17 01:10:09'),
(46, 6, '$2y$10$QQ0YulniXHfB/4nQNmcl2uYUsHElPd9wPuEU.4UXaCK7VvhLBuBqG', '2020-11-02 21:22:56', '2020-11-02 21:22:56'),
(47, 1, '$2y$10$Qb1ZZpxWV1b6t5qTgqqSr.iMfOpr1UVzxQMcCffC9D4AL1CDUFg5K', '2020-11-02 21:26:42', '2020-11-02 21:26:42'),
(48, 1, '$2y$10$NdjwGpDkYa.OlykHfUXqMOJBp36Oudxh8MD9kExpXvElf8TaK4YPi', '2020-11-02 21:30:29', '2020-11-02 21:30:29'),
(49, 10, '$2y$10$U0Zz8ZVySwnNjbLMPD6TL.fZeRDJWkQSTtfcbxZ11cvxR8qG8/Lxe', '2020-11-02 21:30:51', '2020-11-02 21:30:51'),
(50, 1, '$2y$10$jHkXldM47.VINYw8FlfJvuArVSE9qTmu/fZDU6arvBZ15bEP/0owm', '2020-11-02 21:32:14', '2020-11-02 21:32:14'),
(51, 1, '$2y$10$HDqRVV2lt2xQipbeR2LVeuh5IUa2bPl/7YPm1FGIEe7GnMqGAujh2', '2020-11-02 23:11:25', '2020-11-02 23:11:25'),
(52, 10, '$2y$10$5cbxn/Fm2xZpjySuXdZOWuzaoBHCnl2wy9oPwpFdXQd/9cVy.zZee', '2020-11-02 23:11:38', '2020-11-02 23:11:38'),
(53, 1, '$2y$10$o1mOGeu5AQTzRfWfX8HB2eIORWXF/YPlHnM7mukCFvfpK7t/8iQ8G', '2020-11-03 00:03:52', '2020-11-03 00:03:52'),
(54, 11, '$2y$10$bT8rJ.co/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC', '2020-11-04 04:00:16', '2020-11-04 04:00:16'),
(55, 12, '$2y$10$P8LaVa9JfIok4Bc/kgkqWuP2Bubkpkxl/RauFx4vWk9dMUXwkZD4.', '2020-11-18 04:29:44', '2020-11-18 04:29:44'),
(56, 13, '$2y$10$xVkThvbHS3x4fpoW.C65vu02qQB.5F/DoGbJJR/CUrmsSO7mM7XDS', '2020-11-18 05:19:15', '2020-11-18 05:19:15'),
(57, 14, '$2y$10$zeOv35sEDls2/X8ZDlO7SOd.aLLfN8a4HC/JUxuSYOqilQ6DcnUG6', '2020-11-18 11:18:53', '2020-11-18 11:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'view backend', 'web', 1, '2020-08-03 16:41:45', '2020-08-03 16:41:45'),
(2, 'setup-user-deactivated', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(3, 'setup-user-deleted', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(4, 'setup-user-index', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(5, 'setup-user-create', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(6, 'setup-user-store', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(7, 'setup-user-show', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(8, 'setup-user-edit', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(9, 'setup-user-mark', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(10, 'setup-user-delete', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(11, 'setup-user-delete-permanently', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(12, 'setup-user-restore', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(13, 'setup-user-login-as', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(14, 'setup-user-clear-session', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(15, 'setup-log-viewer-dashboard', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(16, 'setup-log-viewer-logs', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(17, 'setup-log-viewer-download', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(18, 'setup-log-viewer-delete', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(19, 'setup-role-index', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(20, 'setup-role-create', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(21, 'setup-role-store', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(22, 'setup-role-edit', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(23, 'setup-role-update', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(24, 'setup-role-destroy', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(25, 'setup-role-mark', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(26, 'setup-role-view-deleted', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(27, 'setup-role-view-deactivated', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(28, 'setup-permission-index', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(29, 'setup-permission-create', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(30, 'setup-permission-store', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(31, 'setup-permission-edit', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(32, 'setup-permission-update', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(33, 'setup-company-index', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(34, 'setup-company-create', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(35, 'setup-company-store', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(36, 'setup-company-show', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(37, 'setup-company-edit', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(38, 'setup-company-update', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(39, 'setup-company-delete', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(40, 'setup-company-destroy', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(41, 'setup-company-restore', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(42, 'setup-company-mark', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(43, 'setup-company-view-deactivated', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(44, 'setup-company-view-deleted', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(45, 'setup-api-clients', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(46, 'setup-api-tokens', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(47, 'setup-branch-index', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(48, 'setup-branch-create', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(49, 'setup-branch-store', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(50, 'setup-branch-show', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(51, 'setup-branch-edit', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(52, 'setup-branch-update', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(53, 'setup-branch-delete', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(54, 'setup-branch-destroy', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(55, 'setup-branch-restore', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(56, 'setup-branch-mark', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(57, 'setup-branch-view-deactivated', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(58, 'setup-branch-view-deleted', 'web', 1, '2019-06-27 23:34:01', '2019-06-27 23:34:01'),
(59, 'transactions-fuel-rate-index', 'web', 1, '2020-08-05 22:43:48', '2020-08-05 22:43:48'),
(60, 'transactions-fuel-rate-create', 'web', 1, '2020-08-05 22:45:25', '2020-08-05 22:45:25'),
(61, 'transactions-fuel-rate-update', 'web', 1, '2020-08-05 22:45:35', '2020-08-05 22:45:35'),
(62, 'transactions-fuel-rate-edit', 'web', 1, '2020-08-05 22:45:44', '2020-08-05 22:45:44'),
(63, 'transactions-fuel-rate-store', 'web', 1, '2020-08-05 22:45:44', '2020-08-05 22:45:44'),
(64, 'transactions-logbook-entry-create', 'web', 1, '2020-08-07 18:43:37', '2020-08-07 18:43:37'),
(65, 'transactions-logbook-entry-store', 'web', 1, '2020-08-07 18:43:51', '2020-08-07 18:43:51'),
(66, 'transactions-logbook-entry-edit', 'web', 1, '2020-08-07 18:44:01', '2020-08-07 18:44:01'),
(67, 'transactions-logbook-entry-update', 'web', 1, '2020-08-07 18:44:10', '2020-08-07 18:44:10'),
(68, 'transactions-logbook-entry-delete', 'web', 1, '2020-08-07 18:44:18', '2020-08-07 18:44:18'),
(69, 'transactions-logbook-entry-index', 'web', 1, '2020-08-07 18:55:57', '2020-08-07 18:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'web', 1, '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(2, 'administrator', 'web', 1, '2020-08-03 16:41:44', '2020-08-03 16:41:44'),
(3, 'user', 'web', 1, '2020-08-03 16:41:45', '2020-08-03 16:41:45'),
(4, 'facerole', 'web', 1, '2020-09-08 02:25:06', '2020-09-08 02:25:06');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(2, 4),
(59, 3),
(60, 3),
(61, 3),
(62, 3),
(63, 3),
(64, 3),
(65, 3),
(66, 3),
(67, 3),
(68, 3),
(69, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_transaction_settings`
--

CREATE TABLE `system_transaction_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `previous` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'is previous',
  `interval` int(11) NOT NULL DEFAULT 2 COMMENT 'date interval days',
  `day` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'm' COMMENT 'day/month',
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `expire_at` datetime DEFAULT NULL COMMENT 'restriction will expired',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_transaction_settings`
--

INSERT INTO `system_transaction_settings` (`id`, `user_id`, `branch_id`, `previous`, `interval`, `day`, `from_date`, `to_date`, `expire_at`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(2, 1, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(3, 7, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(4, 7, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(5, 7, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(6, 7, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(7, 7, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(8, 7, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(9, 7, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(10, 7, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(11, 7, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(12, 7, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(13, 7, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(14, 7, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(15, 7, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(16, 7, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(17, 7, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(18, 7, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(19, 7, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(20, 7, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(21, 7, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(22, 7, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(23, 7, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(24, 7, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(25, 7, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(26, 7, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(27, 7, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(28, 7, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(29, 7, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(30, 7, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(31, 7, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(32, 7, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(33, 7, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(34, 7, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(35, 11, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(36, 9, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(37, 9, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(38, 9, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(39, 9, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(40, 9, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(41, 9, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(42, 9, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(43, 9, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(44, 9, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(45, 9, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(46, 9, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(47, 9, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(48, 9, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(49, 9, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(50, 9, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(51, 9, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(52, 9, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(53, 9, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(54, 9, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(55, 9, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(56, 9, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(57, 9, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(58, 9, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(59, 9, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(60, 9, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(61, 9, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(62, 9, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(63, 9, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(64, 9, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(65, 9, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(66, 9, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(67, 9, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(68, 12, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(69, 13, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(70, 14, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(71, 15, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(72, 16, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(73, 17, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(74, 18, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(75, 19, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(76, 20, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(77, 21, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(78, 22, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(79, 23, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(80, 24, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(81, 25, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(82, 26, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(83, 27, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(84, 28, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(85, 29, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(86, 31, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(87, 32, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(88, 33, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(89, 34, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(90, 36, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(91, 37, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(92, 38, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(93, 39, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(94, 40, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(95, 41, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(96, 42, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(97, 35, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(98, 8, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(99, 8, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(100, 8, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(101, 8, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(102, 8, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(103, 8, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(104, 8, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(105, 8, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(106, 8, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(107, 8, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(108, 8, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(109, 8, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(110, 8, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(111, 8, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(112, 8, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(113, 8, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(114, 8, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(115, 8, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(116, 8, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(117, 8, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(118, 8, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(119, 8, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(120, 8, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(121, 8, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(122, 8, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(123, 8, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(124, 8, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(125, 8, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(126, 8, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(127, 8, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(128, 8, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(129, 8, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(130, 4, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(131, 4, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(132, 4, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(133, 4, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(134, 4, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(135, 4, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(136, 4, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(137, 4, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(138, 4, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(139, 4, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(140, 4, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(141, 4, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(142, 4, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(143, 4, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(144, 4, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(145, 4, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(146, 4, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(147, 4, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(148, 4, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(149, 4, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(150, 4, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(151, 4, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(152, 4, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(153, 4, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(154, 4, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(155, 4, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(156, 4, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(157, 4, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(158, 4, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(159, 4, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(160, 4, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(161, 4, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(162, 43, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-11 09:08:01'),
(163, 44, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(164, 45, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(165, 46, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(166, 30, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(167, 47, 14, 1, 27, 'd', '2020-04-03', '2020-04-30', '2020-05-05 21:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-05-05 19:54:20'),
(168, 48, 14, 0, 0, 'd', '2020-05-23', '2020-05-23', '2020-05-27 12:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-05-27 10:17:49'),
(169, 49, 22, 0, 0, 'd', '2020-08-05', '2020-08-05', '2020-09-01 15:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-01 14:56:18'),
(170, 50, 22, 0, 0, 'd', '2020-08-08', '2020-08-08', '2020-09-09 13:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-09 12:35:40'),
(171, 51, 15, 1, 36, 'm', '2020-04-12', '2020-05-18', '2020-05-21 15:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-05-21 10:12:27'),
(172, 52, 15, 0, 0, 'd', '2020-06-15', '2020-06-15', '2020-06-22 17:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-22 16:15:20'),
(173, 53, 32, 0, 0, 'd', '2020-07-28', '2020-07-28', '2020-08-12 11:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-12 10:19:09'),
(174, 54, 32, 0, 0, 'd', '2020-06-09', '2020-06-09', '2020-06-18 18:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-18 17:58:30'),
(175, 55, 9, 1, 2, 'd', '2020-07-14', '2020-07-16', '2020-07-22 17:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-22 16:35:13'),
(176, 56, 9, 1, 16, 'd', '2020-07-13', '2020-07-29', '2020-08-09 17:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-09 16:23:00'),
(177, 57, 28, 0, 0, 'd', '2020-05-10', '2020-05-10', '2020-05-17 12:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-05-17 11:35:22'),
(178, 58, 28, 1, 22, 'd', '2020-07-06', '2020-07-28', '2020-08-03 20:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-03 19:25:30'),
(179, 10, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(180, 10, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(181, 10, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(182, 10, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(183, 10, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(184, 10, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(185, 10, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(186, 10, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(187, 10, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(188, 10, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(189, 10, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(190, 10, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(191, 10, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(192, 10, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(193, 10, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(194, 10, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(195, 10, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(196, 10, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(197, 10, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(198, 10, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(199, 10, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(200, 10, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(201, 10, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(202, 10, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(203, 10, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(204, 10, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(205, 10, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(206, 10, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(207, 10, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(208, 10, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(209, 10, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(210, 10, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(211, 6, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(212, 6, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(213, 6, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(214, 6, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(215, 6, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(216, 6, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(217, 6, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(218, 6, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(219, 6, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(220, 6, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(221, 6, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(222, 6, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(223, 6, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(224, 6, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(225, 6, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(226, 6, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(227, 6, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(228, 6, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(229, 6, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(230, 6, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(231, 6, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(232, 6, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(233, 6, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(234, 6, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(235, 6, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(236, 6, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(237, 6, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(238, 6, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(239, 6, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(240, 6, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(241, 6, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(242, 6, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(243, 62, 31, 0, 0, 'd', '2020-06-02', '2020-06-02', '2020-06-10 14:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-10 13:40:03'),
(244, 61, 31, 0, 0, 'd', '2020-08-18', '2020-08-18', '2020-08-26 13:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-26 13:04:03'),
(245, 63, 5, 1, 2, 'd', '2020-09-06', '2020-09-08', '2020-09-14 23:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-14 10:50:20'),
(246, 65, 6, 0, 0, 'd', '2020-07-16', '2020-07-16', '2020-08-04 13:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-04 13:03:19'),
(247, 66, 6, 1, 1, 'd', '2019-10-25', '2019-10-26', '2019-11-28 20:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-11-28 10:01:41'),
(248, 64, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(249, 67, 7, 1, 2, 'd', '2020-07-14', '2020-07-16', '2020-07-22 13:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-22 12:28:55'),
(250, 68, 7, 1, 9, 'd', '2019-11-03', '2019-11-12', '2019-12-09 23:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-12-09 13:34:21'),
(251, 69, 8, 1, 69, 'm', '2020-05-06', '2020-07-14', '2020-08-27 11:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-27 11:01:08'),
(252, 70, 8, 1, 9, 'd', '2020-06-09', '2020-06-18', '2020-08-29 12:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-29 12:00:55'),
(253, 71, 10, 1, 47, 'm', '2020-03-29', '2020-05-15', '2020-05-20 19:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-05-20 15:18:58'),
(254, 72, 10, 0, 0, 'd', '2020-05-21', '2020-05-21', '2020-06-09 20:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-09 19:53:29'),
(255, 76, 16, 0, 0, 'd', '2020-06-14', '2020-06-14', '2020-06-24 12:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-24 11:38:49'),
(256, 73, 13, 0, 0, 'd', '2019-05-14', '2019-05-14', '2020-08-05 19:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-05 18:00:14'),
(257, 74, 13, 0, 0, 'd', '2020-04-02', '2020-04-02', '2020-04-07 13:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-04-07 11:52:10'),
(258, 75, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(259, 77, 17, 0, 0, 'd', '2020-03-12', '2020-03-12', '2020-04-08 11:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-04-08 10:17:47'),
(260, 78, 17, 1, 17, 'd', '2020-06-13', '2020-06-30', '2020-07-09 13:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-09 12:18:26'),
(261, 79, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(262, 80, 18, 0, 0, 'd', '2020-08-06', '2020-08-06', '2020-08-12 10:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-12 09:41:33'),
(263, 81, 19, 0, 0, 'd', '2020-01-08', '2020-01-08', '2020-02-09 16:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-02-09 15:41:33'),
(264, 83, 20, 0, 0, 'd', '2020-08-31', '2020-08-31', '2020-09-05 14:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-05 13:49:13'),
(265, 84, 20, 0, 0, 'd', '2020-02-08', '2020-02-08', '2020-03-02 13:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-03-02 12:37:43'),
(266, 85, 21, 0, 0, 'd', '2020-08-19', '2020-08-19', '2020-08-26 09:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-26 09:13:22'),
(267, 86, 21, 1, 1, 'd', '2020-02-26', '2020-02-27', '2020-03-02 18:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-03-02 16:56:28'),
(268, 87, 23, 1, 13, 'd', '2020-02-06', '2020-02-19', '2020-02-27 13:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-02-29 12:13:02'),
(269, 88, 23, 1, 2, 'd', '2019-11-02', '2019-11-04', '2019-12-08 20:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-12-08 05:00:47'),
(270, 89, 24, 0, 0, 'd', '2020-06-30', '2020-06-30', '2020-07-09 20:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-09 19:16:42'),
(271, 90, 25, 0, 0, 'd', '2020-05-23', '2020-05-23', '2020-05-30 17:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-05-30 17:07:46'),
(272, 91, 25, 0, 0, 'd', '2020-02-17', '2020-02-17', '2020-02-24 19:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-02-24 17:16:46'),
(273, 92, 26, 0, 0, 'd', '2020-07-02', '2020-07-02', '2020-07-09 20:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-09 19:29:11'),
(274, 93, 26, 1, 5, 'd', '2020-02-10', '2020-02-15', '2020-03-15 13:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-03-15 11:41:27'),
(275, 94, 27, 0, 0, 'd', '2019-11-19', '2019-11-19', '2019-12-09 12:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-12-09 05:39:00'),
(276, 95, 27, 0, 0, 'd', '2020-06-02', '2020-06-02', '2020-07-25 14:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-25 13:26:13'),
(277, 96, 29, 0, 0, 'd', '2020-03-05', '2020-03-05', '2020-03-22 12:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-03-22 11:15:34'),
(278, 97, 29, 1, 125, 'm', '2020-02-06', '2020-06-10', '2020-07-11 13:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-11 11:43:43'),
(280, 82, 19, 0, 0, 'd', '2020-08-06', '2020-08-06', '2020-08-26 15:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-26 15:16:43'),
(282, 59, 12, 1, 5, 'd', '2020-06-04', '2020-06-09', '2020-06-17 21:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-17 20:57:56'),
(283, 100, 24, 1, 1, 'd', '2020-02-01', '2020-02-02', '2020-02-09 17:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-02-09 15:46:25'),
(284, 101, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(285, 102, 30, 0, 0, 'd', '2020-06-05', '2020-06-05', '2020-06-20 17:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-20 16:46:15'),
(286, 60, 12, 1, 4, 'd', '2019-10-18', '2019-10-22', '2019-10-28 20:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-10-28 16:27:40'),
(287, 103, 4, 1, 11, 'd', '2020-08-20', '2020-08-31', '2020-09-05 16:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-05 12:01:25'),
(288, 104, 4, 0, 0, 'd', '2020-08-20', '2020-08-20', '2020-09-06 17:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-06 17:00:11'),
(289, 105, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(290, 105, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(291, 105, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(292, 105, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(293, 105, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(294, 105, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(295, 105, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(296, 105, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(297, 105, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(298, 105, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(299, 105, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(300, 105, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(301, 105, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(302, 105, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(303, 105, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(304, 105, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(305, 105, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(306, 105, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(307, 105, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(308, 105, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(309, 105, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(310, 105, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(311, 105, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(312, 105, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(313, 105, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(314, 105, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(315, 105, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(316, 105, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(317, 105, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(318, 105, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(319, 105, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(320, 105, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(321, 106, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(322, 106, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(323, 106, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(324, 106, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(325, 106, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(326, 106, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(327, 106, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(328, 106, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(329, 106, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(330, 106, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(331, 106, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(332, 106, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(333, 106, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(334, 106, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(335, 106, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(336, 106, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(337, 106, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(338, 106, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(339, 106, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(340, 106, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(341, 106, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(342, 106, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(343, 106, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(344, 106, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(345, 106, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(346, 106, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(347, 106, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(348, 106, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(349, 106, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(350, 106, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(351, 106, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(352, 106, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(353, 2, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(354, 2, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(355, 2, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(356, 2, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(357, 2, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(358, 2, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(359, 2, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(360, 2, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(361, 2, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(362, 2, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(363, 2, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(364, 2, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(365, 2, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(366, 2, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(367, 2, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(368, 2, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(369, 2, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(370, 2, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(371, 2, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(372, 2, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(373, 2, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(374, 2, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(375, 2, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(376, 2, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(377, 2, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(378, 2, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(379, 2, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(380, 2, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(381, 2, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(382, 2, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(383, 2, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(384, 2, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(385, 108, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(386, 108, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(387, 108, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(388, 108, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(389, 108, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(390, 108, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(391, 108, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(392, 108, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(393, 108, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(394, 108, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(395, 108, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(396, 108, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(397, 108, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(398, 108, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(399, 108, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(400, 108, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(401, 108, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(402, 108, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(403, 108, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(404, 108, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(405, 108, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(406, 108, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(407, 108, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(408, 108, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(409, 108, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(410, 108, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(411, 108, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(412, 108, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00');
INSERT INTO `system_transaction_settings` (`id`, `user_id`, `branch_id`, `previous`, `interval`, `day`, `from_date`, `to_date`, `expire_at`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(413, 108, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(414, 108, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(415, 108, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(416, 108, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(417, 109, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(418, 109, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(419, 109, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(420, 109, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(421, 109, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(422, 109, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(423, 109, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(424, 109, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(425, 109, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(426, 109, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(427, 109, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(428, 109, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(429, 109, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(430, 109, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(431, 109, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(432, 109, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(433, 109, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(434, 109, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(435, 109, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(436, 109, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(437, 109, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(438, 109, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(439, 109, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(440, 109, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(441, 109, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(442, 109, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(443, 109, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(444, 109, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(445, 109, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(446, 109, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(447, 109, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(448, 109, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(449, 110, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(450, 110, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(451, 110, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(452, 110, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(453, 110, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(454, 110, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(455, 110, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(456, 110, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(457, 110, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(458, 110, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(459, 110, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(460, 110, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(461, 110, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(462, 110, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(463, 110, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(464, 110, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(465, 110, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(466, 110, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(467, 110, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(468, 110, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(469, 110, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(470, 110, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(471, 110, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(472, 110, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(473, 110, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(474, 110, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(475, 110, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(476, 110, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(477, 110, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(478, 110, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(479, 110, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(480, 110, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(481, 111, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(482, 111, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(483, 111, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(484, 111, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(485, 111, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(486, 111, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(487, 111, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(488, 111, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(489, 111, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(490, 111, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(491, 111, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(492, 111, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(493, 111, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(494, 111, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(495, 111, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(496, 111, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(497, 111, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(498, 111, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(499, 111, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(500, 111, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(501, 111, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(502, 111, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(503, 111, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(504, 111, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(505, 111, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(506, 111, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(507, 111, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(508, 111, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(509, 111, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(510, 111, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(511, 111, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(512, 111, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(513, 112, 1, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(514, 112, 2, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(515, 112, 3, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(516, 112, 4, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(517, 112, 5, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(518, 112, 6, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(519, 112, 7, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(520, 112, 8, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(521, 112, 9, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(522, 112, 10, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(523, 112, 11, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(524, 112, 12, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(525, 112, 13, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(526, 112, 14, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(527, 112, 15, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(528, 112, 16, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(529, 112, 17, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(530, 112, 18, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(531, 112, 19, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(532, 112, 20, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(533, 112, 21, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(534, 112, 22, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(535, 112, 23, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(536, 112, 24, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(537, 112, 25, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(538, 112, 26, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(539, 112, 27, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(540, 112, 28, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(541, 112, 29, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(542, 112, 30, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(543, 112, 31, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(544, 112, 32, 1, 2, 'd', NULL, NULL, '2019-09-09 00:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-08 18:00:00'),
(1089, 114, 11, 1, 1, 'd', '2020-08-09', '2020-08-10', '2020-09-03 18:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-09-03 18:20:17'),
(1090, 115, 11, 0, 0, 'd', '2020-06-29', '2020-06-29', '2020-07-06 17:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-07-06 16:56:25'),
(1091, 116, 2, 0, 0, 'd', '2020-07-30', '2020-07-30', '2020-08-04 17:30:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-08-04 17:08:58'),
(1092, 117, 2, 1, 1, 'd', '2020-05-20', '2020-05-21', '2020-06-06 11:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2020-06-06 10:19:45'),
(1093, 118, 3, 1, 0, 'd', '2019-09-23', '2019-09-23', '2019-09-23 19:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-09-23 11:43:48'),
(1094, 119, 3, 1, 2, 'd', '2019-11-01', '2019-11-03', '2019-12-08 11:00:00', 1, NULL, NULL, '2019-09-08 18:00:00', '2019-12-08 03:56:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT 1,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `avatar_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'photo',
  `avatar_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `confirmation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT 1,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_logged` tinyint(1) NOT NULL DEFAULT 0,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `company_id`, `first_name`, `last_name`, `email`, `username`, `email_verified_at`, `avatar_type`, `avatar_location`, `password`, `password_changed_at`, `active`, `confirmation_code`, `confirmed`, `timezone`, `is_logged`, `last_login_at`, `last_login_ip`, `to_be_logged_out`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Super', 'Admin', 'root@admin.com', 'root', NULL, 'photo', NULL, '$2y$10$8AOTWzxUh3qK2ji7Y7E.Y.M/PC9p06heJWwJjZ2eHUZnQ4j1/UiTy', NULL, 1, '3886ede9f760a9238679af97c75e41b3', 1, 'America/New_York', 0, '2020-11-03 00:03:51', '127.0.0.1', 0, 'N38rRsVtuWfx77EkJzLE54CNmIXdHiJGxY2M3qHQhuLwk46roSHn1U4d0Dux', '2020-08-03 16:41:44', '2020-11-03 00:03:52', NULL),
(2, 1, 'Admin', 'Istator', 'admin@admin.com', 'admin', NULL, 'photo', NULL, '$2y$10$8AOTWzxUh3qK2ji7Y7E.Y.M/PC9p06heJWwJjZ2eHUZnQ4j1/UiTy', NULL, 1, '5b81902f59b14f18b943def25f90bf57', 1, NULL, 0, NULL, NULL, 0, 'jv06h2kXZWnVbW9UZQouygMjBM7cSzmhWE4XimXWvbxv9bYI9PL6Tc8bvFqJ', '2020-08-03 16:41:44', '2020-08-03 16:41:44', NULL),
(3, 1, 'Default', 'User', 'user@user.com', 'user', NULL, 'photo', NULL, '$2y$10$HNAJwDaVyBR5.aK9t.FWMuyhfnk3RMbEh8ZjWYh9UlHWSbitSsV9C', NULL, 1, '348f99993e519525907f257ebfbf7736', 1, NULL, 0, NULL, NULL, 0, NULL, '2020-08-03 16:41:44', '2020-08-03 16:41:44', NULL),
(5, 1, 'Rejaul Islam', 'Royel', 'rejaul.islam@transcombd.com', 'almas', NULL, 'photo', NULL, '$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL/cdyHSOdnbrDXc8O5cUh8kG', NULL, 1, '29b575c492a20aafaad3a0aa89807a5f', 0, NULL, 0, NULL, NULL, 0, NULL, '2020-08-03 18:44:21', '2020-09-07 22:34:45', NULL),
(6, 1, 'AIC', 'BOG', 'aicbog@tdcl.transcombd.com', 'AICBOG', NULL, 'photo', NULL, '$2y$10$QQ0YulniXHfB/4nQNmcl2uYUsHElPd9wPuEU.4UXaCK7VvhLBuBqG', NULL, 1, '80369097ebdd7d606a5bbf629cbf2565', 1, 'America/New_York', 0, '2020-11-02 21:22:55', '127.0.0.1', 0, 'DLAI8KM1k2yHiSiQj6SNr12SeoesuiyByqJOEhYGd6xGloK6iknAUvuu1E2V', '2020-08-05 22:41:05', '2020-11-02 21:22:56', NULL),
(7, 1, 'Bashir', 'Uddin', 'bashir@bashir.com', 'bashir', NULL, 'photo', NULL, '$2y$10$XFe6dqOEiYVO0QLk9O8oqOxEOD50/uGlOVaa1fA9ruzdbqvokPwyK', NULL, 1, '7aa9cdfb31ece02418f281eaf4d86869', 0, NULL, 0, NULL, NULL, 0, NULL, '2020-08-15 16:47:42', '2020-08-15 16:47:42', NULL),
(8, 1, 'john1', 'due', 'john@gmail.com', 'john', NULL, 'photo', NULL, '$2y$10$5Wmsnn/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6', NULL, 1, '3e09600c3bec3dde2016d82ce64d0499', 1, NULL, 0, NULL, NULL, 0, NULL, '2020-09-08 02:12:52', '2020-09-08 02:21:51', '2020-09-08 02:21:51'),
(10, 1, 'AIC', 'MOT', 'aicmot@tdcl.transcombd.com', 'AICMOT', NULL, 'photo', NULL, '$2y$10$5cbxn/Fm2xZpjySuXdZOWuzaoBHCnl2wy9oPwpFdXQd/9cVy.zZee', NULL, 1, '1fa4475ffdf9ce336b3418c9fa432cc8', 1, 'America/New_York', 0, '2020-11-02 23:11:38', '127.0.0.1', 0, 'VwGkiKPWi7FQqJm84O6KP8hqAA706764jLnrbFjotUQ0fEPpjurYSWgfOHjH', '2020-10-10 22:25:36', '2020-11-02 23:11:38', NULL),
(11, 1, 'MYM', 'AIC', 'aicmym@tdcl.transcombd.com', 'AICMOM', NULL, 'photo', NULL, '$2y$10$bT8rJ.co/OUmgB8yTGnkU.z8UpBzmAAOtNaZXey9kup.BSdZIOpPC', NULL, 1, '8c1d0e3449d59bbac9259f85525763d2', 1, NULL, 0, NULL, NULL, 0, '2pfmwNbXGY1yFiOdBeh3jMkLWoeSGFakOveITRaW2l441sVNNps2dvR1woO4', '2020-11-04 04:00:16', '2020-11-04 05:38:51', NULL),
(12, 1, 'Saiful', 'Islam', 'saiful.islam@transcombd.com', 'saiful', NULL, 'photo', NULL, '$2y$10$P8LaVa9JfIok4Bc/kgkqWuP2Bubkpkxl/RauFx4vWk9dMUXwkZD4.', NULL, 1, '83ec5c2a94a8ecbfc0df31c70670f87a', 1, NULL, 0, NULL, NULL, 0, '62zAEGHZNuDXHeHEIphfH5RRc5K3M8Nma4862qqGzOfVdZCXHmLXwPrXoanH', '2020-11-18 04:29:44', '2020-11-18 04:29:44', NULL),
(13, 1, 'Din', 'Mohammad', 'din.mohammad@transcombd.com', 'sajib', NULL, 'photo', NULL, '$2y$10$xVkThvbHS3x4fpoW.C65vu02qQB.5F/DoGbJJR/CUrmsSO7mM7XDS', NULL, 1, '3ce448bd789ea0a603f083afa4f6087a', 0, NULL, 0, NULL, NULL, 0, NULL, '2020-11-18 05:19:15', '2020-11-18 05:19:15', NULL),
(14, 1, 'Hamza Bin', 'Habib', 'hamza.habib@transcombd.com', 'Hamza', NULL, 'photo', NULL, '$2y$10$zeOv35sEDls2/X8ZDlO7SOd.aLLfN8a4HC/JUxuSYOqilQ6DcnUG6', NULL, 1, '52dd55e5af7bd1e1f3851da03a2a554b', 1, NULL, 0, NULL, NULL, 0, 'azrwx09AJ34mAKpZ44FTrlhrxjdWYYwm8RNUVLLTY9BgzFmOwYVR3tThDpwV', '2020-11-18 11:18:53', '2020-11-18 11:18:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_branches`
--

CREATE TABLE `user_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_branches`
--

INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`) VALUES
(1, 5, 2),
(2, 5, 3),
(3, 5, 5),
(4, 6, 21),
(5, 7, 2),
(6, 7, 3),
(7, 7, 4),
(8, 7, 5),
(9, 7, 6),
(10, 7, 7),
(11, 7, 8),
(12, 7, 9),
(13, 7, 10),
(14, 7, 11),
(15, 7, 12),
(16, 7, 13),
(17, 7, 14),
(18, 7, 15),
(19, 7, 16),
(20, 7, 17),
(21, 7, 18),
(22, 7, 19),
(23, 7, 20),
(24, 7, 21),
(25, 7, 22),
(26, 7, 23),
(27, 7, 24),
(28, 7, 25),
(29, 7, 26),
(30, 7, 27),
(31, 7, 28),
(32, 7, 29),
(33, 7, 30),
(34, 7, 31),
(35, 7, 32),
(36, 8, 2),
(37, 10, 2),
(38, 11, 6),
(39, 12, 4),
(40, 13, 33);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `vehicle_type` tinyint(4) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manufacturer_year` int(11) NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lifetime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_plate_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_year` int(11) NOT NULL,
  `is_dual_tank` tinyint(1) NOT NULL DEFAULT 0,
  `main_tank_fuel_id` int(11) NOT NULL,
  `main_tank_fuel_capacity` int(11) NOT NULL,
  `second_tank_fuel_id` int(11) DEFAULT NULL,
  `second_tank_fuel_capacity` int(11) DEFAULT NULL,
  `chassis_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `engine_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vin_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Vehicle identification no',
  `opening` decimal(10,4) NOT NULL,
  `purchase_date` date DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `company_id`, `vehicle_type`, `name`, `manufacturer`, `model`, `manufacturer_year`, `weight`, `lifetime`, `license_plate_no`, `license_year`, `is_dual_tank`, `main_tank_fuel_id`, `main_tank_fuel_capacity`, `second_tank_fuel_id`, `second_tank_fuel_capacity`, `chassis_no`, `engine_no`, `vin_no`, `opening`, `purchase_date`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 4, 'Semi Truck', 'Runner', 'T51', 2019, '120 pound', NULL, 'DHAKA-METRO-HA-244855', 2017, 1, 1, 50, 4, 25, 'LEZZWAAADDTTTAAS', 'E11AAATT1', NULL, '12980.0000', '2017-11-08', 1, NULL, NULL, NULL, 1, 1, 12, NULL, '2020-08-04 18:30:19', '2020-11-22 05:58:02', NULL),
(3, 1, 4, 'Semi Van', 'Runner', 'H51', 2009, NULL, NULL, 'MTL-DM-MA-51-3755', 2019, 0, 1, 9, 4, 5, 'LEZZWAAADDTTTAAS1', 'E11AAATT12', NULL, '11590.0000', '2017-08-01', 2, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-05 22:16:07', '2020-08-05 22:16:07', NULL),
(6, 1, 3, 'Freezer Truck', 'Ace', 'AW22', 2017, '120 pound', NULL, 'DHAKA-METRO-HA-124522', 2017, 0, 1, 12, 4, 15, 'LEZZWAAADDTTTAAS11', 'E11AAATT121', NULL, '14123.0000', '2019-08-01', 3, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-08 23:23:50', '2020-08-08 23:23:50', NULL),
(7, 1, 3, 'Semi Freezar Van Van', 'Toyota', 'AW22', 2009, '120 pound', '12', 'DHAKA-METRO-HA-124522', 2019, 0, 3, 35, 4, 34, 'LEZZWAAADDTTTAAS11', 'E11AAATT121', NULL, '12000.0000', '2020-08-15', 4, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-15 17:09:19', '2020-08-15 17:09:19', NULL),
(8, 1, 3, 'Demo Truck', 'Ace', 'AW33', 2018, '130 pound', NULL, 'DHAKA-METRO-KA-124987', 2018, 0, 2, 14, 1, 13, 'LEZZ11111111112222222', 'E11AAATT111', NULL, '10970.0000', '2020-11-02', 5, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-11-04 05:04:56', '2020-11-04 05:04:56', NULL),
(9, 1, 3, 'Toyota', 'Toyota', 'TYTA-003', 1988, '102', NULL, 'DHK-Kha-20', 2019, 1, 1, 50, 3, 60, 'TYTA-1099', 'TYTA-2323', 'TYTA-002', '20000.0000', '2011-01-01', 6, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 05:55:29', '2020-11-22 05:55:29', NULL),
(10, 1, 4, '1_Demo Frezer Van', 'TATA', 'TATA-234', 2000, NULL, NULL, '123', 2019, 1, 2, 60, 4, 100, 'TATA-2345', 'TATA-253', 'TATA-4343', '12000.0000', '2020-11-21', 7, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 08:39:45', '2020-11-22 08:39:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_documents`
--

CREATE TABLE `vehicle_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `document_type` int(11) NOT NULL,
  `document_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `issuing_authority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_documents`
--

INSERT INTO `vehicle_documents` (`id`, `vehicle_id`, `document_type`, `document_no`, `issue_date`, `expiry_date`, `issuing_authority`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 3, 'aaaaa', '2020-08-05', '2020-08-05', 'BRTA', NULL, NULL, NULL, NULL, 0, 1, 1, NULL, '2020-08-04 18:51:58', '2020-08-08 23:51:50', NULL),
(2, 7, 3, '1213123', '2020-08-16', '2020-08-16', '1231', 2, NULL, NULL, NULL, 0, 1, NULL, NULL, '2020-08-15 17:09:19', '2020-08-15 17:09:19', NULL),
(3, 7, 4, '1qwqe', '2020-08-15', '2020-08-15', 'qweqwe', 3, NULL, NULL, NULL, 0, 1, NULL, NULL, '2020-08-15 17:09:19', '2020-08-15 17:09:19', NULL),
(4, 7, 5, '1213', '2020-08-14', '2020-08-16', 'qweqe', 4, NULL, NULL, NULL, 0, 1, NULL, NULL, '2020-08-15 17:09:19', '2020-08-15 17:09:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_drivers`
--

CREATE TABLE `vehicle_drivers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `take_over` date NOT NULL,
  `hand_over` date DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_drivers`
--

INSERT INTO `vehicle_drivers` (`id`, `company_id`, `vehicle_id`, `branch_id`, `driver_id`, `take_over`, `hand_over`, `order`, `custom1`, `custom2`, `custom3`, `archived`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 2, 4, '2020-08-05', '2020-11-18', 8, NULL, NULL, NULL, 0, 0, 1, 1, NULL, '2020-08-04 19:48:17', '2020-11-18 04:47:15', NULL),
(2, 1, 3, 2, 4, '2020-08-06', NULL, 2, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-08-05 22:16:33', '2020-08-05 22:16:33', NULL),
(3, 1, 3, 2, 1, '2020-08-06', NULL, 3, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-08-05 22:17:48', '2020-08-05 22:17:48', NULL),
(4, 1, 3, 2, 5, '2020-11-03', NULL, 4, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-11-03 11:45:59', '2020-11-03 11:45:59', NULL),
(5, 1, 2, 6, 1, '2020-11-04', NULL, 5, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-11-04 04:15:33', '2020-11-04 04:15:33', NULL),
(6, 1, 8, 6, 1, '2020-11-03', NULL, 6, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-11-04 05:10:21', '2020-11-04 05:10:21', NULL),
(7, 1, 7, 2, 1, '2020-11-01', NULL, 7, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-11-18 04:46:46', '2020-11-18 04:46:46', NULL),
(8, 1, 6, 2, 1, '2020-11-18', NULL, 8, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-11-18 04:47:39', '2020-11-18 04:47:39', NULL),
(9, 1, 6, 1, 9, '2020-11-01', NULL, 9, NULL, NULL, NULL, 0, 1, 12, NULL, NULL, '2020-11-18 05:55:47', '2020-11-18 05:55:47', NULL),
(10, 1, 2, 3, 1, '2020-11-22', NULL, 10, NULL, NULL, NULL, 0, 1, 12, NULL, NULL, '2020-11-22 07:43:47', '2020-11-22 07:43:47', NULL),
(11, 1, 2, 3, 1, '2020-05-08', '2020-11-22', 11, NULL, NULL, NULL, 1, 1, 12, NULL, NULL, '2020-11-22 08:57:12', '2020-11-22 08:57:12', NULL),
(12, 1, 6, 1, 9, '2020-11-01', '2020-11-21', 12, NULL, NULL, NULL, 1, 1, 12, NULL, NULL, '2020-11-22 09:02:32', '2020-11-22 09:02:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_histories`
--

CREATE TABLE `vehicle_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `fuel_id` int(11) NOT NULL,
  `tanks` tinyint(4) NOT NULL COMMENT 'number of fuel tanks',
  `tank_type` tinyint(4) NOT NULL COMMENT '1=main or 2=secondary tank',
  `capacity` decimal(10,4) NOT NULL COMMENT 'fuel capacity',
  `millage` decimal(10,4) NOT NULL COMMENT 'standard millage',
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_histories`
--

INSERT INTO `vehicle_histories` (`id`, `company_id`, `entry_date`, `vehicle_id`, `fuel_id`, `tanks`, `tank_type`, `capacity`, `millage`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2020-08-09', 6, 1, 1, 1, '12.0000', '9.0000', 1, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-08 23:23:50', '2020-08-08 23:23:50', NULL),
(2, 1, '2020-08-09', 6, 4, 1, 2, '15.0000', '12.0000', 2, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-08 23:23:50', '2020-08-08 23:23:50', NULL),
(3, 1, '2020-08-09', 2, 1, 1, 1, '9.0000', '12.0000', 3, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-08 23:51:50', '2020-08-08 23:51:50', NULL),
(4, 1, '2020-08-13', 7, 3, 1, 1, '35.0000', '9.0000', 4, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-15 17:09:19', '2020-08-15 17:09:19', NULL),
(5, 1, '2020-08-01', 2, 1, 1, 1, '50.0000', '15.0000', 5, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-15 17:33:15', '2020-08-15 17:33:15', NULL),
(6, 1, '2020-10-11', 2, 1, 1, 1, '50.0000', '9.0000', 6, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-10-10 22:20:14', '2020-10-10 22:20:14', NULL),
(7, 1, '2020-10-17', 2, 1, 1, 1, '50.0000', '9.0000', 7, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-10-17 01:14:42', '2020-10-17 01:14:42', NULL),
(8, 1, '2020-10-17', 2, 1, 1, 1, '50.0000', '9.0000', 8, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-10-17 01:15:02', '2020-10-17 01:15:02', NULL),
(9, 1, '2020-11-04', 8, 2, 1, 1, '14.0000', '5.0000', 9, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-11-04 05:04:56', '2020-11-04 05:04:56', NULL),
(10, 1, '2020-11-22', 9, 3, 2, 2, '60.0000', '55.0000', 10, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 05:55:30', '2020-11-22 05:55:30', NULL),
(11, 1, '2020-11-22', 9, 1, 2, 1, '50.0000', '45.0000', 11, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 05:55:30', '2020-11-22 05:55:30', NULL),
(12, 1, '2020-11-21', 2, 4, 2, 2, '25.0000', '45.0000', 12, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 05:58:02', '2020-11-22 05:58:02', NULL),
(13, 1, '2020-11-21', 2, 1, 2, 1, '50.0000', '55.0000', 13, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 05:58:02', '2020-11-22 05:58:02', NULL),
(14, 1, '2020-11-22', 10, 4, 2, 2, '100.0000', '60.0000', 14, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 08:39:45', '2020-11-22 08:39:45', NULL),
(15, 1, '2020-11-22', 10, 2, 2, 1, '60.0000', '50.0000', 15, NULL, NULL, NULL, 1, 12, NULL, NULL, '2020-11-22 08:39:45', '2020-11-22 08:39:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_types`
--

CREATE TABLE `vehicle_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_types`
--

INSERT INTO `vehicle_types` (`id`, `company_id`, `name`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Car', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'Motorcycle', 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
(3, 1, 'Truck', 3, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, 1, 'Van', 4, 1, 1, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branches_id_index` (`id`),
  ADD KEY `branches_created_by_index` (`created_by`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companies_id_index` (`id`),
  ADD KEY `companies_created_by_index` (`created_by`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_types_id_index` (`id`),
  ADD KEY `documents_types_company_id_index` (`company_id`),
  ADD KEY `documents_types_created_by_index` (`created_by`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_id_index` (`id`),
  ADD KEY `drivers_company_id_index` (`company_id`),
  ADD KEY `drivers_created_by_index` (`created_by`);

--
-- Indexes for table `driver_documents`
--
ALTER TABLE `driver_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driver_documents_id_index` (`id`),
  ADD KEY `driver_documents_driver_id_index` (`driver_id`),
  ADD KEY `driver_documents_created_by_index` (`created_by`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_id_index` (`id`),
  ADD KEY `employees_company_id_index` (`company_id`),
  ADD KEY `employees_created_by_index` (`created_by`);

--
-- Indexes for table `employee_designations`
--
ALTER TABLE `employee_designations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_designations_id_index` (`id`),
  ADD KEY `employee_designations_company_id_index` (`company_id`),
  ADD KEY `employee_designations_created_by_index` (`created_by`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fuels`
--
ALTER TABLE `fuels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fuels_id_index` (`id`),
  ADD KEY `fuels_company_id_index` (`company_id`),
  ADD KEY `fuels_created_by_index` (`created_by`);

--
-- Indexes for table `fuel_rates`
--
ALTER TABLE `fuel_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fuel_rates_id_index` (`id`),
  ADD KEY `fuel_rates_company_id_index` (`company_id`),
  ADD KEY `fuel_rates_fuel_id_index` (`fuel_id`),
  ADD KEY `fuel_rates_branch_id_index` (`branch_id`),
  ADD KEY `fuel_rates_created_by_index` (`created_by`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ledgers_recordable_type_recordable_id_index` (`recordable_type`,`recordable_id`),
  ADD KEY `ledgers_user_id_user_type_index` (`user_id`,`user_type`);

--
-- Indexes for table `logbook_entries`
--
ALTER TABLE `logbook_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logbook_entries_id_index` (`id`),
  ADD KEY `logbook_entries_company_id_index` (`company_id`),
  ADD KEY `logbook_entries_branch_id_index` (`branch_id`),
  ADD KEY `logbook_entries_employee_id_index` (`employee_id`),
  ADD KEY `logbook_entries_vehicle_id_index` (`vehicle_id`),
  ADD KEY `logbook_entries_driver_id_index` (`driver_id`),
  ADD KEY `logbook_entries_fuel_id_index` (`fuel_id`),
  ADD KEY `logbook_entries_created_by_index` (`created_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_histories`
--
ALTER TABLE `password_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_name_index` (`name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_transaction_settings`
--
ALTER TABLE `system_transaction_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_transaction_settings_user_id_index` (`user_id`),
  ADD KEY `system_transaction_settings_branch_id_index` (`branch_id`),
  ADD KEY `system_transaction_settings_created_by_index` (`created_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_company_id_index` (`company_id`),
  ADD KEY `users_is_logged_index` (`is_logged`);

--
-- Indexes for table `user_branches`
--
ALTER TABLE `user_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_branches_id_index` (`id`),
  ADD KEY `user_branches_user_id_index` (`user_id`),
  ADD KEY `user_branches_branch_id_index` (`branch_id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicles_id_index` (`id`),
  ADD KEY `vehicles_company_id_index` (`company_id`),
  ADD KEY `vehicles_created_by_index` (`created_by`);

--
-- Indexes for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_documents_id_index` (`id`),
  ADD KEY `vehicle_documents_vehicle_id_index` (`vehicle_id`),
  ADD KEY `vehicle_documents_document_type_index` (`document_type`),
  ADD KEY `vehicle_documents_created_by_index` (`created_by`);

--
-- Indexes for table `vehicle_drivers`
--
ALTER TABLE `vehicle_drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_drivers_company_id_index` (`company_id`),
  ADD KEY `vehicle_drivers_vehicle_id_index` (`vehicle_id`),
  ADD KEY `vehicle_drivers_branch_id_index` (`branch_id`),
  ADD KEY `vehicle_drivers_driver_id_index` (`driver_id`),
  ADD KEY `vehicle_drivers_created_by_index` (`created_by`);

--
-- Indexes for table `vehicle_histories`
--
ALTER TABLE `vehicle_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_history_id_index` (`id`),
  ADD KEY `vehicle_history_company_id_index` (`company_id`),
  ADD KEY `vehicle_history_vehicle_id_index` (`vehicle_id`),
  ADD KEY `vehicle_history_fuel_id_index` (`fuel_id`),
  ADD KEY `vehicle_history_created_by_index` (`created_by`);

--
-- Indexes for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_types_id_index` (`id`),
  ADD KEY `vehicle_types_company_id_index` (`company_id`),
  ADD KEY `vehicle_types_created_by_index` (`created_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=378;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `driver_documents`
--
ALTER TABLE `driver_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee_designations`
--
ALTER TABLE `employee_designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fuels`
--
ALTER TABLE `fuels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fuel_rates`
--
ALTER TABLE `fuel_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledgers`
--
ALTER TABLE `ledgers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=374;

--
-- AUTO_INCREMENT for table `logbook_entries`
--
ALTER TABLE `logbook_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_histories`
--
ALTER TABLE `password_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_transaction_settings`
--
ALTER TABLE `system_transaction_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1095;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_branches`
--
ALTER TABLE `user_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vehicle_drivers`
--
ALTER TABLE `vehicle_drivers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vehicle_histories`
--
ALTER TABLE `vehicle_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `vehicle_types`
--
ALTER TABLE `vehicle_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `password_histories`
--
ALTER TABLE `password_histories`
  ADD CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
