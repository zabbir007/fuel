<?php

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        Company::create([
            'code' => '100',
            'name' => 'Default Company',
            'short_name' => 'DFTCMP',
            'address' => 'Default Company Address',
            'logo' => 'logo.png',
            'email_suffix' => '@example.com',
            'time_zone' => 'Asia/Dhaka',
            'active' => true,
        ]);

        $this->enableForeignKeys();
    }
}
