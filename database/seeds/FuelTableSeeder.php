<?php

use Illuminate\Database\Seeder;

class FuelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fuels = array(
            array('id' => '1','company_id' => '1','name' => 'Deisel','short_name' => NULL,'order' => '1','custom1' => NULL,'custom2' => NULL,'custom3' => NULL,'active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '2','company_id' => '1','name' => 'Petrol','short_name' => NULL,'order' => '2','custom1' => NULL,'custom2' => NULL,'custom3' => NULL,'active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '3','company_id' => '1','name' => 'Octane','short_name' => NULL,'order' => '3','custom1' => NULL,'custom2' => NULL,'custom3' => NULL,'active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '4','company_id' => '1','name' => 'CNG','short_name' => NULL,'order' => '4','custom1' => NULL,'custom2' => NULL,'custom3' => NULL,'active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL)
        );

        DB::table('fuels')->insert($fuels);
    }
}
