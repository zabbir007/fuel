<?php

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserAccessTokenSeed
 */
class UserAccessTokenSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::all() as $user) {
            $user->createToken($user->username.' Personal Token');
        }
    }
}
