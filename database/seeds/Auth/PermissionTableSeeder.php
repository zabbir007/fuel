<?php

use Illuminate\Database\Seeder;


/**
 * Class PermissionTableSeeder
 */
class PermissionTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        $permissions = [
            [
                'id' => '2', 'name' => 'setup-user-deactivated', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '3', 'name' => 'setup-user-deleted', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '4', 'name' => 'setup-user-index', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '5', 'name' => 'setup-user-create', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '6', 'name' => 'setup-user-store', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '7', 'name' => 'setup-user-show', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '8', 'name' => 'setup-user-edit', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '9', 'name' => 'setup-user-mark', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '10', 'name' => 'setup-user-delete', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '11', 'name' => 'setup-user-delete-permanently', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '12', 'name' => 'setup-user-restore', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '13', 'name' => 'setup-user-login-as', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '14', 'name' => 'setup-user-clear-session', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '15', 'name' => 'setup-log-viewer-dashboard', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '16', 'name' => 'setup-log-viewer-logs', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '17', 'name' => 'setup-log-viewer-download', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '18', 'name' => 'setup-log-viewer-delete', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '19', 'name' => 'setup-role-index', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '20', 'name' => 'setup-role-create', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '21', 'name' => 'setup-role-store', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '22', 'name' => 'setup-role-edit', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '23', 'name' => 'setup-role-update', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '24', 'name' => 'setup-role-destroy', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '25', 'name' => 'setup-role-mark', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '26', 'name' => 'setup-role-view-deleted', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '27', 'name' => 'setup-role-view-deactivated', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '28', 'name' => 'setup-permission-index', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '29', 'name' => 'setup-permission-create', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '30', 'name' => 'setup-permission-store', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '31', 'name' => 'setup-permission-edit', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '32', 'name' => 'setup-permission-update', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '33', 'name' => 'setup-company-index', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '34', 'name' => 'setup-company-create', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '35', 'name' => 'setup-company-store', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '36', 'name' => 'setup-company-show', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '37', 'name' => 'setup-company-edit', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '38', 'name' => 'setup-company-update', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '39', 'name' => 'setup-company-delete', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '40', 'name' => 'setup-company-destroy', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '41', 'name' => 'setup-company-restore', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '42', 'name' => 'setup-company-mark', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '43', 'name' => 'setup-company-view-deactivated', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '44', 'name' => 'setup-company-view-deleted', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '45', 'name' => 'setup-api-clients', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '46', 'name' => 'setup-api-tokens', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '47', 'name' => 'setup-branch-index', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '48', 'name' => 'setup-branch-create', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '49', 'name' => 'setup-branch-store', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '50', 'name' => 'setup-branch-show', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '51', 'name' => 'setup-branch-edit', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '52', 'name' => 'setup-branch-update', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '53', 'name' => 'setup-branch-delete', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '54', 'name' => 'setup-branch-destroy', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '55', 'name' => 'setup-branch-restore', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '56', 'name' => 'setup-branch-mark', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '57', 'name' => 'setup-branch-view-deactivated', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ],
            [
                'id' => '58', 'name' => 'setup-branch-view-deleted', 'guard_name' => 'web', 'active' => '1',
                'created_at' => '2019-06-28 11:34:01', 'updated_at' => '2019-06-28 11:34:01'
            ]
        ];

        DB::table('permissions')->insert($permissions);

        $this->enableForeignKeys();
    }
}
