<?php

use Illuminate\Database\Seeder;

class VehicleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicle_types = array(
            array('id' => '1','company_id' => '1','name' => 'Car','order' => '1','active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '2','company_id' => '1','name' => 'Motorcycle','order' => '2','active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '3','company_id' => '1','name' => 'Truck','order' => '3','active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '4','company_id' => '1','name' => 'Van','order' => '4','active' => '1','created_by' => '1','updated_by' => NULL,'deleted_by' => NULL,'created_at' => NULL,'updated_at' => NULL,'deleted_at' => NULL)
        );

        DB::table('vehicle_types')->insert($vehicle_types);
    }
}
