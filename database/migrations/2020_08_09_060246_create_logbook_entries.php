<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogbookEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logbook_entries', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->integer('company_id')->index();
            $table->string('sl')->comment('transaction no');
            $table->date('trans_date');

            $table->integer('branch_id')->index();
            $table->string('branch_name');

            $table->integer('employee_id')->index()->comment('NDM id');
            $table->string('employee_name');

            $table->integer('vehicle_id')->index();
            $table->string('vehicle_name')->nullable();
            $table->string('vehicle_license_no');
            $table->integer('vehicle_mnf_yr')->comment('vehicle manufacturing year');
            $table->integer('vehicle_purchase_yr')->comment('vehicle purchase year');
            $table->integer('vehicle_lifetime')->comment('vehicle lifetime till date');
            $table->integer('vehicle_used')->comment('vehicle used (in days) as per action plan');

            $table->integer('driver_id')->index();
            $table->string('driver_name');
            $table->date('driver_take_ovr_dt')->comment('vehicle key take over date');
            $table->date('driver_hand_ovr_dt')->comment('vehicle key hand over date');

            $table->decimal('rkm_data', 10,4)->comment('RKM VTS Data (KM)');
            $table->decimal('logbook_opening', 10,4)->comment('KM of Logbook opening');
            $table->decimal('logbook_closing', 10,4)->comment('KM of Logbook closing');
            $table->decimal('logbook_running', 10,4)->comment('KM of total running');

            $table->integer('fuel_id')->index();
            $table->string('fuel_name', 15);
            $table->decimal('fuel_consumption', 10, 4);
            $table->decimal('fuel_rate', 10, 4);
            $table->decimal('fuel_cost', 10, 4);
            $table->decimal('std_fuel_consumption', 10, 4)->comment('standard_fuel_consumption');

            $table->boolean('approved')->default(0);
            $table->integer('approved_by')->nullable();

            $table->tinyInteger('status');
            $table->boolean('posted')->default(false);

            $table->integer('year');
            $table->integer('month');
            $table->string('month_name', 15);

            $table->integer('order');

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logbook_entries_summary');
    }
}
