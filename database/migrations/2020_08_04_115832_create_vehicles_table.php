<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();

            $table->integer('company_id')->index();

            $table->tinyInteger('vehicle_type');
            $table->string('name');
            $table->string('manufacturer');
            $table->string('model')->nullable();
            $table->integer('manufacturer_year');
            $table->string('weight')->nullable();
            $table->string('lifetime')->nullable();

            $table->string('license_plate_no');
            $table->integer('license_year');

            $table->boolean('is_dual_tank')->default(0);
            $table->integer('main_tank_fuel_id');
            $table->integer('main_tank_fuel_capacity');

            $table->integer('second_tank_fuel_id')->nullable();
            $table->integer('second_tank_fuel_capacity')->nullable();

            $table->string('chassis_no');
            $table->string('engine_no');
            $table->string('vin_no')->nullable()->comment('Vehicle identification no');

            $table->date('purchase_date')->nullable();

            $table->integer('order');

            $table->string('custom1')->nullable();
            $table->string('custom2')->nullable();
            $table->string('custom3')->nullable();

            $table->boolean('active')->default(1);

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
