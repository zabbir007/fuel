<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_histories', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();

            $table->integer('company_id')->index();
            $table->date('entry_date');
            $table->integer('vehicle_id')->index();
            $table->integer('fuel_id')->index();
            $table->tinyInteger('tanks')->comment('number of fuel tanks');
            $table->tinyInteger('tank_type')->comment('1=main or 2=secondary tank');

            $table->decimal('capacity', 10,4)->comment('fuel capacity');
            $table->decimal('millage', 10,4)->comment('standard millage');

            $table->integer('order');

            $table->string('custom1')->nullable();
            $table->string('custom2')->nullable();
            $table->string('custom3')->nullable();

            $table->boolean('active')->default(1);

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_history');
    }
}
