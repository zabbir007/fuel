<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_rates', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->integer('company_id')->index();

            $table->date('entry_date');

            $table->integer('fuel_id')->index();
            $table->integer('branch_id')->index();
            $table->decimal('rate', 10,6)->default(0);

            $table->integer('order');

            $table->boolean('archived')->default(0);
            $table->boolean('active')->default(1);

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_rates');
    }
}
