<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();

            $table->integer('company_id')->index();

            $table->string('employee_id')->nullable()->comment('driver payroll id');
            $table->string('name')->nullable();
            $table->string('mobile', 15)->nullable();

            $table->date('dob')->nullable();
            $table->date('doj')->nullable();
            $table->string('blood_group', 3)->nullable();
            $table->string('present_address')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('em_mobile', 15)->nullable()->comment('emergency mobile number');

            $table->string('photo')->nullable();

            $table->integer('order');

            $table->string('custom1')->nullable();
            $table->string('custom2')->nullable();
            $table->string('custom3')->nullable();

            $table->boolean('active')->default(1);

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
