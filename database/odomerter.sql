-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table odometer.activity_log
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) unsigned DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) unsigned DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`),
  KEY `subject` (`subject_id`,`subject_type`),
  KEY `causer` (`causer_id`,`causer_type`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.activity_log: ~171 rows (approximately)
DELETE FROM `activity_log`;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
	(1, 'system', 'created', 1, 'App\\Models\\User', NULL, NULL, '[]', '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(2, 'system', 'created', 2, 'App\\Models\\User', NULL, NULL, '[]', '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(3, 'system', 'created', 3, 'App\\Models\\User', NULL, NULL, '[]', '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(4, 'system', 'created', 1, 'App\\Models\\Company', NULL, NULL, '[]', '2020-08-03 22:41:45', '2020-08-03 22:41:45'),
	(5, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 22:41:58', '2020-08-03 22:41:58'),
	(6, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 22:41:59', '2020-08-03 22:41:59'),
	(7, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-03 22:41:59', '2020-08-03 22:41:59'),
	(8, 'system', 'updated', 1, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-08-03 23:24:24', '2020-08-03 23:24:24'),
	(10, 'system', 'created', 5, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 00:44:21', '2020-08-04 00:44:21'),
	(11, 'system', 'created', 1, 'App\\Models\\Employee', 1, 'App\\Models\\User', '[]', '2020-08-04 00:44:21', '2020-08-04 00:44:21'),
	(12, 'system', 'created', 1, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-04 02:07:06', '2020-08-04 02:07:06'),
	(13, 'system', 'updated', 1, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-04 02:12:57', '2020-08-04 02:12:57'),
	(14, 'system', 'updated', 1, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-04 02:14:09', '2020-08-04 02:14:09'),
	(17, 'system', 'created', 4, 'App\\Models\\Driver', 1, 'App\\Models\\User', '[]', '2020-08-04 04:36:10', '2020-08-04 04:36:10'),
	(18, 'system', 'created', 1, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 04:36:10', '2020-08-04 04:36:10'),
	(19, 'system', 'created', 2, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 04:36:10', '2020-08-04 04:36:10'),
	(20, 'system', 'created', 3, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 04:49:19', '2020-08-04 04:49:19'),
	(21, 'system', 'created', 4, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 04:49:19', '2020-08-04 04:49:19'),
	(22, 'system', 'updated', 1, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 04:55:38', '2020-08-04 04:55:38'),
	(23, 'system', 'updated', 2, 'App\\Models\\DriverDocument', 1, 'App\\Models\\User', '[]', '2020-08-04 04:55:38', '2020-08-04 04:55:38'),
	(24, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 22:33:57', '2020-08-04 22:33:57'),
	(25, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 22:34:03', '2020-08-04 22:34:03'),
	(26, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-04 22:34:03', '2020-08-04 22:34:03'),
	(28, 'system', 'created', 2, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-05 00:30:19', '2020-08-05 00:30:19'),
	(29, 'system', 'created', 1, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-05 00:51:58', '2020-08-05 00:51:58'),
	(30, 'system', 'updated', 1, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-08-05 03:17:02', '2020-08-05 03:17:02'),
	(31, 'system', 'created', 1, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(32, 'system', 'created', 2, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(33, 'system', 'created', 3, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(34, 'system', 'created', 4, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:28:25', '2020-08-05 22:28:25'),
	(35, 'system', 'created', 5, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(36, 'system', 'created', 6, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(37, 'system', 'created', 7, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(38, 'system', 'created', 8, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(47, 'system', 'updated', 5, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(48, 'system', 'updated', 6, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(49, 'system', 'updated', 7, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(50, 'system', 'updated', 8, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(51, 'system', 'created', 3, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-06 04:16:07', '2020-08-06 04:16:07'),
	(52, 'system', 'created', 2, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-08-06 04:16:33', '2020-08-06 04:16:33'),
	(53, 'system', 'created', 3, 'App\\Models\\VehicleDriver', 1, 'App\\Models\\User', '[]', '2020-08-06 04:17:48', '2020-08-06 04:17:48'),
	(54, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:36:23', '2020-08-06 04:36:23'),
	(55, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:36:29', '2020-08-06 04:36:29'),
	(56, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:37:42', '2020-08-06 04:37:42'),
	(57, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:37:51', '2020-08-06 04:37:51'),
	(58, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:37:51', '2020-08-06 04:37:51'),
	(59, 'system', 'created', 6, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:41:05', '2020-08-06 04:41:05'),
	(60, 'system', 'created', 59, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-06 04:43:48', '2020-08-06 04:43:48'),
	(61, 'system', 'created', 60, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-06 04:45:25', '2020-08-06 04:45:25'),
	(62, 'system', 'created', 61, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-06 04:45:35', '2020-08-06 04:45:35'),
	(63, 'system', 'created', 62, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-06 04:45:44', '2020-08-06 04:45:44'),
	(64, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-06 04:46:47', '2020-08-06 04:46:47'),
	(65, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-06 04:46:54', '2020-08-06 04:46:54'),
	(66, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-06 04:46:54', '2020-08-06 04:46:54'),
	(67, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-06 05:00:24', '2020-08-06 05:00:24'),
	(68, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-06 05:00:34', '2020-08-06 05:00:34'),
	(69, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-06 05:00:35', '2020-08-06 05:00:35'),
	(70, 'system', 'created', 9, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(71, 'system', 'created', 10, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(72, 'system', 'created', 11, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(73, 'system', 'created', 12, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(74, 'system', 'updated', 9, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(75, 'system', 'updated', 10, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(76, 'system', 'updated', 11, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(77, 'system', 'updated', 12, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(78, 'system', 'created', 13, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(79, 'system', 'created', 14, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(80, 'system', 'created', 15, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(81, 'system', 'created', 16, 'App\\Models\\FuelRate', 6, 'App\\Models\\User', '[]', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(82, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 23:52:58', '2020-08-07 23:52:58'),
	(83, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 23:53:11', '2020-08-07 23:53:11'),
	(84, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-07 23:53:11', '2020-08-07 23:53:11'),
	(85, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 00:43:03', '2020-08-08 00:43:03'),
	(86, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 00:43:11', '2020-08-08 00:43:11'),
	(87, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 00:43:11', '2020-08-08 00:43:11'),
	(88, 'system', 'created', 64, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-08 00:43:37', '2020-08-08 00:43:37'),
	(89, 'system', 'created', 65, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-08 00:43:51', '2020-08-08 00:43:51'),
	(90, 'system', 'created', 66, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-08 00:44:01', '2020-08-08 00:44:01'),
	(91, 'system', 'created', 67, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-08 00:44:10', '2020-08-08 00:44:10'),
	(92, 'system', 'created', 68, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-08 00:44:18', '2020-08-08 00:44:18'),
	(93, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 00:46:23', '2020-08-08 00:46:23'),
	(94, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 00:46:29', '2020-08-08 00:46:29'),
	(95, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 00:46:29', '2020-08-08 00:46:29'),
	(96, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 00:55:34', '2020-08-08 00:55:34'),
	(97, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 00:55:40', '2020-08-08 00:55:40'),
	(98, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 00:55:40', '2020-08-08 00:55:40'),
	(99, 'system', 'created', 69, 'App\\Models\\Permission', 1, 'App\\Models\\User', '[]', '2020-08-08 00:55:57', '2020-08-08 00:55:57'),
	(100, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-08 00:56:26', '2020-08-08 00:56:26'),
	(101, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 00:56:34', '2020-08-08 00:56:34'),
	(102, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-08 00:56:34', '2020-08-08 00:56:34'),
	(103, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-08-09 00:51:27', '2020-08-09 00:51:27'),
	(104, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-09 00:51:48', '2020-08-09 00:51:48'),
	(105, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-09 00:51:49', '2020-08-09 00:51:49'),
	(108, 'system', 'created', 6, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-09 05:23:50', '2020-08-09 05:23:50'),
	(109, 'system', 'created', 1, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-09 05:23:50', '2020-08-09 05:23:50'),
	(110, 'system', 'created', 2, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-09 05:23:50', '2020-08-09 05:23:50'),
	(111, 'system', 'updated', 1, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-09 05:51:50', '2020-08-09 05:51:50'),
	(112, 'system', 'created', 3, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-09 05:51:50', '2020-08-09 05:51:50'),
	(113, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-12 03:39:06', '2020-08-12 03:39:06'),
	(114, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-12 03:39:06', '2020-08-12 03:39:06'),
	(115, 'system', 'created', 7, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-08-15 22:47:42', '2020-08-15 22:47:42'),
	(116, 'system', 'created', 2, 'App\\Models\\Employee', 1, 'App\\Models\\User', '[]', '2020-08-15 22:47:42', '2020-08-15 22:47:42'),
	(117, 'system', 'created', 17, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(118, 'system', 'created', 18, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(119, 'system', 'created', 19, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(120, 'system', 'created', 20, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(121, 'system', 'updated', 17, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(122, 'system', 'updated', 18, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(123, 'system', 'updated', 19, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(124, 'system', 'updated', 20, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(125, 'system', 'created', 7, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(126, 'system', 'created', 2, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(127, 'system', 'created', 3, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(128, 'system', 'created', 4, 'App\\Models\\VehicleDocument', 1, 'App\\Models\\User', '[]', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(129, 'system', 'created', 4, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(132, 'system', 'created', 21, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(133, 'system', 'created', 22, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(134, 'system', 'created', 23, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(135, 'system', 'created', 24, 'App\\Models\\FuelRate', 1, 'App\\Models\\User', '[]', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(136, 'system', 'updated', 2, 'App\\Models\\Vehicle', 1, 'App\\Models\\User', '[]', '2020-08-15 23:33:15', '2020-08-15 23:33:15'),
	(137, 'system', 'created', 5, 'App\\Models\\VehicleHistory', 1, 'App\\Models\\User', '[]', '2020-08-15 23:33:15', '2020-08-15 23:33:15'),
	(138, 'system', 'created', 1, 'App\\Models\\LogbookEntry', 1, 'App\\Models\\User', '[]', '2020-08-15 23:42:22', '2020-08-15 23:42:22'),
	(139, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:33:03', '2020-09-08 04:33:03'),
	(140, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:33:03', '2020-09-08 04:33:03'),
	(141, 'system', 'updated', 5, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:34:45', '2020-09-08 04:34:45'),
	(142, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:34:54', '2020-09-08 04:34:54'),
	(143, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:34:59', '2020-09-08 04:34:59'),
	(144, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:34:59', '2020-09-08 04:34:59'),
	(145, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:35:33', '2020-09-08 04:35:33'),
	(146, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:35:37', '2020-09-08 04:35:37'),
	(147, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:35:37', '2020-09-08 04:35:37'),
	(148, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:38:07', '2020-09-08 04:38:07'),
	(149, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:38:11', '2020-09-08 04:38:11'),
	(150, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:38:11', '2020-09-08 04:38:11'),
	(151, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 04:38:13', '2020-09-08 04:38:13'),
	(152, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:38:19', '2020-09-08 04:38:19'),
	(153, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 04:38:19', '2020-09-08 04:38:19'),
	(154, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:09:27', '2020-09-08 08:09:27'),
	(155, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:09:27', '2020-09-08 08:09:27'),
	(156, 'system', 'created', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:12:52', '2020-09-08 08:12:52'),
	(157, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 08:14:09', '2020-09-08 08:14:09'),
	(158, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:14:11', '2020-09-08 08:14:11'),
	(159, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:14:11', '2020-09-08 08:14:11'),
	(160, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:14:16', '2020-09-08 08:14:16'),
	(161, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:14:16', '2020-09-08 08:14:16'),
	(162, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:14:20', '2020-09-08 08:14:20'),
	(163, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 08:14:34', '2020-09-08 08:14:34'),
	(164, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 08:14:35', '2020-09-08 08:14:35'),
	(165, 'system', 'created', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 08:17:54', '2020-09-08 08:17:54'),
	(166, 'system', 'updated', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:18:46', '2020-09-08 08:18:46'),
	(167, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 08:18:52', '2020-09-08 08:18:52'),
	(168, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 08:19:03', '2020-09-08 08:19:03'),
	(169, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 08:19:26', '2020-09-08 08:19:26'),
	(172, 'system', 'updated', 2, 'App\\Models\\Company', 1, 'App\\Models\\User', '[]', '2020-09-08 08:20:18', '2020-09-08 08:20:18'),
	(173, 'system', 'updated', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:21:09', '2020-09-08 08:21:09'),
	(174, 'system', 'updated', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:21:26', '2020-09-08 08:21:26'),
	(175, 'system', 'deleted', 8, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:21:51', '2020-09-08 08:21:51'),
	(176, 'system', 'created', 4, 'App\\Models\\Role', 1, 'App\\Models\\User', '[]', '2020-09-08 08:25:06', '2020-09-08 08:25:06'),
	(177, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 08:28:42', '2020-09-08 08:28:42'),
	(178, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:28:47', '2020-09-08 08:28:47'),
	(179, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 08:28:47', '2020-09-08 08:28:47'),
	(180, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 10:00:54', '2020-09-08 10:00:54'),
	(181, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 10:00:55', '2020-09-08 10:00:55'),
	(182, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-08 10:24:14', '2020-09-08 10:24:14'),
	(183, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 10:24:27', '2020-09-08 10:24:27'),
	(184, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-08 10:24:27', '2020-09-08 10:24:27'),
	(185, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-11 08:38:13', '2020-09-11 08:38:13'),
	(186, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-11 08:38:14', '2020-09-11 08:38:14'),
	(187, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 08:20:41', '2020-09-19 08:20:41'),
	(188, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 08:20:42', '2020-09-19 08:20:42'),
	(189, 'system', 'updated', 1, 'App\\Models\\User', 1, 'App\\Models\\User', '[]', '2020-09-19 08:21:17', '2020-09-19 08:21:17'),
	(190, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 08:21:34', '2020-09-19 08:21:34'),
	(191, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 08:21:34', '2020-09-19 08:21:34'),
	(192, 'system', 'updated', 6, 'App\\Models\\User', 6, 'App\\Models\\User', '[]', '2020-09-19 08:39:02', '2020-09-19 08:39:02');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;

-- Dumping structure for table odometer.branches
CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bic_id` int(11) DEFAULT NULL,
  `dbic_id` int(11) DEFAULT NULL,
  `aic_id` int(11) DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `branches_id_index` (`id`),
  KEY `branches_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.branches: ~32 rows (approximately)
DELETE FROM `branches`;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` (`id`, `code`, `name`, `short_name`, `address1`, `address2`, `address3`, `address4`, `email`, `location`, `bic_id`, `dbic_id`, `aic_id`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '000', 'Corporate', 'COR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(2, '110', 'Motijheel', 'MOT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(3, '115', 'Mirpur', 'MIR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(4, '120', 'Mohakhali', 'MOH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(5, '130', 'Narayangonj', 'NAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(6, '140', 'Mymensingh', 'MYM', 'MYM', NULL, NULL, NULL, 'mymensingh@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(7, '150', 'Tangail', 'TAN', 'TNG', NULL, NULL, NULL, 'tangail@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(8, '160', 'Gazipur', 'GAZ', 'GZP', NULL, NULL, NULL, 'gazipur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(9, '170', 'Savar', 'SAV', 'SVR', NULL, NULL, NULL, 'savar@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(10, '180', 'Bhairab', 'BHA', 'abc', NULL, NULL, NULL, 'b.baria@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(11, '190', 'Kishorgonj', 'KIS', 'KSR', NULL, NULL, NULL, 'kisoregonj@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(12, '280', 'Keranigonj', 'KER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(13, '210', 'Chattogram', 'CTGS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(14, '220', 'Cumilla', 'CUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(15, '230', 'Noakhali', 'NOA', 'NOK', NULL, NULL, NULL, 'noakhali@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(16, '240', 'Cox\'s bazar', 'COX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(17, '250', 'Chandpur', 'CHA', 'CHD', NULL, NULL, NULL, 'chandpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(18, '260', 'CTG-N', 'CTGN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(19, '270', 'Feni', 'FEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(20, '310', 'Rajshahi', 'RAJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(21, '320', 'Bogura', 'BOG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(22, '330', 'Rangpur', 'RAN', 'RNG', NULL, NULL, NULL, 'rangpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(23, '340', 'Dinajpur', 'DIN', 'DNJ', NULL, NULL, NULL, 'dinajpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(24, '350', 'Pabna', 'PAB', 'PBN', NULL, NULL, NULL, 'pabna@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(25, '410', 'Khulna', 'KHU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(26, '420', 'Faridpur', 'FAR', 'FRD', NULL, NULL, NULL, 'faridpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(27, '430', 'Kushtia', 'KUS', 'KUS', NULL, NULL, NULL, 'kushtia@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 113, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(28, '440', 'Jashore', 'JAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(29, '510', 'Barishal', 'BAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(30, '520', 'Patuakhali', 'PAT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(31, '610', 'Sylhet', 'SYL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL),
	(32, '620', 'Mowlovibazar', 'MOW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2020-08-04 05:13:57', '2020-08-04 05:14:29', NULL);
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;

-- Dumping structure for table odometer.cache
CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.cache: ~0 rows (approximately)
DELETE FROM `cache`;
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;

-- Dumping structure for table odometer.companies
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_suffix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Asia/Dhaka',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_id_index` (`id`),
  KEY `companies_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.companies: ~2 rows (approximately)
DELETE FROM `companies`;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `code`, `name`, `short_name`, `address`, `logo`, `email_suffix`, `time_zone`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '100', 'Transcom Distribution Company Limited', 'TDCL', 'Default Company Address', 'logo.png', '@tdcl.transcombd.com', 'Asia/Dhaka', 1, 1, 1, NULL, '2020-08-03 22:41:45', '2020-08-03 23:24:24', NULL),
	(2, '2222', 'brix', 'R soft', 'dhaka', 'hiclipart.com.png', 'great', 'Asia/Dhaka', 1, 1, 1, NULL, '2020-09-08 08:17:54', '2020-09-08 08:20:18', NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- Dumping structure for table odometer.document_types
CREATE TABLE IF NOT EXISTS `document_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=driver, 2=vehicle',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_types_id_index` (`id`),
  KEY `documents_types_company_id_index` (`company_id`),
  KEY `documents_types_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.document_types: ~5 rows (approximately)
DELETE FROM `document_types`;
/*!40000 ALTER TABLE `document_types` DISABLE KEYS */;
INSERT INTO `document_types` (`id`, `company_id`, `type`, `name`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 1, 'NID Card', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 1, 'Driving License', 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 2, 'Fitness Paper', 3, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 2, 'Insurance Paper', 4, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(5, 1, 2, 'Tax Token', 5, 1, 1, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `document_types` ENABLE KEYS */;

-- Dumping structure for table odometer.drivers
CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'driver payroll id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `blood_group` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `present_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'emergency mobile number',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drivers_id_index` (`id`),
  KEY `drivers_company_id_index` (`company_id`),
  KEY `drivers_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.drivers: ~2 rows (approximately)
DELETE FROM `drivers`;
/*!40000 ALTER TABLE `drivers` DISABLE KEYS */;
INSERT INTO `drivers` (`id`, `company_id`, `employee_id`, `name`, `mobile`, `dob`, `doj`, `blood_group`, `present_address`, `permanent_address`, `em_mobile`, `photo`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '1234567890', 'Hasan', '0172222222', NULL, NULL, 'A-', 'test present addresss', 'test parmanent address', '0172222222', NULL, 1, NULL, NULL, NULL, 1, 1, 1, NULL, '2020-08-04 02:07:06', '2020-08-04 02:14:09', NULL),
	(4, 1, '11111111111', 'Parvez', '0172222222', '2020-08-04', '2020-08-04', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-04 04:36:10', '2020-08-04 04:36:10', NULL);
/*!40000 ALTER TABLE `drivers` ENABLE KEYS */;

-- Dumping structure for table odometer.driver_documents
CREATE TABLE IF NOT EXISTS `driver_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `document_type` tinyint(4) NOT NULL COMMENT '1 = NID card, 2=license',
  `document_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `issuing_authority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `driver_documents_id_index` (`id`),
  KEY `driver_documents_driver_id_index` (`driver_id`),
  KEY `driver_documents_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.driver_documents: ~2 rows (approximately)
DELETE FROM `driver_documents`;
/*!40000 ALTER TABLE `driver_documents` DISABLE KEYS */;
INSERT INTO `driver_documents` (`id`, `driver_id`, `document_type`, `document_no`, `issue_date`, `expiry_date`, `issuing_authority`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 4, 2, 'AAAAAAefff', '2020-08-04', '2020-08-04', 'BRTA', NULL, NULL, NULL, NULL, 0, 1, 1, NULL, '2020-08-04 04:36:10', '2020-08-04 04:55:38', NULL),
	(2, 4, 1, '1000211', '2020-08-04', '2020-08-04', 'EC', NULL, NULL, NULL, NULL, 0, 1, 1, NULL, '2020-08-04 04:36:10', '2020-08-04 04:55:38', NULL);
/*!40000 ALTER TABLE `driver_documents` ENABLE KEYS */;

-- Dumping structure for table odometer.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation_id` int(10) unsigned NOT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employees_id_index` (`id`),
  KEY `employees_company_id_index` (`company_id`),
  KEY `employees_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.employees: ~2 rows (approximately)
DELETE FROM `employees`;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `company_id`, `user_id`, `employee_id`, `name`, `designation_id`, `grade`, `mobile_no`, `email`, `department`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 5, NULL, 'Almas Estiak', 1, '1', NULL, 'almas.estiak@transcombd.com', 'ISA', NULL, 1, 1, NULL, NULL, '2020-08-04 00:44:21', '2020-08-04 00:44:21', NULL),
	(2, 1, 7, '123456789', 'Bashir Uddin', 2, NULL, NULL, 'bashir@bashir.com', NULL, '2', 1, 1, NULL, NULL, '2020-08-15 22:47:42', '2020-08-15 22:47:42', NULL);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table odometer.employee_designations
CREATE TABLE IF NOT EXISTS `employee_designations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_designations_id_index` (`id`),
  KEY `employee_designations_company_id_index` (`company_id`),
  KEY `employee_designations_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.employee_designations: ~2 rows (approximately)
DELETE FROM `employee_designations`;
/*!40000 ALTER TABLE `employee_designations` DISABLE KEYS */;
INSERT INTO `employee_designations` (`id`, `company_id`, `name`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Jr. System Analyst', '1', 1, 1, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'NDM', '1', 1, 1, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `employee_designations` ENABLE KEYS */;

-- Dumping structure for table odometer.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table odometer.fuels
CREATE TABLE IF NOT EXISTS `fuels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuels_id_index` (`id`),
  KEY `fuels_company_id_index` (`company_id`),
  KEY `fuels_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.fuels: ~4 rows (approximately)
DELETE FROM `fuels`;
/*!40000 ALTER TABLE `fuels` DISABLE KEYS */;
INSERT INTO `fuels` (`id`, `company_id`, `name`, `short_name`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Deisel', NULL, 1, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'Petrol', NULL, 2, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 'Octane', NULL, 3, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 'CNG', NULL, 4, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `fuels` ENABLE KEYS */;

-- Dumping structure for table odometer.fuel_rates
CREATE TABLE IF NOT EXISTS `fuel_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `fuel_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `rate` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `order` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuel_rates_id_index` (`id`),
  KEY `fuel_rates_company_id_index` (`company_id`),
  KEY `fuel_rates_fuel_id_index` (`fuel_id`),
  KEY `fuel_rates_branch_id_index` (`branch_id`),
  KEY `fuel_rates_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.fuel_rates: ~24 rows (approximately)
DELETE FROM `fuel_rates`;
/*!40000 ALTER TABLE `fuel_rates` DISABLE KEYS */;
INSERT INTO `fuel_rates` (`id`, `company_id`, `entry_date`, `fuel_id`, `branch_id`, `rate`, `order`, `archived`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '2020-08-06', 1, 4, 65.000000, 1, 0, 1, 1, NULL, NULL, '2020-08-05 22:28:24', '2020-08-05 22:28:24', NULL),
	(2, 1, '2020-08-06', 2, 4, 86.000000, 2, 0, 1, 1, NULL, NULL, '2020-08-05 22:28:24', '2020-08-05 22:28:24', NULL),
	(3, 1, '2020-08-06', 3, 4, 89.000000, 3, 0, 1, 1, NULL, NULL, '2020-08-05 22:28:24', '2020-08-05 22:28:24', NULL),
	(4, 1, '2020-08-06', 4, 4, 43.000000, 4, 0, 1, 1, NULL, NULL, '2020-08-05 22:28:24', '2020-08-05 22:28:24', NULL),
	(5, 1, '2020-08-06', 1, 5, 65.000000, 5, 0, 1, 1, 1, NULL, '2020-08-05 22:56:06', '2020-08-05 23:58:30', NULL),
	(6, 1, '2020-08-06', 2, 5, 86.000000, 6, 0, 1, 1, 1, NULL, '2020-08-05 22:56:06', '2020-08-05 23:58:30', NULL),
	(7, 1, '2020-08-06', 3, 5, 89.000000, 7, 0, 1, 1, 1, NULL, '2020-08-05 22:56:06', '2020-08-05 23:58:30', NULL),
	(8, 1, '2020-08-06', 4, 5, 430.000000, 8, 0, 1, 1, 1, NULL, '2020-08-05 22:56:06', '2020-08-05 23:58:30', NULL),
	(9, 1, '2020-08-06', 1, 21, 0.000000, 1, 0, 1, 6, 6, NULL, '2020-08-06 05:18:58', '2020-08-06 05:25:22', NULL),
	(10, 1, '2020-08-06', 2, 21, 86.300000, 2, 0, 1, 6, 6, NULL, '2020-08-06 05:18:58', '2020-08-06 05:25:22', NULL),
	(11, 1, '2020-08-06', 3, 21, 89.300000, 3, 0, 1, 6, 6, NULL, '2020-08-06 05:18:58', '2020-08-06 05:25:22', NULL),
	(12, 1, '2020-08-06', 4, 21, 43.000000, 4, 0, 1, 6, 6, NULL, '2020-08-06 05:18:58', '2020-08-06 05:25:22', NULL),
	(13, 1, '2020-08-06', 1, 21, 0.000000, 5, 0, 1, 6, NULL, NULL, '2020-08-06 05:35:17', '2020-08-06 05:35:17', NULL),
	(14, 1, '2020-08-06', 2, 21, 89.000000, 6, 0, 1, 6, NULL, NULL, '2020-08-06 05:35:17', '2020-08-06 05:35:17', NULL),
	(15, 1, '2020-08-06', 3, 21, 84.000000, 7, 0, 1, 6, NULL, NULL, '2020-08-06 05:35:17', '2020-08-06 05:35:17', NULL),
	(16, 1, '2020-08-06', 4, 21, 60.000000, 8, 0, 1, 6, NULL, NULL, '2020-08-06 05:35:17', '2020-08-06 05:35:17', NULL),
	(17, 1, '2020-08-16', 1, 6, 86.000000, 17, 0, 1, 1, 1, NULL, '2020-08-15 22:57:25', '2020-08-15 22:57:32', NULL),
	(18, 1, '2020-08-16', 2, 6, 60.000000, 18, 0, 1, 1, 1, NULL, '2020-08-15 22:57:25', '2020-08-15 22:57:32', NULL),
	(19, 1, '2020-08-16', 3, 6, 65.000000, 19, 0, 1, 1, 1, NULL, '2020-08-15 22:57:25', '2020-08-15 22:57:32', NULL),
	(20, 1, '2020-08-16', 4, 6, 86.000000, 20, 0, 1, 1, 1, NULL, '2020-08-15 22:57:25', '2020-08-15 22:57:32', NULL),
	(21, 1, '2020-08-01', 1, 2, 86.000000, 21, 0, 1, 1, NULL, NULL, '2020-08-15 23:26:56', '2020-08-15 23:26:56', NULL),
	(22, 1, '2020-08-01', 2, 2, 65.000000, 22, 0, 1, 1, NULL, NULL, '2020-08-15 23:26:56', '2020-08-15 23:26:56', NULL),
	(23, 1, '2020-08-01', 3, 2, 60.000000, 23, 0, 1, 1, NULL, NULL, '2020-08-15 23:26:56', '2020-08-15 23:26:56', NULL),
	(24, 1, '2020-08-01', 4, 2, 75.000000, 24, 0, 1, 1, NULL, NULL, '2020-08-15 23:26:56', '2020-08-15 23:26:56', NULL);
/*!40000 ALTER TABLE `fuel_rates` ENABLE KEYS */;

-- Dumping structure for table odometer.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.jobs: ~0 rows (approximately)
DELETE FROM `jobs`;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table odometer.ledgers
CREATE TABLE IF NOT EXISTS `ledgers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `recordable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recordable_id` bigint(20) unsigned NOT NULL,
  `context` tinyint(3) unsigned NOT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pivot` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ledgers_recordable_type_recordable_id_index` (`recordable_type`,`recordable_id`),
  KEY `ledgers_user_id_user_type_index` (`user_id`,`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.ledgers: ~167 rows (approximately)
DELETE FROM `ledgers`;
/*!40000 ALTER TABLE `ledgers` DISABLE KEYS */;
INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$duQ\\/Luo3Fp\\/vyBSWVv62ruiQ692QTgy31p5slAREJcflK\\/je.NE\\/q","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":"zMVJrptTKQXe4ZeR51xAhX6WivIZK3KwxTKLFiSM8IpA1dXiRZz2H0CNFHjX","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-04 04:41:44","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9623856ca43f3a76f3413996de077db8ccfa6341b392bdd727aaee8a44564385b0fdf8d107b929b346355adf2dfa647247e2f58d72a4b09ae167bf26cbf824dd', '2020-08-03 22:41:58', '2020-08-03 22:41:58'),
	(2, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$duQ\\/Luo3Fp\\/vyBSWVv62ruiQ692QTgy31p5slAREJcflK\\/je.NE\\/q","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-04 04:41:58","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"zMVJrptTKQXe4ZeR51xAhX6WivIZK3KwxTKLFiSM8IpA1dXiRZz2H0CNFHjX","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-04 04:41:59","deleted_at":null}', '["timezone","last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '782b58b09bb5510dad366f855530dacf6af54d5d88e0495e57b1740e487c387987a22e10c99d7b803945d313f4f99c7128b5e372936390d6993f6580646ae54f', '2020-08-03 22:41:59', '2020-08-03 22:41:59'),
	(3, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-04 04:41:58","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"zMVJrptTKQXe4ZeR51xAhX6WivIZK3KwxTKLFiSM8IpA1dXiRZz2H0CNFHjX","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-04 04:41:59","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '832ab600dd47553c2a088efc8923254c6a8079973f7062f686b2af67412959bf4f1b6cfd98246dc2678a64490773aacf24a8e24616a2a1b7c4f5d499c2b173ff', '2020-08-03 22:41:59', '2020-08-03 22:41:59'),
	(4, 'App\\Models\\User', 1, 'App\\Models\\Company', 1, 4, 'updated', '{"id":1,"code":"100","name":"Transcom Distribution Company Limited","short_name":"TDCL","address":"Default Company Address","logo":"logo.png","email_suffix":"@tdcl.transcombd.com","time_zone":"Asia\\/Dhaka","active":true,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-04 04:41:45","updated_at":"2020-08-04 05:24:24","deleted_at":null}', '["name","short_name","email_suffix","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/company/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '3ff1507901e614510eba634fa268f57674fba167135b0c9dea5224b862cea7e083d494b30005b35ab29d70e091d3a3658cab8b2592013bd925d425b7dfdf38e6', '2020-08-03 23:24:24', '2020-08-03 23:24:24'),
	(6, 'App\\Models\\User', 1, 'App\\Models\\User', 5, 4, 'created', '{"first_name":"Almas","last_name":"Estiak","email":"almas.estiak@transcombd.com","username":"almas","password":"$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL\\/cdyHSOdnbrDXc8O5cUh8kG","active":true,"confirmation_code":"29b575c492a20aafaad3a0aa89807a5f","confirmed":false,"updated_at":"2020-08-04 06:44:21","created_at":"2020-08-04 06:44:21","id":5}', '["first_name","last_name","email","username","password","active","confirmation_code","confirmed","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/employee', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ced7d25ec6d0b943e829041fdc4d8e54afe5142378bc9c796d956c491781388613776a7067aa933424debb344195060ae8dd4e7b69b1e629fc0d63b63bbc5b82', '2020-08-04 00:44:21', '2020-08-04 00:44:21'),
	(7, 'App\\Models\\User', 1, 'App\\Models\\Employee', 1, 4, 'created', '{"company_id":"1","user_id":5,"employee_id":null,"name":"Almas Estiak","designation":"Jr. System Analyst","department":"ISA","grade":"1","email":"almas.estiak@transcombd.com","order":null,"active":true,"created_by":1,"updated_at":"2020-08-04 06:44:21","created_at":"2020-08-04 06:44:21","id":1}', '["company_id","user_id","employee_id","name","designation","department","grade","email","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/employee', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '08e0821a9e5da144e63ba70bc32376e6a981cc1d9b294936e97183cca15c8fe868d23239be565da02bb9e8c8f11406382b79d2cc795a1ac861cf53af64019fda', '2020-08-04 00:44:21', '2020-08-04 00:44:21'),
	(8, 'App\\Models\\User', 1, 'App\\Models\\Driver', 1, 4, 'created', '{"company_id":"1","employee_id":"1234567890","name":"Hasan","mobile":"0172222222","dob":null,"doj":null,"blood_group":"A-","em_mobile":"0172222222","present_address":"test present addresss","permanent_address":"test parmanent address","photo":"bcb319b817317a6416f9f726bc96747f.jpg","order":1,"active":true,"created_by":1,"updated_at":"2020-08-04 08:07:06","created_at":"2020-08-04 08:07:06","id":1}', '["company_id","employee_id","name","mobile","dob","doj","blood_group","em_mobile","present_address","permanent_address","photo","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'bbe479ea25379918eb837899e61cde9cd3f46228da4a383eaf2e8eb2d0f91074e8cf178208158bfae76768367129f36dcc63a157c14fd2e24938dad95ef1c80f', '2020-08-04 02:07:06', '2020-08-04 02:07:06'),
	(9, 'App\\Models\\User', 1, 'App\\Models\\Driver', 1, 4, 'updated', '{"id":1,"company_id":"1","employee_id":null,"name":"Hasan","mobile":"0172222222","dob":null,"doj":null,"blood_group":"A-","present_address":"test present addresss","permanent_address":"test parmanent address","em_mobile":"0172222222","photo":null,"order":"1","custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-04 08:07:06","updated_at":"2020-08-04 08:12:57","deleted_at":null}', '["employee_id","photo","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5ff6fbb93a33c9cc22ca19a20fdc158405e12bfcb790ec6f6f77add006698f2f3565a1f7108b18468a88e67a8991c839bb7ed54fff80bda52a42248006bab6de', '2020-08-04 02:12:57', '2020-08-04 02:12:57'),
	(10, 'App\\Models\\User', 1, 'App\\Models\\Driver', 1, 4, 'updated', '{"id":1,"company_id":"1","employee_id":"1234567890","name":"Hasan","mobile":"0172222222","dob":null,"doj":null,"blood_group":"A-","present_address":"test present addresss","permanent_address":"test parmanent address","em_mobile":"0172222222","photo":null,"order":"1","custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-04 08:07:06","updated_at":"2020-08-04 08:14:09","deleted_at":null}', '["employee_id","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '7f8647f09b9b4d8575a954de31f5592c7e93b7d7237dedb3f74d94ddc884fa3cb2dc8a9adb6142defe854673adaaf38f95e158094b787245c2fec08bae1d4fd1', '2020-08-04 02:14:09', '2020-08-04 02:14:09'),
	(13, 'App\\Models\\User', 1, 'App\\Models\\Driver', 4, 4, 'created', '{"company_id":"1","employee_id":"11111111111","name":"Parvez","mobile":"0172222222","dob":"2020-08-04","doj":"2020-08-04","blood_group":null,"em_mobile":null,"present_address":null,"permanent_address":null,"photo":null,"order":2,"active":true,"created_by":1,"updated_at":"2020-08-04 10:36:10","created_at":"2020-08-04 10:36:10","id":4}', '["company_id","employee_id","name","mobile","dob","doj","blood_group","em_mobile","present_address","permanent_address","photo","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a1aeef4fccdbfd0b2848ea93ad098de70b330e9cb611d6514ac04f6b82d06f81dcd197e9ee09416b9be2e7b94621ee1b1c06815fc3a71b938460ac0fcc307201', '2020-08-04 04:36:10', '2020-08-04 04:36:10'),
	(14, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 1, 4, 'created', '{"driver_id":4,"document_type":"2","document_no":"AAAAAAe","issue_date":"2020-08-04","expiry_date":"2020-08-04","issuing_authority":"BRTA","order":1,"active":false,"created_by":1,"updated_at":"2020-08-04 10:36:10","created_at":"2020-08-04 10:36:10","id":1}', '["driver_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '560cc306cad11ad869691e1ae99856321c3363cc3630dfd1222f09142902b6853c4ec37e6ede369c8d3d294464f6c967aa4e6a550e27f311a4249334ee4b0d14', '2020-08-04 04:36:10', '2020-08-04 04:36:10'),
	(15, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 2, 4, 'created', '{"driver_id":4,"document_type":"1","document_no":"10002","issue_date":"2020-08-04","expiry_date":"2020-08-04","issuing_authority":"EC","order":2,"active":false,"created_by":1,"updated_at":"2020-08-04 10:36:10","created_at":"2020-08-04 10:36:10","id":2}', '["driver_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '06f1e5725dbbf840e2ce87333eed9f908bb4675bccbd92a26ffa84ccd2c49d318cb97d77d8bb3dd11af3baff35599c349951e25b9252124cd10743233a1f699c', '2020-08-04 04:36:10', '2020-08-04 04:36:10'),
	(16, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 3, 4, 'created', '{"driver_id":4,"document_type":"2","document_no":"AAAAAAeff","issue_date":"2020-08-04","expiry_date":"2020-08-04","issuing_authority":"BRTA","order":3,"active":false,"created_by":1,"updated_at":"2020-08-04 10:49:19","created_at":"2020-08-04 10:49:19","id":3}', '["driver_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '617ef270a5b9ab4eb04a0bdcb87d329016a5d7a8cbd0bd7457aa937636125909fc62bfc92db710ce4867286b38c89cd89f47267ba5a5615b00dcad2f068b24b7', '2020-08-04 04:49:19', '2020-08-04 04:49:19'),
	(17, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 4, 4, 'created', '{"driver_id":4,"document_type":"1","document_no":"10002q","issue_date":"2020-08-04","expiry_date":"2020-08-04","issuing_authority":"EC","order":4,"active":false,"created_by":1,"updated_at":"2020-08-04 10:49:19","created_at":"2020-08-04 10:49:19","id":4}', '["driver_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'bd311d5c66cf888040c42ae194c4229a4a49fa9d038e44d695668280f2881d890df121759695555624f457deb83d24aec3fb9c29ce5dc789672d81d52260d850', '2020-08-04 04:49:19', '2020-08-04 04:49:19'),
	(18, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 1, 4, 'updated', '{"id":1,"driver_id":4,"document_type":"2","document_no":"AAAAAAefff","issue_date":"2020-08-04","expiry_date":"2020-08-04","issuing_authority":"BRTA","order":null,"custom1":null,"custom2":null,"custom3":null,"active":false,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-04 10:36:10","updated_at":"2020-08-04 10:55:38","deleted_at":null}', '["document_no","order","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c9b8b221307efcda7faa45f25e3c35633240c03de8a3551fd6131d5627423aba179a89bb0d5bd60c9232ffdf240d34968b9860de98baad47dee0ac5cb904b7b7', '2020-08-04 04:55:38', '2020-08-04 04:55:38'),
	(19, 'App\\Models\\User', 1, 'App\\Models\\DriverDocument', 2, 4, 'updated', '{"id":2,"driver_id":4,"document_type":"1","document_no":"1000211","issue_date":"2020-08-04","expiry_date":"2020-08-04","issuing_authority":"EC","order":null,"custom1":null,"custom2":null,"custom3":null,"active":false,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-04 10:36:10","updated_at":"2020-08-04 10:55:38","deleted_at":null}', '["document_no","order","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/driver/4', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '96661135ce475f96543ac43511ae055f8be42a2cd6ab746a5dfa74392a959592af79cde0bf1c6c9a30785327dc465fc6ca7d6e1c587f91524062900190316ee8', '2020-08-04 04:55:38', '2020-08-04 04:55:38'),
	(20, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-04 04:41:58","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"nLbYNSMQgA7XkXpJzKq3m4n7mQLKUyIqTY3ni4g5cVl4cIGyb8l8SjBPOFkB","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-04 04:41:59","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'cfb705e2e7cacf93767fa628aeb9e785421fc722971119531d2177a5119410f834de3d66833c60ed9b099f5d6b1a38b134811005475fd9c8678eb2bde3579210', '2020-08-04 22:33:57', '2020-08-04 22:33:57'),
	(21, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-05 04:34:02","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"nLbYNSMQgA7XkXpJzKq3m4n7mQLKUyIqTY3ni4g5cVl4cIGyb8l8SjBPOFkB","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-05 04:34:02","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8c6294a7e69a0b6b55caf531da4333330114521691fea25b863b729b8ccd6f1a99beb27e99623ac9cce465cb139810dc385d7d6310e8d55950f7a06c125c1c17', '2020-08-04 22:34:02', '2020-08-04 22:34:02'),
	(22, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-05 04:34:02","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"nLbYNSMQgA7XkXpJzKq3m4n7mQLKUyIqTY3ni4g5cVl4cIGyb8l8SjBPOFkB","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-05 04:34:03","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9cd45ad4bf1e931ce02f50c84ea7ab3bdd9a44d317522b5de604d016b1da4290f62e68e7d5919fe74e879c1dafbbda81da09e01e4dffb2a2063c146017f36485', '2020-08-04 22:34:03', '2020-08-04 22:34:03'),
	(24, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 2, 4, 'created', '{"company_id":"1","vehicle_type":"1","name":"Semi Truck","manufacturer":"Runner","model":"T51","manufacturer_year":"2019","weight":"120 pound","lifetime":null,"chassis_no":"LEZZWAAADDTTTAAS","engine_no":"E11AAATT1","vin_no":null,"purchase_date":"2017-11-08","license_plate_no":"DHAKA-METRO-HA-244855","license_year":"2017","main_tank_fuel_id":"1","main_tank_fuel_capacity":"9","second_tank_fuel_id":"1","second_tank_fuel_capacity":null,"order":1,"custom1":null,"custom2":null,"custom3":null,"is_dual_tank":false,"active":true,"created_by":1,"updated_at":"2020-08-05 06:30:19","created_at":"2020-08-05 06:30:19","id":2}', '["company_id","vehicle_type","name","manufacturer","model","manufacturer_year","weight","lifetime","chassis_no","engine_no","vin_no","purchase_date","license_plate_no","license_year","main_tank_fuel_id","main_tank_fuel_capacity","second_tank_fuel_id","second_tank_fuel_capacity","order","custom1","custom2","custom3","is_dual_tank","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c8f1751db7febfacf2154168b1c6605592db81c9657e896d8afb2993c3c71205c5537994a8de6edd3b7d2ab0635b29f641b29e47abaaf6856ee4d9ea34bd40e6', '2020-08-05 00:30:19', '2020-08-05 00:30:19'),
	(25, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 1, 4, 'created', '{"vehicle_id":2,"document_type":"3","document_no":"aaaaa","issue_date":"2020-08-05","expiry_date":"2020-08-05","issuing_authority":"BRTA","order":1,"active":false,"created_by":1,"updated_at":"2020-08-05 06:51:58","created_at":"2020-08-05 06:51:58","id":1}', '["vehicle_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle/2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '676efae9e9bc0ffdfdd4c04d2bf6e55a920adde7c77058433c6a8e07e863d6f9536ddddb334767ca4632b9e61c82b1be25ebee72c9b0022aae53a16994f6e0a1', '2020-08-05 00:51:58', '2020-08-05 00:51:58'),
	(26, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 1, 4, 'updated', '{"id":1,"company_id":1,"vehicle_id":"2","branch_id":"2","driver_id":"4","take_over":"2020-08-05","hand_over":null,"order":2,"custom1":null,"custom2":null,"custom3":null,"archived":0,"active":false,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-05 07:48:17","updated_at":"2020-08-05 09:17:02","deleted_at":null}', '["order","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle_driver/1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '107e63d7bf7c88a42cfe3112440d4516647f36845617c888a1e071898870278adb2dc418cf28dfa9e62b9f24e10394c544e9cc1670c14b161136d4cbd16f810f', '2020-08-05 03:17:02', '2020-08-05 03:17:02'),
	(27, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 1, 4, 'created', '{"company_id":"1","branch_id":"4","entry_date":"2020-08-06","fuel_id":"1","rate":"65","order":1,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:28:24","created_at":"2020-08-06 04:28:24","id":1}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'e367dfeeaac5e2d588ab9cc80803f4f21567732bfb6a93730adc34484d479da223ad147f521881ca531a20674d30633f2c0ad29ee5a98cd8bb5ba1db4d870b8b', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(28, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 2, 4, 'created', '{"company_id":"1","branch_id":"4","entry_date":"2020-08-06","fuel_id":"2","rate":"86","order":2,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:28:24","created_at":"2020-08-06 04:28:24","id":2}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'b16c0a185c1e1ce3be8ccea09f52be8aa0c9d7914ff3b05ed7df127ed22196011dac1d649e0107db6fd2345237b1519104fd31f6caba19a18b5e7abda1af690d', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(29, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 3, 4, 'created', '{"company_id":"1","branch_id":"4","entry_date":"2020-08-06","fuel_id":"3","rate":"89","order":3,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:28:24","created_at":"2020-08-06 04:28:24","id":3}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '2f074f79841417cfc2cb900e2aebb4ef115aedbe536051697b34cf89aa601f261bd0c29af87170fafab183b01754d33a371da921634bae931c994aa4a041ba56', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(30, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 4, 4, 'created', '{"company_id":"1","branch_id":"4","entry_date":"2020-08-06","fuel_id":"4","rate":"43","order":4,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:28:24","created_at":"2020-08-06 04:28:24","id":4}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ce930738fe4e1ce55fa94d261bcd16e089f17eb135194a1ee7f75cd6af3f8485010429814b078378f57471741ede6740a54ccb03291a57ef871147b4473833d9', '2020-08-05 22:28:24', '2020-08-05 22:28:24'),
	(31, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 5, 4, 'created', '{"company_id":"1","branch_id":"5","entry_date":"2020-08-06","fuel_id":"1","rate":"65","order":5,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:56:06","created_at":"2020-08-06 04:56:06","id":5}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '047afdc6a68f8be472440d9737577e1cf25cf4892555eed573661672bceab5d49ee42b20bbcc340c26c563b564ef7c35142562fadd751149ad562213aaa22747', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(32, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 6, 4, 'created', '{"company_id":"1","branch_id":"5","entry_date":"2020-08-06","fuel_id":"2","rate":"86","order":6,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:56:06","created_at":"2020-08-06 04:56:06","id":6}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '86e2ecdbd5d1b9ed8fb688d4f96ee6b4b938fc617b3cca386b9122e4e75bcf12e581cc56e4fd31002acf87170d30506906ab8c6b10b5e2cd7eb2e690530b0ff7', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(33, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 7, 4, 'created', '{"company_id":"1","branch_id":"5","entry_date":"2020-08-06","fuel_id":"3","rate":"89","order":7,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:56:06","created_at":"2020-08-06 04:56:06","id":7}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '4fab3b85afab9e68f4b394798669ab7aa70f2765259eac3394b8bb99312b46fa26b8f7edfff80d3c8ae9c0f58470f9ca98776c7c9e41672c5f8fac96ac9b2502', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(34, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 8, 4, 'created', '{"company_id":"1","branch_id":"5","entry_date":"2020-08-06","fuel_id":"4","rate":"43","order":8,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 04:56:06","created_at":"2020-08-06 04:56:06","id":8}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8a9e020b6f29a0c57f32037c32a10eab6ee83854b3e96a72b06cba8fd47ca23241ef3edac8681be4dce27a2e9f79bf6fb3c26ed3dbac918e4ec6901f9dfd9152', '2020-08-05 22:56:06', '2020-08-05 22:56:06'),
	(43, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 5, 4, 'updated', '{"id":5,"company_id":1,"entry_date":"2020-08-06","fuel_id":"1","branch_id":5,"rate":"65.00","order":5,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-06 04:56:06","updated_at":"2020-08-06 05:58:30","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c492703b8e68ca9e61108dab645644339d5c1858ad02e2ff032a01ddad25aa73f001a9c49f2b83278ae949d129d737b08c16cfa3343b530a4c52cb95b89b76a6', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(44, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 6, 4, 'updated', '{"id":6,"company_id":1,"entry_date":"2020-08-06","fuel_id":"2","branch_id":5,"rate":"86.00","order":6,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-06 04:56:06","updated_at":"2020-08-06 05:58:30","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '69528175c43deed997f8b232e630de8413cb084fc7ca1951b7cdc531d556c1e6ce272b72d9e4129e90756e0a66a512a9c8185e2cb638c1215420871d0141d010', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(45, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 7, 4, 'updated', '{"id":7,"company_id":1,"entry_date":"2020-08-06","fuel_id":"3","branch_id":5,"rate":"89.00","order":7,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-06 04:56:06","updated_at":"2020-08-06 05:58:30","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '86a9c2273e63613ec835a4b3c35519104414d4c5d8c8eba28718e18e4f2277f3b0a3e9cc90a3cbf2599a00164fb51504b7bbb406167196c514974b84a4241d54', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(46, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 8, 4, 'updated', '{"id":8,"company_id":1,"entry_date":"2020-08-06","fuel_id":"4","branch_id":5,"rate":"430.00","order":8,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-06 04:56:06","updated_at":"2020-08-06 05:58:30","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=5&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '4a90b5493bd1007adc27619d76ebd6b3655cfc589f1d65304cf22575fdf9206b92d97be3e9fbfdf867b59c98fab4e8c635495321dc44e549e8991ebb02cfd84e', '2020-08-05 23:58:30', '2020-08-05 23:58:30'),
	(47, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 3, 4, 'created', '{"company_id":"1","vehicle_type":"4","name":"Semi Van","manufacturer":"Runner","model":"H51","manufacturer_year":"2009","weight":null,"lifetime":null,"chassis_no":"LEZZWAAADDTTTAAS1","engine_no":"E11AAATT12","vin_no":null,"purchase_date":"2017-08-01","license_plate_no":"MTL-DM-MA-51-3755","license_year":"2019","main_tank_fuel_id":"1","main_tank_fuel_capacity":"9","second_tank_fuel_id":"4","second_tank_fuel_capacity":"5","order":2,"custom1":null,"custom2":null,"custom3":null,"is_dual_tank":false,"active":true,"created_by":1,"updated_at":"2020-08-06 10:16:07","created_at":"2020-08-06 10:16:07","id":3}', '["company_id","vehicle_type","name","manufacturer","model","manufacturer_year","weight","lifetime","chassis_no","engine_no","vin_no","purchase_date","license_plate_no","license_year","main_tank_fuel_id","main_tank_fuel_capacity","second_tank_fuel_id","second_tank_fuel_capacity","order","custom1","custom2","custom3","is_dual_tank","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '49a8d36f29c60e4b00c3bdb9d96086e23276785d88d46ea010b0059b4ae7935e0e8e0e89f434a3e6f76d259a1cfe226fbc5a788f73fdf712b03195a93adc33cc', '2020-08-06 04:16:07', '2020-08-06 04:16:07'),
	(48, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 2, 4, 'created', '{"company_id":1,"vehicle_id":"3","branch_id":"2","driver_id":"4","take_over":"2020-08-06","hand_over":null,"order":2,"custom1":null,"custom2":null,"custom3":null,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 10:16:33","created_at":"2020-08-06 10:16:33","id":2}', '["company_id","vehicle_id","branch_id","driver_id","take_over","hand_over","order","custom1","custom2","custom3","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle_driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f733feff2161560a877937c7f2a70be349ae557fe8675364d89260cc61d96547c029ba1d416e4bc297b6a2e93eeb2b34f596dd6d37bdd116d74318b556085051', '2020-08-06 04:16:33', '2020-08-06 04:16:33'),
	(49, 'App\\Models\\User', 1, 'App\\Models\\VehicleDriver', 3, 4, 'created', '{"company_id":1,"vehicle_id":"3","branch_id":"2","driver_id":"1","take_over":"2020-08-06","hand_over":null,"order":3,"custom1":null,"custom2":null,"custom3":null,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-06 10:17:48","created_at":"2020-08-06 10:17:48","id":3}', '["company_id","vehicle_id","branch_id","driver_id","take_over","hand_over","order","custom1","custom2","custom3","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle_driver', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'b1c05b185ef0cb1bbf6ef73c0ed3ceb66d50207170a8810024352767616207f65e8ba41b00cf001bb5ae1f4c53f2f0628bd3f81c4834dbe8b59b3e2be8ec605b', '2020-08-06 04:17:48', '2020-08-06 04:17:48'),
	(50, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-05 04:34:02","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"z5D97oysdUL6NSopMRiabNz0KWC0AnpWqGWcol99sKKAZfzO2lDBJR9hzOQA","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-05 04:34:03","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '01278e7599b1a361bdc09f67465814ee77940156373318781810d778be815953b7c3dd8d27c4b293c224afba6033f8f2b54d67524b8a7164bb98e4b0f5be0001', '2020-08-06 04:36:23', '2020-08-06 04:36:23'),
	(51, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:36:29","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"z5D97oysdUL6NSopMRiabNz0KWC0AnpWqGWcol99sKKAZfzO2lDBJR9hzOQA","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-06 10:36:29","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'd9b085fce7f7900c378121e51098308a094d713c1bb72efade866c74f7d2a9fbf9d0c59ff2ca15d805f910102055cb264ea2112c8a9cf2a8ba032e8127011242', '2020-08-06 04:36:29', '2020-08-06 04:36:29'),
	(52, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:36:29","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"qCluGq2LCqKjKQi03nkgwzt23TnsrROwrOdoygv8vJcos0ybpCBjy41p7vcX","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-06 10:36:29","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '693a44310fe4364f4cc86a67c56f20bc017d54442c4ce93dbdf0b40df9bb05ba68f8842e16db09a3ab49af71999a33679b20212246f16db6a99fb9bae84adb93', '2020-08-06 04:37:42', '2020-08-06 04:37:42'),
	(53, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:37:50","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"qCluGq2LCqKjKQi03nkgwzt23TnsrROwrOdoygv8vJcos0ybpCBjy41p7vcX","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-06 10:37:50","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '903e49f46a43daa0ffde64277c95c35f47271bd07bace11679da5a3087f1d1a7a6243fc538e2ee25e46c878760bcee5907afed7ac70d3f9ad72c30427a0b7b84', '2020-08-06 04:37:51', '2020-08-06 04:37:51'),
	(54, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms\\/8qOmPS0g7wb9QPB1WUVmCOC","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:37:50","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"qCluGq2LCqKjKQi03nkgwzt23TnsrROwrOdoygv8vJcos0ybpCBjy41p7vcX","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-06 10:37:51","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '45e5238b72bf3070aad4ebf3225f20e606dd7c9df1fba6cf8be11fcbbf6bf07f1c8b3d7baa402debf918b7acabe9afadf7f679cf7c170f26d70431f1eaec8c8b', '2020-08-06 04:37:51', '2020-08-06 04:37:51'),
	(55, 'App\\Models\\User', 1, 'App\\Models\\User', 6, 4, 'created', '{"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","password":"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW","active":true,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":true,"updated_at":"2020-08-06 10:41:05","created_at":"2020-08-06 10:41:05","id":6}', '["first_name","last_name","email","username","password","active","confirmation_code","confirmed","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/user', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '782690e87b1afc261b237a354b30bc85e18f2ca3eb9fa9c958cc041ffdf18f46f55985a1fb5f717a6d253972c1ee63b5e27de03943ca1f1f3f9da2bc56f6519c', '2020-08-06 04:41:05', '2020-08-06 04:41:05'),
	(56, 'App\\Models\\User', 1, 'App\\Models\\Permission', 59, 4, 'created', '{"name":"transaction-fuel-rate-index","guard_name":"web","updated_at":"2020-08-06 10:43:48","created_at":"2020-08-06 10:43:48","id":59}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'eb7fb8a1c8de4ef89230b17be591345412352abd47434160845de8bb21897525644832d7d5b4e65b28a103479f19f898b10aa532e7a37b2c48ac59a7363fe884', '2020-08-06 04:43:48', '2020-08-06 04:43:48'),
	(57, 'App\\Models\\User', 1, 'App\\Models\\Permission', 60, 4, 'created', '{"name":"transaction-fuel-rate-create","guard_name":"web","updated_at":"2020-08-06 10:45:25","created_at":"2020-08-06 10:45:25","id":60}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'b6c901a43ccf34e31206717d7305557a4b0c61ceda34b2f521d6cb2207ba1e876f0edd33ba071d02ff422995357c8b00cfa0b0317d0a31cb8dc9c89bd99a975f', '2020-08-06 04:45:25', '2020-08-06 04:45:25'),
	(58, 'App\\Models\\User', 1, 'App\\Models\\Permission', 61, 4, 'created', '{"name":"transaction-fuel-rate-update","guard_name":"web","updated_at":"2020-08-06 10:45:35","created_at":"2020-08-06 10:45:35","id":61}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '321a652234c5c1f7093e933fedd66ea35b5f2c233d0282e9743f3c30b9b0316cd720e6453ceede88025c814ffed814f368cfcfbacbb041e3b809478c3f0743d6', '2020-08-06 04:45:35', '2020-08-06 04:45:35'),
	(59, 'App\\Models\\User', 1, 'App\\Models\\Permission', 62, 4, 'created', '{"name":"transaction-fuel-rate-edit","guard_name":"web","updated_at":"2020-08-06 10:45:44","created_at":"2020-08-06 10:45:44","id":62}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6df917bdd172944574ac80a33eba19bed818e3d2ec053bccaa6bc0f4b39d9ccc5b71e1f1fca0175c2ada0f141dfadc4029f97917c7f0ea51a99a726d143a0903', '2020-08-06 04:45:44', '2020-08-06 04:45:44'),
	(60, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms\\/8qOmPS0g7wb9QPB1WUVmCOC","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:37:50","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"OoqdZlemSru6nmzANMk56f3KKLaOcu4fVZESZEIAAe68lxwAWrmeanXmfXJQ","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-06 10:37:51","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f039dac3f7388ab89cc6c9817b3bb980bb02a9c06540b1984b865d06901dabe687d1538c897a6b8f50d86f2aa626a873ec6df9105a54cd070cf7e7bb8d88fa15', '2020-08-06 04:46:47', '2020-08-06 04:46:47'),
	(61, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":"lxtynmxb3qms7zTa66EUfRp9gaGPuRCmhn9s1Cw5IzOi8GMNSpuUk02tcn6M","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-06 10:41:05","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c8992e7a93b6514055cf574752733a91278539b721ca8a18bd6f580ad975394e331926ef20a5fb0768f1ee1d4dba8c2be056320bd927b83b602503489536580c', '2020-08-06 04:46:54', '2020-08-06 04:46:54'),
	(62, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:46:54","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"lxtynmxb3qms7zTa66EUfRp9gaGPuRCmhn9s1Cw5IzOi8GMNSpuUk02tcn6M","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-06 10:46:54","deleted_at":null}', '["timezone","last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '1979aae09fc66bf1ccd8da9b0bbbac4eb837516c2b1ef1d426c8bd72a2a94be931c1ee79b60775fe32fe515a841c2a06fde7632bec87d6429d42036af4c90cd6', '2020-08-06 04:46:54', '2020-08-06 04:46:54'),
	(63, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 10:46:54","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"5taMkqNPAyMadShZ6JzBdKzblLZ6mPgm95H1IPKRpUftKfP8RgfHw0JO2wUK","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-06 10:46:54","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6154df8beb1988e23dc879e8f999c09f0ac8ca6f8965df3234bba9bc173ee00e28607eaa43b66f47d102f729836149ff32b12c5e51543ec06c6f2e03675170cd', '2020-08-06 05:00:24', '2020-08-06 05:00:24'),
	(64, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 11:00:34","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"5taMkqNPAyMadShZ6JzBdKzblLZ6mPgm95H1IPKRpUftKfP8RgfHw0JO2wUK","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-06 11:00:34","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5fb4cd1a0c0620c219ec6748a1b07b7ce360c40d49dc77af53edc586152ae795601f48080461adcaf0a581eaf5c15935da720cb14f02b9a76d94447211629464', '2020-08-06 05:00:34', '2020-08-06 05:00:34'),
	(65, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg\\/qNpTlmCFfgN9nJtAhrq","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 11:00:34","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"5taMkqNPAyMadShZ6JzBdKzblLZ6mPgm95H1IPKRpUftKfP8RgfHw0JO2wUK","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-06 11:00:35","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ae5724e0c764934243ba0c1ca67addd52a3a887230931297616ce7ded69e032c6610da326615a2e70eed38d6fe6b90321ea227512a5ff13d1facf7e0da93a305', '2020-08-06 05:00:35', '2020-08-06 05:00:35'),
	(66, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 9, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06","fuel_id":"1","rate":0,"order":1,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:18:58","created_at":"2020-08-06 11:18:58","id":9}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8cd1c02adceb65fcb279a129f8d242535a1ccc2964023626d59473d2620cd1a62b5174c974caf22bbfa3cb5a1067e94eeea934fde7495b67121835618571dd76', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(67, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 10, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06","fuel_id":"2","rate":"86.3","order":2,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:18:58","created_at":"2020-08-06 11:18:58","id":10}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '7528951bb46a783179901e47e5d3af59af3ca142f12aa4a7466faf23f990a8d231b8740b886ff0650d7e9679d8833841e8cfad58c7e5751dc6ef860c866432f6', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(68, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 11, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06","fuel_id":"3","rate":"89.3","order":3,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:18:58","created_at":"2020-08-06 11:18:58","id":11}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'e1ba14c33e0ca31e2107221a063d00d9d0f3ad9ce698c9d059fc36ab13964a09d7a6b989f473c8ea660501835359ef8e77404b189e976b6c8fcc6cb99b8453bf', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(69, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 12, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06","fuel_id":"4","rate":"43","order":4,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:18:58","created_at":"2020-08-06 11:18:58","id":12}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '4f5d21bea6d365c99723bcb4c289ad3ad435078f0a1de706c0bfd3cf8bb3ebbb39b9645bc387fd9f3156c8ba7ac45e81fa7d110168493585706a42cfc273024a', '2020-08-06 05:18:58', '2020-08-06 05:18:58'),
	(70, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 9, 4, 'updated', '{"id":9,"company_id":1,"entry_date":"2020-08-06","fuel_id":"1","branch_id":21,"rate":"0.00","order":1,"archived":0,"active":1,"created_by":6,"updated_by":6,"deleted_by":null,"created_at":"2020-08-06 11:18:58","updated_at":"2020-08-06 11:25:22","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9c9e29a0191c6e940e1ecb1ef798f8a90be748ac18405454a4705c1818a73f6180553a60ed888b220d1219e4aa3a73f489caede9d4894975193d6ad691879b36', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(71, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 10, 4, 'updated', '{"id":10,"company_id":1,"entry_date":"2020-08-06","fuel_id":"2","branch_id":21,"rate":"86.30","order":2,"archived":0,"active":1,"created_by":6,"updated_by":6,"deleted_by":null,"created_at":"2020-08-06 11:18:58","updated_at":"2020-08-06 11:25:22","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c97010916b11f08f1fee2cfacd1af7dd50303c29d5990e0929dab31e7f9e82739c3feaa338afb31cc6fc121bf83e120329f08ecaf58d62f636e47cb57f75cf5a', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(72, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 11, 4, 'updated', '{"id":11,"company_id":1,"entry_date":"2020-08-06","fuel_id":"3","branch_id":21,"rate":"89.30","order":3,"archived":0,"active":1,"created_by":6,"updated_by":6,"deleted_by":null,"created_at":"2020-08-06 11:18:58","updated_at":"2020-08-06 11:25:22","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '212e0cc91808768a392bbb51a02802e498c5577b5132f46b8e32702051ff47c5bfe96c5f4c30b0c41bc3bd8d48b93b564b19e24c82551eb5841ca4e18ecdd42b', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(73, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 12, 4, 'updated', '{"id":12,"company_id":1,"entry_date":"2020-08-06","fuel_id":"4","branch_id":21,"rate":"43.00","order":4,"archived":0,"active":1,"created_by":6,"updated_by":6,"deleted_by":null,"created_at":"2020-08-06 11:18:58","updated_at":"2020-08-06 11:25:22","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate?branch_id=21&date=2020-08-06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5cab55726e82f942a5138034998f48e471b70b6b87de0a3edfb93dfdf605e99c4fee164cb200d7b0acf8df686ade33e8b5edb57bbeec04525195fee297661453', '2020-08-06 05:25:22', '2020-08-06 05:25:22'),
	(74, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 13, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06 00:00:00","fuel_id":"1","rate":"0","order":5,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:35:17","created_at":"2020-08-06 11:35:17","id":13}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a409bd3c55390b77216d1519470e1e1ce3bdc7a56794dc75baa608e538ae2d22f73f9a72045424df84a29ae971b8ac7648e4c4d3c5447d2bc0012cb24b410b12', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(75, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 14, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06 00:00:00","fuel_id":"2","rate":"89","order":6,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:35:17","created_at":"2020-08-06 11:35:17","id":14}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a74a196bf7787a95928d73fcd3fa473a162ce4da3cd5cf6eeb91d4ad9121b81c0c147c5302876164c6d6b34c6f402b82dff1db837fdf87298bf6036ce9bcddeb', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(76, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 15, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06 00:00:00","fuel_id":"3","rate":"84","order":7,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:35:17","created_at":"2020-08-06 11:35:17","id":15}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '80268ab6036b52e388518f7e5d0a2e6b08ebe2bb3cb263f54a33ec2e7e13d30db81d1fc5980ba1f6e954b7658a2e1d6143eff1b843aed75473612a35de602519', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(77, 'App\\Models\\User', 6, 'App\\Models\\FuelRate', 16, 4, 'created', '{"company_id":1,"branch_id":"21","entry_date":"2020-08-06 00:00:00","fuel_id":"4","rate":"60","order":8,"archived":0,"active":1,"created_by":6,"updated_at":"2020-08-06 11:35:17","created_at":"2020-08-06 11:35:17","id":16}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'be0e73a2f03ff276cd41104b3bd7126f25132e80f44ba2412ba5666147e51911c7d61b9b463326a3f692294e6ecff872ba9188f068403ab3f0ee3d0c9528f112', '2020-08-06 05:35:17', '2020-08-06 05:35:17'),
	(78, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg\\/qNpTlmCFfgN9nJtAhrq","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-06 11:00:34","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"wtx4sISe7kGGI1f34JuhNbXGmTE4FUoHPEJ406JwVYQEB7OxjCz5EuKdMr4B","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-06 11:00:35","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/transactions/fuel_rate', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9b170d7df1060d9f6d30bd8e03997b2eb9c430e487040c0908b4f7de713cac5e27ea542cbd634d56378d9a95691dcb6a26e9d0b4daa5f4d184dbe83d90cc203e', '2020-08-07 23:52:58', '2020-08-07 23:52:58'),
	(79, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg\\/qNpTlmCFfgN9nJtAhrq","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 05:53:10","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"wtx4sISe7kGGI1f34JuhNbXGmTE4FUoHPEJ406JwVYQEB7OxjCz5EuKdMr4B","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 05:53:11","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '98e7849d7f908cad42e17e3548b046a3317ec77b7fa16b7a67d4c416d03b7fdf897b48a62314d870958cc90cbd1e8aee9c59cd1f1b8d66ac065222a84d889c67', '2020-08-07 23:53:11', '2020-08-07 23:53:11'),
	(80, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$\\/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 05:53:10","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"wtx4sISe7kGGI1f34JuhNbXGmTE4FUoHPEJ406JwVYQEB7OxjCz5EuKdMr4B","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 05:53:11","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5abecf0649e5b4b12026db229bb3698ba19b9f3b3ac72cbc91f1ff98fd1eda2502e11aa3418d7c089a3e483b6a98724dbcdbcbd3f8cfbca418138fcfac971e44', '2020-08-07 23:53:11', '2020-08-07 23:53:11'),
	(81, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$\\/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 05:53:10","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"guleyUwINfU8nLtfz1et6gCtzxgISyZuL8rpzsinzj3oqu8pNzzSSHF2ojpP","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 05:53:11","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '202bd9311c4f9373e1b36994d058982a05bba9bbed7800a8c900137623445282a5a3a4810bc855190383647656c8ee8cdf12c3befefdc27a4b5fde386a926bb4', '2020-08-08 00:43:03', '2020-08-08 00:43:03'),
	(82, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms\\/8qOmPS0g7wb9QPB1WUVmCOC","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:43:11","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"OoqdZlemSru6nmzANMk56f3KKLaOcu4fVZESZEIAAe68lxwAWrmeanXmfXJQ","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-08 06:43:11","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6dbcbb70b8b9b5f988df911ca315114c2f7007bde7b5c9b5589db8fd2470ef95801e7d70a6d2fca0ed757cef140ff5ab68823a1d8c29b9870e0fc3426a97ce85', '2020-08-08 00:43:11', '2020-08-08 00:43:11'),
	(83, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$CNB\\/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:43:11","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"OoqdZlemSru6nmzANMk56f3KKLaOcu4fVZESZEIAAe68lxwAWrmeanXmfXJQ","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-08 06:43:11","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '31b304b6a0e54f74ce3beeafa00657ccd870e6260c838cf108e6476846240647a056ae428f4a651d46c16cabe09b923579b3d37e1a9ef19e63c4250d02b71388', '2020-08-08 00:43:11', '2020-08-08 00:43:11'),
	(84, 'App\\Models\\User', 1, 'App\\Models\\Permission', 64, 4, 'created', '{"name":"transaction-logbook-entry-create","guard_name":"web","updated_at":"2020-08-08 06:43:37","created_at":"2020-08-08 06:43:37","id":64}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c1f525ff74c7ab448a614ad9adb0390735b644d9b9916c09c3c246a34aee6176d8feccff69620def4b4d4e7456ad141fec72d1b1c9510d0a3b886389ad14c1dd', '2020-08-08 00:43:37', '2020-08-08 00:43:37'),
	(85, 'App\\Models\\User', 1, 'App\\Models\\Permission', 65, 4, 'created', '{"name":"transaction-logbook-entry-store","guard_name":"web","updated_at":"2020-08-08 06:43:51","created_at":"2020-08-08 06:43:51","id":65}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '023c943dff30ec6b709e55a9f4f83a72ac4abcd89dc70a2572fdafa76df440aad4f0dc91c39cad5d8d964fda669c69c9a6f1a757330cbb36a47272a47fbd703e', '2020-08-08 00:43:51', '2020-08-08 00:43:51'),
	(86, 'App\\Models\\User', 1, 'App\\Models\\Permission', 66, 4, 'created', '{"name":"transaction-logbook-entry-edit","guard_name":"web","updated_at":"2020-08-08 06:44:01","created_at":"2020-08-08 06:44:01","id":66}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5ddbf1d7884986455314866cc945ab977d60a894b895c99fc268a8515d6845a3c8bf7a9e8373c08fdc1cdc3f1b5b508a21c6f76c040c7afa25140e9c46451e8f', '2020-08-08 00:44:01', '2020-08-08 00:44:01'),
	(87, 'App\\Models\\User', 1, 'App\\Models\\Permission', 67, 4, 'created', '{"name":"transaction-logbook-entry-update","guard_name":"web","updated_at":"2020-08-08 06:44:10","created_at":"2020-08-08 06:44:10","id":67}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '8c916662dbb93f4ae84ba41629a9ff3fe89a4432a58d1f702fcdd2df36c1d59bcf3ef8bbc670789e7c95610f8d9316c8454e5c038889cf5a9e04331f4574279e', '2020-08-08 00:44:10', '2020-08-08 00:44:10'),
	(88, 'App\\Models\\User', 1, 'App\\Models\\Permission', 68, 4, 'created', '{"name":"transaction-logbook-entry-delete","guard_name":"web","updated_at":"2020-08-08 06:44:18","created_at":"2020-08-08 06:44:18","id":68}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '7c4c549f43d040321206dbd12b6c40bce3943e73a60b1d55c8f6208b1084ca97feabc937cbc54231ab2532cf3a482412ef07746704f80ec952b2730c821f2bbf', '2020-08-08 00:44:18', '2020-08-08 00:44:18'),
	(89, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$CNB\\/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:43:11","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"CNOlmWGObCe7eCzgQsXMrv0qM8KwJkLWbvmOsnbXtujlo1XrwTpmqbFojQrv","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-08 06:43:11","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a7b02e333a85f872257be41e434864b431f50a9578136551220fc210737db4cd273807fd882a1f3c58bf3c4b23789fbe4d30ba1deb2ad688447e742abf38ea5f', '2020-08-08 00:46:23', '2020-08-08 00:46:23'),
	(90, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$\\/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:46:29","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"guleyUwINfU8nLtfz1et6gCtzxgISyZuL8rpzsinzj3oqu8pNzzSSHF2ojpP","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 06:46:29","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6b9a485f6338caaa005cc151899788f08e5bb805e79e2940becd1798e03d3eea4577f79c14ebf4bc406411e4398eae0724e5c069e1f892734dea2ad2c1af100f', '2020-08-08 00:46:29', '2020-08-08 00:46:29'),
	(91, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:46:29","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"guleyUwINfU8nLtfz1et6gCtzxgISyZuL8rpzsinzj3oqu8pNzzSSHF2ojpP","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 06:46:29","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '83b0608ee24c5395fa2758e835a428038e50849d9b87c53782b29c5211a42b6f89a9f2680760e3cc98ae111689c585ddec596d8aad93af1477fceef009c1979f', '2020-08-08 00:46:29', '2020-08-08 00:46:29'),
	(92, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:46:29","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"j6cdrAAFJyl3Ki0wIPzmlyhCToLM8JOpvN2Mz4Q8L5Jo0Ot09FZutFybor0Y","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 06:46:29","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '0543b2565469b2584662d49b315f8608c0d5b58c3a3eefc55666c3aefb40f1efb65406485e91c9a446abac4489781c7c291e9ffbe068b104e4212a394570cbe1', '2020-08-08 00:55:34', '2020-08-08 00:55:34'),
	(93, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$CNB\\/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:55:39","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"CNOlmWGObCe7eCzgQsXMrv0qM8KwJkLWbvmOsnbXtujlo1XrwTpmqbFojQrv","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-08 06:55:40","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'd488518eeef199ccc2f5150bcc741f72b50e3c13335f6eb63ae21287dc6817d264f3d0f1833cc8f7fef106c31be2a01c47bf1167c819c25a77a3867db5918f81', '2020-08-08 00:55:40', '2020-08-08 00:55:40'),
	(94, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$ONvL9H\\/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:55:39","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"CNOlmWGObCe7eCzgQsXMrv0qM8KwJkLWbvmOsnbXtujlo1XrwTpmqbFojQrv","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-08 06:55:40","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '79df3600e4cd5fe066c267e9f65298877339c82543a3068f0f9d8c5ce329baa788499ee7d542fbcc9f0743e174f8db1fbbedb1cd320e9f281decbbb133454934', '2020-08-08 00:55:40', '2020-08-08 00:55:40'),
	(95, 'App\\Models\\User', 1, 'App\\Models\\Permission', 69, 4, 'created', '{"name":"transaction-logbook-entry-index","guard_name":"web","updated_at":"2020-08-08 06:55:57","created_at":"2020-08-08 06:55:57","id":69}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/permission', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '1e94a08ad9cba66492490c6aa634225ff086f48539d5d5feef7587af49ea218431929bf9f3d9e692805f82b831dad3ec4ce2b63319b0f4434fdcb5880d18ab94', '2020-08-08 00:55:57', '2020-08-08 00:55:57'),
	(96, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$ONvL9H\\/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:55:39","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-08 06:55:40","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a5b083e8940ba4acd916f08cddb741d1333879a8be9950a63a2e26caa68c45b41ee897bf3f01a7ecee8fbc303eac229e142698ab30b935f1c62473816142b7f4', '2020-08-08 00:56:26', '2020-08-08 00:56:26'),
	(97, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:56:33","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"j6cdrAAFJyl3Ki0wIPzmlyhCToLM8JOpvN2Mz4Q8L5Jo0Ot09FZutFybor0Y","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 06:56:34","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '2833372ad59544713e8754c1de199460046f94c9dba879356af7cfc42b69d0cb7004f3ebca30b5bf23c60572c3faf54108fde572ed748d867ba7eff73c941b3b', '2020-08-08 00:56:34', '2020-08-08 00:56:34'),
	(98, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:56:33","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"j6cdrAAFJyl3Ki0wIPzmlyhCToLM8JOpvN2Mz4Q8L5Jo0Ot09FZutFybor0Y","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 06:56:34","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '81d7445258bae999460b0268cdee59349adc90a90bda1f66660e9ec079c8bc887b9a0ca83818536640ae9163cb198bd5cb41a154fd53353ae1001b75305e078f', '2020-08-08 00:56:34', '2020-08-08 00:56:34'),
	(99, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-08 06:56:33","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"w0htWJxoVGfRWEwRd4vBxrsw27WkGPLbqDdoHJZe195UuLpENfw4svjIW7YD","created_at":"2020-08-06 10:41:05","updated_at":"2020-08-08 06:56:34","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://localhost:8080/odometer/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f3322568971b2a40e5d99b2271148e3d4d62ff23ec93fa1436c4671945508046947dfe121f33c72a5194ff5d97d6450e1eddaaa9e8ab94502a2d575f876364f4', '2020-08-09 00:51:27', '2020-08-09 00:51:27'),
	(100, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$ONvL9H\\/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-09 06:51:48","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-09 06:51:48","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c694418df80e063851f92e5c2541b9f6a9764d434e3cab602971962d22b91ece01c555869dff5b8792dc42ab157058abc2722037f03be14dcb3f609801200fc0', '2020-08-09 00:51:48', '2020-08-09 00:51:48'),
	(101, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Eqn4puXstk0B7kXpKHiJWuvg\\/xMVaa08HjaMe32vdCuQ5lWjx7b9.","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-09 06:51:48","last_login_ip":"::1","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-09 06:51:48","deleted_at":null}', '["password"]', '[]', '[]', 'http://localhost:8080/odometer/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '814f096dba942fd11435d08cebba934be09f292c4579df18e6151140283d1c24945f0a6910e9622d3a7796610810cae9df240edbeb229baf460b7cb6a5ce7f1b', '2020-08-09 00:51:49', '2020-08-09 00:51:49'),
	(104, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 6, 4, 'created', '{"company_id":"1","vehicle_type":"3","name":"Freezer Truck","manufacturer":"Ace","model":"AW22","manufacturer_year":"2017","weight":"120 pound","lifetime":null,"chassis_no":"LEZZWAAADDTTTAAS11","engine_no":"E11AAATT121","vin_no":null,"purchase_date":"2019-08-01","license_plate_no":"DHAKA-METRO-HA-124522","license_year":"2017","main_tank_fuel_id":"1","main_tank_fuel_capacity":"12","second_tank_fuel_id":"4","second_tank_fuel_capacity":"15","opening":"14123","order":3,"custom1":null,"custom2":null,"custom3":null,"is_dual_tank":false,"active":true,"created_by":1,"updated_at":"2020-08-09 11:23:50","created_at":"2020-08-09 11:23:50","id":6}', '["company_id","vehicle_type","name","manufacturer","model","manufacturer_year","weight","lifetime","chassis_no","engine_no","vin_no","purchase_date","license_plate_no","license_year","main_tank_fuel_id","main_tank_fuel_capacity","second_tank_fuel_id","second_tank_fuel_capacity","opening","order","custom1","custom2","custom3","is_dual_tank","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '835a6c5ea8b4aa40a7a96bd7f13f180ab067f4a8e0045423074728e22b3cfe1505421d6ef87a11eddbe6b3f0f02345d8ff6beca355f65ac41f24152885225768', '2020-08-09 05:23:50', '2020-08-09 05:23:50'),
	(105, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 1, 4, 'created', '{"company_id":"1","entry_date":"2020-08-09","vehicle_id":6,"tanks":1,"tank_type":1,"fuel_id":"1","capacity":"12","millage":"9","order":1,"custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_at":"2020-08-09 11:23:50","created_at":"2020-08-09 11:23:50","id":1}', '["company_id","entry_date","vehicle_id","tanks","tank_type","fuel_id","capacity","millage","order","custom1","custom2","custom3","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '10c9270b0633d7f165975c7d160829e68cb3ecdfe1ccef27634622e082348aed7176d022f609987c0c56c41b379c6c4dfd7e8c2c07f6a05c8bc59020fd5766ca', '2020-08-09 05:23:50', '2020-08-09 05:23:50'),
	(106, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 2, 4, 'created', '{"company_id":"1","entry_date":"2020-08-09","vehicle_id":6,"tanks":1,"tank_type":2,"fuel_id":"4","capacity":"15","millage":"12","order":2,"custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_at":"2020-08-09 11:23:50","created_at":"2020-08-09 11:23:50","id":2}', '["company_id","entry_date","vehicle_id","tanks","tank_type","fuel_id","capacity","millage","order","custom1","custom2","custom3","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ee23dec0593d8e1e8024aa6e6dab5275b1c9fbd002a810100f315e1b907630ddbaa9af23529a353649f5ece82ffb5335f5e7cca5d2b2c33a8db86bde5607bd66', '2020-08-09 05:23:50', '2020-08-09 05:23:50'),
	(107, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 1, 4, 'updated', '{"id":1,"vehicle_id":2,"document_type":"3","document_no":"aaaaa","issue_date":"2020-08-05","expiry_date":"2020-08-05","issuing_authority":"BRTA","order":null,"custom1":null,"custom2":null,"custom3":null,"active":false,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-05 06:51:58","updated_at":"2020-08-09 11:51:50","deleted_at":null}', '["order","updated_by","updated_at"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle/2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a17b076ad3e3c602b1004e467771f097ca380a185f0b9073aec16f5a2dfdf2fc61e16e572bff6c1630e56335a315f15a0ee05c6ecd354296f269a2544381c981', '2020-08-09 05:51:50', '2020-08-09 05:51:50'),
	(108, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 3, 4, 'created', '{"company_id":"1","entry_date":"2020-08-09","vehicle_id":2,"tanks":1,"tank_type":1,"fuel_id":"1","capacity":"9","millage":"12","order":3,"custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_at":"2020-08-09 11:51:50","created_at":"2020-08-09 11:51:50","id":3}', '["company_id","entry_date","vehicle_id","tanks","tank_type","fuel_id","capacity","millage","order","custom1","custom2","custom3","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://localhost:8080/odometer/public/master/vehicle/2', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '69962bb1e3d503d986fee27abded6eadea4a75952aab458412b198cff9caf038e5ec2ca02f52e5af118aa77823a17a82e84ce16707dc16b60aad3710757711e1', '2020-08-09 05:51:50', '2020-08-09 05:51:50'),
	(109, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Eqn4puXstk0B7kXpKHiJWuvg\\/xMVaa08HjaMe32vdCuQ5lWjx7b9.","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-12 09:39:05","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-12 09:39:05","deleted_at":null}', '["last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://odometer.test:8080/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '43a8820320ef227c82e215ce54b19b81c8121e273975080f7b7bff62c175b31e117254fa91a8dff9e45869320fe9dc799876538ffc786c41b6e25296c9c330a6', '2020-08-12 03:39:06', '2020-08-12 03:39:06'),
	(110, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$TFz5OpeJVpDmaqhNCDSc5uiov6m0C30lw6ngWc6raurxaCwkk0cbq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-08-12 09:39:05","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-04 04:41:44","updated_at":"2020-08-12 09:39:06","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.test:8080/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ea4f08944184f1ea1c8fe8313569bacb439442df6bc848c242c0d1af151921d41fed96ab0fc297c1a132c7bc8c6c4ef54ae3f93b71c137f0a90e26096281d3d2', '2020-08-12 03:39:06', '2020-08-12 03:39:06'),
	(111, 'App\\Models\\User', 1, 'App\\Models\\User', 7, 4, 'created', '{"first_name":"Bashir","last_name":"Uddin","email":"bashir@bashir.com","username":"bashir","password":"$2y$10$XFe6dqOEiYVO0QLk9O8oqOxEOD50\\/uGlOVaa1fA9ruzdbqvokPwyK","active":true,"confirmation_code":"7aa9cdfb31ece02418f281eaf4d86869","confirmed":false,"updated_at":"2020-08-16 04:47:42","created_at":"2020-08-16 04:47:42","id":7}', '["first_name","last_name","email","username","password","active","confirmation_code","confirmed","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/employee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'db844e34494a72452380fae421e835cf05fd5d5134e98fb215f83a698654f1c4ea1017b43ea92e2e5aff31831a496f299ccf859aff7fc84546b239fb345638f1', '2020-08-15 22:47:42', '2020-08-15 22:47:42'),
	(112, 'App\\Models\\User', 1, 'App\\Models\\Employee', 2, 4, 'created', '{"company_id":"1","user_id":7,"employee_id":"123456789","name":"Bashir Uddin","designation_id":"2","department":null,"grade":null,"email":"bashir@bashir.com","order":2,"active":true,"created_by":1,"updated_at":"2020-08-16 04:47:42","created_at":"2020-08-16 04:47:42","id":2}', '["company_id","user_id","employee_id","name","designation_id","department","grade","email","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/employee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '6e26a25b2d31dd0a434bb3d8bef5fb9fb37a93d175f6c53ae6f31289ac44d06c10cb41ee46d55dce0169263081719a16806cef3e8af2d4cd7f9e7951dc14dd6f', '2020-08-15 22:47:42', '2020-08-15 22:47:42'),
	(113, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 17, 4, 'created', '{"company_id":1,"branch_id":"6","entry_date":"2020-08-16 00:00:00","fuel_id":"1","rate":"86","order":17,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 04:57:25","created_at":"2020-08-16 04:57:25","id":17}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'eb289d8f1494396420cba398cbb8e79cf99ae0b142120756735bdeba42aff492e01946fd48be8bfe597f8e1529c02086bfbc49245c1b1247a1d89505e0e76313', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(114, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 18, 4, 'created', '{"company_id":1,"branch_id":"6","entry_date":"2020-08-16 00:00:00","fuel_id":"2","rate":"60","order":18,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 04:57:25","created_at":"2020-08-16 04:57:25","id":18}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5eef908c16fbd755e23960c10eeac44522c205f636332743437e7b42253086d9d3c1bc1bd516712289dd81e53dbb9864c0c85a376bb597233b7b8f677c65f094', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(115, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 19, 4, 'created', '{"company_id":1,"branch_id":"6","entry_date":"2020-08-16 00:00:00","fuel_id":"3","rate":"65","order":19,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 04:57:25","created_at":"2020-08-16 04:57:25","id":19}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '11cea66f5ae2cd746efcf7cd9a12e76a1393a06ded5b61f9ebec220a5e824f1b264f37bd16389f7ce85f6cc0fbe4690159462f35cb8eaa83ba122ccc7dc1eab0', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(116, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 20, 4, 'created', '{"company_id":1,"branch_id":"6","entry_date":"2020-08-16 00:00:00","fuel_id":"4","rate":"86","order":20,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 04:57:25","created_at":"2020-08-16 04:57:25","id":20}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '9e1c93a926cd582fc471c82b307b8a93bf41149195c124d2b59c88e5ad8bbda2639b43d13eb9fafdeea65dd41bac35a87ee18151d955eeb0df2327613b24aaec', '2020-08-15 22:57:25', '2020-08-15 22:57:25'),
	(117, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 17, 4, 'updated', '{"id":17,"company_id":1,"entry_date":"2020-08-16","fuel_id":"1","branch_id":6,"rate":"86.00","order":17,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-16 04:57:25","updated_at":"2020-08-16 04:57:32","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'acfdc6e6d5ba2c204b69fd735d6348cf9627bce240ab021a6327be13010dc4d5f8c0100f034dd3900d359dec12a38768b92f95cd2b3d0283b7a2cb2acfae9d90', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(118, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 18, 4, 'updated', '{"id":18,"company_id":1,"entry_date":"2020-08-16","fuel_id":"2","branch_id":6,"rate":"60.00","order":18,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-16 04:57:25","updated_at":"2020-08-16 04:57:32","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'ab73525b65b142b87dc64fdda18abccf76e15272c15894b5461f7a0785b35170161e2696b8a023985d3f03afadd9addbbcdffb43b6a3b989a26b29b4c183b589', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(119, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 19, 4, 'updated', '{"id":19,"company_id":1,"entry_date":"2020-08-16","fuel_id":"3","branch_id":6,"rate":"65.00","order":19,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-16 04:57:25","updated_at":"2020-08-16 04:57:32","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '406fb14709280dddf29e17bbe60ef820c1bae574bd979143efd68a9e5fbb00ff225cebe870d348a2e0982658d5897fb47d34d16218b3133b3d0548b36e288e51', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(120, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 20, 4, 'updated', '{"id":20,"company_id":1,"entry_date":"2020-08-16","fuel_id":"4","branch_id":6,"rate":"86.00","order":20,"archived":0,"active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-16 04:57:25","updated_at":"2020-08-16 04:57:32","deleted_at":null}', '["rate","updated_by","updated_at"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate?branch_id=6&date=2020-08-16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'd5ca585d48ae241913549c5608d708de7918d77c06bbe6c9503b1f224c2c6f570a056203757d5bfa48ff83f63efea353b61396cf5e36c2f8bcb21ddee140f8a7', '2020-08-15 22:57:32', '2020-08-15 22:57:32'),
	(121, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 7, 4, 'created', '{"company_id":"1","vehicle_type":"3","name":"Semi Freezar Van Van","manufacturer":"Toyota","model":"AW22","manufacturer_year":"2009","weight":"120 pound","lifetime":"12","chassis_no":"LEZZWAAADDTTTAAS11","engine_no":"E11AAATT121","vin_no":null,"purchase_date":"2020-08-15","license_plate_no":"DHAKA-METRO-HA-124522","license_year":"2019","main_tank_fuel_id":"3","main_tank_fuel_capacity":"35","second_tank_fuel_id":"4","second_tank_fuel_capacity":"34","opening":"12000","order":4,"custom1":null,"custom2":null,"custom3":null,"is_dual_tank":false,"active":true,"created_by":1,"updated_at":"2020-08-16 05:09:19","created_at":"2020-08-16 05:09:19","id":7}', '["company_id","vehicle_type","name","manufacturer","model","manufacturer_year","weight","lifetime","chassis_no","engine_no","vin_no","purchase_date","license_plate_no","license_year","main_tank_fuel_id","main_tank_fuel_capacity","second_tank_fuel_id","second_tank_fuel_capacity","opening","order","custom1","custom2","custom3","is_dual_tank","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'c7a6e83e233e2b195659827d9a9c19f6c622060ecee35a9d19e56e176ca37fdfe0d54f61ff5898a05a5fb356e2593a75bcbaab2ad5e7e6f0d48e2a82167b73d4', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(122, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 2, 4, 'created', '{"vehicle_id":7,"document_type":"3","document_no":"1213123","issue_date":"2020-08-16","expiry_date":"2020-08-16","issuing_authority":"1231","order":2,"active":false,"created_by":1,"updated_at":"2020-08-16 05:09:19","created_at":"2020-08-16 05:09:19","id":2}', '["vehicle_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '65a0f9165b4626f41d682f03f4c5020dbc84884ebe7906b719df12cf307279c3f404e4915b7085c0b387b957d91098cf3b766e2adb50cbf655c04df32b468e33', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(123, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 3, 4, 'created', '{"vehicle_id":7,"document_type":"4","document_no":"1qwqe","issue_date":"2020-08-15","expiry_date":"2020-08-15","issuing_authority":"qweqwe","order":3,"active":false,"created_by":1,"updated_at":"2020-08-16 05:09:19","created_at":"2020-08-16 05:09:19","id":3}', '["vehicle_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'fa2ae6fea4dd3aa71c866007cf767b7a50766755b0f40eaa4d0d93946072a913d113ffcb2fd563be1fd51865a3e4d5f6c3270af54fc40c864099d31a7a0a79b1', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(124, 'App\\Models\\User', 1, 'App\\Models\\VehicleDocument', 4, 4, 'created', '{"vehicle_id":7,"document_type":"5","document_no":"1213","issue_date":"2020-08-14","expiry_date":"2020-08-16","issuing_authority":"qweqe","order":4,"active":false,"created_by":1,"updated_at":"2020-08-16 05:09:19","created_at":"2020-08-16 05:09:19","id":4}', '["vehicle_id","document_type","document_no","issue_date","expiry_date","issuing_authority","order","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'bbc81f2b32797d7635af414449ff22a80ce0e2f68315df465cf4f86b7cba597a020b2a7f3e23490b4a401d90909031d3d6bc1361fd78d6515e1a1b1b28ba690d', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(125, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 4, 4, 'created', '{"company_id":"1","entry_date":"2020-08-13","vehicle_id":7,"tanks":1,"tank_type":1,"fuel_id":"3","capacity":"35","millage":"9","order":4,"custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_at":"2020-08-16 05:09:19","created_at":"2020-08-16 05:09:19","id":4}', '["company_id","entry_date","vehicle_id","tanks","tank_type","fuel_id","capacity","millage","order","custom1","custom2","custom3","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'a3f495cfa518a1a575e4632c7d3a106f788861857241733464f094d2e301253a7bf266d80559d88bc2a6f1db38825340c9bba1df89c7303fcf2e930142734a79', '2020-08-15 23:09:19', '2020-08-15 23:09:19'),
	(128, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 21, 4, 'created', '{"company_id":1,"branch_id":"2","entry_date":"2020-08-01 00:00:00","fuel_id":"1","rate":"86","order":21,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 05:26:56","created_at":"2020-08-16 05:26:56","id":21}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '3b122d095f10df7976a1d696f49fede3fdb559257acd645664629c4344ab6c45b3ac98a343d71c6a87ce1e4e22b92470bce7be5a6c352bb09fb821550b9f3fbe', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(129, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 22, 4, 'created', '{"company_id":1,"branch_id":"2","entry_date":"2020-08-01 00:00:00","fuel_id":"2","rate":"65","order":22,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 05:26:56","created_at":"2020-08-16 05:26:56","id":22}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'cfe1f4afc96a8b579c01d2c0ffaafbf99e02c22253840805bac74ee313b5e046cac8c7bfa6fd5e610bb8e2e0741cb977f3d46d31c6593ca645a2e7d8b23e2362', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(130, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 23, 4, 'created', '{"company_id":1,"branch_id":"2","entry_date":"2020-08-01 00:00:00","fuel_id":"3","rate":"60","order":23,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 05:26:56","created_at":"2020-08-16 05:26:56","id":23}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '353310c0fab2564198d3f30a3ac1c1e92d8a784c9ae753ee6c9d14130f3ef322cb222bd236e30c37e566dcb47d9ded278000629513b3305e1881d80e28d286d4', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(131, 'App\\Models\\User', 1, 'App\\Models\\FuelRate', 24, 4, 'created', '{"company_id":1,"branch_id":"2","entry_date":"2020-08-01 00:00:00","fuel_id":"4","rate":"75","order":24,"archived":0,"active":1,"created_by":1,"updated_at":"2020-08-16 05:26:56","created_at":"2020-08-16 05:26:56","id":24}', '["company_id","branch_id","entry_date","fuel_id","rate","order","archived","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/fuel_rate', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', 'f395ca03e83cae3abad26d726299f47c1495f1437f4ed7be52e4f07e03ab75667a7bbeab411e973f1474563cd0699ed28b50ac3d3e93e727b30fa00b4457395e', '2020-08-15 23:26:56', '2020-08-15 23:26:56'),
	(132, 'App\\Models\\User', 1, 'App\\Models\\Vehicle', 2, 4, 'updated', '{"id":2,"company_id":"1","vehicle_type":"4","name":"Semi Truck","manufacturer":"Runner","model":"T51","manufacturer_year":"2019","weight":"120 pound","lifetime":null,"license_plate_no":"DHAKA-METRO-HA-244855","license_year":"2017","is_dual_tank":false,"main_tank_fuel_id":"1","main_tank_fuel_capacity":"50","second_tank_fuel_id":"4","second_tank_fuel_capacity":"25","chassis_no":"LEZZWAAADDTTTAAS","engine_no":"E11AAATT1","vin_no":null,"opening":"12980.0000","purchase_date":"2017-11-08","order":"1","custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-08-05 06:30:19","updated_at":"2020-08-16 05:33:15","deleted_at":null}', '["main_tank_fuel_capacity","second_tank_fuel_id","second_tank_fuel_capacity","updated_by","updated_at"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '5b43f314ab395eacd04850de9a6c6e84ae11540b98337d7172e2254dee480aed1bb2c0ef01b2d6ea52072e18edbeff5aee16c86e1aa2336da490b89ec374d13e', '2020-08-15 23:33:15', '2020-08-15 23:33:15'),
	(133, 'App\\Models\\User', 1, 'App\\Models\\VehicleHistory', 5, 4, 'created', '{"company_id":"1","entry_date":"2020-08-01","vehicle_id":2,"tanks":1,"tank_type":1,"fuel_id":"1","capacity":"50","millage":"15","order":5,"custom1":null,"custom2":null,"custom3":null,"active":true,"created_by":1,"updated_at":"2020-08-16 05:33:15","created_at":"2020-08-16 05:33:15","id":5}', '["company_id","entry_date","vehicle_id","tanks","tank_type","fuel_id","capacity","millage","order","custom1","custom2","custom3","active","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/master/vehicle/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '967c49b71d424ece8d638186165340ea4ddbb8a60cf9c05d3a89e7debe47302229d00d922efa330f999760c1f3acc82bd18ae11d99b8d237be0e5488996b5683', '2020-08-15 23:33:15', '2020-08-15 23:33:15'),
	(134, 'App\\Models\\User', 1, 'App\\Models\\LogbookEntry', 1, 4, 'created', '{"company_id":1,"sl":1,"trans_date":"2020-08-01","branch_id":"2","branch_name":"Motijheel","employee_id":"2","employee_name":"Bashir Uddin","vehicle_id":"2","vehicle_name":"Semi Truck","vehicle_license_no":"DHAKA-METRO-HA-244855","vehicle_mnf_yr":"2019","vehicle_purchase_yr":"2017","vehicle_lifetime":"2019","vehicle_used":"2017","driver_id":"4","driver_name":"Parvez","driver_take_ovr_dt":"2020-08-01T00:00:00.000000Z","driver_hand_ovr_dt":"2020-08-01T00:00:00.000000Z","rkm_data":"30","logbook_opening":"12980.0000","logbook_closing":"13015","logbook_running":"35","fuel_id":"1","fuel_name":"Deisel","fuel_consumption":"45","fuel_rate":"86.000000","fuel_cost":"3870","std_fuel_consumption":"50","approved":0,"approved_by":null,"status":0,"posted":false,"order":1,"year":2020,"month":8,"month_name":"08","created_by":1,"updated_at":"2020-08-16 05:42:22","created_at":"2020-08-16 05:42:22","id":1}', '["company_id","sl","trans_date","branch_id","branch_name","employee_id","employee_name","vehicle_id","vehicle_name","vehicle_license_no","vehicle_mnf_yr","vehicle_purchase_yr","vehicle_lifetime","vehicle_used","driver_id","driver_name","driver_take_ovr_dt","driver_hand_ovr_dt","rkm_data","logbook_opening","logbook_closing","logbook_running","fuel_id","fuel_name","fuel_consumption","fuel_rate","fuel_cost","std_fuel_consumption","approved","approved_by","status","posted","order","year","month","month_name","created_by","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.test:8080/transactions/logbook_entry', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '06c4492825a7d946ada3bb6595b6ea6c0c9142317de8864922cc877fde4eee3aff22384d11c0da63fc16551e0a204ce69e0ad2feeba48a5e39b431deefab1257', '2020-08-15 23:42:22', '2020-08-15 23:42:22'),
	(135, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$TFz5OpeJVpDmaqhNCDSc5uiov6m0C30lw6ngWc6raurxaCwkk0cbq","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:33:03","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:33:03","deleted_at":null}', '["timezone","last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'fb2189abc7cfedac02518dc7876d2d805da8b9d9a7645e5a41585658fc2afa4c3e7d9eaff004a5712461297d75f19e3c838b6591ff922333714b2a87698a65fc', '2020-09-08 04:33:03', '2020-09-08 04:33:03'),
	(136, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$9SMbOJqey\\/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD\\/wI5vlNUZEe","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:33:03","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"QGNcVmH32FhaUA8xySZEE1PNKSWZ5JpkSliXmqS2Xb5gxCCwfOmqgIPAjKwx","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:33:03","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '05642bfb77f88324c46117da5fed708901cda9f155eb42139d6022d536f6c451754fb9dca54ac8d54d31abf71e6d69531db6e254e560731c0847b6b4efb1c2b1', '2020-09-08 04:33:03', '2020-09-08 04:33:03'),
	(137, 'App\\Models\\User', 1, 'App\\Models\\User', 5, 4, 'updated', '{"id":5,"company_id":1,"first_name":"Rejaul Islam","last_name":"Royel","email":"rejaul.islam@transcombd.com","username":"almas","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL\\/cdyHSOdnbrDXc8O5cUh8kG","password_changed_at":null,"active":1,"confirmation_code":"29b575c492a20aafaad3a0aa89807a5f","confirmed":0,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":null,"created_at":"2020-08-04 00:44:21","updated_at":"2020-09-08 04:34:45","deleted_at":null}', '["first_name","last_name","email","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/user/5', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '2320e175077dcf0231871469dfc1527f2788340f5d0a75cf5faa0257a53319162166f722a75450dcc92a12664b3124da7aea5f199d9f9e6cadb1a7a5b8cf10c4', '2020-09-08 04:34:45', '2020-09-08 04:34:45'),
	(138, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$9SMbOJqey\\/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD\\/wI5vlNUZEe","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:33:03","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Ei5sQ2PSFny7Iuv6yFWYQr4I8jyh6wzkr5xEL9S5tj0CnssGY5G7RoRGUwtI","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:33:03","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'a68f12489b723fe931f9b5779525200068ff1f030ffff6cc75276a075d36fb964eca0fd6207b17e5dea6ad10ae59bbbb1c538da5456fc91941d18b66cfbb52b8', '2020-09-08 04:34:54', '2020-09-08 04:34:54'),
	(139, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:34:59","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"w0htWJxoVGfRWEwRd4vBxrsw27WkGPLbqDdoHJZe195UuLpENfw4svjIW7YD","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 04:34:59","deleted_at":null}', '["timezone","last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '05078867269122aa43cd338fa248222298ca2e5475b6cf25e9caa7c920a4de026eb33ae3e812a3b372cf76482152c2e9903519189f8c6f217beec9a0ca093694', '2020-09-08 04:34:59', '2020-09-08 04:34:59'),
	(140, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L\\/wSMS3bNURqZJNefOnW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:34:59","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"w0htWJxoVGfRWEwRd4vBxrsw27WkGPLbqDdoHJZe195UuLpENfw4svjIW7YD","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 04:34:59","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '6e20cd115074f0226ec3c213c5a33865e893103e5484b20cc60c8adea6dee83f50f6e0ae054e2312ca6a112e26ba22f1478a2d01118a328cd4bc9d7d04eb8086', '2020-09-08 04:34:59', '2020-09-08 04:34:59'),
	(141, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L\\/wSMS3bNURqZJNefOnW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:34:59","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"XN84dBdUmjHmLRutVrPEYWgwDq4rTY7VbbtnClijzWa6d0VKSrKReDwbs7oz","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 04:34:59","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'f971b31031167892e7d1bf6645018dbd5a93cfcea3385377f13ad7a29626e7af65cfa5402bfd217841d7866b80ee2c441c5601076965c204399a6392f6c095ee', '2020-09-08 04:35:33', '2020-09-08 04:35:33'),
	(142, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$9SMbOJqey\\/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD\\/wI5vlNUZEe","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:35:37","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Ei5sQ2PSFny7Iuv6yFWYQr4I8jyh6wzkr5xEL9S5tj0CnssGY5G7RoRGUwtI","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:35:37","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'dcbd81bf9ebe4b5eee3494911c3564556a24f24a1e67738c429ef447e32e9dff9c0d8f784b0e3442548cf3302de0105648ab849e63c4dc4fe3057060b9c13de0', '2020-09-08 04:35:37', '2020-09-08 04:35:37'),
	(143, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Dst.N445dreP4kNI\\/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:35:37","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Ei5sQ2PSFny7Iuv6yFWYQr4I8jyh6wzkr5xEL9S5tj0CnssGY5G7RoRGUwtI","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:35:37","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '3e8fe17b800dd334ecd44b61ee820c52d9f285d44d20e3cd3581bad0e11161235213f67f1fb854a726d9e5820b8daec350e95587667213421a50c69378103a0e', '2020-09-08 04:35:37', '2020-09-08 04:35:37'),
	(144, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Dst.N445dreP4kNI\\/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:35:37","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"7ICzAUyQFRNtu0px45oaWvmD483J1ATRhb8uZmSlfVl2Iqt1aiK476hY1FYN","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:35:37","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'fa137a0a0e6896cd55cec93f69fd92a50c85f1ff176976cc4c808904188a0158f88cf4579d041a10f37ec86cf7d9638cb591420525393f81e35e73d0f077b7c5', '2020-09-08 04:38:07', '2020-09-08 04:38:07'),
	(145, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$Dst.N445dreP4kNI\\/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:38:11","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"7ICzAUyQFRNtu0px45oaWvmD483J1ATRhb8uZmSlfVl2Iqt1aiK476hY1FYN","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:38:11","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '34dc29fe135d2f746f360b02f4e9fc0f9471037367a21ff5a6daf7cc4af98a528c5113ba4cd2b43001bba60bc9ca6e86e5e06f12e51c057dedcd1622713c0c9e', '2020-09-08 04:38:11', '2020-09-08 04:38:11'),
	(146, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:38:11","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"7ICzAUyQFRNtu0px45oaWvmD483J1ATRhb8uZmSlfVl2Iqt1aiK476hY1FYN","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:38:11","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'd551a882db615efb2335ee7b5db1846ad768dd298c1c9e8b70dd376d3335efa5611f851090d335a7f236fd49039d59715aa03cfd1917e8c445416818a8c68576', '2020-09-08 04:38:11', '2020-09-08 04:38:11'),
	(147, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:38:11","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 04:38:11","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'dea8845d8393e95755c6d724761e5e3d8a6724864a55a2fa087bb8e2d3cd7a39fc90f7a4d799a0fca91427e8f4d5c3408623b7098caf0cb4d112162da7da9a0f', '2020-09-08 04:38:13', '2020-09-08 04:38:13'),
	(148, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L\\/wSMS3bNURqZJNefOnW","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:38:19","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"XN84dBdUmjHmLRutVrPEYWgwDq4rTY7VbbtnClijzWa6d0VKSrKReDwbs7oz","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 04:38:19","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'd5338b336ffc3347ba3edf994e65cb7adb634ea9cad21f0ffb769755956c6c26139207383c8bc15bd08ab06c4cff7914a681c64bca608393fde20a08c573a75f', '2020-09-08 04:38:19', '2020-09-08 04:38:19'),
	(149, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:38:19","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"XN84dBdUmjHmLRutVrPEYWgwDq4rTY7VbbtnClijzWa6d0VKSrKReDwbs7oz","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 04:38:19","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '47de98a8b3914caeb04611933882ca3da75d2f017b603ad9cf76cedb36f719557ec5403df59e06f4fd6eca7a3e10ac754482dca755510a8f1f937bdcf017699f', '2020-09-08 04:38:19', '2020-09-08 04:38:19'),
	(150, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:09:26","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:09:26","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '40fe110aae5f6a7e5ee9d9185c5d6cc30d9bc13a38ad3e67c4bf5f73ff66982607fea58143403865624e99a7735346a7207bc33cf33ac3ab8a3e89716b832083', '2020-09-08 08:09:26', '2020-09-08 08:09:26'),
	(151, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$\\/43fgsfG48meVrs6WxOqIejm5KytE0qcCtG8.38j.4RprD1ttR5PC","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:09:26","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:09:27","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '74a32e56aa2d6abcde17be6ff2d7f261c91be874b72719195651b51fe513abb4e0de634ac28adee82ce94b84c281089532e42bc36fe4deabdaef4b7bc0789407', '2020-09-08 08:09:27', '2020-09-08 08:09:27'),
	(152, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'created', '{"first_name":"john","last_name":"due","email":"john@gmail.com","username":"john","password":"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6","active":true,"confirmation_code":"3e09600c3bec3dde2016d82ce64d0499","confirmed":true,"updated_at":"2020-09-08 08:12:52","created_at":"2020-09-08 08:12:52","id":8}', '["first_name","last_name","email","username","password","active","confirmation_code","confirmed","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.erprnd.com/user', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '34692baf835b179fa733da45c2a2529080655391ae1ea943d550b289a74b0e22937d6fcf22b39830c23436a4e835feda6426d063a78c6ba2c8872212c81ac1d8', '2020-09-08 08:12:52', '2020-09-08 08:12:52'),
	(153, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 04:38:19","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"8eEkyPpzhiUz2uOTpxiZCJvi4Y8FEuc4sFCfLvYDLTYFYVvKOQvTd2DZJ3Uz","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 04:38:19","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'df512d3ff244e658f474cb60cf4225f434ecd5ef5beebab5f467f891fafee466662fdc1946c13d4f914ae53620aaa6615c40368a0ca35cbfd503bce9e142cba3', '2020-09-08 08:14:09', '2020-09-08 08:14:09'),
	(154, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$\\/43fgsfG48meVrs6WxOqIejm5KytE0qcCtG8.38j.4RprD1ttR5PC","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:10","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:14:11","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '4972699a922b2b47ad73e3f14142ec847f8fc0468794f1abc1f7a3d52a9d1f8d35669730a5da0ce774d7e89f5fd96da1d28ced6c0f27882d66d83a4d702345cf', '2020-09-08 08:14:11', '2020-09-08 08:14:11'),
	(155, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$rReBoyduZYkwcdrQrfeR1OdONlMYQoQ90XgCo6oRFvgHEkSSxLnty","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:10","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:14:11","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'ec7e882c7dc78e9e6b5744960144d74b867a55b098c2bd5dc99bfc96491f03c87ee6ce243bb8f8bd3ba83ce119c148fc7eac1c08e339ba8c5a6f30a6f51be0cd', '2020-09-08 08:14:11', '2020-09-08 08:14:11'),
	(156, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$rReBoyduZYkwcdrQrfeR1OdONlMYQoQ90XgCo6oRFvgHEkSSxLnty","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:16","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:14:16","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'f0827c94687b4380a49bd364be06fc90f748cc110340758df22a7157e51de6774c66157c57f1256085c5cc14267cbbae79d7210deb5890855c478bdbf3aae8d6', '2020-09-08 08:14:16', '2020-09-08 08:14:16'),
	(157, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:16","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"s5PANOxVGvgMwfJzPdOcxqSy2B6x5TRW4TNz8AXYGufKsl8PwgCouHHvLvAM","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:14:16","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '7f89770bd664b9724ed9e7ab55abf9eb7814179fc24882dbb9a87beed1414f16d1ec0cf8334a41b297f375c52943a2e66e25f49247ce41b0ee87a07be5050104', '2020-09-08 08:14:16', '2020-09-08 08:14:16'),
	(158, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:16","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"BGdW1CrteXQwhyifuBdHzdfPkryDG0vCyWAWrMBwkhMiiuRvJD2OWCO2SX6Y","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:14:16","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '699861ac81fe613b4c0c24fdba6492faa5508a57056fb11ce1971082b54c9ed2d18199a477840479b4be1ceee1c0a460920456373f33306eac4a0c5f5fb5dbc9', '2020-09-08 08:14:20', '2020-09-08 08:14:20'),
	(159, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:34","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"8eEkyPpzhiUz2uOTpxiZCJvi4Y8FEuc4sFCfLvYDLTYFYVvKOQvTd2DZJ3Uz","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 08:14:34","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '49e8c5f422e1e31eb341d2c163127057418b9499cb71f9bb7c86c3f80f6ae8977b522d22f6b2c12e4cb844a3994e6bacf0e808b117f43837ca60bddccbb0f9ea', '2020-09-08 08:14:34', '2020-09-08 08:14:34'),
	(160, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU\\/MmKKHR95SsTYCyS","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:34","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"8eEkyPpzhiUz2uOTpxiZCJvi4Y8FEuc4sFCfLvYDLTYFYVvKOQvTd2DZJ3Uz","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 08:14:35","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '44efaee2aeb8d3d64e9fd25e817f6678a35d5d01bcdc9fb5378f45e00e741dae663e07e68403a0131153220280463c7d8818214d3fb06b2c7dc6fc3961cd10ef', '2020-09-08 08:14:35', '2020-09-08 08:14:35'),
	(161, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'created', '{"code":"2222","name":"Rabby soft","short_name":"R soft","address":"dhaka","email_suffix":"great","logo":"pngwing.com (1).png","time_zone":"Asia\\/Dhaka","active":true,"created_by":1,"created_at":"2020-09-08 08:17:54","updated_at":"2020-09-08 08:17:54","id":2}', '["code","name","short_name","address","email_suffix","logo","time_zone","active","created_by","created_at","updated_at","id"]', '[]', '[]', 'http://odometer.erprnd.com/master/company', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'b75c02e6954228aa69f9947ec5440a02c3b2555de598db827f3efa712737ab69fe26ca68857bdf9d171f8f99a74b9a16c02e0f8e17809e8dd046e57b7dfdcd2a', '2020-09-08 08:17:54', '2020-09-08 08:17:54'),
	(162, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'updated', '{"id":8,"company_id":1,"first_name":"john1","last_name":"due","email":"john@gmail.com","username":"john","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6","password_changed_at":null,"active":1,"confirmation_code":"3e09600c3bec3dde2016d82ce64d0499","confirmed":1,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":null,"created_at":"2020-09-08 08:12:52","updated_at":"2020-09-08 08:18:46","deleted_at":null}', '["first_name","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/user/8', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '481236c511b309114c1e89156e2307aaabc2d26e6d63aae9d9eb244cc14fcadde894e87c1ee4ed0da0dbcaa2d0218d677655a470867acad1cc15f78e9691ca54', '2020-09-08 08:18:46', '2020-09-08 08:18:46'),
	(163, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{"id":2,"code":"2222","name":"brix","short_name":"R soft","address":"dhaka","logo":"pngwing.com (1).png","email_suffix":"great","time_zone":"Asia\\/Dhaka","active":true,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-09-08 08:17:54","updated_at":"2020-09-08 08:18:52","deleted_at":null}', '["name","updated_by","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '29f808f5ef3b11ad2e6a8e79f6204b9afb8af6af32f837af4d2f1969d00b5c5dabab311e2165b258cdbcabf1ca9661e636eafb6e24724092e8ee55e48858ac78', '2020-09-08 08:18:52', '2020-09-08 08:18:52'),
	(164, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{"id":2,"code":"2222","name":"brix","short_name":"R soft","address":"dhaka","logo":"pngwing.com (1).png","email_suffix":"great","time_zone":"Asia\\/Dhaka","active":0,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-09-08 08:17:54","updated_at":"2020-09-08 08:19:03","deleted_at":null}', '["active","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '11ca21a3a29eb0cdcd85f3af583de617e025bc753fd8adc777aa9b1f5ce2080fb0e7ee91281e81164b87d6b6a798c8824c210d8be7756762bbcfd7f291ca3552', '2020-09-08 08:19:03', '2020-09-08 08:19:03'),
	(165, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{"id":2,"code":"2222","name":"brix","short_name":"R soft","address":"dhaka","logo":"pngwing.com (1).png","email_suffix":"great","time_zone":"Asia\\/Dhaka","active":1,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-09-08 08:17:54","updated_at":"2020-09-08 08:19:26","deleted_at":null}', '["active","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '63426853a60323dd2aba38f4dc60561939543b2bc71de687b36a109c5056bc1b9983910eef2ffd9fd1c94655993d724a7d7fa617ba381bd6b1c761932c46467d', '2020-09-08 08:19:26', '2020-09-08 08:19:26'),
	(168, 'App\\Models\\User', 1, 'App\\Models\\Company', 2, 4, 'updated', '{"id":2,"code":"2222","name":"brix","short_name":"R soft","address":"dhaka","logo":"hiclipart.com.png","email_suffix":"great","time_zone":"Asia\\/Dhaka","active":true,"created_by":1,"updated_by":1,"deleted_by":null,"created_at":"2020-09-08 08:17:54","updated_at":"2020-09-08 08:20:18","deleted_at":null}', '["logo","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/master/company/2', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'd422367006a162a31b8a8a2f5952144bb179ca89c18bf16390e83abc9dac3020d7c1d9b80c2a0ea8d485c9fc8e48e6ecc1a38f63e399481d208ea21c07f3402a', '2020-09-08 08:20:18', '2020-09-08 08:20:18'),
	(169, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'updated', '{"id":8,"company_id":1,"first_name":"john1","last_name":"due","email":"john@gmail.com","username":"john","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6","password_changed_at":null,"active":0,"confirmation_code":"3e09600c3bec3dde2016d82ce64d0499","confirmed":1,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":null,"created_at":"2020-09-08 08:12:52","updated_at":"2020-09-08 08:21:09","deleted_at":null}', '["active","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/user/8/mark/0', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '78d38c315485803aef5b8a21ccae1beecfbafbde110cd0635a8f431d8f6bf16b55890ac6b9a7ca1c4a6aac77f1ff10f2247fb6160506488f574299c09ea5f6d2', '2020-09-08 08:21:09', '2020-09-08 08:21:09'),
	(170, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'updated', '{"id":8,"company_id":1,"first_name":"john1","last_name":"due","email":"john@gmail.com","username":"john","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6","password_changed_at":null,"active":1,"confirmation_code":"3e09600c3bec3dde2016d82ce64d0499","confirmed":1,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":null,"created_at":"2020-09-08 08:12:52","updated_at":"2020-09-08 08:21:26","deleted_at":null}', '["active","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/user/8/mark/1', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '3a9c03cb5a64969c10b10930748838afb1e7df268a335b8d48998b82046110efd8bd7235cf9d8ec773bd44425527422c16b6ae807a72dfcd7f9fb182658be3b5', '2020-09-08 08:21:26', '2020-09-08 08:21:26'),
	(171, 'App\\Models\\User', 1, 'App\\Models\\User', 8, 4, 'deleted', '{"id":8,"company_id":1,"first_name":"john1","last_name":"due","email":"john@gmail.com","username":"john","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$5Wmsnn\\/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6","password_changed_at":null,"active":1,"confirmation_code":"3e09600c3bec3dde2016d82ce64d0499","confirmed":1,"timezone":null,"is_logged":0,"last_login_at":null,"last_login_ip":null,"to_be_logged_out":0,"remember_token":null,"created_at":"2020-09-08 08:12:52","updated_at":"2020-09-08 08:21:51","deleted_at":"2020-09-08 08:21:51"}', '[]', '[]', '[]', 'http://odometer.erprnd.com/user/8', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'a290d75a5e20f01af614cf8c500042264aa27316eed9e738203acd2d75dbff105221ef904d99566c6ef0f594eaa4d7209412b8223074a14bf41e61210c439c8c', '2020-09-08 08:21:51', '2020-09-08 08:21:51'),
	(172, 'App\\Models\\User', 1, 'App\\Models\\Role', 4, 4, 'created', '{"name":"facerole","guard_name":"web","updated_at":"2020-09-08 08:25:06","created_at":"2020-09-08 08:25:06","id":4}', '["name","guard_name","updated_at","created_at","id"]', '[]', '[]', 'http://odometer.erprnd.com/role', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '9233c120b974f489c6daa5c819385aff8c10032601301d3855ea6208002a146356c5fefe77cd387d0bbe73ee63d291e52e889e2baa397fdd85b29303cde1ca60', '2020-09-08 08:25:06', '2020-09-08 08:25:06'),
	(173, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU\\/MmKKHR95SsTYCyS","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:14:34","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 08:14:35","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'd7fa44dd383a1b447c519882e8d9fbf788a067bd3027d504cb2a67f9f47b1177e2a18840ad988b12ab644279eaa08715b7e0f6b2b221caeeadb1ba09b0af8329', '2020-09-08 08:28:42', '2020-09-08 08:28:42'),
	(174, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:28:47","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"BGdW1CrteXQwhyifuBdHzdfPkryDG0vCyWAWrMBwkhMiiuRvJD2OWCO2SX6Y","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:28:47","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '3ba11a2db1081a9f6742b33fe4e535ea219d7ccbfaad3461506eca4098e32597e4ed48294da4d80cbd79682a3950a7c9f324f1d9db1af3dc430ed074c170b852', '2020-09-08 08:28:47', '2020-09-08 08:28:47'),
	(175, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:28:47","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"BGdW1CrteXQwhyifuBdHzdfPkryDG0vCyWAWrMBwkhMiiuRvJD2OWCO2SX6Y","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:28:47","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '821f5b97818f685131a3669cfd268dbdf83a4f26ee16df3daf56b94a217523d0a58980c4a3879acb94ff2367f1549fd14cf8976f7f805ab84f848f1392cdd9b9', '2020-09-08 08:28:47', '2020-09-08 08:28:47'),
	(176, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU\\/MmKKHR95SsTYCyS","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 10:00:53","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 10:00:54","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '7b19b800812e78dd971c63d70acc77adfedd30476b53c7918650312d58a5456aac45acbd0a2869dc90961127c8ef59fe433928eb4e358cd8dd7ea4ed31f3a8cd', '2020-09-08 10:00:54', '2020-09-08 10:00:54'),
	(177, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$3kCpwukGAxvU5UBSp.h6MO8asDFLi\\/zTwY.dUqi\\/fFsHB\\/ZTy1ay6","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 10:00:53","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 10:00:55","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'bba63675c083e604e4d07471d35538f6779a3803bd4ea7c40cb802d5d788d6b8e35feb0395225f16a21d8432597fda73074f0213696861c5558ee151ba46f889', '2020-09-08 10:00:55', '2020-09-08 10:00:55'),
	(178, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 08:28:47","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw","created_at":"2020-08-03 22:41:44","updated_at":"2020-09-08 08:28:47","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.erprnd.com/logout', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '27bfcde8dc38594ccef03f21f1908e98cf1e8578b4e1da3aa193b4d7c247d7629d8e635bd5ac5e1f1ece0a849741d652ac2e5c2017403b04dd6253d811160c7f', '2020-09-08 10:24:14', '2020-09-08 10:24:14'),
	(179, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$3kCpwukGAxvU5UBSp.h6MO8asDFLi\\/zTwY.dUqi\\/fFsHB\\/ZTy1ay6","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 10:24:27","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 10:24:27","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'a8b5fa9b053c27c107ff5fca78714de1a01568ffbf29e43ebd694882ea04dea67b521ee7f4b3fb742e7941b544a5b9fa0af2e37303b5fd09ea1320dae9ccac84', '2020-09-08 10:24:27', '2020-09-08 10:24:27'),
	(180, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$OLCCEDtdSjvfeSFxeh\\/mI.Uzg61kOKWIiICF9KmKGIA\\/Mz26BhD4m","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"Asia\\/Dhaka","is_logged":0,"last_login_at":"2020-09-08 10:24:27","last_login_ip":"182.16.157.232","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 04:41:05","updated_at":"2020-09-08 10:24:27","deleted_at":null}', '["password"]', '[]', '[]', 'http://odometer.erprnd.com/login', '182.16.157.232', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '2fd70ec2f43995985b2f774c2abb05159396c16cbe61a9b4289753bdf95c152392c2e234ff64c23ccbf6996a181d38abdf8a9c84f3dacc03bc37dec5edf866cd', '2020-09-08 10:24:27', '2020-09-08 10:24:27'),
	(181, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-11 14:38:12","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw","created_at":"2020-08-04 04:41:44","updated_at":"2020-09-11 14:38:12","deleted_at":null}', '["timezone","last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '10e8be6c41851bb9b04b516a7b38d0e92059e13d30da943d8112765267ea0e90d97acb863c61d87ae6477f0100cf039c0009904eb99c4fbf5fff5a16d8750464', '2020-09-11 08:38:12', '2020-09-11 08:38:12'),
	(182, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$e59TfnIdHX5szbAnl4YP2e9QOvGn3vv7zvqPcoGYsnZEXUOCWGxxK","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-11 14:38:12","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw","created_at":"2020-08-04 04:41:44","updated_at":"2020-09-11 14:38:13","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', 'dfc58d04b9d2982f92ea136309201c5dec26e34e383564ec94f8a446b71214bc1898e7798ccd4c69a8a674d8d97ad66c15d22ec8ed4aafd9268170467fc6b795', '2020-09-11 08:38:13', '2020-09-11 08:38:13'),
	(183, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$e59TfnIdHX5szbAnl4YP2e9QOvGn3vv7zvqPcoGYsnZEXUOCWGxxK","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-19 14:20:40","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw","created_at":"2020-08-04 04:41:44","updated_at":"2020-09-19 14:20:40","deleted_at":null}', '["last_login_at","updated_at"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '9d56893768dff4c8e1ed74c420df339dd3b1c50b374de3d7e33ce6f8e04274d0f5656ce5c7fe2f0bec5364ba21d29c4c79a042a41697b3f2e2a8e5e335e79b19', '2020-09-19 08:20:41', '2020-09-19 08:20:41'),
	(184, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$HYbe34AnTZ8W\\/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-19 14:20:40","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"ddnRuXzsK9ozVbhSqdJatOvobQVj5BsPsLl6Mpufk0a5wyLXXmplryoDkVGw","created_at":"2020-08-04 04:41:44","updated_at":"2020-09-19 14:20:42","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '1329551886a6360cadd5a01a07c25922f84874c9c2dfe55207e95fbdfd449864729a5c9d7d5cc8a3d6e8146594fe1d906433e7129ae93eeaf85940d09cbcf303', '2020-09-19 08:20:42', '2020-09-19 08:20:42'),
	(185, 'App\\Models\\User', 1, 'App\\Models\\User', 1, 4, 'updated', '{"id":1,"company_id":1,"first_name":"Super","last_name":"Admin","email":"root@admin.com","username":"root","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$HYbe34AnTZ8W\\/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe","password_changed_at":null,"active":1,"confirmation_code":"3886ede9f760a9238679af97c75e41b3","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-19 14:20:40","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"yiRGOdmgsQTwhNjACGuzte2MPThbB3S1Uw40JjUmkWzpLep6uJiVUxi5XQJb","created_at":"2020-08-04 04:41:44","updated_at":"2020-09-19 14:20:42","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.test/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '7153f49c5fa51b6404179f3b3ba1c1dd2ea9cdcffc170ec8d92f83c6e4918aadb3f352527a21479628913a3c6b1e05c307ba93a05c174d4cd416e980dacddd70', '2020-09-19 08:21:17', '2020-09-19 08:21:17'),
	(186, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$OLCCEDtdSjvfeSFxeh\\/mI.Uzg61kOKWIiICF9KmKGIA\\/Mz26BhD4m","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-19 14:21:33","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 10:41:05","updated_at":"2020-09-19 14:21:33","deleted_at":null}', '["timezone","last_login_at","last_login_ip","updated_at"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '0dcbe20af6a2a21a9f56206a21ac9fe4033794ad1f521bf7b351f632b55c8e1c6b4c3f957eca0d2b49df83d8fd0a6236b5bc82626d3111a45c76c02e7f9d1a9e', '2020-09-19 08:21:34', '2020-09-19 08:21:34'),
	(187, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-19 14:21:33","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"Q6I6CfwVNZVitFKFbF651thC9fullkhG4WPhxNuvHlFLW7aKS77FYegK3f0Z","created_at":"2020-08-06 10:41:05","updated_at":"2020-09-19 14:21:34","deleted_at":null}', '["password","updated_at"]', '[]', '[]', 'http://odometer.test/public/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '007d569979a58818a77c723d9efa40466e2b04daff0d1fe8b3c8c6666f362331360e61023f1d8fa30c5043b55d255ea183bde66ee0d8174d02b2828ae0f588d8', '2020-09-19 08:21:34', '2020-09-19 08:21:34'),
	(188, 'App\\Models\\User', 6, 'App\\Models\\User', 6, 4, 'updated', '{"id":6,"company_id":1,"first_name":"AIC","last_name":"BOG","email":"aicbog@tdcl.transcombd.com","username":"AICBOG","email_verified_at":null,"avatar_type":"photo","avatar_location":null,"password":"$2y$10$zN5nvSvDg0JEdD2\\/m0MfAu\\/7yW..hm2JPr7GtgUSB3LvooLeaYg0e","password_changed_at":null,"active":1,"confirmation_code":"80369097ebdd7d606a5bbf629cbf2565","confirmed":1,"timezone":"America\\/New_York","is_logged":0,"last_login_at":"2020-09-19 14:21:33","last_login_ip":"127.0.0.1","to_be_logged_out":0,"remember_token":"3Oo7ITWXMOS9QLnP6aIUIe0smx3JvWHgJLF6WsGWHVkITGSJz4GyFWGso2Hn","created_at":"2020-08-06 10:41:05","updated_at":"2020-09-19 14:21:34","deleted_at":null}', '["remember_token"]', '[]', '[]', 'http://odometer.test/public/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '9239d63488d12e85eaedd316495a352f9cf070c74db3ae42584973fadf4ec1909c4136f613d171d1b54c0630ef5163805c26c80e1c3ec16ed82958b49c18586d', '2020-09-19 08:39:02', '2020-09-19 08:39:02');
/*!40000 ALTER TABLE `ledgers` ENABLE KEYS */;

-- Dumping structure for table odometer.logbook_entries
CREATE TABLE IF NOT EXISTS `logbook_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `sl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'transaction no',
  `trans_date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL COMMENT 'NDM id',
  `employee_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `vehicle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_license_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_mnf_yr` int(11) NOT NULL COMMENT 'vehicle manufacturing year',
  `vehicle_purchase_yr` int(11) NOT NULL COMMENT 'vehicle purchase year',
  `vehicle_lifetime` int(11) NOT NULL COMMENT 'vehicle lifetime till date',
  `vehicle_used` int(11) NOT NULL COMMENT 'vehicle used (in days) as per action plan',
  `driver_id` int(11) NOT NULL,
  `driver_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_take_ovr_dt` date NOT NULL COMMENT 'vehicle key take over date',
  `driver_hand_ovr_dt` date NOT NULL COMMENT 'vehicle key hand over date',
  `rkm_data` decimal(10,4) NOT NULL COMMENT 'RKM VTS Data (KM)',
  `logbook_opening` decimal(10,4) NOT NULL COMMENT 'KM of Logbook opening',
  `logbook_closing` decimal(10,4) NOT NULL COMMENT 'KM of Logbook closing',
  `logbook_running` decimal(10,4) NOT NULL COMMENT 'KM of total running',
  `fuel_id` int(11) NOT NULL,
  `fuel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fuel_consumption` decimal(10,4) NOT NULL,
  `fuel_rate` decimal(10,4) NOT NULL,
  `fuel_cost` decimal(10,4) NOT NULL,
  `std_fuel_consumption` decimal(10,4) NOT NULL COMMENT 'standard_fuel_consumption',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `approved_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `posted` tinyint(4) NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `month_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logbook_entries_id_index` (`id`),
  KEY `logbook_entries_company_id_index` (`company_id`),
  KEY `logbook_entries_branch_id_index` (`branch_id`),
  KEY `logbook_entries_employee_id_index` (`employee_id`),
  KEY `logbook_entries_vehicle_id_index` (`vehicle_id`),
  KEY `logbook_entries_driver_id_index` (`driver_id`),
  KEY `logbook_entries_fuel_id_index` (`fuel_id`),
  KEY `logbook_entries_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.logbook_entries: ~0 rows (approximately)
DELETE FROM `logbook_entries`;
/*!40000 ALTER TABLE `logbook_entries` DISABLE KEYS */;
INSERT INTO `logbook_entries` (`id`, `company_id`, `sl`, `trans_date`, `branch_id`, `branch_name`, `employee_id`, `employee_name`, `vehicle_id`, `vehicle_name`, `vehicle_license_no`, `vehicle_mnf_yr`, `vehicle_purchase_yr`, `vehicle_lifetime`, `vehicle_used`, `driver_id`, `driver_name`, `driver_take_ovr_dt`, `driver_hand_ovr_dt`, `rkm_data`, `logbook_opening`, `logbook_closing`, `logbook_running`, `fuel_id`, `fuel_name`, `fuel_consumption`, `fuel_rate`, `fuel_cost`, `std_fuel_consumption`, `approved`, `approved_by`, `status`, `posted`, `year`, `month`, `month_name`, `order`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '1', '2020-08-01', 2, 'Motijheel', 2, 'Bashir Uddin', 2, 'Semi Truck', 'DHAKA-METRO-HA-244855', 2019, 2017, 2019, 2017, 4, 'Parvez', '2020-08-01', '2020-08-01', 30.0000, 12980.0000, 13015.0000, 35.0000, 1, 'Deisel', 45.0000, 86.0000, 3870.0000, 50.0000, 0, NULL, 0, 0, 2020, 8, '08', 1, 1, NULL, NULL, '2020-08-15 23:42:22', '2020-08-15 23:42:22', NULL);
/*!40000 ALTER TABLE `logbook_entries` ENABLE KEYS */;

-- Dumping structure for table odometer.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.migrations: ~33 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2017_09_03_144628_create_permission_tables', 1),
	(9, '2017_09_26_140332_create_cache_table', 1),
	(10, '2017_09_26_140528_create_sessions_table', 1),
	(11, '2017_09_26_140609_create_jobs_table', 1),
	(12, '2018_04_08_033256_create_password_histories_table', 1),
	(13, '2018_11_21_000001_create_ledgers_table', 1),
	(14, '2019_07_24_150207_setup_users_table', 1),
	(15, '2019_08_19_000000_create_failed_jobs_table', 1),
	(16, '2019_10_18_075255_create_companies_table', 1),
	(17, '2020_07_30_095141_create_activity_log_table', 1),
	(18, '2020_08_03_052921_create_branches_table', 1),
	(19, '2020_08_03_053025_create_user_branches_table', 1),
	(20, '2020_08_04_055416_create_employees_table', 2),
	(23, '2020_08_04_072329_create_drivers_table', 3),
	(24, '2020_08_04_072617_create_driver_documents_table', 3),
	(25, '2020_08_04_111420_create_fuels_table', 4),
	(26, '2020_08_04_115832_create_vehicles_table', 5),
	(27, '2020_08_05_050021_create_vehicle_types_table', 5),
	(28, '2020_08_05_051823_create_documents_types_table', 6),
	(29, '2020_08_05_062735_create_vehicle_documents_table', 7),
	(30, '2020_08_05_071436_create_vehicle_drivers_table', 8),
	(31, '2020_08_05_104908_create_fuel_rates_table', 9),
	(32, '2020_08_09_060246_create_logbook_entries', 10),
	(33, '2020_08_09_065317_add_columns_in_vehicle_tables', 11),
	(34, '2020_08_09_065341_create_vehicle_history_table', 11),
	(35, '2020_08_10_075022_create_employee_designations_table', 12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table odometer.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.model_has_permissions: ~2 rows (approximately)
DELETE FROM `model_has_permissions`;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Models\\User', 8),
	(7, 'App\\Models\\User', 8);
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table odometer.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.model_has_roles: ~7 rows (approximately)
DELETE FROM `model_has_roles`;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Models\\User', 1),
	(2, 'App\\Models\\User', 2),
	(3, 'App\\Models\\User', 3),
	(1, 'App\\Models\\User', 5),
	(3, 'App\\Models\\User', 6),
	(3, 'App\\Models\\User', 7),
	(3, 'App\\Models\\User', 8);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table odometer.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.oauth_access_tokens: ~0 rows (approximately)
DELETE FROM `oauth_access_tokens`;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table odometer.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.oauth_auth_codes: ~0 rows (approximately)
DELETE FROM `oauth_auth_codes`;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table odometer.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.oauth_clients: ~0 rows (approximately)
DELETE FROM `oauth_clients`;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table odometer.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.oauth_personal_access_clients: ~0 rows (approximately)
DELETE FROM `oauth_personal_access_clients`;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table odometer.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.oauth_refresh_tokens: ~0 rows (approximately)
DELETE FROM `oauth_refresh_tokens`;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table odometer.password_histories
CREATE TABLE IF NOT EXISTS `password_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_histories_user_id_foreign` (`user_id`),
  CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.password_histories: ~32 rows (approximately)
DELETE FROM `password_histories`;
/*!40000 ALTER TABLE `password_histories` DISABLE KEYS */;
INSERT INTO `password_histories` (`id`, `user_id`, `password`, `created_at`, `updated_at`) VALUES
	(1, 1, '$2y$10$duQ/Luo3Fp/vyBSWVv62ruiQ692QTgy31p5slAREJcflK/je.NE/q', '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(2, 2, '$2y$10$tFLTbB5ODvY6beF3M9cBNOoppQbGh0n6NWge.jzgwcdAFW7cPuFai', '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(3, 3, '$2y$10$HNAJwDaVyBR5.aK9t.FWMuyhfnk3RMbEh8ZjWYh9UlHWSbitSsV9C', '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(4, 1, '$2y$10$SzuFq.sLACSHq3wNyPF44uFVhlyJoi6ATJpHqNBBJyYN06DSjNWmG', '2020-08-03 22:41:59', '2020-08-03 22:41:59'),
	(6, 5, '$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL/cdyHSOdnbrDXc8O5cUh8kG', '2020-08-04 00:44:21', '2020-08-04 00:44:21'),
	(7, 1, '$2y$10$KRDoh1KESJR6980AgOs7suZZgdSW69mCoZUYqRdENPBWtGT6r20aq', '2020-08-04 22:34:03', '2020-08-04 22:34:03'),
	(8, 1, '$2y$10$3Ui3Jy6XfulyZqGRoZjIk.8o7KTms/8qOmPS0g7wb9QPB1WUVmCOC', '2020-08-06 04:37:51', '2020-08-06 04:37:51'),
	(9, 6, '$2y$10$yGvLOMBY7O54Jhm.lACnhuKg98U1nsIauDKL1v9nRo8QtUjJeJ5jW', '2020-08-06 04:41:05', '2020-08-06 04:41:05'),
	(10, 6, '$2y$10$Pt9DVapSDYhJ0pQ1J6pvVOQTHfn9sEPrg/qNpTlmCFfgN9nJtAhrq', '2020-08-06 05:00:35', '2020-08-06 05:00:35'),
	(11, 6, '$2y$10$/hOmYIlIw9.sOAcflz1Edu81aZ5lZO2FxguafMmAVUSw4LzxlFMbW', '2020-08-07 23:53:11', '2020-08-07 23:53:11'),
	(12, 1, '$2y$10$CNB/cqmB0kVIvcFRmo9ex.acoT8nRYwEJSyy3A8PS.CNJGoTPij0G', '2020-08-08 00:43:11', '2020-08-08 00:43:11'),
	(13, 6, '$2y$10$dwUVCeuc7NNSMJ2X02mgru1RyYC8aZH9JPHkobmLQ6eP7o9WaEJmW', '2020-08-08 00:46:29', '2020-08-08 00:46:29'),
	(14, 1, '$2y$10$ONvL9H/7.82TFGycadUVG.8kA7hPtXzgNxytUT8Es0FZTiEekz7qa', '2020-08-08 00:55:40', '2020-08-08 00:55:40'),
	(15, 6, '$2y$10$4kf7vMLhzGQKq3QEjiHyeei1cpc1PxdzSNbUDfbCQM2zinEAzUJ2q', '2020-08-08 00:56:34', '2020-08-08 00:56:34'),
	(16, 1, '$2y$10$Eqn4puXstk0B7kXpKHiJWuvg/xMVaa08HjaMe32vdCuQ5lWjx7b9.', '2020-08-09 00:51:49', '2020-08-09 00:51:49'),
	(17, 1, '$2y$10$TFz5OpeJVpDmaqhNCDSc5uiov6m0C30lw6ngWc6raurxaCwkk0cbq', '2020-08-12 03:39:06', '2020-08-12 03:39:06'),
	(18, 7, '$2y$10$XFe6dqOEiYVO0QLk9O8oqOxEOD50/uGlOVaa1fA9ruzdbqvokPwyK', '2020-08-15 22:47:42', '2020-08-15 22:47:42'),
	(19, 1, '$2y$10$9SMbOJqey/0LtobtUgObOuMjyciL0Mu22zqgSoGiAD/wI5vlNUZEe', '2020-09-08 04:33:03', '2020-09-08 04:33:03'),
	(20, 6, '$2y$10$a5TgAPwmTjJCPHYFh1970.Oc.sj9D6gZ8L/wSMS3bNURqZJNefOnW', '2020-09-08 04:34:59', '2020-09-08 04:34:59'),
	(21, 1, '$2y$10$Dst.N445dreP4kNI/Y1c6uQeZuZBcHj0wFFavbJ7WbZPCUXRwegCi', '2020-09-08 04:35:37', '2020-09-08 04:35:37'),
	(22, 1, '$2y$10$wIW7CLU4IxTIl4Oh4gnzDuT.fdJdDip1no0hDDzqtT1mWgro6xi8W', '2020-09-08 04:38:11', '2020-09-08 04:38:11'),
	(23, 6, '$2y$10$ujer8eZw2DJfBgxsSHPPxOesrcJ4yrxl9SQWEL2NH0mqFTXBjb84O', '2020-09-08 04:38:19', '2020-09-08 04:38:19'),
	(24, 1, '$2y$10$/43fgsfG48meVrs6WxOqIejm5KytE0qcCtG8.38j.4RprD1ttR5PC', '2020-09-08 08:09:27', '2020-09-08 08:09:27'),
	(25, 8, '$2y$10$5Wmsnn/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6', '2020-09-08 08:12:52', '2020-09-08 08:12:52'),
	(26, 1, '$2y$10$rReBoyduZYkwcdrQrfeR1OdONlMYQoQ90XgCo6oRFvgHEkSSxLnty', '2020-09-08 08:14:11', '2020-09-08 08:14:11'),
	(27, 1, '$2y$10$9GZaEEbVlmpCC5VyeDs4iOAEtYyX74xJenbI4LxHZIMsU.kME6iKi', '2020-09-08 08:14:16', '2020-09-08 08:14:16'),
	(28, 6, '$2y$10$oA2NimWB71sB0FpVwNcq4u5grOJuUIbs2ZwjU/MmKKHR95SsTYCyS', '2020-09-08 08:14:35', '2020-09-08 08:14:35'),
	(29, 1, '$2y$10$CEZLuaYf4dhBIRvd9qrFh.wyU6FmprR22C82jeie6rFjL31Dcffsa', '2020-09-08 08:28:47', '2020-09-08 08:28:47'),
	(30, 6, '$2y$10$3kCpwukGAxvU5UBSp.h6MO8asDFLi/zTwY.dUqi/fFsHB/ZTy1ay6', '2020-09-08 10:00:55', '2020-09-08 10:00:55'),
	(31, 6, '$2y$10$OLCCEDtdSjvfeSFxeh/mI.Uzg61kOKWIiICF9KmKGIA/Mz26BhD4m', '2020-09-08 10:24:27', '2020-09-08 10:24:27'),
	(32, 1, '$2y$10$e59TfnIdHX5szbAnl4YP2e9QOvGn3vv7zvqPcoGYsnZEXUOCWGxxK', '2020-09-11 08:38:14', '2020-09-11 08:38:14'),
	(33, 1, '$2y$10$HYbe34AnTZ8W/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe', '2020-09-19 08:20:42', '2020-09-19 08:20:42'),
	(34, 6, '$2y$10$zN5nvSvDg0JEdD2/m0MfAu/7yW..hm2JPr7GtgUSB3LvooLeaYg0e', '2020-09-19 08:21:34', '2020-09-19 08:21:34');
/*!40000 ALTER TABLE `password_histories` ENABLE KEYS */;

-- Dumping structure for table odometer.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table odometer.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.permissions: ~69 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'view backend', 'web', 1, '2020-08-03 22:41:45', '2020-08-03 22:41:45'),
	(2, 'setup-user-deactivated', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(3, 'setup-user-deleted', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(4, 'setup-user-index', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(5, 'setup-user-create', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(6, 'setup-user-store', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(7, 'setup-user-show', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(8, 'setup-user-edit', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(9, 'setup-user-mark', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(10, 'setup-user-delete', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(11, 'setup-user-delete-permanently', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(12, 'setup-user-restore', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(13, 'setup-user-login-as', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(14, 'setup-user-clear-session', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(15, 'setup-log-viewer-dashboard', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(16, 'setup-log-viewer-logs', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(17, 'setup-log-viewer-download', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(18, 'setup-log-viewer-delete', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(19, 'setup-role-index', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(20, 'setup-role-create', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(21, 'setup-role-store', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(22, 'setup-role-edit', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(23, 'setup-role-update', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(24, 'setup-role-destroy', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(25, 'setup-role-mark', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(26, 'setup-role-view-deleted', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(27, 'setup-role-view-deactivated', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(28, 'setup-permission-index', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(29, 'setup-permission-create', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(30, 'setup-permission-store', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(31, 'setup-permission-edit', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(32, 'setup-permission-update', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(33, 'setup-company-index', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(34, 'setup-company-create', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(35, 'setup-company-store', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(36, 'setup-company-show', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(37, 'setup-company-edit', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(38, 'setup-company-update', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(39, 'setup-company-delete', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(40, 'setup-company-destroy', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(41, 'setup-company-restore', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(42, 'setup-company-mark', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(43, 'setup-company-view-deactivated', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(44, 'setup-company-view-deleted', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(45, 'setup-api-clients', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(46, 'setup-api-tokens', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(47, 'setup-branch-index', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(48, 'setup-branch-create', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(49, 'setup-branch-store', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(50, 'setup-branch-show', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(51, 'setup-branch-edit', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(52, 'setup-branch-update', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(53, 'setup-branch-delete', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(54, 'setup-branch-destroy', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(55, 'setup-branch-restore', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(56, 'setup-branch-mark', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(57, 'setup-branch-view-deactivated', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(58, 'setup-branch-view-deleted', 'web', 1, '2019-06-28 05:34:01', '2019-06-28 05:34:01'),
	(59, 'transactions-fuel-rate-index', 'web', 1, '2020-08-06 04:43:48', '2020-08-06 04:43:48'),
	(60, 'transactions-fuel-rate-create', 'web', 1, '2020-08-06 04:45:25', '2020-08-06 04:45:25'),
	(61, 'transactions-fuel-rate-update', 'web', 1, '2020-08-06 04:45:35', '2020-08-06 04:45:35'),
	(62, 'transactions-fuel-rate-edit', 'web', 1, '2020-08-06 04:45:44', '2020-08-06 04:45:44'),
	(63, 'transactions-fuel-rate-store', 'web', 1, '2020-08-06 04:45:44', '2020-08-06 04:45:44'),
	(64, 'transactions-logbook-entry-create', 'web', 1, '2020-08-08 00:43:37', '2020-08-08 00:43:37'),
	(65, 'transactions-logbook-entry-store', 'web', 1, '2020-08-08 00:43:51', '2020-08-08 00:43:51'),
	(66, 'transactions-logbook-entry-edit', 'web', 1, '2020-08-08 00:44:01', '2020-08-08 00:44:01'),
	(67, 'transactions-logbook-entry-update', 'web', 1, '2020-08-08 00:44:10', '2020-08-08 00:44:10'),
	(68, 'transactions-logbook-entry-delete', 'web', 1, '2020-08-08 00:44:18', '2020-08-08 00:44:18'),
	(69, 'transactions-logbook-entry-index', 'web', 1, '2020-08-08 00:55:57', '2020-08-08 00:55:57');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table odometer.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.roles: ~4 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', 'web', 1, '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(2, 'administrator', 'web', 1, '2020-08-03 22:41:44', '2020-08-03 22:41:44'),
	(3, 'user', 'web', 1, '2020-08-03 22:41:45', '2020-08-03 22:41:45'),
	(4, 'facerole', 'web', 1, '2020-09-08 08:25:06', '2020-09-08 08:25:06');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table odometer.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.role_has_permissions: ~15 rows (approximately)
DELETE FROM `role_has_permissions`;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(1, 2),
	(1, 3),
	(59, 3),
	(60, 3),
	(61, 3),
	(62, 3),
	(63, 3),
	(64, 3),
	(65, 3),
	(66, 3),
	(67, 3),
	(68, 3),
	(69, 3),
	(1, 4),
	(2, 4);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Dumping structure for table odometer.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.sessions: ~0 rows (approximately)
DELETE FROM `sessions`;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table odometer.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '1',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `avatar_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'photo',
  `avatar_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '1',
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_logged` tinyint(1) NOT NULL DEFAULT '0',
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_company_id_index` (`company_id`),
  KEY `users_is_logged_index` (`is_logged`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.users: ~7 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `company_id`, `first_name`, `last_name`, `email`, `username`, `email_verified_at`, `avatar_type`, `avatar_location`, `password`, `password_changed_at`, `active`, `confirmation_code`, `confirmed`, `timezone`, `is_logged`, `last_login_at`, `last_login_ip`, `to_be_logged_out`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Super', 'Admin', 'root@admin.com', 'root', NULL, 'photo', NULL, '$2y$10$HYbe34AnTZ8W/0IYWElisuJXRDjWA7XE8vQbBEwPjOy09g7Ca9ISe', NULL, 1, '3886ede9f760a9238679af97c75e41b3', 1, 'America/New_York', 0, '2020-09-19 08:20:40', '127.0.0.1', 0, 'yiRGOdmgsQTwhNjACGuzte2MPThbB3S1Uw40JjUmkWzpLep6uJiVUxi5XQJb', '2020-08-03 22:41:44', '2020-09-19 08:20:42', NULL),
	(2, 1, 'Admin', 'Istator', 'admin@admin.com', 'admin', NULL, 'photo', NULL, '$2y$10$tFLTbB5ODvY6beF3M9cBNOoppQbGh0n6NWge.jzgwcdAFW7cPuFai', NULL, 1, '5b81902f59b14f18b943def25f90bf57', 1, NULL, 0, NULL, NULL, 0, NULL, '2020-08-03 22:41:44', '2020-08-03 22:41:44', NULL),
	(3, 1, 'Default', 'User', 'user@user.com', 'user', NULL, 'photo', NULL, '$2y$10$HNAJwDaVyBR5.aK9t.FWMuyhfnk3RMbEh8ZjWYh9UlHWSbitSsV9C', NULL, 1, '348f99993e519525907f257ebfbf7736', 1, NULL, 0, NULL, NULL, 0, NULL, '2020-08-03 22:41:44', '2020-08-03 22:41:44', NULL),
	(5, 1, 'Rejaul Islam', 'Royel', 'rejaul.islam@transcombd.com', 'almas', NULL, 'photo', NULL, '$2y$10$kMUtLUEWy7X.dnq0Et70BuvGs7RyhL/cdyHSOdnbrDXc8O5cUh8kG', NULL, 1, '29b575c492a20aafaad3a0aa89807a5f', 0, NULL, 0, NULL, NULL, 0, NULL, '2020-08-04 00:44:21', '2020-09-08 04:34:45', NULL),
	(6, 1, 'AIC', 'BOG', 'aicbog@tdcl.transcombd.com', 'AICBOG', NULL, 'photo', NULL, '$2y$10$zN5nvSvDg0JEdD2/m0MfAu/7yW..hm2JPr7GtgUSB3LvooLeaYg0e', NULL, 1, '80369097ebdd7d606a5bbf629cbf2565', 1, 'America/New_York', 0, '2020-09-19 08:21:33', '127.0.0.1', 0, '3Oo7ITWXMOS9QLnP6aIUIe0smx3JvWHgJLF6WsGWHVkITGSJz4GyFWGso2Hn', '2020-08-06 04:41:05', '2020-09-19 08:21:34', NULL),
	(7, 1, 'Bashir', 'Uddin', 'bashir@bashir.com', 'bashir', NULL, 'photo', NULL, '$2y$10$XFe6dqOEiYVO0QLk9O8oqOxEOD50/uGlOVaa1fA9ruzdbqvokPwyK', NULL, 1, '7aa9cdfb31ece02418f281eaf4d86869', 0, NULL, 0, NULL, NULL, 0, NULL, '2020-08-15 22:47:42', '2020-08-15 22:47:42', NULL),
	(8, 1, 'john1', 'due', 'john@gmail.com', 'john', NULL, 'photo', NULL, '$2y$10$5Wmsnn/7Y3dMQxfhOwNSrePIgW6ghqIT9KBWgj87tZiYaidkOpEj6', NULL, 1, '3e09600c3bec3dde2016d82ce64d0499', 1, NULL, 0, NULL, NULL, 0, NULL, '2020-09-08 08:12:52', '2020-09-08 08:21:51', '2020-09-08 08:21:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table odometer.user_branches
CREATE TABLE IF NOT EXISTS `user_branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_branches_id_index` (`id`),
  KEY `user_branches_user_id_index` (`user_id`),
  KEY `user_branches_branch_id_index` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.user_branches: ~36 rows (approximately)
DELETE FROM `user_branches`;
/*!40000 ALTER TABLE `user_branches` DISABLE KEYS */;
INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`) VALUES
	(1, 5, 2),
	(2, 5, 3),
	(3, 5, 5),
	(4, 6, 21),
	(5, 7, 2),
	(6, 7, 3),
	(7, 7, 4),
	(8, 7, 5),
	(9, 7, 6),
	(10, 7, 7),
	(11, 7, 8),
	(12, 7, 9),
	(13, 7, 10),
	(14, 7, 11),
	(15, 7, 12),
	(16, 7, 13),
	(17, 7, 14),
	(18, 7, 15),
	(19, 7, 16),
	(20, 7, 17),
	(21, 7, 18),
	(22, 7, 19),
	(23, 7, 20),
	(24, 7, 21),
	(25, 7, 22),
	(26, 7, 23),
	(27, 7, 24),
	(28, 7, 25),
	(29, 7, 26),
	(30, 7, 27),
	(31, 7, 28),
	(32, 7, 29),
	(33, 7, 30),
	(34, 7, 31),
	(35, 7, 32),
	(36, 8, 2);
/*!40000 ALTER TABLE `user_branches` ENABLE KEYS */;

-- Dumping structure for table odometer.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `vehicle_type` tinyint(4) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manufacturer_year` int(11) NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lifetime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_plate_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_year` int(11) NOT NULL,
  `is_dual_tank` tinyint(1) NOT NULL DEFAULT '0',
  `main_tank_fuel_id` int(11) NOT NULL,
  `main_tank_fuel_capacity` int(11) NOT NULL,
  `second_tank_fuel_id` int(11) DEFAULT NULL,
  `second_tank_fuel_capacity` int(11) DEFAULT NULL,
  `chassis_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `engine_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vin_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Vehicle identification no',
  `opening` decimal(10,4) NOT NULL,
  `purchase_date` date DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id_index` (`id`),
  KEY `vehicles_company_id_index` (`company_id`),
  KEY `vehicles_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.vehicles: ~4 rows (approximately)
DELETE FROM `vehicles`;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`id`, `company_id`, `vehicle_type`, `name`, `manufacturer`, `model`, `manufacturer_year`, `weight`, `lifetime`, `license_plate_no`, `license_year`, `is_dual_tank`, `main_tank_fuel_id`, `main_tank_fuel_capacity`, `second_tank_fuel_id`, `second_tank_fuel_capacity`, `chassis_no`, `engine_no`, `vin_no`, `opening`, `purchase_date`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, 4, 'Semi Truck', 'Runner', 'T51', 2019, '120 pound', NULL, 'DHAKA-METRO-HA-244855', 2017, 0, 1, 50, 4, 25, 'LEZZWAAADDTTTAAS', 'E11AAATT1', NULL, 12980.0000, '2017-11-08', 1, NULL, NULL, NULL, 1, 1, 1, NULL, '2020-08-05 00:30:19', '2020-08-15 23:33:15', NULL),
	(3, 1, 4, 'Semi Van', 'Runner', 'H51', 2009, NULL, NULL, 'MTL-DM-MA-51-3755', 2019, 0, 1, 9, 4, 5, 'LEZZWAAADDTTTAAS1', 'E11AAATT12', NULL, 11590.0000, '2017-08-01', 2, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-06 04:16:07', '2020-08-06 04:16:07', NULL),
	(6, 1, 3, 'Freezer Truck', 'Ace', 'AW22', 2017, '120 pound', NULL, 'DHAKA-METRO-HA-124522', 2017, 0, 1, 12, 4, 15, 'LEZZWAAADDTTTAAS11', 'E11AAATT121', NULL, 14123.0000, '2019-08-01', 3, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-09 05:23:50', '2020-08-09 05:23:50', NULL),
	(7, 1, 3, 'Semi Freezar Van Van', 'Toyota', 'AW22', 2009, '120 pound', '12', 'DHAKA-METRO-HA-124522', 2019, 0, 3, 35, 4, 34, 'LEZZWAAADDTTTAAS11', 'E11AAATT121', NULL, 12000.0000, '2020-08-15', 4, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-15 23:09:19', '2020-08-15 23:09:19', NULL);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping structure for table odometer.vehicle_documents
CREATE TABLE IF NOT EXISTS `vehicle_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `document_type` int(11) NOT NULL,
  `document_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `issuing_authority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_documents_id_index` (`id`),
  KEY `vehicle_documents_vehicle_id_index` (`vehicle_id`),
  KEY `vehicle_documents_document_type_index` (`document_type`),
  KEY `vehicle_documents_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.vehicle_documents: ~4 rows (approximately)
DELETE FROM `vehicle_documents`;
/*!40000 ALTER TABLE `vehicle_documents` DISABLE KEYS */;
INSERT INTO `vehicle_documents` (`id`, `vehicle_id`, `document_type`, `document_no`, `issue_date`, `expiry_date`, `issuing_authority`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 2, 3, 'aaaaa', '2020-08-05', '2020-08-05', 'BRTA', NULL, NULL, NULL, NULL, 0, 1, 1, NULL, '2020-08-05 00:51:58', '2020-08-09 05:51:50', NULL),
	(2, 7, 3, '1213123', '2020-08-16', '2020-08-16', '1231', 2, NULL, NULL, NULL, 0, 1, NULL, NULL, '2020-08-15 23:09:19', '2020-08-15 23:09:19', NULL),
	(3, 7, 4, '1qwqe', '2020-08-15', '2020-08-15', 'qweqwe', 3, NULL, NULL, NULL, 0, 1, NULL, NULL, '2020-08-15 23:09:19', '2020-08-15 23:09:19', NULL),
	(4, 7, 5, '1213', '2020-08-14', '2020-08-16', 'qweqe', 4, NULL, NULL, NULL, 0, 1, NULL, NULL, '2020-08-15 23:09:19', '2020-08-15 23:09:19', NULL);
/*!40000 ALTER TABLE `vehicle_documents` ENABLE KEYS */;

-- Dumping structure for table odometer.vehicle_drivers
CREATE TABLE IF NOT EXISTS `vehicle_drivers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `take_over` date NOT NULL,
  `hand_over` date DEFAULT NULL,
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_drivers_company_id_index` (`company_id`),
  KEY `vehicle_drivers_vehicle_id_index` (`vehicle_id`),
  KEY `vehicle_drivers_branch_id_index` (`branch_id`),
  KEY `vehicle_drivers_driver_id_index` (`driver_id`),
  KEY `vehicle_drivers_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.vehicle_drivers: ~3 rows (approximately)
DELETE FROM `vehicle_drivers`;
/*!40000 ALTER TABLE `vehicle_drivers` DISABLE KEYS */;
INSERT INTO `vehicle_drivers` (`id`, `company_id`, `vehicle_id`, `branch_id`, `driver_id`, `take_over`, `hand_over`, `order`, `custom1`, `custom2`, `custom3`, `archived`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 2, 2, 4, '2020-08-05', NULL, 2, NULL, NULL, NULL, 0, 1, 1, 1, NULL, '2020-08-05 01:48:17', '2020-08-05 03:17:02', NULL),
	(2, 1, 3, 2, 4, '2020-08-06', NULL, 2, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-08-06 04:16:33', '2020-08-06 04:16:33', NULL),
	(3, 1, 3, 2, 1, '2020-08-06', NULL, 3, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, '2020-08-06 04:17:48', '2020-08-06 04:17:48', NULL);
/*!40000 ALTER TABLE `vehicle_drivers` ENABLE KEYS */;

-- Dumping structure for table odometer.vehicle_histories
CREATE TABLE IF NOT EXISTS `vehicle_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `fuel_id` int(11) NOT NULL,
  `tanks` tinyint(4) NOT NULL COMMENT 'number of fuel tanks',
  `tank_type` tinyint(4) NOT NULL COMMENT '1=main or 2=secondary tank',
  `capacity` decimal(10,4) NOT NULL COMMENT 'fuel capacity',
  `millage` decimal(10,4) NOT NULL COMMENT 'standard millage',
  `order` int(11) NOT NULL,
  `custom1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_history_id_index` (`id`),
  KEY `vehicle_history_company_id_index` (`company_id`),
  KEY `vehicle_history_vehicle_id_index` (`vehicle_id`),
  KEY `vehicle_history_fuel_id_index` (`fuel_id`),
  KEY `vehicle_history_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.vehicle_histories: ~5 rows (approximately)
DELETE FROM `vehicle_histories`;
/*!40000 ALTER TABLE `vehicle_histories` DISABLE KEYS */;
INSERT INTO `vehicle_histories` (`id`, `company_id`, `entry_date`, `vehicle_id`, `fuel_id`, `tanks`, `tank_type`, `capacity`, `millage`, `order`, `custom1`, `custom2`, `custom3`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '2020-08-09', 6, 1, 1, 1, 12.0000, 9.0000, 1, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-09 05:23:50', '2020-08-09 05:23:50', NULL),
	(2, 1, '2020-08-09', 6, 4, 1, 2, 15.0000, 12.0000, 2, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-09 05:23:50', '2020-08-09 05:23:50', NULL),
	(3, 1, '2020-08-09', 2, 1, 1, 1, 9.0000, 12.0000, 3, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-09 05:51:50', '2020-08-09 05:51:50', NULL),
	(4, 1, '2020-08-13', 7, 3, 1, 1, 35.0000, 9.0000, 4, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-15 23:09:19', '2020-08-15 23:09:19', NULL),
	(5, 1, '2020-08-01', 2, 1, 1, 1, 50.0000, 15.0000, 5, NULL, NULL, NULL, 1, 1, NULL, NULL, '2020-08-15 23:33:15', '2020-08-15 23:33:15', NULL);
/*!40000 ALTER TABLE `vehicle_histories` ENABLE KEYS */;

-- Dumping structure for table odometer.vehicle_types
CREATE TABLE IF NOT EXISTS `vehicle_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_types_id_index` (`id`),
  KEY `vehicle_types_company_id_index` (`company_id`),
  KEY `vehicle_types_created_by_index` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table odometer.vehicle_types: ~4 rows (approximately)
DELETE FROM `vehicle_types`;
/*!40000 ALTER TABLE `vehicle_types` DISABLE KEYS */;
INSERT INTO `vehicle_types` (`id`, `company_id`, `name`, `order`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Car', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'Motorcycle', 2, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 'Truck', 3, 1, 1, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 'Van', 4, 1, 1, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `vehicle_types` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
