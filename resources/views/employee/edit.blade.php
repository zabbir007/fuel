@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->modelForm($module, 'PATCH', route($module_route.'.update', $module->id))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Company ID')->class('col-md-3 col-form-label col-form-label-sm')->for('company_id') }}

                        <div class="col-md-7">
                            {{ html()->select('company_id', $companies, $module->company_id)
                                ->class('form-control form-control-sm select2')}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Employee ID')->class('col-md-3 col-form-label col-form-label-sm')->for('employee_id') }}

                        <div class="col-md-7">
                            {{ html()->text('employee_code')
                                ->class('form-control form-control-sm')
                                ->placeholder('Employee Code')
                                ->attribute('maxlength', 20)}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('First Name')->class('col-md-3 col-form-label col-form-label-sm')->for('first_name') }}

                        <div class="col-md-7">
                            {{ html()->text('first_name')
                                ->class('form-control form-control-sm')
                                ->placeholder('Employee First Name')
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Last Name')->class('col-md-3 col-form-label col-form-label-sm')->for('last_name') }}

                        <div class="col-md-7">
                            {{ html()->text('last_name')
                                ->class('form-control form-control-sm')
                                ->placeholder('Employee Last Name')
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Designation')->class('col-md-3 col-form-label col-form-label-sm')->for('designation_id') }}

                        <div class="col-md-7">
                            {{ html()->select('designation_id', $designations)
                               ->class('form-control form-control-sm select2')}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Department')->class('col-md-3 col-form-label col-form-label-sm')->for('department') }}

                        <div class="col-md-7">
                            {{ html()->text('department')
                                ->class('form-control form-control-sm')
                                ->placeholder('Department')
                                ->attribute('maxlength', 20)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Grade')->class('col-md-3 col-form-label col-form-label-sm')->for('grade') }}

                        <div class="col-md-7">
                            {{ html()->text('grade')
                                ->class('form-control form-control-sm')
                                ->placeholder('Grade')
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Mobile')->class('col-md-3 col-form-label col-form-label-sm')->for('mobile_no') }}

                        <div class="col-md-7">
                            {{ html()->text('address2')
                                ->class('form-control form-control-sm')
                                ->placeholder('Mobile Number')
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Email')->class('col-md-3 col-form-label col-form-label-sm')->for('email_suffix') }}

                        <div class="col-md-7">
                            {{ html()->text('email')
                                ->class('form-control form-control-sm')
                                ->placeholder('Employee Email')
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Order')->class('col-md-3 col-form-label col-form-label-sm')->for('order') }}

                        <div class="col-md-7">
                            {{ html()->text('order')
                                ->class('form-control form-control-sm')
                                ->placeholder('Enter next order number, keep blank for auto') }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        {{ html()->label('Active')->class('col-md-2 col-form-label col-form-label-sm')->for('active') }}

                        <div class="col-md-10">
                            <label class="switch switch-label switch-pill switch-primary">
                                {{ html()->checkbox('active', true)->class('switch-input') }}
                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                            </label>
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->closeModelForm() }}
@endsection
