@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->modelForm($module, 'PATCH', route($module_route.'.update', $module->id))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Code')->class('col-md-3 col-form-label col-form-label-sm')->for('code') }}

                        <div class="col-md-6">
                            {{ html()->text('code')
                                ->class('form-control form-control-sm')
                                ->placeholder('Optional Code')
                                ->attribute('maxlength', 20)
                                ->disabled()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Name')->class('col-md-3 col-form-label col-form-label-sm')->for('name') }}

                        <div class="col-md-8">
                            {{ html()->text('name')
                                ->class('form-control form-control-sm')
                                ->placeholder('Branch Name')
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus()
                                ->disabled()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Short Name')->class('col-md-3 col-form-label col-form-label-sm')->for('short_name') }}

                        <div class="col-md-7">
                            {{ html()->text('short_name')
                                ->class('form-control form-control-sm')
                                ->placeholder('Branch Short Name')
                                ->attribute('maxlength', 20)
                                ->required()
                                 ->disabled()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Address1')->class('col-md-3 col-form-label col-form-label-sm')->for('address') }}

                        <div class="col-md-8">
                            {{ html()->text('address1')
                                ->class('form-control form-control-sm')
                                ->placeholder('Optional address')
                                ->attribute('maxlength', 191)
                                ->disabled()
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Address2')->class('col-md-3 col-form-label col-form-label-sm')->for('address') }}

                        <div class="col-md-7">
                            {{ html()->text('address2')
                                ->class('form-control form-control-sm')
                                ->placeholder('Optional address')
                                ->attribute('maxlength', 191)
                                ->disabled()
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Email')->class('col-md-3 col-form-label col-form-label-sm')->for('email_suffix') }}

                        <div class="col-md-8">
                            {{ html()->text('email')
                                ->class('form-control form-control-sm')
                                ->placeholder('Branch Email')
                                ->attribute('maxlength', 191)
                                ->disabled()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Order')->class('col-md-3 col-form-label col-form-label-sm')->for('order') }}

                        <div class="col-md-7">
                            {{ html()->text('order')
                                ->class('form-control form-control-sm')
                                ->placeholder('Enter next order number, keep blank for auto')
                                 ->disabled()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        {{ html()->label('Active')->class('col-md-2 col-form-label col-form-label-sm')->for('active') }}

                        <div class="col-md-10">
                            <label class="switch switch-label switch-pill switch-primary">
                                {{ html()->checkbox('active', true)->class('switch-input')->disabled() }}
                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                            </label>
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
        @endslot
    @endcomponent

    {{ html()->closeModelForm() }}
@endsection
