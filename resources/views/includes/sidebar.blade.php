<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
        {{--        <img src="{{ asset('img/AdminLTELogo.png') }}" alt="AdminLTE Logo"--}}
        <img src="{{URL::asset('/img/logo/logo.png')}}" class="img-responsive img-fluid center-block" alt="{{app_name()}}"
             class="brand-image img-responsive elevation-3"
             style="opacity: .8">
        {{--        <span class="brand-text font-weight-light">{{ app_name() }}</span>--}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-1 pb-1 mb-1 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{ $logged_in_user->full_name }} ( {{ $logged_in_user_branches->pluck('short_name')->implode(', ') }} )</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2 text-sm">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                {{--                <li class="nav-header p-1">@lang('menus.sidebar.general')</li>--}}
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}"
                       class="nav-link {{ active_class(request()->routeIs('dashboard')) }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>{{ __('Dashboard') }}</p>
                    </a>
                </li>

                @canany(['setup-user-index', 'setup-log-viewer-dashboard','setup-log-viewer-logs','setup-role-index','setup-permission-index','setup-api-clients','setup-api-tokens'])
                    {{--                    <li class="nav-header p-1 pt-3"><i class="fa fa-fw fa-terminal"></i>&nbsp;@lang('menus.sidebar.system')</li>--}}
                    <li class="nav-header p-1 pt-2">{{ __('Preferences') }}</li>

                    @canany(['setup-user-index', 'setup-role-index','setup-permission-index'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs(['user.*', 'role.*']), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('user.*')) }}">
                                <i class="nav-icon fas fa-users-cog"></i>
                                <p>
                                    @lang('menus.access.title')
                                    @if ($pending_approval > 0)
                                        <span class="right badge badge-danger">{{ $pending_approval }}</span>
                                    @endif
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-user-index')
                                    <li class="nav-item">
                                        <a href="{{ route('user.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('user.*')) }}">
                                            <i class="fas fa-users nav-icon"></i>
                                            <p>
                                                {{ __('User Manager') }}
                                                @if ($pending_approval > 0)
                                                    <span class="right badge badge-danger">{{ $pending_approval }}</span>
                                                @endif
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-role-index')
                                    <li class="nav-item ">
                                        <a href="{{ route('role.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('role.index')) }}">
                                            <i class="fas fa-user-lock nav-icon"></i>
                                            <p>{{ __('Role Manager') }}</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-permission-index')
                                    <li class="nav-item ">
                                        <a href="{{ route('permission.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('permission.*')) }}">
                                            <i class="fas fa-user-lock nav-icon"></i>
                                            <p>{{ __('Permission Manager') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany


                    @canany(['setup-api-clients','setup-api-tokens'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('passport.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('passport.*')) }}">
                                <i class="nav-icon fas fa-external-link-alt"></i>
                                <p>
                                    {{ __('API Management') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-api-clients')
                                    <li class="nav-item ">
                                        <a href="{{ route('passport.clients') }}"
                                           class="nav-link {{ active_class(request()->routeIs('passport.clients')) }}">
                                            <i class="fas fa-user-lock nav-icon"></i>
                                            <p>{{ __('API Clients') }}</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-api-tokens')
                                    <li class="nav-item ">
                                        <a href="{{ route('passport.tokens') }}"
                                           class="nav-link {{ active_class(request()->routeIs('passport.tokens')) }}">
                                            <i class="fas fa-user-lock nav-icon"></i>
                                            <p>{{ __('API Tokens') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany


                    @canany(['setup-log-viewer-dashboard','setup-log-viewer-logs'])
                        {{--                    <li class="nav-header p-1 pt-3"><i class="fa fa-fw fa-terminal"></i>&nbsp;@lang('menus.sidebar.system')</li>--}}
                        <li class="nav-header p-1 pt-2">{{ __('Log Manager') }}</li>
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('log-viewer::*'), ' menu-open') }}">
                            <a href="#"
                               class="nav-link {{ active_class(request()->routeIs('log-viewer::*'), 'active') }}">
                                <i class="nav-icon fas fa-user-secret"></i>
                                <p>
                                    {{ __('System Logs') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-log-viewer-dashboard')
                                    <li class="nav-item">
                                        <a href="{{ route('log-viewer::dashboard') }}"
                                           class="nav-link {{ active_class(request()->routeIs('log-viewer::dashboard')) }}">
                                            <i class="far fa-list-alt nav-icon"></i>
                                            <p> {{ __('Log Dashboard') }}</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-log-viewer-logs')
                                    <li class="nav-item ">
                                        <a href="{{ route('log-viewer::logs.list') }}"
                                           class="nav-link {{ active_class(request()->routeIs('log-viewer::logs.list')) }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p> {{ __('All Logs') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                @endcanany


                @canany(['setup-company-index', 'setup-company-create',
'setup-branch-index', 'setup-branch-create',
'setup-employee-index','setup-employee-create',
'setup-driver-index','setup-driver-create',
'setup-vehicle-index', 'setup-vehicle-create','setup-vehicle-driver-index',
])
                    <li class="nav-header p-1 pt-2">{{ __('Master Setup') }}</li>

                    @canany(['setup-company-index', 'setup-company-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.company.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.company.*')) }}">
                                <i class="nav-icon fas fa-industry"></i>
                                <p>
                                    {{ __('Company Manager') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-company-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.company.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.company.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Companies') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-company-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.company.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.company.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Company') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                    @canany(['setup-branch-index', 'setup-branch-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.branch.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.branch*')) }}">
                                <i class="fas fa-building nav-icon"></i>
                                <p>
                                    {{ __('Branch Manager') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-branch-assign')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.user_branch.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.user_branch.index')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('Assign Branch') }}</p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-branch-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.branch.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.branch.index')) }}">
                                            <i class="fas fa-building nav-icon "></i>
                                            <p>
                                                {{ __('All Branches') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-branch-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.branch.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.branch.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Branch') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                    @canany(['setup-employee-index', 'setup-employee-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.employee.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.employee*')) }}">
                                <i class="nav-icon fas fa-users-cog"></i>
                                <p>
                                    {{ __('Employee Manager') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-employee-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.employee.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.employee.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Employees') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-employee-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.employee.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.employee.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Employee') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                    @canany(['setup-driver-index', 'setup-driver-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.driver.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.driver*')) }}">
                                <i class="nav-icon fas fa-id-card"></i>
                                <p>
                                    {{ __('Driver Manager') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-driver-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.driver.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.driver.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Drivers') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-driver-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.driver.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.driver.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Driver') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                    @canany(['setup-vehicle-index', 'setup-vehicle-create', 'setup-vehicle-driver-index'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('master.vehicle.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('master.vehicle*')) }}">
                                <i class="nav-icon fas fa-truck"></i>
                                <p>
                                    {{ __('Vehicle Manager') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('setup-vehicle-driver-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.vehicle_driver.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.vehicle_driver.index')) }}">
                                            <i class="fas fa-user-check nav-icon"></i>
                                            <p>
                                                {{ __('Assign Driver') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-vehicle-index')
                                    <li class="nav-item">
                                        <a href="{{ route('master.vehicle.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.vehicle.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Vehicles') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('setup-vehicle-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('master.vehicle.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('master.vehicle.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Vehicle') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                @endcanany

                @canany([
    'transactions-fuel-rate-index', 'transactions-fuel-rate-create',
    'transactions-logbook-entry-index', 'transactions-logbook-entry-create',
    ])
                    <li class="nav-header p-1 pt-2">{{ __('Transactions') }}</li>

                    @canany(['transactions-fuel-rate-index', 'transactions-fuel-rate-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('transactions.fuel_rate.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('transactions.fuel_rate*')) }}">
                                <i class="nav-icon fas fa-oil-can"></i>
                                <p>
                                    {{ __('Fuel Rates') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('transactions-fuel-rate-index')
                                    <li class="nav-item">
                                        <a href="{{ route('transactions.fuel_rate.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('transactions.fuel_rate.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Fuel Rates') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('transactions-fuel-rate-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('transactions.fuel_rate.create') }}"
                                           class="nav-link {{ active_class(request()->routeIs('transactions.fuel_rate.create')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Fuel Rate') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany

                    @canany(['transactions-logbook-entry-index', 'transactions-logbook-entry-create'])
                        <li class="nav-item has-treeview {{ active_class(request()->routeIs('transactions.logbook_entry.*'), ' menu-open') }}">
                            <a href="#" class="nav-link {{ active_class(request()->routeIs('transactions.logbook_entry*')) }}">
                                <i class="nav-icon fas fa-newspaper warning"></i>
                                <p>
                                    {{ __('Logbook Entries') }}
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('transactions-logbook-entry-index')
                                    <li class="nav-item">
                                        <a href="{{ route('transactions.logbook_entry.index') }}"
                                           class="nav-link {{ active_class(request()->routeIs('transactions.logbook_entry.index')) }}">
                                            <i class="fas fa-list-alt nav-icon"></i>
                                            <p>
                                                {{ __('All Logbook Entries') }}
                                            </p>
                                        </a>
                                    </li>
                                @endcan

                                @can('transactions-logbook-entry-create')
                                    <li class="nav-item ">
                                        <a href="{{ route('transactions.logbook_entry.generate') }}"
                                           class="nav-link {{ active_class(request()->routeIs('transactions.logbook_entry.generate')) }}">
                                            <i class="fas fa-plus nav-icon"></i>
                                            <p>{{ __('New Logbook Entry') }}</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcanany
                @endcanany

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
