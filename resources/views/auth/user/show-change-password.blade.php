@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->form('PATCH', route('user.profile.change-password.post', $module))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')

        @endslot

        @slot('content')
            <div class="form-group row">
                {{ html()->label(__('Current Password'))->class('col-md-2 form-control-label form-control-label-sm')->for('current_password') }}

                <div class="col-md-10">
                    {{ html()->password('current_password')
                        ->class('form-control form-control-sm')
                        ->placeholder( __('Current Password'))
                        ->required()
                        ->autofocus() }}
                </div><!--col-->
            </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.access.users.password'))->class('col-md-2 form-control-label form-control-label-sm')->for('password') }}

                    <div class="col-md-10">
                        {{ html()->password('password')
                            ->class('form-control form-control-sm')
                            ->placeholder( __('validation.attributes.access.users.password'))
                            ->required()
                            ->autofocus() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('validation.attributes.access.users.password_confirmation'))->class('col-md-2 form-control-label form-control-label-sm')->for('password_confirmation') }}

                    <div class="col-md-10">
                        {{ html()->password('password_confirmation')
                            ->class('form-control form-control-sm')
                            ->placeholder( __('validation.attributes.access.users.password_confirmation'))
                            ->required() }}
                    </div><!--col-->
                </div><!--form-group-->
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route('user.profile'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
        {{ html()->closeModelForm() }}
    @endcomponent
@endsection
