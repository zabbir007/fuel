@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->form('POST', route($module_route.'.store'))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')

            <div class="form-group row">
                {{ html()->label(__('validation.attributes.access.roles.name'))
                    ->class('col-md-2 form-control-label form-control-label-sm')
                    ->for('name') }}

                <div class="col-md-5">
                    {{ html()->text('name')
                        ->class('form-control form-control-sm')
                        ->placeholder(__('validation.attributes.access.roles.name'))
                        ->attribute('maxlength', 191)
                        ->required()
                        ->autofocus() }}
                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group row">
                {{ html()->label(__('validation.attributes.access.roles.associated_permissions'))
                    ->class('col-md-2 form-control-label form-control-label-sm')
                    ->for('permissions') }}

                <div class="col-md-10">
                    @if($permissions->count())
                        @foreach($permissions as $permission)
                            <div class="checkbox d-flex align-items-center">
                                {{ html()->label(
                                        html()->checkbox('permissions[]', old('permissions') && in_array($permission->name, old('permissions')) ? true : false, $permission->name)
                                              ->class('switch-input')
                                              ->id('permission-'.$permission->id)
                                            . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                        ->class('switch switch-label switch-pill switch-primary mr-2')
                                    ->for('permission-'.$permission->id) }}
                                {{ html()->label(ucwords($permission->name))->for('permission-'.$permission->id) }}
                            </div>
                        @endforeach
                    @endif
                </div><!--col-->
            </div><!--form-group-->
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent
    {{ html()->form()->close() }}
@endsection
