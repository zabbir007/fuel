@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    <passport-clients></passport-clients>
    <passport-authorized-clients></passport-authorized-clients>
@endsection

@push('after-scripts')
    <script>
        new Vue({
            el: '#app',
        })
    </script>
@endpush

