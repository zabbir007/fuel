<!-- build {{ config('application.version') }} -->
<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Laravel Boilerplate')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/app.css')) }}

        @stack('after-styles')
    </head>

    <body class="hold-transition sidebar-mini text-sm">
    <div id="app">
        @include('includes.partials.read-only')
        @include('includes.partials.logged-in-as')
        <div class="wrapper">

            <!-- Navbar -->
        @include('includes.nav')
        <!-- /.navbar -->

            <!-- Main Sidebar Container -->
        @include('includes.sidebar')

        <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper py-3">
                <section class="content">
                    <div class="container-fluid">
                    @yield('content')

                    <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Control Sidebar -->
        @include('includes.aside')
        <!-- /.control-sidebar -->


            <!-- Main Footer -->
            @include('includes.footer')
        </div>
        <!-- ./wrapper -->
    </div>
    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/app.js')) !!}

    @include('includes.partials.messages')
    @stack('after-scripts')
    <script type="application/javascript">
        // Init datepicker (with .js-datepicker and .input-daterange class)
        jQuery('.datepicker:not(.datepicker-enabled)').each(function () {
            let el = jQuery(this);

            let enableDays = "{!! $enable_dates ?? ',' !!}".split(',');

            {{--// Add .js-datepicker-enabled class to tag it as activated--}}
            {{--el.addClass('js-datepicker-enabled');--}}

            {{--// Init--}}
            {{--el.daterangepicker({--}}
            {{--    --}}
            {{--    format: '{{ config('application.js_date_format') }}',--}}
            {{--    weekStart: el.data('week-start') || 0,--}}
            {{--    autoclose: el.data('autoclose') || false,--}}
            {{--    todayHighlight: el.data('today-highlight') || false,--}}
            {{--    orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported--}}
            {{--});--}}

            // Init
           // let el = $(".datepicker");
            el.daterangepicker({
                beforeShowDay: function (date) {
                    if (enableDays.indexOf(formatDate(date)) < 0)
                        return {
                            enabled: false
                        };
                    else
                        return {
                            enabled: true
                        }
                },
                maxDate: new Date(),
                endDate: new Date(),
                singleDatePicker: el.data('single-picker') || true,
                showDropdowns: el.data('show-dropdown') || true,
                autoApply: el.data('auto-apply') || true,
                autoclose: el.data('autoclose') || false,
                todayHighlight: el.data('today-highlight') || false,
                autoUpdateInput: el.data('auto-update') || false,
                locale: {
                    "format": el.data('format') || "MM/DD/YYYY",
                },
                orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY'));
            }).on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });


        });

        $(function () {
            // Init
            let el = $(".datepicker1");
            el.datepicker({
                endDate: new Date(),
                weekStart: el.data('week-start') || 0,
                autoclose: el.data('autoclose') || false,
                todayHighlight: el.data('today-highlight') || false,
                orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
            });
        });

        $(function () {
            // Init
            let el = $(".datepicker2");
            el.datepicker({
                weekStart: el.data('week-start') || 0,
                autoclose: el.data('autoclose') || false,
                todayHighlight: el.data('today-highlight') || false,
                orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
            });
        });
    </script>
    </body>
    </html>