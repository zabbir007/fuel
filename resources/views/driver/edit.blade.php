@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->modelForm($module, 'PATCH', route($module_route.'.update', $module->id))->acceptsFiles()->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="card card-primary card-tabs">
                <div class="card-header p-0 pt-1">
                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill"
                               href="#driver-info" role="tab" aria-controls="driver-info" aria-selected="true"><i
                                    class="fas fa-user"></i>&nbsp;Driver Info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#driver-docs"
                               role="tab" aria-controls="driver-docs" aria-selected="false"><i class="fas fa-file"></i>&nbsp;Documents</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                        <div class="tab-pane fade show active" id="driver-info" role="tabpanel"
                             aria-labelledby="driver-info">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Company ID')->class('col-md-3 col-form-label col-form-label-sm')->for('company_id') }}

                                        <div class="col-md-7">
                                            {{ html()->select('company_id', $companies, $logged_in_user->company_id)
                                                ->class('form-control form-control-sm select2')}}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Driver ID')->class('col-md-3 col-form-label col-form-label-sm')->for('employee_id') }}

                                        <div class="col-md-7">
                                            {{ html()->text('employee_id')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Driver ID')
                                                ->attribute('maxlength', 20)}}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Name')->class('col-md-3 col-form-label col-form-label-sm')->for('name') }}

                                        <div class="col-md-7">
                                            {{ html()->text('name')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Driver Name')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                                ->autofocus()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Mobile')->class('col-md-3 col-form-label col-form-label-sm')->for('mobile') }}

                                        <div class="col-md-7">
                                            {{ html()->text('mobile')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Mobile Number')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Date of Birth')->class('col-md-3 col-form-label col-form-label-sm')->for('dob') }}

                                        <div class="col-md-7">
                                            {{ html()->text('dob')
                                                ->class('form-control form-control-sm datepicker')
                                                ->placeholder('Date of birth')
                                                ->attribute('maxlength', 20)
                                               }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Date of Joining')->class('col-md-3 col-form-label col-form-label-sm')->for('doj') }}

                                        <div class="col-md-7">
                                            {{ html()->text('doj')
                                                ->class('form-control form-control-sm datepicker')
                                                ->placeholder('Date of joining')
                                                ->attribute('maxlength', 20)
                                                }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Blood Group')->class('col-md-3 col-form-label col-form-label-sm')->for('blood_group') }}

                                        <div class="col-md-7">
                                            {{ html()->text('blood_group')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Blood group')
                                                ->attribute('maxlength', 191)
                                                 }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Emergency Mobile')->class('col-md-3 col-form-label col-form-label-sm')->for('em_mobile') }}

                                        <div class="col-md-7">
                                            {{ html()->text('em_mobile')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Emergency Mobile Number')
                                                ->attribute('maxlength', 191)
                                                 }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Present Address')->class('col-md-3 col-form-label col-form-label-sm')->for('present_address') }}

                                        <div class="col-md-7">
                                            {{ html()->text('present_address')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Driver Present Address')
                                                ->attribute('maxlength', 191)
                                                 }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Permanent Address')->class('col-md-3 col-form-label col-form-label-sm')->for('permanent_address') }}

                                        <div class="col-md-7">
                                            {{ html()->text('permanent_address')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Driver Permanent Address')
                                                ->attribute('maxlength', 191)
                                                 }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Photo')->class('col-md-3 col-form-label col-form-label-sm')->for('photo') }}

                                        <div class="col-md-7">
                                            {{ html()->file('photo')
                                                ->class('form-control form-control-sm')
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Order')->class('col-md-3 col-form-label col-form-label-sm')->for('order') }}

                                        <div class="col-md-7">
                                            {{ html()->text('order')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Enter next order number, keep blank for auto') }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        {{ html()->label('Active')->class('col-md-2 col-form-label col-form-label-sm')->for('active') }}

                                        <div class="col-md-10">
                                            <label class="switch switch-label switch-pill switch-primary">
                                                {{ html()->checkbox('active', true)->class('switch-input') }}
                                                <span class="switch-slider" data-checked="yes"
                                                      data-unchecked="no"></span>
                                            </label>
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="driver-docs" role="tabpanel" aria-labelledby="driver-docs">

                            @foreach($module->documents as $document)
                                {{ html()->hidden('docs['.$document->id.'][id]', $document->id) }}

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Document Type')->class('col-md-3 col-form-label col-form-label-sm')->for('document_type') }}

                                            <div class="col-md-7">
                                                {{ html()->select('docs['.$document->id.'][document_type]', $document_types, $document->document_type)
                                                    ->class('form-control form-control-sm select2')
                                                    ->style('width: 100%')}}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Document No')->class('col-md-3 col-form-label col-form-label-sm')->for('document_no') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$document->id.'][document_no]', $document->document_no)
                                                    ->class('form-control form-control-sm')
                                                    ->placeholder('Document No')
                                                    ->attribute('maxlength', 191)
                                                 }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Issuing Authority')->class('col-md-3 col-form-label col-form-label-sm')->for('issuing_authority') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$document->id.'][issuing_authority]', $document->issuing_authority)
                                                    ->class('form-control form-control-sm')
                                                    ->placeholder('Document Issuing Authority')
                                                    ->attribute('maxlength', 191)
                                                 }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Issue Date')->class('col-md-3 col-form-label col-form-label-sm')->for('issue_date') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$document->id.'][issue_date]', $document->issue_date)
                                                    ->class('form-control form-control-sm datepicker')
                                                    ->placeholder('Issue date')
                                                   }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Expiry Date')->class('col-md-3 col-form-label col-form-label-sm')->for('expiry_date') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$document->id.'][expiry_date]', $document->expiry_date)
                                                    ->class('form-control form-control-sm datepicker')
                                                    ->placeholder('Expiry Date')
                                                    }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                </div>
                                <hr>
                            @endforeach

                                @for($i = $module->documents->count() + 1; $i<$module->documents->count() + 4; $i++)
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group row mb-2">
                                                {{ html()->label('Document Type')->class('col-md-3 col-form-label col-form-label-sm')->for('document_type') }}

                                                <div class="col-md-7">
                                                    {{ html()->select('new_docs['.$i.'][document_type]', $document_types)
                                                        ->class('form-control form-control-sm select2')
                                                        ->style('width: 100%')}}
                                                </div><!--col-->
                                            </div><!--form-group-->
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group row mb-2">
                                                {{ html()->label('Document No')->class('col-md-3 col-form-label col-form-label-sm')->for('document_no') }}

                                                <div class="col-md-7">
                                                    {{ html()->text('new_docs['.$i.'][document_no]')
                                                        ->class('form-control form-control-sm')
                                                        ->placeholder('Document No')
                                                        ->attribute('maxlength', 191)
                                                     }}
                                                </div><!--col-->
                                            </div><!--form-group-->
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group row mb-2">
                                                {{ html()->label('Issuing Authority')->class('col-md-3 col-form-label col-form-label-sm')->for('issuing_authority') }}

                                                <div class="col-md-7">
                                                    {{ html()->text('new_docs['.$i.'][issuing_authority]')
                                                        ->class('form-control form-control-sm')
                                                        ->placeholder('Document Issuing Authority')
                                                        ->attribute('maxlength', 191)
                                                     }}
                                                </div><!--col-->
                                            </div><!--form-group-->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group row mb-2">
                                                {{ html()->label('Issue Date')->class('col-md-3 col-form-label col-form-label-sm')->for('issue_date') }}

                                                <div class="col-md-7">
                                                    {{ html()->text('new_docs['.$i.'][issue_date]')
                                                        ->class('form-control form-control-sm datepicker')
                                                        ->placeholder('Issue date')
                                                       }}
                                                </div><!--col-->
                                            </div><!--form-group-->
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group row mb-2">
                                                {{ html()->label('Expiry Date')->class('col-md-3 col-form-label col-form-label-sm')->for('expiry_date') }}

                                                <div class="col-md-7">
                                                    {{ html()->text('new_docs['.$i.'][expiry_date]')
                                                        ->class('form-control form-control-sm datepicker')
                                                        ->placeholder('Expiry Date')
                                                        }}
                                                </div><!--col-->
                                            </div><!--form-group-->
                                        </div>
                                    </div>
                                    <hr>
                                @endfor
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>

        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->closeModelForm() }}
@endsection
