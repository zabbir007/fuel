@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->form('POST', route($module_route.'.store'))->class('form-horizontal')->open() }}

    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div id="logbook">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Entry Date')->class('col-3 col-form-label col-form-label-sm')->for('entry_date') }}

                            <div class="col-7">
                                {{ html()->text('entry_date')
                                   ->class('form-control form-control-sm datepicker')
                                   ->value($trans_date)
                                   ->required()
                                   ->readonly()
                                  }}
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Branch')->class('col-3 col-form-label col-form-label-sm')->for('branch_id') }}

                            <div class="col-7">
                                {{ html()->select('branch_id', $branches, $branch_id)
                                   ->class('form-control form-control-sm')
                                   ->required()
                                   ->readonly()
                                   }}
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('NDM')->class('col-3 col-form-label col-form-label-sm')->for('employee_name') }}

                            <div class="col-7">
                                {{ html()->text('employee_name')
                                       ->class('form-control form-control-sm')
                                      ->value($employee->name)
                                      ->readonly()
                                    }}

                                {{ html()->hidden('employee_id', $employee->id) }}
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Vehicle')->class('col-3 col-form-label col-form-label-sm')->for('vehicle_id') }}

                            <div class="col-7">
                                <select id="vehicle_id"
                                        class="form-control form-control-sm select2"
                                        name="vehicle_id"
                                        v-model="vehicles.selected.id"
                                        :readonly="vehicles.selectedAll">

                                    <option value="ALL" selected>Select a vehicle</option>
                                    <option v-for="vehicle in vehicles.all"
                                            :value="vehicle.id"
                                            v-text="vehicle.format_name"></option>
                                </select>
                            </div><!--col-->

                            {{ html()->hidden('vehicle_name')->attributes(['v-model'=>'vehicles.selected.name']) }}
                            {{ html()->hidden('vehicle_license_no')->attributes(['v-model'=>'vehicles.selected.license_plate_no']) }}
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Manf. Year')->class('col-4 col-form-label col-form-label-sm')->for('manufacturing_year') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('manufacturing_year')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Manufacturing Year')
                                        ->attributes([
                                            'v-model' =>'vehicles.selected.manufacturer_year'
])
                                        ->readonly()
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Year</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Purchase Year')->class('col-4 col-form-label col-form-label-sm')->for('purchase_year') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('purchase_year')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Year of purchase')
                                        ->attributes([
                                            'v-model' =>'vehicles.selected.purchase_year'
])
                                        ->readonly()
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Year</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Driver')->class('col-3 col-form-label col-form-label-sm')->for('driver_id') }}

                            <div class="col-7">
                                <select id="driver_id"
                                        class="form-control form-control-sm select2"
                                        name="driver_id"
                                        v-model="drivers.selected.id"
                                        :readonly="drivers.selectedAll && vehicles.selected != null">

                                    <option value="ALL" selected>Select a vehicle</option>
                                    <option v-for="driver in drivers.all"
                                            :value="driver.id"
                                            v-text="driver.name"></option>
                                </select>
                            </div><!--col-->
                            {{ html()->hidden('driver_name')->attributes(['v-model'=>'drivers.selected.name']) }}
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Take Ovr Dt')->class('col-4 col-form-label col-form-label-sm')->for('take_over_date') }}

                            <div class="col-6">
                                {{ html()->text('take_over_date')
                                       ->class('form-control form-control-sm datepicker')
                                       ->placeholder('Take over date')
                                       ->required()
                                       ->attributes([
                                             'v-model' => 'takeOverDt'
])
                                      }}

                            </div><!--col-->
                        </div><!--form-group-->
                    </div>

                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Hand Ovr Dt')->class('col-4 col-form-label col-form-label-sm')->for('hand_over_date') }}

                            <div class="col-6">
                                {{ html()->text('hand_over_date')
                                       ->class('form-control form-control-sm datepicker')
                                       ->placeholder('Hand over date')
                                       ->required()

                                      }}
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('RKM VTS Data')->class('col-4 col-form-label col-form-label-sm')->for('vts_data') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('vts_data')
                                       ->class('form-control form-control-sm')
                                       ->placeholder('RKM VTS Data (KM)')
                                       ->required()
                                         ->attributes(['step'=>'any'])
                                    }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">KM</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Fuel')->class('col-3 col-form-label col-form-label-sm')->for('fuel_id') }}

                            <div class="col-7">
                                <select id="fuel_id"
                                        class="form-control form-control-sm select2"
                                        name="fuel_id"
                                        v-model="fuels.selected.id"
                                        :readonly="fuels.selectedAll && fuels.selected != null">

                                    <option value="ALL" selected>Select a fuel</option>
                                    <option v-for="fuel in fuels.all"
                                            :value="fuel.id"
                                            v-text="fuel.name"></option>
                                </select>
                            </div><!--col-->
                            {{ html()->hidden('fuel_name')->attributes(['v-model'=>'fuels.selected.name']) }}
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Opening KM')->class('col-4 col-form-label col-form-label-sm')->for('opening_logbook') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('opening_logbook')
                                         ->class('form-control form-control-sm')
                                         ->placeholder('Opening KM of Logbook')
                                         ->readonly()
                                         ->required()
                                         ->attributes([
                                             'step'=>'any',
                                             'v-model' => 'vehicles.selected.opening'
])
                                      }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">KM</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Closing KM')->class('col-4 col-form-label col-form-label-sm')->for('closing_logbook') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('closing_logbook')
                                       ->class('form-control form-control-sm')
                                       ->placeholder('Closing KM of Logbook')
                                       ->required()
                                       ->attributes([
                                           'step'=>'any',
                                             'v-model' => 'vehicles.closingKm'
])
                                      }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">KM</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Total Running')->class('col-4 col-form-label col-form-label-sm')->for('total_running') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('total_running')
                                      ->class('form-control form-control-sm')
                                      ->placeholder('Total Running KM')
                                      ->attributes(['step'=>'any'])
                                      ->readonly()
                                      ->required()
                                      ->attributes([
                                           'step'=>'any',
                                             'v-model' => 'vehicles.totalKm'
])
                                      }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">KM</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Consumption')->class('col-4 col-form-label col-form-label-sm')->for('actual_consumption') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('actual_consumption')
                                         ->class('form-control form-control-sm')
                                         ->placeholder('Actual fuel consumption')
                                         ->required()
                                         ->attributes([
                                              'step'=>'any',
                                             'v-model' => 'fuelRates.fuelConsumption'
])
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Ltr</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Fuel Rate')->class('col-4 col-form-label col-form-label-sm')->for('fuel_rate') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('fuel_rate')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Fuel Rate')
                                        ->required()
                                        ->readonly()
                                         ->attributes([
                                              'step'=>'any',
                                             'v-model' => 'fuelRates.selected.rate'
])
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Taka/Ltr</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Fuel Cost')->class('col-4 col-form-label col-form-label-sm')->for('fuel_cost') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('fuel_cost')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Fuel Cost')
                                        ->required()
                                        ->readonly()
                                         ->attributes([
                                              'step'=>'any',
                                             'v-model' => 'fuelRates.fuelCost'
])
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Taka</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Standard RKM')->class('col-4 col-form-label col-form-label-sm')->for('standard_rkm') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('standard_rkm')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Standard RKM')
                                        ->required()
                                        ->readonly()
                                        ->attributes([
                                             'step'=>'any',
                                             'v-model' => 'vehicles.selected.main_tank_fuel_capacity'
])
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Ltr/CM</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Std. Consume')->class('col-4 col-form-label col-form-label-sm')->for('standard_consumption_qty') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('standard_consumption_qty')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Standard Fuel Consumption')
                                        ->required()
                                        ->readonly()
                                        ->attributes([
                                             'step'=>'any',
                                             'v-model' => 'vehicles.stdConsumeQty'
])
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Qty</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                    <div class="col-4">
                        <div class="form-group row mb-2">
                            {{ html()->label('Std. Consume')->class('col-4 col-form-label col-form-label-sm')->for('standard_consumption_taka') }}

                            <div class="col-6">
                                <div class="input-group input-group-sm">
                                    {{ html()->number('standard_consumption_taka')
                                        ->class('form-control form-control-sm')
                                        ->placeholder('Standard Fuel Consumption TK')
                                        ->required()
                                        ->readonly()
                                        ->attributes([
                                             'step'=>'any',
                                             'v-model' => 'vehicles.stdConsumeCost'
])
                                     }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">Tk</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>

                {{ html()->hidden('order', null) }}
                {{ html()->hidden('active', 1) }}
                {{ html()->hidden('approved', 0) }}
            </div>
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Submit', 'btn btn-primary btn-sm btn-block pull-right btn-flat')->name('submit')->id('submitBtn') }}
                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-check"></i>&nbsp;Submit & Post', 'btn btn-success btn-sm btn-block pull-right btn-flat')->name('post')->id('postBtn') }}
                </div><!--col-->

                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat')->id('cancelBtn') }}

                </div><!--col-->

            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->form()->close() }}
@endsection

@push('after-scripts')
    <script>
        new Vue({
            el: '#logbook',
            data: {
                loading: false,

                branchId: {{ $branch_id }},
                transDate: '{{ $trans_date }}',
                takeOverDt: '',
                handOverDt: '',

                // basic: {
                //     name: '',
                //     circularNo: '',
                //     // startDate: moment().format('DD-MMM-YYYY'),
                //     endDate: '',
                //     continues: false
                // },

                vehicles: {
                    all: [],
                    selected: [],
                    msg: 'All Vehicles Selected.',
                    closingKm: '',
                    totalKm: '',
                    stdConsumeQty: '',
                    stdConsumeCost: '',
                },

                vehicleHistories: {
                    all: [],
                    selected: [],
                    msg: 'All Vehicle Histories Selected.',
                },

                drivers: {
                    all: [],
                    selected: [],
                    msg: 'All Drivers Selected.',
                },

                fuels: {
                    all: [],
                    selected: [],
                    msg: 'All Fuels Selected.'
                },

                fuelRates: {
                    all: [],
                    selected: [],
                    fuelConsumption: '',
                    fuelCost: '',
                }
            },
            created: function () {
                this.loading = false;
                this.getVehicles();
                this.getFuels();
            },

            mounted: function () {
                $("#vehicle_id").select2(window.select2Configs).on("change", (e) => {
                    this.setVehicle(e);
                });
                $("#driver_id").select2(window.select2Configs).on("change", (e) => {
                  this.setDriver(e);
                });
                $("#fuel_id").select2(window.select2Configs).on("change", (e) => {
                    this.setFuel(e);
                });
            },

            methods: {
                //common
                selectOne(object, value, event) {
                    value.checked = event.currentTarget.checked;

                    object.selected = _.filter(object.all, ['checked', true]);
                    object.selectAll = false;
                    object.msg = '';
                },
                clearAll(object) {
                    object.selectedAll = true;
                    object.all = [];
                    object.selected = [];
                },

                //ajax requests
                getVehicles() {
                    this.loading = true;

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch_id: this.branchId,
                            trans_date: this.transDate
                        }
                    };

                    axios.get('{{ route($module_route.'.ajax.vehicles') }}', config)
                        .then((response) => {
                            this.loading = false;
                            if (response.data.length > 0) {
                                this.vehicles.msg = '';

                                _.forEach(response.data, (value) => {
                                    value.checked = true;
                                    this.vehicles.all.push(value);
                                    // this.vehicles.selected.push(value);
                                });
                            } else this.vehicles.msg = 'No Results!';

                        }, (error) => {
                            this.loading = false;

                            console.log(error);
                        })
                },
                getDrivers() {
                    this.loading = true;

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch_id: this.branchId,
                            vehicle_id: this.vehicles.selected.id,
                            trans_date: this.transDate
                        }
                    };

                    axios.get('{{ route($module_route.'.ajax.drivers') }}', config)
                        .then((response) => {
                            this.loading = false;
                            if (response.data.length > 0) {
                                this.drivers.msg = '';
                                this.drivers.all = [];
                                // this.drivers.selected = [];

                                _.forEach(response.data, (value) => {
                                    value.checked = true;
                                    this.drivers.all.push(value);
                                    // this.drivers.selected.push(value);
                                });
                            } else this.drivers.msg = 'No Results!';

                        }, (error) => {
                            this.loading = false;

                            console.log(error);
                        })
                },
                getFuels() {
                    this.loading = true;

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch_id: this.branchId,
                            trans_date: this.transDate
                        }
                    };
                    axios.get('{{ route($module_route.'.ajax.fuels') }}', config)
                        .then((response) => {
                            this.loading = false;
                            if (response.data.length > 0) {
                                this.fuels.msg = '';

                                _.forEach(response.data, (value) => {
                                    value.checked = true;
                                    this.fuels.all.push(value);
                                    // this.fuels.selected.push(value);
                                });
                            } else this.fuels.msg = 'No Results!';

                        }, (error) => {
                            this.loading = false;

                            console.log(error);
                        })
                },

                //changes
                setVehicle(ev){
                    let id = _.uniq($(ev.currentTarget).val());
                    this.vehicles.selected = _.find(this.vehicles.all, (v) => {
                        return v.id == id;
                    });

                    //set params
                    this.vehicleHistories.selected = _.head(this.vehicles.selected.histories);
                    this.vehicles.stdConsumeQty = this.vehicles.selected.main_tank_fuel_capacity;

                    //
                    // console.log(this.handOverDt);

                    this.takeOverDt = this.vehicleHistories.selected.take_over;
                    // this.handOverDt = this.vehicleHistories.selected.hand_over;

                    //call dependicies
                    this.getDrivers();
                    this.getVehicleHistory();
                },
                setDriver(ev){
                    let id = _.uniq($(ev.currentTarget).val());

                    this.drivers.selected = _.find(this.drivers.all, (v) => {
                        return v.id == id;
                    });

                    this.getVehicleHistory();
                },
                setFuel(ev){
                    let id = _.uniq($(ev.currentTarget).val());
                    this.fuels.selected = _.find(this.fuels.all, (v) => {
                        return v.id == id;
                    });

                    this.fuelRates.all = [];
                    this.fuelRates.all = this.fuels.selected.rates;

                    //set params
                    this.fuelRates.selected = _.find(this.fuelRates.all, (v) => {
                        return v.fuel_id == this.fuels.selected.id;
                    });

                    this.calculateFuelCost();
                },

                //calculations
                calculateFuelCost() {
                    this.fuelRates.fuelCost = this.fuelRates.fuelConsumption * parseFloat(this.fuelRates.selected.rate);
                    this.vehicles.stdConsumeCost = this.vehicles.stdConsumeQty * parseFloat(this.fuelRates.selected.rate);
                },
                getVehicleHistory(){
                   // console.log(this.vehicles.selected.histories);
                }
            },

            watch: {
                'fuelRates.fuelConsumption': {
                    handler: function (v) {
                        this.calculateFuelCost();
                    },
                    deep: true
                },
                'vehicles.closingKm': {
                    handler: function (v) {
                        this.vehicles.totalKm = v - this.vehicles.selected.opening;
                    },
                    deep: true
                }
            }
        });
    </script>
@endpush
