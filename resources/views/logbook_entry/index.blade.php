@extends('layouts.app')

@section('title', $page_heading)

@push('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endpush

@section('content')
    <div id="logbook">
        @component('components.block')
            @slot('main_heading', $main_heading)
            @slot('sub_heading', $sub_heading)
            @slot('icon', $module_icon)
            @slot('options')
                @include($module_view.'.header-buttons')
            @endslot

            @slot('content')
                <div class="table-responsive">
                    @component('components.table')
                        @slot('thead_content')
                            <tr>
                                <th>&nbsp;</th>
                                <th>{{ __('#') }}</th>
                                <th>{{ __('Trans. SL') }}</th>
                                <th>{{ __('Trans. Date') }}</th>
                                <th>{{ __('Branch') }}</th>
                                <th>{{ __('Vehicle') }}</th>
                                <th>{{ __('Vehicle License') }}</th>
                                <th>{{ __('Driver') }}</th>
                                <th>{{ __('Fuel') }}</th>
                                <th>{{ __('Closing KM') }}</th>
                                <th>{{ __('Consumption') }}</th>
                                <th>{{ __('Fuel Rate') }}</th>
                                <th>{{ __('Fuel Cost') }}</th>

                                <th>{{ __('Approved') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Actions') }}</th>
                            </tr>
                        @endslot
                    @endcomponent


                </div>
            @endslot

            @slot('footer')
                <div class="row">
                    <div class="col-2 text-right">
                        {{ form_submit('<i class="fas fa-check"></i>&nbsp;Post', 'btn btn-primary btn-sm btn-block pull-right btn-flat')->name('submit')->id('postBtn')->attribute(':disabled', 'disablePostBtn') }}
                    </div><!--col-->

                    <div class="col-2 text-right">
                        {{ form_submit('<i class="fas fa-check"></i>&nbsp;Approve', 'btn btn-success btn-sm btn-block pull-right btn-flat')->name('post')->id('approveBtn')->attribute(':disabled', 'disableApproveBtn') }}
                    </div><!--col-->


                </div><!--row-->
            @endslot
        @endcomponent
    </div>
@endsection

@push('after-scripts')
    <script>
        // let logbook = new Vue({
        //     el: '#logbook',
        //     data: {
        //         loading: false,
        //         dataTable: '',
        //         selectedIds: [],
        //
        //         disableApproveBtn: true,
        //         disablePostBtn: true,
        //
        //     },
        //     created: function () {
        //         this.loading = false;
        //
        //     },
        //
        //     mounted: function () {
        //
        //     },
        //
        //     methods: {
        //         //common
        //         selectOne(object, value, event) {
        //             value.checked = event.currentTarget.checked;
        //
        //             object.selected = _.filter(object.all, ['checked', true]);
        //             object.selectAll = false;
        //             object.msg = '';
        //         },
        //         clearAll(object) {
        //             object.selectedAll = true;
        //             object.all = [];
        //             object.selected = [];
        //         },
        //
        //         //
        //         selectTrans(ev){
        //             console.log(ev);
        //         }
        //     },
        //
        //     watch: {
        //         // 'fuelRates.fuelConsumption': {
        //         //     handler: function (v) {
        //         //         this.calculateFuelCost();
        //         //     },
        //         //     deep: true
        //         // },
        //         // 'vehicles.closingKm': {
        //         //     handler: function (v) {
        //         //         this.vehicles.totalKm = v - this.vehicles.selected.opening;
        //         //     },
        //         //     deep: true
        //         // }
        //     }
        // });

        $(function () {
            let dataTable = $('#datatable').dataTable($.extend(dataTableOptions, {
                processing: false,
                serverSide: true,
                // order: [[0, "desc"]],
                ajax: {
                    url: '{{ route($module_route.'.get') }}',
                    type: 'post',
                    data: {status: true, trashed: false},
                    error: function (xhr, err) {
                        console.log(err);
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'row_checkbox', sortable: false, className: 'text-center'},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', sortable: true},
                    {data: 'sl', sortable: true, className: 'text-center'},
                    {data: 'trans_date', searchable: true, sortable: true},
                    {data: 'branch_name', searchable: true, sortable: true},
                    {data: 'vehicle_name', searchable: true, sortable: true},
                    {data: 'vehicle_license_no', searchable: true, sortable: true},
                    {data: 'driver_name', searchable: true, sortable: true},
                    {data: 'fuel_name', searchable: true, sortable: true},
                    {data: 'logbook_closing', searchable: true, sortable: true},
                    {data: 'fuel_consumption', searchable: true, sortable: true},
                    {data: 'fuel_rate', searchable: true, sortable: true},
                    {data: 'fuel_cost', searchable: true, sortable: true},

                    {data: 'approve_label', searchable: true, sortable: true, className: 'text-center'},
                    {data: 'status_label', searchable: true, sortable: true, className: 'text-center'},
                    {data: 'actions', searchable: false, sortable: false, className: 'text-left'}
                ],
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                select: {
                    style: 'multi'
                }
            }));

            $("#postBtn").click(function (ev) {
                ev.preventDefault();

                let transIds = [];
                $("input:checkbox[name=trans_id]:checked").each(function () {
                    transIds.push($(this).val());
                });

                if (transIds.length == 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Please choose at least one row!'
                    })
                } else {
                    Swal.fire({
                        title: 'Confirm?',
                        showCancelButton: true,
                        confirmButtonText: 'Confirm Post',
                        cancelButtonText: 'Cancel',
                        type: 'warning'
                    }).then((result) => {
                        //api call
                        axios({
                            method: 'post',
                            url: '{{ route('transactions.logbook_entry.ajax.post') }}',
                            data: {
                                transIds: transIds,
                                _token: '{{ csrf_token() }}'
                            }
                        }).then(function (response) {
                            if (response.status == 200) {
                                toastr.success('Transactions has been posted!');
                            } else {
                                toastr.error('Failed to post transactions!');
                            }
                            location.reload();
                        })
                            .catch(function (error) {
                                console.log(error);
                            });
                    });
                }
            })

            $("#approveBtn").click(function (ev) {
                ev.preventDefault();

                let transIds = [];
                $("input:checkbox[name=trans_id]:checked").each(function () {
                    transIds.push($(this).val());
                });

                if (transIds.length == 0) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Please choose at least one row!'
                    })
                } else {
                    Swal.fire({
                        title: 'Confirm?',
                        showCancelButton: true,
                        confirmButtonText: 'Approve',
                        cancelButtonText: 'Cancel',
                        type: 'warning'
                    }).then((result) => {
                        //api call
                        axios({
                            method: 'post',
                            url: '{{ route('transactions.logbook_entry.ajax.approve') }}',
                            data: {
                                transIds: transIds,
                                _token: '{{ csrf_token() }}'
                            }
                        }).then(function (response) {
                            if (response.status == 200) {
                                toastr.success('Transactions has been approved!');
                            } else {
                                toastr.error('Failed to approve transactions!');
                            }

                            location.reload();
                        })
                            .catch(function (error) {
                                console.log(error);
                            });
                    });
                }
            })

            // .on( 'select', function ( e, dt, type, indexes ) {
            //     let rowData = dataTable.rows( indexes ).data().toArray();
            //     console.log(rowData);
            //
            //     logbook.selectTrans(rowData);
            // } )
            // .on( 'deselect', function ( e, dt, type, indexes ) {
            //     let rowData = dataTable.rows( indexes ).data().toArray();
            //     logbook.selectTrans(rowData);
            // } );
        });
    </script>
@endpush
