<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <div class="btn-group btn-group-sm">
        @can($module_permission.'-edit')
            @if(request()->routeIs('*.show'))
                <a href="{{ route($module_route.'.edit', $module) }}"
                   class="btn bg-primary btn-flat btn-outline-primary bg-gradient-primary mr-1"><i
                            class="fas fa-pen"></i>&nbsp;Update Entry</a>
            @endif
        @endcan

        @can($module_permission.'-create')
            <a href="{{ route($module_route.'.generate') }}"
               class="btn bg-success btn-flat btn-outline-success bg-gradient-success mr-1"><i
                        class="fas fa-plus-circle"></i>&nbsp;New Entry</a>
        @endcan

        @can($module_permission.'-index')
            <a href="{{ route($module_route.'.index') }}"
               class="btn bg-success btn-flat btn-outline-success bg-gradient-success mr-1">&nbsp;<i
                        class="fas fa-th-list"></i>&nbsp;All Entries</a>
        @endcan

    </div>
</div>
