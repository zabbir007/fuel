
@extends('layouts.app')

@section('title', $page_heading)


@section('content')
    {{ html()->form('GET', route($module_route.'.create'))->class('form-horizontal')->open() }}

    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-4">
                    <div class="form-group row mb-2">
                        {{ html()->label('Entry Date')->class('col-md-3 col-form-label col-form-label-sm')->for('trans_date') }}

                        <div class="col-md-7">
                            {{ html()->text('trans_date')
                               ->class('form-control form-control-sm datepicker')
                               ->value(now()->format('m/d/Y'))
                              }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-4">
                    <div class="form-group row mb-2">
                        {{ html()->label('Branch')->class('col-md-3 col-form-label col-form-label-sm')->for('branch_id') }}

                        <div class="col-md-7">
                            {{ html()->select('branch_id', $branches)
                               ->class('form-control form-control-sm select2')
                               ->required()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                {{ html()->hidden('sess_id', $sess_id) }}
                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-check"></i>&nbsp;Process', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div>

        @endslot

        @slot('footer')
            {{--            <div class="row">--}}
            {{--                <div class="col-3">--}}
            {{--                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}--}}

            {{--                </div><!--col-->--}}


            {{--            </div><!--row-->--}}
        @endslot
    @endcomponent

    {{ html()->form()->close() }}
@endsection