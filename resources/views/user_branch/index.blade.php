@extends('layouts.app')

@section('title', $page_heading)

@push('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endpush

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="table-responsive">
                @component('components.table')
                    @slot('thead_content')
                        <tr>
                            <th>{{ __('#') }}</th>
                            <th>{{ __('User') }}</th>
                            <th>{{ __('Branch(es)') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    @endslot

                    @slot('tbody_content')
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->full_name }}</td>
                                <td>{{ $user->branches->pluck('name')->implode(', ') }}</td>
                                <td>{!! $user->branch_actions !!}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

            </div>
        @endslot
    @endcomponent
@endsection

@push('after-scripts')

@endpush
