@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->modelForm($module, 'PATCH', route($module_route.'.update', $module->id))->class('form-horizontal')->open() }}
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('User')->class('col-md-3 col-form-label col-form-label-sm')->for('user') }}

                        <div class="col-md-7">
                            {{ html()->select('user_id', $users, $user_id)
                                ->class('form-control form-control-sm select2')
                                ->placeholder('Select an user')}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Branch(es)')->class('col-md-3 col-form-label col-form-label-sm')->for('branch_ids') }}

                        <div class="col-md-8">
                            {{ html()->select('branch_ids[]', $branches, $user_branches)
                               ->class('form-control form-control-sm select2')
                               ->placeholder('Select branch')
                               ->multiple()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>

            </div>
        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->closeModelForm() }}
@endsection
