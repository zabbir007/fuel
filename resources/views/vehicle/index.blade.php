@extends('layouts.app')

@section('title', $page_heading)

@push('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endpush

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="table-responsive">
                @component('components.table')
                    @slot('thead_content')
                        <tr>
                            <th>{{ __('#') }}</th>
                            <th>{{ __('Vehicle Name') }}</th>
                            <th>{{ __('Vehicle Type') }}</th>
                            <th>{{ __('Manufacturer') }}</th>
                            <th>{{ __('Manf. Year') }}</th>
                            <th>{{ __('License No') }}</th>
                            <th>{{ __('License Year') }}</th>
                            <th>{{ __('Tank Type') }}</th>
                            <th>{{ __('Chassis No') }}</th>
                            <th>{{ __('Engine No') }}</th>
                            <th>{{ __('Branch') }}</th>
                            <th>{{ __('Last Updated') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    @endslot
                @endcomponent

            </div>
        @endslot
    @endcomponent
@endsection

@push('after-scripts')
    <script>
        $(function () {
            $('#datatable').dataTable($.extend(dataTableOptions, {
                processing: true,
                serverSide: true,
               // order: [[0, "desc"]],
                ajax: {
                    url: '{{ route($module_route.'.get') }}',
                    type: 'post',
                    data: {status: true, trashed: false},
                    error: function (xhr, err) {
                        console.log(err);
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    // {data: 'DT_RowIndex', name:'DT_RowIndex', sortable: true  },
                    {data: 'id', name:'id', sortable: true, className: 'text-center'},
                    {data: 'name', searchable: true, sortable: true},
                    {data: 'type', searchable: true, sortable: true},
                    {data: 'manufacturer', searchable: true, sortable: true},
                    {data: 'manufacturer_year', searchable: true, sortable: true},
                    {data: 'license_plate_no', searchable: true, sortable: true},
                    {data: 'license_year', searchable: true, sortable: true},
                    {data: 'tank_type', searchable: true, sortable: true},
                    {data: 'chassis_no', searchable: true, sortable: true},
                    {data: 'engine_no', searchable: true, sortable: true},
                    {data: 'branches', searchable: true, sortable: true},
                    {data: 'updated_at', searchable: true, sortable: true},
                    {data: 'status_label', searchable: true, sortable: true, className: 'text-center'},
                    {data: 'actions', searchable: false, sortable: false, className: 'text-left'}
                ],
            }));
        });
    </script>
@endpush
