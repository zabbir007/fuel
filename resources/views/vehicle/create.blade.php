@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->form('POST', route($module_route.'.store'))->class('form-horizontal')->acceptsFiles()->open() }}

    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="card card-primary card-tabs">
                <div class="card-header p-0 pt-1">
                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill"
                               href="#vehicle-info" role="tab" aria-controls="vehicle-info" aria-selected="true"><i
                                    class="fas fa-shuttle-van"></i>&nbsp;Vehicle Info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#vehicle-docs"
                               role="tab" aria-controls="vehicle-docs" aria-selected="false"><i class="fas fa-file"></i>&nbsp;Documents</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#vehicle-millage"
                               role="tab" aria-controls="vehicle-docs" aria-selected="false"><i class="fas fa-oil-can"></i>&nbsp;Fuels</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                        <div class="tab-pane fade show active" id="vehicle-info" role="tabpanel"
                             aria-labelledby="vehicle-info">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Company ID')->class('col-md-3 col-form-label col-form-label-sm')->for('company_id') }}

                                        <div class="col-md-7">
                                            {{ html()->select('company_id', $companies, $logged_in_user->company_id)
                                                ->class('form-control form-control-sm select2')}}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Vehicle Type')->class('col-md-3 col-form-label col-form-label-sm')->for('vehicle_type') }}

                                        <div class="col-md-7">
                                            {{ html()->select('vehicle_type', $vehicle_types)
                                               ->class('form-control form-control-sm select2')
                                               ->required()}}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Name')->class('col-md-3 col-form-label col-form-label-sm')->for('name') }}

                                        <div class="col-md-7">
                                            {{ html()->text('name')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Vehicle Name')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                                ->autofocus()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Manufacturer')->class('col-md-3 col-form-label col-form-label-sm')->for('manufacturer') }}

                                        <div class="col-md-7">
                                            {{ html()->text('manufacturer')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Manf. Year')->class('col-md-3 col-form-label col-form-label-sm')->for('manufacturer_year') }}

                                        <div class="col-md-7">
                                            {{ html()->text('manufacturer_year')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Manufacturing Year')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Model')->class('col-md-3 col-form-label col-form-label-sm')->for('model') }}

                                        <div class="col-md-7">
                                            {{ html()->text('model')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Weight')->class('col-md-3 col-form-label col-form-label-sm')->for('weight') }}

                                        <div class="col-md-7">
                                            {{ html()->text('weight')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Lifetime')->class('col-md-3 col-form-label col-form-label-sm')->for('lifetime') }}

                                        <div class="col-md-7">
                                            {{ html()->text('lifetime')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('License No.')->class('col-md-3 col-form-label col-form-label-sm')->for('license_plate_no') }}

                                        <div class="col-md-7">
                                            {{ html()->text('license_plate_no')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('License Year')->class('col-md-3 col-form-label col-form-label-sm')->for('license_year') }}

                                        <div class="col-md-7">
                                            {{ html()->text('license_year')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Chassis No.')->class('col-md-3 col-form-label col-form-label-sm')->for('chassis_no') }}

                                        <div class="col-md-7">
                                            {{ html()->text('chassis_no')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Engine No.')->class('col-md-3 col-form-label col-form-label-sm')->for('engine_no') }}

                                        <div class="col-md-7">
                                            {{ html()->text('engine_no')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('VIN No.')->class('col-md-3 col-form-label col-form-label-sm')->for('vin_no') }}

                                        <div class="col-md-7">
                                            {{ html()->text('vin_no')
                                                ->class('form-control form-control-sm')
                                                ->attribute('maxlength', 191)
                                                ->placeholder('Vehicle Identification No')
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Purchase Date')->class('col-md-3 col-form-label col-form-label-sm')->for('purchase_date') }}

                                        <div class="col-md-7">
                                            {{ html()->text('purchase_date')
                                               ->class('form-control form-control-sm datepicker')
                                              }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Opening Millage')->class('col-md-3 col-form-label col-form-label-sm')->for('opening_millage') }}

                                        <div class="col-md-7">
                                            {{ html()->number('opening_millage')
                                                ->class('form-control form-control-sm')
                                                ->required()
                                             }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Order')->class('col-md-3 col-form-label col-form-label-sm')->for('order') }}

                                        <div class="col-md-7">
                                            {{ html()->text('order')
                                                ->class('form-control form-control-sm')
                                                ->placeholder('Enter next order number, keep blank for auto') }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        {{ html()->label('Active')->class('col-md-2 col-form-label col-form-label-sm')->for('active') }}

                                        <div class="col-md-10">
                                            <label class="switch switch-label switch-pill switch-primary">
                                                {{ html()->checkbox('active', true)->class('switch-input') }}
                                                <span class="switch-slider" data-checked="yes"
                                                      data-unchecked="no"></span>
                                            </label>
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="vehicle-docs" role="tabpanel" aria-labelledby="vehicle-docs">

                            @for($i = 0; $i<$document_types->count()+3; $i++)
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Document Type')->class('col-md-3 col-form-label col-form-label-sm')->for('document_type') }}

                                            <div class="col-md-7">
                                                {{ html()->select('docs['.$i.'][document_type]', $document_types)
                                                    ->class('form-control form-control-sm select2')
                                                    ->style('width: 100%')}}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Document No')->class('col-md-3 col-form-label col-form-label-sm')->for('document_no') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$i.'][document_no]')
                                                    ->class('form-control form-control-sm')
                                                    ->placeholder('Document No')
                                                    ->attribute('maxlength', 191)
                                                 }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Issuing Authority')->class('col-md-3 col-form-label col-form-label-sm')->for('issuing_authority') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$i.'][issuing_authority]')
                                                    ->class('form-control form-control-sm')
                                                    ->placeholder('Document Issuing Authority')
                                                    ->attribute('maxlength', 191)
                                                 }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Issue Date')->class('col-md-3 col-form-label col-form-label-sm')->for('issue_date') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$i.'][issue_date]')
                                                    ->class('form-control form-control-sm datepicker')
                                                    ->placeholder('Issue date')
                                                   }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row mb-2">
                                            {{ html()->label('Expiry Date')->class('col-md-3 col-form-label col-form-label-sm')->for('expiry_date') }}

                                            <div class="col-md-7">
                                                {{ html()->text('docs['.$i.'][expiry_date]')
                                                    ->class('form-control form-control-sm datepicker')
                                                    ->placeholder('Expiry Date')
                                                    }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    </div>
                                </div>
                                <hr>
                            @endfor
                        </div>
                        <div class="tab-pane fade" id="vehicle-millage" role="tabpanel" aria-labelledby="vehicle-millage">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row mb-2">
                                        {{ html()->label('Entry Date')->class('col-md-3 col-form-label col-form-label-sm')->for('entry_date') }}

                                        <div class="col-md-7">
                                            {{ html()->text('entry_date')
                                               ->class('form-control form-control-sm datepicker')
                                              }}
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        {{ html()->label('Dual Tank')->class('col-md-2 col-form-label col-form-label-sm')->for('is_dual_tank') }}

                                        <div class="col-md-7">
                                            <label class="switch switch-label switch-pill switch-primary">
                                                {{ html()->checkbox('is_dual_tank', false)->class('switch-input') }}
                                                <span class="switch-slider" data-checked="yes"
                                                      data-unchecked="no"></span>
                                            </label>
                                        </div><!--col-->
                                    </div><!--form-group-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card card-primary">
                                        <div class="card-header p-1">
                                            <h3 class="card-title">1. Main Fuel Tank</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div class="form-group row mb-2">
                                                        {{ html()->label('Fuel')->class('col-md-3 col-form-label col-form-label-sm')->for('main_tank_fuel_id') }}

                                                        <div class="col-md-9">
                                                            {{ html()->select('main_tank_fuel_id', $fuel_types)
                                                                ->class('form-control form-control-sm select2')
                                                                 ->required()}}
                                                        </div><!--col-->
                                                    </div><!--form-group-->
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group row mb-2">
                                                        {{ html()->label('Capacity')->class('col-md-4 col-form-label col-form-label-sm')->for('main_tank_fuel_capacity') }}

                                                        <div class="col-md-8">
                                                            <div class="input-group input-group-sm">
                                                                {{ html()->number('main_tank_fuel_capacity')
                                                               ->class('form-control form-control-sm')
                                                               ->placeholder('Main Tank Fuel Capacity')
                                                                ->required()}}
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Ltr</span>
                                                                </div>
                                                            </div>
                                                        </div><!--col-->
                                                    </div><!--form-group-->
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group row mb-2">
                                                        {{ html()->label('Standard Millage')->class('col-md-7 col-form-label col-form-label-sm')->for('millage') }}

                                                        <div class="col-md-5">
                                                            {{ html()->number('main_tank_millage')
                                                                ->class('form-control form-control-sm')
                                                                ->required()
                                                             }}
                                                        </div><!--col-->
                                                    </div><!--form-group-->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card card-primary">
                                        <div class="card-header p-1">
                                            <h3 class="card-title">2. Secondary Fuel Tank</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-3">
                                                    <div class="form-group row mb-2">
                                                        {{ html()->label('Fuel')->class('col-md-3 col-form-label col-form-label-sm')->for('main_tank_fuel_id') }}

                                                        <div class="col-md-9">
                                                            {{ html()->select('second_tank_fuel_id', $fuel_types)
                                                                ->class('form-control form-control-sm select2')}}
                                                        </div><!--col-->
                                                    </div><!--form-group-->
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group row mb-2">
                                                        {{ html()->label('Capacity')->class('col-md-4 col-form-label col-form-label-sm')->for('main_tank_fuel_capacity') }}

                                                        <div class="col-md-8">
                                                            <div class="input-group input-group-sm">
                                                                {{ html()->number('second_tank_fuel_capacity')
                                                               ->class('form-control form-control-sm')
                                                               ->placeholder('Secondary Tank Fuel Capacity')
                                                                }}
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Ltr</span>
                                                                </div>
                                                            </div>
                                                        </div><!--col-->
                                                    </div><!--form-group-->
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group row mb-2">
                                                        {{ html()->label('Standard Millage')->class('col-md-7 col-form-label col-form-label-sm')->for('second_tank_millage') }}

                                                        <div class="col-md-5">
                                                            {{ html()->number('second_tank_millage')
                                                                ->class('form-control form-control-sm')
                                                                ->required()
                                                             }}
                                                        </div><!--col-->
                                                    </div><!--form-group-->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>

        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->form()->close() }}
@endsection
