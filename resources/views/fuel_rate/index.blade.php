@extends('layouts.app')

@section('title', $page_heading)

@push('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endpush

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="table-responsive">
                @component('components.table')
                    @slot('thead_content')
                        <tr>
                            <th>{{ __('#') }}</th>
                            <th>{{ __('Branch') }}</th>
                            @foreach($fuels as $id => $fuel)
                                <th>{{ __($fuel) }}</th>
                            @endforeach
                            <th>{{ __('Entry Date') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    @endslot

                    @slot('tbody_content')
                        @foreach($all_fuel_rates as $fuel_rates)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $fuel_rates->first()->branch->name }}</td>

                                @foreach($fuels as $id => $fuel)
                                    <td>{{ _money_format($fuel_rates->where('fuel_id', $id)->where('branch_id', $fuel_rates->first()->branch_id)->first()->rate) }}</td>
                                @endforeach

                                <td>{!! $fuel_rates->first()->entry_date->format('m/d/Y') !!}</td>
                                <td>{!! $fuel_rates->first()->status_label !!}</td>
                                <td>{!! $fuel_rates->first()->actions !!}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

            </div>
        @endslot
    @endcomponent
@endsection

@push('after-scripts')
    <script>
        $(function () {
            $('#datatable').dataTable($.extend(dataTableOptions, {
                processing: false,
                serverSide: false,
                // order: [[0, "desc"]],
            }));
        });
    </script>
@endpush
