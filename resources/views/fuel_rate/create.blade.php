@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->form('POST', route($module_route.'.store'))->class('form-horizontal')->open() }}

    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Branch')->class('col-md-3 col-form-label col-form-label-sm')->for('branch_id') }}

                        <div class="col-md-7">
                            {{ html()->select('branch_id', $branches)
                               ->class('form-control form-control-sm select2')
                               ->required()}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Entry Date')->class('col-md-3 col-form-label col-form-label-sm')->for('entry_date') }}

                        <div class="col-md-7">
                            {{ html()->text('entry_date')
                               ->class('form-control form-control-sm datepicker')
                               ->value(now()->format('m/d/Y'))
                              }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            @foreach($fuels as $id => $fuel)
                {{ html()->hidden('rates['.$id.'][id]', $id) }}
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label($fuel . ' Rate')->class('col-md-3 col-form-label col-form-label-sm')->for('fuel_rate') }}

                        <div class="col-md-7">
                            <div class="input-group input-group-sm">
                                {{ html()->number('rates['.$id.'][rate]')
                                ->class('form-control form-control-sm')
                                ->placeholder('Enter '.$fuel . ' Rate')
                                ->attributes([
                                    'step' => 'any'
])
                             }}
                                <div class="input-group-append">
                                    <span class="input-group-text">Taka/Ltr</span>
                                </div>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
            @endforeach

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Order')->class('col-md-3 col-form-label col-form-label-sm')->for('order') }}

                        <div class="col-md-7">
                            {{ html()->text('order')
                                ->class('form-control form-control-sm')
                                ->placeholder('Enter next order number, keep blank for auto') }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        {{ html()->label('Active')->class('col-md-2 col-form-label col-form-label-sm')->for('active') }}

                        <div class="col-md-10">
                            <label class="switch switch-label switch-pill switch-primary">
                                {{ html()->checkbox('active', true)->class('switch-input') }}
                                <span class="switch-slider" data-checked="yes"
                                      data-unchecked="no"></span>
                            </label>
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->form()->close() }}
@endsection
