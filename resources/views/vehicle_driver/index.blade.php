@extends('layouts.app')

@section('title', $page_heading)

@push('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endpush

@section('content')
    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="table-responsive">
                @component('components.table')
                    @slot('thead_content')
                        <tr>
                            <th>{{ __('#') }}</th>
                            <th>{{ __('Vehicle') }}</th>
                            <th>{{ __('Branch') }}</th>
                            <th>{{ __('Driver') }}</th>
                            <th>{{ __('Take Over Date') }}</th>
                            <th>{{ __('Hand Over Date') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    @endslot

                    @slot('tbody_content')
                        @foreach($vehicles as $vehicle)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $vehicle->vehicle->format_name }}</td>
                                <td>{{ $vehicle->branch->name }}</td>
                                <td>{{ $vehicle->driver->name }}</td>
                                <td>{{ $vehicle->take_over }}</td>
                                <td>{{ $vehicle->hand_over }}</td>
                                <td>{!! $vehicle->status_label !!}</td>
                                <td>{!! $vehicle->actions !!}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

            </div>
        @endslot
    @endcomponent
@endsection

@push('after-scripts')
    <script>
        $(function () {
            $('#datatable').dataTable($.extend(dataTableOptions, {
                processing: false,
                serverSide: false,
            }));
        });
    </script>
@endpush
