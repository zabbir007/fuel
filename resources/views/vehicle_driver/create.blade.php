@extends('layouts.app')

@section('title', $page_heading)

@section('content')
    {{ html()->form('POST', route($module_route.'.store'))->class('form-horizontal')->open() }}

    @component('components.block')
        @slot('main_heading', $main_heading)
        @slot('sub_heading', $sub_heading)
        @slot('icon', $module_icon)
        @slot('options')
            @include($module_view.'.header-buttons')
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Branch')->class('col-md-3 col-form-label col-form-label-sm')->for('branch_id') }}

                        <div class="col-md-7">
                            {{ html()->select('branch_id', $branches)
                               ->class('form-control form-control-sm select2')
                               ->placeholder('Select branch')}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>

            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Vehicle')->class('col-md-3 col-form-label col-form-label-sm')->for('vehicle_id') }}

                        <div class="col-md-7">
                            {{ html()->select('vehicle_id', $vehicles)
                                ->class('form-control form-control-sm select2')
                                ->placeholder('Select an vehicle')}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Driver')->class('col-md-3 col-form-label col-form-label-sm')->for('driver_id') }}

                        <div class="col-md-7">
                            {{ html()->select('driver_id', $drivers)
                                ->class('form-control form-control-sm select2')
                                ->placeholder('Select an driver')}}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Take Over Date')->class('col-md-3 col-form-label col-form-label-sm')->for('take_over') }}

                        <div class="col-md-7">
                            {{ html()->text('take_over')
                                ->class('form-control form-control-sm datepicker')
                                ->placeholder('Date of take over')
                                ->attribute('maxlength', 20)
                               }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
                <div class="col-6">
                    <div class="form-group row mb-2">
                        {{ html()->label('Hand Over Date')->class('col-md-3 col-form-label col-form-label-sm')->for('hand_over') }}

                        <div class="col-md-7">
                            {{ html()->text('hand_over')
                                ->class('form-control form-control-sm datepicker')
                                ->placeholder('Date of hand over')
                                ->attribute('maxlength', 20)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>

        @endslot

        @slot('footer')
            <div class="row">
                <div class="col-3">
                    {{ form_cancel(route($module_route.'.index'), '<i class="fa fa-arrow-left"></i>&nbsp;Cancel', 'btn btn-danger btn-sm btn-block btn-flat') }}

                </div><!--col-->

                <div class="col-3 text-right">
                    {{ form_submit('<i class="fas fa-save"></i>&nbsp;Save Changes', 'btn btn-success btn-sm btn-block pull-right btn-flat') }}
                </div><!--col-->
            </div><!--row-->
        @endslot
    @endcomponent

    {{ html()->form()->close() }}
@endsection
