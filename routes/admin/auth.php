<?php

// All route names are prefixed with 'admin.auth'.
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\PassportAccessController;
use App\Http\Controllers\Auth\PasswordExpiredController;
use App\Http\Controllers\Auth\Permission\PermissionController;
use App\Http\Controllers\Auth\Permission\PermissionTableController;
use App\Http\Controllers\Auth\Role\RoleController;
use App\Http\Controllers\Auth\Role\RoleStatusController;
use App\Http\Controllers\Auth\Role\RoleTableController;
use App\Http\Controllers\Auth\UpdatePasswordController;
use App\Http\Controllers\Auth\User\UserController;
use App\Http\Controllers\Auth\User\UserPasswordController;
use App\Http\Controllers\Auth\User\UserProfileController;
use App\Http\Controllers\Auth\User\UserSessionController;
use App\Http\Controllers\Auth\User\UserStatusController;
use App\Http\Controllers\Auth\User\UserTableController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;


Route::redirect('/', 'dashboard', 301)->name('index');
//Route::get('/', [DashboardController::class, 'index'])->name('index');
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
    // These routes require the user to be logged in
    Route::group(['middleware' => 'auth'], function () {
        Route::get('logout', [LoginController::class, 'logout'])->name('logout');

        // These routes can not be hit if the password is expired
        Route::group(['middleware' => 'password_expires'], function () {
            // Change Password Routes
            Route::patch('password/update', [UpdatePasswordController::class, 'update'])->name('password.update');
        });

        // Password expired routes
        Route::get('password/expired', [PasswordExpiredController::class, 'expired'])->name('password.expired');
        Route::patch('password/expired', [PasswordExpiredController::class, 'update'])->name('password.expired.update');
    });
});

Route::group(['middleware' => 'auth'], function () {
    //  Route::get('/', [DashboardController::class, 'index'])->name('index');

    Route::group(['namespace' => 'User', 'as' => 'user.', 'middleware' => 'password_expires'], function () {
        // User Dashboard Specific
        //   Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Profile Specific
        Route::get('profile', [UserProfileController::class, 'show'])->name('profile');
        Route::get('profile/edit', [UserProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('profile/update', [UserProfileController::class, 'update'])->name('profile.update');

        // Password
        Route::get('profile/password/change', [UserProfileController::class, 'showPassword'])->name('profile.change-password');
        Route::patch('profile/password/change', [UserProfileController::class, 'postPassword'])->name('profile.change-password.post');
    });
});

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'role:'.config('access.users.superadmin_role')
], function () {
    // User Management
    Route::group(['namespace' => 'User'], function () {
        // For DataTables
        Route::post('user/get', [UserTableController::class, 'datatable'])->name('user.get');

        // User Status'
        Route::get('user/deactivated', [UserStatusController::class, 'getDeactivated'])->name('user.deactivated');
        Route::get('user/deleted', [UserStatusController::class, 'getDeleted'])->name('user.deleted');

        // User CRUD
        Route::get('user', [UserController::class, 'index'])->name('user.index');
        Route::get('user/create', [UserController::class, 'create'])->name('user.create');
        Route::post('user', [UserController::class, 'store'])->name('user.store');

        // Specific User
        Route::group(['prefix' => 'user/{user}'], function () {
            // User
            Route::get('/', [UserController::class, 'show'])->name('user.show');
            Route::get('edit', [UserController::class, 'edit'])->name('user.edit');
            Route::patch('/', [UserController::class, 'update'])->name('user.update');
            Route::delete('/', [UserController::class, 'destroy'])->name('user.destroy');


            // Status
            Route::get('mark/{status}',
                [UserStatusController::class, 'mark'])->name('user.mark')->where(['status' => '[0,1]']);

            // Password
            Route::get('password/change', [UserPasswordController::class, 'edit'])->name('user.change-password');
            Route::patch('password/change',
                [UserPasswordController::class, 'update'])->name('user.change-password.post');

            // Session
            Route::get('clear-session', [UserSessionController::class, 'clearSession'])->name('user.clear-session');

            // Deleted
            Route::get('delete', [UserStatusController::class, 'delete'])->name('user.delete-permanently');
            Route::get('restore', [UserStatusController::class, 'restore'])->name('user.restore');
        });
    });

    // Role Management
    Route::group(['namespace' => 'Role'], function () {
        // For DataTables
        Route::post('role/get', [RoleTableController::class, 'datatable'])->name('role.get');

        // User Status'
        Route::get('role/deactivated', [RoleStatusController::class, 'getDeactivated'])->name('role.deactivated');
        Route::get('role/deleted', [RoleStatusController::class, 'getDeleted'])->name('role.deleted');

        // Role CRUD
        Route::get('role', [RoleController::class, 'index'])->name('role.index');
        Route::get('role/create', [RoleController::class, 'create'])->name('role.create');
        Route::post('role', [RoleController::class, 'store'])->name('role.store');

        Route::group(['prefix' => 'role/{role}'], function () {
            Route::get('/', [RoleController::class, 'show'])->name('role.show');
            Route::get('edit', [RoleController::class, 'edit'])->name('role.edit');
            Route::patch('/', [RoleController::class, 'update'])->name('role.update');
            Route::delete('/', [RoleController::class, 'destroy'])->name('role.destroy');

            // Status
            Route::get('mark/{status}',
                [RoleStatusController::class, 'mark'])->name('role.mark')->where(['status' => '[0,1]']);

//            // Deleted
//            Route::get('delete', [RoleStatusController::class, 'delete'])->name('role.delete-permanently');
//            Route::get('restore', [RoleStatusController::class, 'restore'])->name('role.restore');
        });

    });

    // Permission Management
    Route::group(['namespace' => 'Permission'], function () {
        // For DataTables
        Route::post('permission/get', [PermissionTableController::class, 'datatable'])->name('permission.get');

        // Permission CRUD
        Route::get('permission', [PermissionController::class, 'index'])->name('permission.index');
        Route::get('permission/create', [PermissionController::class, 'create'])->name('permission.create');
        Route::post('permission', [PermissionController::class, 'store'])->name('permission.store');

        Route::group(['prefix' => 'permission/{permission}'], function () {
            Route::get('/', [PermissionController::class, 'show'])->name('permission.show');
            Route::get('edit', [PermissionController::class, 'edit'])->name('permission.edit');
            Route::patch('/', [PermissionController::class, 'update'])->name('permission.update');
            Route::delete('/', [PermissionController::class, 'destroy'])->name('permission.destroy');
        });

    });
});

//for Passport
Route::group([
    'namespace' => 'Auth',
    'middleware' => 'role:'.config('access.users.superadmin_role')
], function () {
    //for passport clients and tokens
    Route::get('passport/clients', [PassportAccessController::class, 'clients'])->name('passport.clients');
    Route::get('passport/tokens', [PassportAccessController::class, 'tokens'])->name('passport.tokens');
});

