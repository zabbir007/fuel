<?php

use App\Http\Controllers\FuelRate\FuelRateController;
use App\Http\Controllers\LogbookEntry\LogbookEntryAjaxController;
use App\Http\Controllers\LogbookEntry\LogbookEntryController;
use App\Http\Controllers\LogbookEntry\LogbookEntryTableController;
use App\Http\Controllers\LogbookEntry\LogbookStatusController;
use Illuminate\Support\Facades\Route;

// All route names are prefixed with 'transaction.'.

Route::group(['prefix' => 'transactions', 'as' => 'transactions.'], function () {

    // FuelRate Management
    Route::group(['namespace' => 'FuelRate', 'prefix' => 'fuel_rate'], function () {

        // FuelRate CRUD
        Route::get('/', [FuelRateController::class, 'index'])->name('fuel_rate.index');
        Route::get('create', [FuelRateController::class, 'create'])->name('fuel_rate.create');
        Route::post('/', [FuelRateController::class, 'store'])->name('fuel_rate.store');

        Route::get('edit', [FuelRateController::class, 'edit'])->name('fuel_rate.edit');
        Route::patch('/', [FuelRateController::class, 'update'])->name('fuel_rate.update');
    });

    // LogbookEntry Management
    Route::group(['namespace' => 'LogbookEntry', 'prefix' => 'logbook_entry'], function () {
        // For DataTables
        Route::post('get', [LogbookEntryTableController::class, 'datatable'])->name('logbook_entry.get');

        // LogbookEntry Status'
        Route::get('pending', [LogbookStatusController::class, 'getPending'])->name('logbook_entry.pending');

        // LogbookEntry CRUD
        Route::get('/', [LogbookEntryController::class, 'index'])->name('logbook_entry.index');
        Route::get('create', [LogbookEntryController::class, 'create'])->name('logbook_entry.create');
        Route::get('generate', [LogbookEntryController::class, 'generate'])->name('logbook_entry.generate');
        Route::post('/', [LogbookEntryController::class, 'store'])->name('logbook_entry.store');

        // Specific LogbookEntry
        Route::group(['prefix' => '/{id}'], function () {
            Route::get('edit', [LogbookEntryController::class, 'edit'])->name('logbook_entry.edit');
            Route::patch('/', [LogbookEntryController::class, 'update'])->name('logbook_entry.update');

            // Status
            Route::get('approve', [LogbookStatusController::class, 'approve'])->name('logbook_entry.approve');
        });

        //Ajax
        Route::group(['as' => 'logbook_entry.ajax.', 'prefix' => 'ajax'], function () {
            Route::get('vehicles', [LogbookEntryAjaxController::class, 'getVehicles'])->name('vehicles');
            Route::get('drivers', [LogbookEntryAjaxController::class, 'getDrivers'])->name('drivers');
            Route::get('fuels', [LogbookEntryAjaxController::class, 'getFuels'])->name('fuels');

            //transactions
            Route::post('post', [LogbookEntryAjaxController::class, 'postEntries'])->name('post');
            Route::post('approve', [LogbookEntryAjaxController::class, 'approveEntries'])->name('approve');
        });

    });
});
