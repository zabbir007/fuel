<?php

use App\Http\Controllers\Branch\BranchController;
use App\Http\Controllers\Branch\BranchStatusController;
use App\Http\Controllers\Branch\BranchTableController;
use App\Http\Controllers\Company\CompanyController;
use App\Http\Controllers\Company\CompanyStatusController;
use App\Http\Controllers\Company\CompanyTableController;
use App\Http\Controllers\Driver\DriverController;
use App\Http\Controllers\Driver\DriverStatusController;
use App\Http\Controllers\Driver\DriverTableController;
use App\Http\Controllers\Employee\EmployeeController;
use App\Http\Controllers\Employee\EmployeeStatusController;
use App\Http\Controllers\Employee\EmployeeTableController;
use App\Http\Controllers\FuelRate\FuelRateController;
use App\Http\Controllers\FuelRate\FuelRateStatusController;
use App\Http\Controllers\FuelRate\FuelRateTableController;
use App\Http\Controllers\UserBranch\UserBranchController;
use App\Http\Controllers\UserBranch\UserBranchStatusController;
use App\Http\Controllers\UserBranch\UserBranchTableController;
use App\Http\Controllers\Vehicle\VehicleController;
use App\Http\Controllers\Vehicle\VehicleStatusController;
use App\Http\Controllers\Vehicle\VehicleTableController;
use App\Http\Controllers\VehicleDriver\VehicleDriverController;
use App\Http\Controllers\VehicleDriver\VehicleDriverStatusController;
use App\Http\Controllers\VehicleDriver\VehicleDriverTableController;
use Illuminate\Support\Facades\Route;

// All route names are prefixed with 'master.'.

Route::group(['prefix' => 'master', 'as' => 'master.'], function () {

    // Company Management
    Route::group(['namespace' => 'Company'], function () {
        // For DataTables
        Route::post('company/get', [CompanyTableController::class, 'datatable'])->name('company.get');

        // Company Status'
        Route::get('company/deactivated',
            [CompanyStatusController::class, 'getDeactivated'])->name('company.deactivated');
        Route::get('company/trashed', [CompanyStatusController::class, 'getDeleted'])->name('company.deleted');

        // Company CRUD
        Route::get('company', [CompanyController::class, 'index'])->name('company.index');
        Route::get('company/create', [CompanyController::class, 'create'])->name('company.create');
        Route::post('company', [CompanyController::class, 'store'])->name('company.store');

        // Specific Company
        Route::group(['prefix' => 'company/{company}'], function () {
            // Company
            Route::get('/', [CompanyController::class, 'show'])->name('company.show');
            Route::get('edit', [CompanyController::class, 'edit'])->name('company.edit');
            Route::patch('/', [CompanyController::class, 'update'])->name('company.update');
            Route::delete('/', [CompanyController::class, 'destroy'])->name('company.destroy');

            // Status
            Route::get('mark/{status}',
                [CompanyStatusController::class, 'mark'])->name('company.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [CompanyStatusController::class, 'delete'])->name('company.delete-permanently');
            Route::get('restore', [CompanyStatusController::class, 'restore'])->name('company.restore');
        });

    });

    // Branch Management
    Route::group(['namespace' => 'Branch'], function () {
        // For DataTables
        Route::post('branch/get', [BranchTableController::class, 'datatable'])->name('branch.get');

        // Branch Status'
        Route::get('branch/deactivated',
            [BranchStatusController::class, 'getDeactivated'])->name('branch.deactivated');
        Route::get('branch/trashed', [BranchStatusController::class, 'getDeleted'])->name('branch.deleted');

        // Branch CRUD
        Route::get('branch', [BranchController::class, 'index'])->name('branch.index');
        Route::get('branch/create', [BranchController::class, 'create'])->name('branch.create');
        Route::post('branch', [BranchController::class, 'store'])->name('branch.store');

        // Specific Branch
        Route::group(['prefix' => 'branch/{branch}'], function () {
            // Branch
            Route::get('/', [BranchController::class, 'show'])->name('branch.show');
            Route::get('edit', [BranchController::class, 'edit'])->name('branch.edit');
            Route::patch('/', [BranchController::class, 'update'])->name('branch.update');
            Route::delete('/', [BranchController::class, 'destroy'])->name('branch.destroy');

            // Status
            Route::get('mark/{status}',
                [BranchStatusController::class, 'mark'])->name('branch.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [BranchStatusController::class, 'delete'])->name('branch.delete-permanently');
            Route::get('restore', [BranchStatusController::class, 'restore'])->name('branch.restore');
        });

    });

    // UserUserBranch Management
    Route::group(['namespace' => 'UserBranch'], function () {
        // For DataTables
        Route::post('user_branch/get', [UserBranchTableController::class, 'datatable'])->name('user_branch.get');

        // UserBranch Status'
        Route::get('user_branch/deactivated',
            [UserBranchStatusController::class, 'getDeactivated'])->name('user_branch.deactivated');
        Route::get('user_branch/trashed', [UserBranchStatusController::class, 'getDeleted'])->name('user_branch.deleted');

        // UserBranch CRUD
        Route::get('user_branch', [UserBranchController::class, 'index'])->name('user_branch.index');
        Route::get('user_branch/create', [UserBranchController::class, 'create'])->name('user_branch.create');
        Route::post('user_branch', [UserBranchController::class, 'store'])->name('user_branch.store');

        // Specific UserBranch
        Route::group(['prefix' => 'user_branch/{user_branch}'], function () {
            // UserBranch
            Route::get('/', [UserBranchController::class, 'show'])->name('user_branch.show');
            Route::get('edit', [UserBranchController::class, 'edit'])->name('user_branch.edit');
            Route::patch('/', [UserBranchController::class, 'update'])->name('user_branch.update');
            Route::delete('/', [UserBranchController::class, 'destroy'])->name('user_branch.destroy');

            // Status
            Route::get('mark/{status}',
                [UserBranchStatusController::class, 'mark'])->name('user_branch.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [UserBranchStatusController::class, 'delete'])->name('user_branch.delete-permanently');
            Route::get('restore', [UserBranchStatusController::class, 'restore'])->name('user_branch.restore');
        });

    });

    // Employee Management
    Route::group(['namespace' => 'Employee'], function () {
        // For DataTables
        Route::post('employee/get', [EmployeeTableController::class, 'datatable'])->name('employee.get');

        // Employee Status'
        Route::get('employee/deactivated',
            [EmployeeStatusController::class, 'getDeactivated'])->name('employee.deactivated');
        Route::get('employee/trashed', [EmployeeStatusController::class, 'getDeleted'])->name('employee.deleted');

        // Employee CRUD
        Route::get('employee', [EmployeeController::class, 'index'])->name('employee.index');
        Route::get('employee/create', [EmployeeController::class, 'create'])->name('employee.create');
        Route::post('employee', [EmployeeController::class, 'store'])->name('employee.store');

        // Specific Employee
        Route::group(['prefix' => 'employee/{employee}'], function () {
            // Employee
            Route::get('/', [EmployeeController::class, 'show'])->name('employee.show');
            Route::get('edit', [EmployeeController::class, 'edit'])->name('employee.edit');
            Route::patch('/', [EmployeeController::class, 'update'])->name('employee.update');
            Route::delete('/', [EmployeeController::class, 'destroy'])->name('employee.destroy');

            // Status
            Route::get('mark/{status}',
                [EmployeeStatusController::class, 'mark'])->name('employee.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [EmployeeStatusController::class, 'delete'])->name('employee.delete-permanently');
            Route::get('restore', [EmployeeStatusController::class, 'restore'])->name('employee.restore');
        });

    });

    // Driver Management
    Route::group(['namespace' => 'Driver'], function () {
        // For DataTables
        Route::post('driver/get', [DriverTableController::class, 'datatable'])->name('driver.get');

        // Driver Status'
        Route::get('driver/deactivated',
            [DriverStatusController::class, 'getDeactivated'])->name('driver.deactivated');
        Route::get('driver/trashed', [DriverStatusController::class, 'getDeleted'])->name('driver.deleted');

        // Driver CRUD
        Route::get('driver', [DriverController::class, 'index'])->name('driver.index');
        Route::get('driver/create', [DriverController::class, 'create'])->name('driver.create');
        Route::post('driver', [DriverController::class, 'store'])->name('driver.store');

        // Specific Driver
        Route::group(['prefix' => 'driver/{driver}'], function () {
            // Driver
            Route::get('/', [DriverController::class, 'show'])->name('driver.show');
            Route::get('edit', [DriverController::class, 'edit'])->name('driver.edit');
            Route::patch('/', [DriverController::class, 'update'])->name('driver.update');
            Route::delete('/', [DriverController::class, 'destroy'])->name('driver.destroy');

            // Status
            Route::get('mark/{status}',
                [DriverStatusController::class, 'mark'])->name('driver.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [DriverStatusController::class, 'delete'])->name('driver.delete-permanently');
            Route::get('restore', [DriverStatusController::class, 'restore'])->name('driver.restore');
        });

    });

    // Vehicle Management
    Route::group(['namespace' => 'Vehicle'], function () {
        // For DataTables
        Route::post('vehicle/get', [VehicleTableController::class, 'datatable'])->name('vehicle.get');

        // Vehicle Status'
        Route::get('vehicle/deactivated',
            [VehicleStatusController::class, 'getDeactivated'])->name('vehicle.deactivated');
        Route::get('vehicle/trashed', [VehicleStatusController::class, 'getDeleted'])->name('vehicle.deleted');

        // Vehicle CRUD
        Route::get('vehicle', [VehicleController::class, 'index'])->name('vehicle.index');
        Route::get('vehicle/create', [VehicleController::class, 'create'])->name('vehicle.create');
        Route::post('vehicle', [VehicleController::class, 'store'])->name('vehicle.store');

        // Specific Vehicle
        Route::group(['prefix' => 'vehicle/{vehicle}'], function () {
            // Vehicle
            Route::get('/', [VehicleController::class, 'show'])->name('vehicle.show');
            Route::get('edit', [VehicleController::class, 'edit'])->name('vehicle.edit');
            Route::patch('/', [VehicleController::class, 'update'])->name('vehicle.update');
            Route::delete('/', [VehicleController::class, 'destroy'])->name('vehicle.destroy');

            // Status
            Route::get('mark/{status}',
                [VehicleStatusController::class, 'mark'])->name('vehicle.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [VehicleStatusController::class, 'delete'])->name('vehicle.delete-permanently');
            Route::get('restore', [VehicleStatusController::class, 'restore'])->name('vehicle.restore');
        });

    });

    // VehicleDriver Management
    Route::group(['namespace' => 'VehicleDriver'], function () {
        // For DataTables
        Route::post('vehicle_driver/get', [VehicleDriverTableController::class, 'datatable'])->name('vehicle_driver.get');

        // VehicleDriver Status'
        Route::get('vehicle_driver/deactivated',
            [VehicleDriverStatusController::class, 'getDeactivated'])->name('vehicle_driver.deactivated');
        Route::get('vehicle_driver/trashed', [VehicleDriverStatusController::class, 'getDeleted'])->name('vehicle_driver.deleted');

        // VehicleDriver CRUD
        Route::get('vehicle_driver', [VehicleDriverController::class, 'index'])->name('vehicle_driver.index');
        Route::get('vehicle_driver/create', [VehicleDriverController::class, 'create'])->name('vehicle_driver.create');
        Route::post('vehicle_driver', [VehicleDriverController::class, 'store'])->name('vehicle_driver.store');

        // Specific VehicleDriver
        Route::group(['prefix' => 'vehicle_driver/{vehicle_driver}'], function () {
            // VehicleDriver
            Route::get('/', [VehicleDriverController::class, 'show'])->name('vehicle_driver.show');
            Route::get('edit', [VehicleDriverController::class, 'edit'])->name('vehicle_driver.edit');
            Route::patch('/', [VehicleDriverController::class, 'update'])->name('vehicle_driver.update');
            Route::delete('/', [VehicleDriverController::class, 'destroy'])->name('vehicle_driver.destroy');

            // Status
            Route::get('mark/{status}',
                [VehicleDriverStatusController::class, 'mark'])->name('vehicle_driver.mark')->where(['status' => '[0,1]']);

            // Deleted
            Route::get('delete', [VehicleDriverStatusController::class, 'delete'])->name('vehicle_driver.delete-permanently');
            Route::get('restore', [VehicleDriverStatusController::class, 'restore'])->name('vehicle_driver.restore');
        });

    });
});
